<?php

Route::get('/', function () {
    return redirect('login');
});

Route::get('/template', function () {
    return view('welcome_lte');
});

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

Route::group([
	'prefix' => 'app',
	'middleware' => 'auth',
	'namespace' => 'Admin'
	], function () {
	Route::group(['middleware' => ['role']], function () {

		Route::get('/', 'DashboardController@index');
		Route::resource('user', 'UserController', ['except' => ['show']]);
		Route::get('user/export/{type}', 'UserController@export');
	    Route::resource('role', 'RoleController', ['except' => ['show', 'destroy', 'create']]);

		Route::resource('auditTrails', 'AuditTrailsController', ['except' => ['show', 'edit', 'update', 'create', 'store', 'destroy']]);
		Route::get('auditTrails/export/{mode}', 'AuditTrailsController@export')->where('mode', 'xls|pdf|html');

		Route::resource('companyProfiles', 'CompanyProfileController', ['except' => ['show']]);

	    Route::group(['prefix'=>'po'], function() {
		    Route::post('searchSupplier','PoController@searchSupplier');
		    Route::post('generatePrefix','PoController@generatePrefix');
		    Route::post('searchProduct','PoController@searchProduct');
		    Route::post('getProduct','PoController@getProduct');
  			Route::post('delItem', 'PoController@delItem');
		    Route::get('{prefix}/print','PoController@printPo');
		    Route::get('{prefix}/pdf','PoController@getPdf');
		    Route::put('close','PoController@close');
		    Route::post('searchPo','ProcessPoController@searchPo');
		    Route::post('loadPoData','ProcessPoController@loadPoData');
		    Route::get('process',['as'=>'processPo.index','uses'=>'ProcessPoController@index']);
		    Route::post('process',['as'=>'processPo.store','uses'=>'ProcessPoController@store']);
		    Route::delete('process/{invoice}',['as'=>'processPo.destroy','uses'=>'ProcessPoController@destroy']);
		    Route::put('process/{invoice}/update',['as'=>'processPo.update','uses'=>'ProcessPoController@update']);
		    Route::post('getInvoice','ProcessPoController@getInvoice');
		    Route::get('printInvoice','ProcessPoController@printInvoice');
		    Route::post('upload','ProcessPoController@upload');

            Route::get('received',['as'=>'productReceived.index','uses'=>'ProductReceived2Controller@index']);


            Route::get('receive',['as'=>'productReceived.index','uses'=>'ProductReceivedController@index']);
		    Route::get('receive/qrcode','ProductReceivedController@index');
		    Route::post('receive',['as'=>'productReceived.store','uses'=>'ProductReceivedController@store']);
		    Route::post('loadProductReceived','ProductReceivedController@loadProductReceived');
		    Route::get('receive/cells/search/{zone}/{warehouse}/{category}','ProductReceivedController@searchCells');
		    Route::get('receive/cells/{zone}/{warehouse}/{category}','ProductReceivedController@getCells');


        Route::get('readyToStockIn/qrcode','readyToStockInController@qrcode');
		    Route::resource('readyToStockIn','readyToStockInController');
	    });
	    Route::resource('po','PoController');

		Route::resource('makePayment','MakePaymentController', ['except' => ['create', 'edit', 'destroy']]);

		Route::resource('brands', 'BrandsController', ['except' => ['show', 'create', 'edit']]);
		Route::post('brands/storeLazadaBrands', 'BrandsController@storeLazadaBrands');

		Route::resource('products', 'ProductController', ['except' => ['show']]);
		Route::post('products/syncLazada', 'ProductController@syncLazada');
		Route::post('products/stock', 'ProductController@stroeStock');

		// Route::resource('cycleCount', 'CycleCountController');
		Route::get('cycleCount', ['as' => 'cycleCount.index', 'uses' => 'CycleCountController@index']);
		Route::post('cycleCount', 'CycleCountController@steps');
		Route::post('cycleCount/getStocks', 'CycleCountController@getStocks');
		Route::post('cycleCount/saveCycleCount', 'CycleCountController@saveCycleCount');
		Route::resource('warehouse', 'WarehouseController');
		Route::resource('warehouseMapping', 'WarehouseMappingController');
		Route::resource('warehouseMappingRacks', 'WarehouseRacksController');
		Route::resource('warehouseTrays', 'WarehouseTraysController');
		Route::resource('warehouseMappingRacksCells', 'WarehouseCellsController');
		Route::group(['prefix' => 'warehouse'], function() {
			Route::post('{wid}/mapping/genPrefix', ['as' => 'warehouse.mapping.genPrefix', 'uses' => 'WarehouseMappingController@genPrefix']);
			Route::post('{wid}/mapping/genPrefix2', ['as' => 'warehouse.mapping.genPrefix2', 'uses' => 'WarehouseMappingController@genPrefix2']);
			Route::post('{wid}/mapping/checkPrefix', ['as' => 'warehouse.mapping.checkPrefix', 'uses' => 'WarehouseMappingController@checkPrefix']);
			Route::post('{wid}/mapping/getRacks', ['as' => 'warehouse.mapping.racks.getRacks', 'uses' => 'WarehouseRacksController@getRacks']);
			Route::post('{wid}/trays/genPrefix', ['as' => 'warehouse.trays.genPrefix', 'uses' => 'WarehouseTraysController@genPrefix']);
			Route::post('{wid}/mapping/{zid}/racks/genLocations', ['as' => 'warehouse.mapping.racks.genLocations', 'uses' => 'WarehouseCellsController@genLocations']);
			Route::post('{wid}/mapping/{zid}/racks/getBarcodes', ['as' => 'warehouse.mapping.racks.getBarcodes', 'uses' => 'WarehouseCellsController@getBarcodes']);
			Route::post('{wid}/trays/getBarcodes', ['as' => 'warehouse.trays.getBarcodes', 'uses' => 'WarehouseTraysController@getBarcodes']);
			Route::resource('{wid}/trays', 'WarehouseTraysController', [
				'names' => [
					'index'		=> 'warehouse.trays.index',
					'show'		=> 'warehouse.trays.show',
					'create'	=> 'warehouse.trays.create',
					'edit'		=> 'warehouse.trays.edit',
					'store'		=> 'warehouse.trays.store',
					'update'	=> 'warehouse.trays.update',
					'destroy'	=> 'warehouse.trays.destroy'
				]
			]);
			Route::resource('{wid}/mapping', 'WarehouseMappingController', [
				'names' => [
					'index'		=> 'warehouse.mapping.index',
					'show'		=> 'warehouse.mapping.show',
					'create'	=> 'warehouse.mapping.create',
					'edit'		=> 'warehouse.mapping.edit',
					'store'		=> 'warehouse.mapping.store',
					'update'	=> 'warehouse.mapping.update',
					'destroy'	=> 'warehouse.mapping.destroy'
				]
			]);
			Route::get('zoneCategories/{wid}', 'WarehouseMappingController@getCategories');
			Route::get('rackCategories/{rid}', 'WarehouseRacksController@getCategories');
			Route::group(['prefix' => '{wid}/mapping'], function() {
				Route::resource('{zid}/racks', 'WarehouseRacksController', [
					'names' => [
						'index'		=> 'warehouse.mapping.racks.index',
						'show'		=> 'warehouse.mapping.racks.show',
						'create'	=> 'warehouse.mapping.racks.create',
						'edit'		=> 'warehouse.mapping.racks.edit',
						'store'		=> 'warehouse.mapping.racks.store',
						'update'	=> 'warehouse.mapping.racks.update',
						'destroy'	=> 'warehouse.mapping.racks.destroy'
					]
				]);
			});
		});

		Route::group(['prefix'=>'sku'],function() {
      Route::get('bundle/search/{q}', 'SkuBundleController@searchSku');
      Route::get('bundle/data', 'SkuBundleController@getSkuData');
      Route::resource('bundle', 'SkuBundleController', ['except' => ['index','show']]);
			Route::get('product/init','SkuController@getProducts');
			Route::get('product/{q}','SkuController@searchProduct');
			Route::get('get/{id}','SkuController@getSku');
			Route::get('getQty/{id}','SkuController@getQty');
      Route::post('qtySetting/set', 'SkuController@setQtySetting');
			Route::post('price/store', 'SkuController@storePrice');
			Route::get('price/{sku_id}/{shop_id}', 'SkuController@getSkuPrice');
			Route::get('image', ['as' => 'skuImage.index', 'uses' => 'SkuController@indexImage']);
			Route::post('image', 'SkuController@setImage');
			Route::delete('image/{id}', 'SkuController@removeImage');
			Route::post('status', 'SkuController@setStatus');
			Route::get('{id}/clone','SkuController@cloneSku');
		});
		Route::resource('sku','SkuController', ['except' => ['show']]);

		Route::resource('carrier', 'CarrierController', ['except' => ['show']]);
		Route::resource('order','OrderController');
		Route::post('order/getSON','OrderController@getSON');
		Route::post('order/getDetail','OrderController@getDetail');
		Route::post('order/syncOrders','OrderController@syncOrders');
		Route::get('order/{prefix}/printOrder','OrderController@printOrder');
		Route::get('order/{prefix}/printInvoice','OrderController@printInvoice');
		Route::get('order/{prefix}/printShippingLabel','OrderController@printShippingLabel');
		Route::get('order/{prefix}/printStockList','OrderController@printStockList');
		Route::get('order/{prefix}/printCarrierManifest','OrderController@printCarrierManifest');
		Route::post('order/multiPrintOrders','OrderController@multiPrintOrders');
		Route::post('order/multiPrintInvoices','OrderController@multiPrintInvoices');
		Route::post('order/multiPrintShippingLabels','OrderController@multiPrintShippingLabels');
		Route::post('order/multiPrintStockLists','OrderController@multiPrintStockLists');
		Route::post('order/multiPrintCarrierManifests','OrderController@multiPrintCarrierManifests');
		Route::resource('logisticInventory','LogisticInventoryController');
		Route::resource('stockInLogistic','StockInLogisticController');
        Route::get('shippingList/checkout','ShippingListController@checkout');
        Route::resource('shippingList','ShippingListController');
        Route::post('shippingList/saveCheckout','ShippingListController@saveCheckout');
        Route::post('shippingList/detail','ShippingListController@detailByBinId');
        Route::post('shippingList/checkBarcode','ShippingListController@checkBarcode');
        Route::get('shippingList/{ID}/getTrackerNumber','ShippingListController@getTrackerNumber');


        Route::get('shippingStatus/{id}/print','ShippingStatusController@printReceipt');
		Route::get('shippingStatus/{id}/pdf','ShippingStatusController@getPDF');
		Route::resource('shippingStatus','ShippingStatusController');
		Route::resource('stockIn','StockInController');
		Route::get('pickingList/assign_tray/{id}','PickingListController@assign_tray');
		Route::post('pickingList/setTray','PickingListController@setTray');
		Route::resource('pickingList','PickingListController');
		Route::post('pickingList/getDetail','PickingListController@getDetail');
		Route::post('pickingList/orderList','PickingListController@orderList');
		Route::post('pickingList/getDetailInStock','PickingListController@getDetailInStock');
		Route::resource('picking','PickingController');
		Route::resource('pickingTypes','PickingTypeController');
		Route::resource('shippingBin','ShippingBinController');
		Route::resource('paymentMethod', 'PaymentMethodController', ['except' => ['show']]);
		Route::resource('packingList','PackingListController');
        Route::post('packingList/orderList','PackingListController@orderList');
        Route::post('packingList/savePACK','PackingListController@savePACK');
        Route::post('packingList/orderListEdit','PackingListController@orderListEdit');
        Route::post('packingList/loadPacking','PackingListController@loadPacking');
        Route::get('packingList/{ID}/printInvoice','PackingListController@printInvoice');

//        Route::post('packingList/loadPacking',['as' => 'packing.loadPacking','uses'=> 'PackingListController@loadPackingList']);
		Route::resource('zones', 'ZonesController', ['except' => ['show']]);

		Route::resource('supplier', 'SupplierController', ['except' => ['show']]);
		Route::group(['prefix' => 'supplier'], function() {
			Route::post('getSuppliers', ['as' => 'supplier.getSuppliers', 'uses' => 'SupplierController@getSuppliers']);
			// Route::post('genPrefix', ['as' => 'supplier.genPrefix', 'uses' => 'SupplierController@genPrefix']);
		});

		Route::resource('productCategories', 'ProductCatController', ['except' => ['show']]);
		Route::get('productCategories/{category}','ProductController@getProductCategories');
		Route::group(['prefix' => 'productCategories'], function() {
			Route::get('export/{type}', 'ProductCatController@export')->where('type', 'xls|pdf|html');
			Route::post('storeCategory', ['as' => 'productCategories.storeCategory', 'uses' => 'ProductCatController@storeCategory']);
			Route::post('storeLazadaCategories', 'ProductCatController@storeLazadaCategories');
			Route::post('getCategories', ['as' => 'productCategories.getCategories', 'uses' => 'ProductCatController@getCategories']);
			Route::post('getCategory', ['as' => 'productCategories.getCategory', 'uses' => 'ProductCatController@getCategory']);
			Route::post('getChildren', ['as' => 'productCategories.getChildren', 'uses' => 'ProductCatController@getChildren']);
		});

		Route::group(['prefix' => 'products'], function() {
			Route::get('{id}/clone', 'ProductController@cloneProduct');
			Route::get('export/{type}', 'ProductController@export')->where('type', 'xls|pdf|html');
			Route::post('checkPrefix', ['as' => 'products.checkPrefix', 'uses' => 'ProductController@checkPrefix']);
			Route::post('genPrefix', ['as' => 'products.genPrefix', 'uses' => 'ProductController@genPrefix']);
			Route::post('getProductIds', ['as' => 'products.getProductIds', 'uses' => 'ProductController@getProductIdList']);
			Route::get('categories/{parent}','ProductController@getCategories');
			Route::get('codeList/{category}','ProductController@getCodeList');
			Route::get('attributes/{category}','ProductController@getAttributes');
			Route::get('attributes/lazada/{category}','ProductController@getLazadaAttributes');
			Route::get('brands','ProductController@getBrands');
			Route::get('brands/{id}','ProductController@getBrands');
			Route::get('prefix/{prefix}','ProductController@validatePrefix');
			Route::get('data/{product_id}','ProductController@getProductData');
			Route::get('dataCode/{code_id}','ProductController@getProductCodeData');
			Route::get('productAttributes/{product}','ProductController@getProductAttributes');
			Route::get('productAttributes/{product}/{attribute}','ProductController@getProductAttributes');
			Route::get('suppliers/{category}','ProductController@getSuppliers');
		});

		Route::group(['prefix' => 'skuProducts'], function() {
//			Route::get('products', 'SKUProductsController@getProducts');
//			Route::post('checkPrefix', ['as' => 'skuProducts.checkPrefix', 'uses' => 'SKUProductsController@checkPrefix']);
//			Route::post('getProductsBySku', ['as' => 'skuProducts.getProductsBySku', 'uses' => 'SKUProductsController@getProductsBySku']);
		});

	    Route::resource('shop', 'ShopController');
	    Route::post('shop/storeCategory', ['as'=>'shop.storeCategory','uses'=>'ShopController@storeCategory']);
	    Route::get('shop/export/{type}', 'ShopController@export');
		Route::group(['prefix' => 'shop'], function() {
			Route::post('getShopsByName', ['as' => 'shop.getShopsByName', 'uses' => 'ShopController@getShopsByName']);
//			Route::post('getProductsBySku', ['as' => 'skuProducts.getProductsBySku', 'uses' => 'SKUProductsController@getProductsBySku']);
			Route::get('testApi/{account}/{api_key}', 'ShopController@testApi');
		});

		Route::group(['prefix' => 'zones'], function() {
			Route::post('checkPrefix', ['as' => 'zones.checkPrefix', 'uses' => 'ZonesController@checkPrefix']);
			Route::post('genPrefix', ['as' => 'zones.genPrefix', 'uses' => 'ZonesController@genPrefix']);
			Route::post('getZones', ['as' => 'zones.getZones', 'uses' => 'ZonesController@getZones']);
			Route::post('storeZone', ['as' => 'zones.storeZone', 'uses' => 'ZonesController@storeZone']);
		});

		Route::resource('reports', 'ReportsController', ['except' => ['show', 'create', 'edit', 'store', 'update', 'destroy']]);
	});
	Route::group([], function () {

		Route::group(['prefix' => 'warehouseMapping'], function() {
			Route::post('genPrefix', ['as' => 'warehouseMapping.genPrefix', 'uses' => 'WarehouseMappingController@genPrefix']);
			Route::post('checkPrefix', ['as' => 'warehouseMapping.checkPrefix', 'uses' => 'WarehouseMappingController@checkPrefix']);
			Route::post('getRacks', ['as' => 'warehouseMapping.getRacks', 'uses' => 'WarehouseRacksController@getRacks']);
		});
		Route::post('warehouseMappingTrays/genPrefix', ['as' => 'warehouseMappingTrays.genPrefix', 'uses' => 'WarehouseTraysController@genPrefix']);
		Route::group(['prefix' => 'warehouseMappingRacksCells'], function() {
			Route::post('genLocations', ['as' => 'warehouse.mapping.racks.genLocations', 'uses' => 'WarehouseCellsController@genLocations']);
			Route::post('getBarcodes', ['as' => 'warehouse.mapping.racks.getBarcodes', 'uses' => 'WarehouseCellsController@getBarcodes']);
		});
		Route::post('supplier/genPrefix', ['as' => 'supplier.genPrefix', 'uses' => 'SupplierController@genPrefix']);
        Route::post('warehouse/genPrefix', ['as' => 'warehouse.genPrefix', 'uses' => 'WarehouseController@genPrefix']);
        Route::post('warehouse/getContact', ['as' => 'warehouse.getContact', 'uses' => 'WarehouseController@getContact']);
	});

	Route::resource('profiles','ProfileController', ['except' => ['index', 'create', 'store', 'destroy']]);
	Route::get('file/{path}/{file}', 'FileController@getFile');

	//Route::get('orderType', 'OrderTypeController');

	Route::resource('orderType','OrderTypeController');
	Route::post('orderType/add', 'OrderTypeController@add');
	Route::post('orderType/edit', 'OrderTypeController@edit');
	Route::delete('orderType/destroy', 'OrderTypeController@destroy');

	Route::group(['prefix' => 'dashboard'], function() {
		Route::get('product',['as' => 'dashboard.product', 'uses' => 'DashboardProduct@index']);
		Route::get('system',['as' => 'dashboard.system', 'uses' => 'DashboardSystem@index']);
		Route::get('warehouse',['as' => 'dashboard.warehouse', 'uses' => 'DashboardWarehouse@index']);
		Route::get('order',['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);
	});

});

Route::group([
	'prefix' => 'app',
	'middleware' => 'auth',
	'namespace' => 'Admin'
	], function() {
		Route::group(["prefix" => "reports"], function() {
			Route::get("bestSellingSKU", ["as" => "reports.bestSellingSKU", "uses" => "ReportsController@bestSellingSKU"]);
			Route::get("dailyAvgVsDailySales", ["as" => "reports.dailyAvgVsDailySales", "uses" => "ReportsController@dailyPriceVsDailySales"]);
			Route::get("emptyCell", ["as" => "reports.emptyCell", "uses" => "ReportsController@emptyCell"]);
			Route::get("fastMovingSKU", ["as" => "reports.fastMovingSKU", "uses" => "ReportsController@fastMovingSKU"]);
			Route::get("slowMovingSKU", ["as" => "reports.slowMovingSKU", "uses" => "ReportsController@slowMovingSKU"]);
			Route::get("newBatch_StockInReport", ["as" => "reports.newBatch_StockInReport", "uses" => "ReportsController@newBatch_StockInReport"]);
			Route::get("salesBySKU", ["as" => "reports.salesBySKU", "uses" => "ReportsController@salesBySKU"]);
			Route::get("stockLevel", ["as" => "reports.stockLevel", "uses" => "ReportsController@stockLevel"]);
			Route::get("warehouseOccupancy", ["as" => "reports.warehouseOccupancy", "uses" => "ReportsController@warehouseOccupancy"]);
			Route::get("staffPickingReport", ["as" => "reports.staffPickingReport", "uses" => "ReportsController@staffPickingReport"]);
			Route::get("staffPackingReport", ["as" => "reports.staffPackingReport", "uses" => "ReportsController@staffPackingReport"]);
			Route::get("staffShippingReport", ["as" => "reports.staffShippingReport", "uses" => "ReportsController@staffShippingReport"]);

			Route::get("bestSellingSKU/export/{mode}", ["as" => "reports.bestSellingSKUHTML", "uses" => "ReportsController@bestSellingSKUExport"])->where("mode", "html");
			Route::get("dailyAvgVsDailySales/export/{mode}", ["as" => "reports.dailyAvgVsDailySalesHTML", "uses" => "ReportsController@dailyPriceVsDailySalesExport"])->where("mode", "html");
			Route::get("emptyCell/export/{mode}", ["as" => "reports.emptyCellHTML", "uses" => "ReportsController@emptyCellExport"])->where("mode", "html|xls|pdf");
			Route::get("fastMovingSKU/export/{mode}", ["as" => "reports.fastMovingSKUHTML", "uses" => "ReportsController@fastMovingSKUExport"])->where("mode", "html");
			Route::get("slowMovingSKU/export/{mode}", ["as" => "reports.slowMovingSKUHTML", "uses" => "ReportsController@slowMovingSKUExport"])->where("mode", "html");
			Route::get("newBatch_StockInReport/export/{mode}", ["as" => "reports.newBatch_StockInReportHTML", "uses" => "ReportsController@newBatch_StockInReportExport"])->where("mode", "html");
			Route::get("salesBySKU/export/{mode}", ["as" => "reports.salesBySKUHTML", "uses" => "ReportsController@salesBySKUExport"])->where("mode", "html");
			Route::get("stockLevel/export/{mode}", ["as" => "reports.stockLevelHTML", "uses" => "ReportsController@stockLevelExport"])->where("mode", "html|xls|pdf");
			Route::get("warehouseOccupancy/export/{mode}", ["as" => "reports.warehouseOccupancyHTML", "uses" => "ReportsController@warehouseOccupancyExport"])->where("mode", "html");
			Route::get("staffPickingReport/export/{mode}", ["as" => "reports.staffPickingReportHTML", "uses" => "ReportsController@staffPickingReportExport"])->where("mode", "html");
			Route::get("staffPackingReport/export/{mode}", ["as" => "reports.staffPackingReportHTML", "uses" => "ReportsController@staffPackingReportExport"])->where("mode", "html");
			Route::get("staffShippingReport/export/{mode}", ["as" => "reports.staffShippingReportHTML", "uses" => "ReportsController@staffShippingReportExport"])->where("mode", "html");
			Route::post("bestSellingSKU/export/{mode}", ["as" => "reports.bestSellingSKUExport", "uses" => "ReportsController@bestSellingSKUExport"])->where("mode", "xls|pdf");
			Route::post("dailyAvgVsDailySales/export/{mode}", ["as" => "reports.dailyAvgVsDailySalesExport", "uses" => "ReportsController@dailyPriceVsDailySalesExport"])->where("mode", "xls|pdf");
			Route::post("fastMovingSKU/export/{mode}", ["as" => "reports.fastMovingSKUExport", "uses" => "ReportsController@fastMovingSKUExport"])->where("mode", "xls|pdf");
			Route::post("slowMovingSKU/export/{mode}", ["as" => "reports.slowMovingSKUExport", "uses" => "ReportsController@slowMovingSKUExport"])->where("mode", "xls|pdf");
			Route::post("newBatch_StockInReport/export/{mode}", ["as" => "reports.newBatch_StockInReportExport", "uses" => "ReportsController@newBatch_StockInReportExport"])->where("mode", "xls|pdf");
			Route::post("salesBySKU/export/{mode}", ["as" => "reports.salesBySKUExport", "uses" => "ReportsController@salesBySKUExport"])->where("mode", "xls|pdf");
			Route::post("warehouseOccupancy/export/{mode}", ["as" => "reports.warehouseOccupancyExport", "uses" => "ReportsController@warehouseOccupancyExport"])->where("mode", "xls|pdf");
			Route::post("staffPickingReport/export/{mode}", ["as" => "reports.staffPickingReportExport", "uses" => "ReportsController@staffPickingReportExport"])->where("mode", "xls|pdf");
			Route::post("staffPackingReport/export/{mode}", ["as" => "reports.staffPackingReportExport", "uses" => "ReportsController@staffPackingReportExport"])->where("mode", "xls|pdf");
			Route::post("staffShippingReport/export/{mode}", ["as" => "reports.staffShippingReportExport", "uses" => "ReportsController@staffShippingReportExport"])->where("mode", "xls|pdf");
		});
});
