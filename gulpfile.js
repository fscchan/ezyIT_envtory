const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('dashboard.scss');
});
elixir(function(mix) {
    mix.scripts([
        'dashboard.js',
    ], 'public/js/dashboard.js');
});
elixir(function(mix) {
    mix.scripts([
        'po.js',
    ], 'public/js/po.js');
});
elixir(function(mix) {
    mix.scripts([
        'poIndex.js',
    ], 'public/js/poIndex.js');
});
elixir(function(mix) {
    mix.scripts([
        'processPo.js',
    ], 'public/js/processPo.js');
});
elixir(function(mix) {
    mix.scripts([
        'productReceived.js',
    ], 'public/js/productReceived.js');
});
elixir(function(mix) {
    mix.scripts([
        'productReceived2.js',
    ], 'public/js/productReceived2.js');
});
elixir(mix => {
    mix.sass('resources/assets/sass/createProduct.scss', 'public/css');
});
elixir(mix => {
    mix.webpack('addProduct.js')
        .webpack('updateProduct.js')
        .webpack('cloneProduct.js')
        .webpack('mappingWirehouse.js')
        .webpack('warehouseRack.js')
        .webpack('registerSku.js')
        .webpack('bundleSku.js')
        .webpack('sku.js')
        .webpack('product.js');
});
