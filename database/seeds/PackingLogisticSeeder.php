<?php

use Illuminate\Database\Seeder;

use Webpatser\Uuid\Uuid;
use App\User;

class PackingLogisticSeeder extends Seeder {
    public function run() {
		$admin = User::first();

		DB::table("m_packing_logistics")->insert([
			["id" => Uuid::generate()->string, "packing_code" => "SHB001", "packing_name" => "Shipping box", "operator" => $admin->id, "qty" => 16, "insert_by" => $admin->id, "update_by" => $admin->id],
			["id" => Uuid::generate()->string, "packing_code" => "ENV001", "packing_name" => "Envelope", "operator" => $admin->id, "qty" => 32, "insert_by" => $admin->id, "update_by" => $admin->id],
			["id" => Uuid::generate()->string, "packing_code" => "BBW001", "packing_name" => "Bubble wrap", "operator" => $admin->id, "qty" => 64, "insert_by" => $admin->id, "update_by" => $admin->id]
		]);
    }
}
