<?php

use Illuminate\Database\Seeder;

class OrderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
        	[
        		'id' => Uuid::generate()->string,
		        'name' => 'single',
		        'min_qty' => 1,
		        'max_qty' => 1,
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now(),
	        ],
	        [
		        'id' => Uuid::generate()->string,
		        'name' => 'small',
		        'min_qty' => 2,
		        'max_qty' => 5,
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now(),
	        ],
	        [
		        'id' => Uuid::generate()->string,
		        'name' => 'medium',
		        'min_qty' => 6,
		        'max_qty' => 10,
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now(),
	        ],
	        [
		        'id' => Uuid::generate()->string,
		        'name' => 'large',
		        'min_qty' => 11,
		        'max_qty' => 9999,
		        'created_at' => \Carbon\Carbon::now(),
		        'updated_at' => \Carbon\Carbon::now(),
	        ]
        ];
	    DB::table('m_order_types')->insert($types);
    }
}
