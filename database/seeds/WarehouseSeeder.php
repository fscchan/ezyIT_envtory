<?php

use Illuminate\Database\Seeder;

class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $warehouse= [
                    [   'id'            => Uuid::generate()->string,
                        'name'          => 'Default Warehouse',
                        'prefix'        => 'DEF1',
                        'address'       => 'default address',
                        'created_at'    => \Carbon\Carbon::now(),
                        'updated_at'    => \Carbon\Carbon::now(),
                    ]
        ];
        DB::table('m_warehouses')->insert($warehouse);
    }
}
