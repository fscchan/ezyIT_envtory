<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseTraysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("m_warehouse_trays", function (Blueprint $table) {
            $table->uuid("id");
            $table->uuid("m_warehouse_id");
            $table->string("prefix",10)->unique();
            $table->tinyInteger("size")->default("0")->comment("0=small, 1=medium, 2=large");
            $table->boolean("status")->default(0)->comment("0=avaiable, 1=in_use");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_warehouse_id")->references("id")->on("m_warehouses")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_warehouse_trays');
    }
}
