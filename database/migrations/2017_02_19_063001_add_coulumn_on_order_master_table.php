<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoulumnOnOrderMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
            $table->string('billing_cust_lastname', 100)->nullable();
            $table->string('ship_first_name', 100)->nullable();
            $table->string('ship_last_name', 100)->nullable();
            $table->string('ship_company_name', 100)->nullable();
            $table->string('ship_address_1', 200)->nullable();
            $table->string('ship_address_2', 300)->nullable();
            $table->string('ship_country', 100)->nullable();
            $table->string('ship_city', 34)->nullable();
            $table->string('ship_state', 50)->nullable();
            $table->string('ship_postal_code', 23)->nullable();
            $table->string('ship_email', 50)->nullable();
            $table->string('ship_phone', 16)->nullable();

            $table->decimal('subtotal', 16, 2)->default(0);
            $table->decimal('shipping_cost', 16, 2)->default(0);
            $table->decimal('discount', 16, 2)->default(0);
            $table->decimal('tax', 16, 2)->default(0);
            $table->decimal('total', 16, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
            $table->dropColumn([
                'billing_cust_lastname',
                'ship_first_name',
                'ship_last_name',
                'ship_company_name',
                'ship_address_1',
                'ship_address_2',
                'ship_country',
                'ship_city',
                'ship_state',
                'ship_postal_code',
                'ship_email',
                'ship_phone',
                'subtotal',
                'shipping_cost',
                'discount',
                'tax',
                'total',
            ]);
        });
    }
}
