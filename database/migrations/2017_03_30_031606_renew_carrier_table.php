<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenewCarrierTable extends Migration {
    public function up() {
		Schema::dropIfExists("m_carriers");
		Schema::create("m_carriers", function(Blueprint $table){
			$table->uuid("id");
			$table->string("Name", 255);
			$table->tinyInteger("Default")->default(0);
			$table->tinyInteger("ApiIntegration")->default(0);
			$table->string("TrackingCodeValidationRegex", 511)->nullable();
			$table->string("TrackingCodeExample", 48)->nullable();
			$table->string("TrackingUrl", 255)->nullable();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->timestamps();
			$table->primary("id");
		});
		Schema::create("m_carrier_delivery_options", function(Blueprint $table){
			$table->increments("id");
			$table->uuid("carrier_id");
			$table->string("DeliveryOption", 16)->default("standard");
			$table->timestamps();
			$table->foreign("carrier_id")->references("id")->on("m_carriers")->onDelete("cascade");
		});
    }

    public function down() {
		Schema::dropIfExists("m_carriers");
		Schema::create("m_carriers", function(Blueprint $table){
			$table->uuid("id");
			$table->string("carrier_name", 255);
			$table->string("carrier_account", 255)->nullable();
			$table->string("carrier_api_address", 255);
			$table->string("carrier_api_username", 255);
			$table->string("carrier_api_password", 255);
			$table->string("carrier_pickup_time", 255)->default("[]")->comment("json format");
			$table->integer("carrier_warehouse")->nullable();
			$table->boolean("carrier_isdefault")->default("0");
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
		Schema::dropIfExists("m_carrier_delivery_options");
    }
}
