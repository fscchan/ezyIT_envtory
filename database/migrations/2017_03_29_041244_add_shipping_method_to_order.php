<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingMethodToOrder extends Migration {
    public function up() {
		Schema::table("t_order_master", function(Blueprint $table){
			$table->uuid("shipping_method")->nullable()->after("update_by");
            // $table->foreign("shipping_method")->references("id")->on("m_carriers");
		});
    }

    public function down() {
		Schema::table("t_order_master", function(Blueprint $table){
			// $table->dropForeign(["shipping_method"]);
			$table->dropColumn("shipping_method");
		});
    }
}
