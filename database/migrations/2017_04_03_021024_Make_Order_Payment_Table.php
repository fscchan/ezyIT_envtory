<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeOrderPaymentTable extends Migration {
    public function up() {
		Schema::create("m_order_payments", function(Blueprint $table){
			$table->uuid("id");
			$table->string("payment_id", 16)->unique();
			$table->uuid("order_id");
			$table->timestamp("payment_date");
			$table->uuid("payment_method");
			$table->uuid("update_by")->nullable();
			$table->uuid("insert_by")->nullable();
			$table->timestamps();
			$table->primary("id");
			$table->foreign("order_id")->references("id")->on("t_po_master")->onDelete("restrict");
			$table->foreign("payment_method")->references("id")->on("m_payment_method")->onDelete("restrict");
		});
		Schema::create("m_invoice_payments", function(Blueprint $table){
			$table->uuid("payment_id");
			$table->uuid("invoice_id");
			$table->double("amount");
			$table->uuid("update_by")->nullable();
			$table->uuid("insert_by")->nullable();
			$table->timestamps();
			$table->foreign("payment_id")->references("id")->on("m_order_payments")->onDelete("restrict");
			$table->foreign("invoice_id")->references("id")->on("t_invoice_master")->onDelete("restrict");
		});
    }

    public function down() {
		Schema::disableForeignKeyConstraints();
        Schema::dropIfExists("m_order_payments");
        Schema::dropIfExists("m_invoice_payments");
    }
}
