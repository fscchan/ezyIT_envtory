<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->dropForeign('m_products_product_category_foreign');
            $table->dropForeign('m_products_product_subcategory_foreign');

            $table->dropColumn('product_category');
            $table->dropColumn('product_subcategory');
            $table->dropColumn('product_stock');
            $table->dropColumn('product_submodel');
            $table->dropColumn('product_size');
            $table->dropColumn('product_color');
            $table->dropColumn('product_suppliers');

            $table->unsignedInteger('m_product_categories_id')->after('id');
            $table->foreign('m_product_categories_id')->references('id')->on('m_product_categories')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->dropForeign('m_products_m_product_categories_id_foreign');
            $table->dropColumn('m_product_categories_id');
            
            $table->unsignedInteger('product_category');
            $table->unsignedInteger('product_subcategory');
            $table->unsignedInteger('product_stock');
            $table->string('product_submodel',255);
            $table->string('product_size',50);
            $table->string('product_color',50);
            $table->unsignedInteger('product_suppliers');
        });
    }
}
