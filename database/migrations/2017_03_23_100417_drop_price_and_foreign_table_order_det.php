<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPriceAndForeignTableOrderDet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_detail', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropForeign('t_order_detail_t_order_master_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_detail', function (Blueprint $table) {
	        $table->float("price");
	        $table->foreign("t_order_master_id")->references("id")->on("t_order_master");
        });
    }
}
