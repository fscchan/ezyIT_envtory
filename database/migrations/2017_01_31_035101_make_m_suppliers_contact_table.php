<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMSuppliersContactTable extends Migration {
    public function up() {
		Schema::create("m_suppliers_contact", function (Blueprint $table) {
			$table->uuid("id");
			$table->uuid("supplier_id");
			$table->string("contact_name", 250);
			$table->string("email", 250)->nullable();
			$table->string("phone", 16)->nullable();
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
			$table->foreign("supplier_id")->references("id")->on("m_suppliers")->onDelete("cascade");
		});
    }

    public function down() {
        Schema::drop('m_suppliers_contact');
    }
}
