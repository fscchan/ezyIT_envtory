<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQtyToSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_product_sku', function (Blueprint $table) {
            $table->unsignedInteger('save_qty')->nullable()->after('cost_price');
            $table->unsignedInteger('max_qty')->nullable()->after('save_qty');
            $table->unsignedInteger('min_qty')->nullable()->after('max_qty');
            $table->unsignedInteger('max_shelved_life')->nullable()->after('min_qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_product_sku', function (Blueprint $table) {
            $table->dropColumn('save_qty');
            $table->dropColumn('max_qty');
            $table->dropColumn('min_qty');
            $table->dropColumn('max_shelved_life');
        });
    }
}
