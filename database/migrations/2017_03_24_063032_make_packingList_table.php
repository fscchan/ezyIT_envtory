<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePackingListTable extends Migration {
    public function up() {
		Schema::create("m_packing_list", function(Blueprint $table){
			$table->uuid("id");
	        $table->string("packing_code", 50);
	        $table->uuid("staff_id")->nullable();
	        $table->timestamp("start_time")->nullable();
	        $table->timestamp("end_time")->nullable();
	        $table->tinyInteger("status")->default(0);
	        $table->uuid("insert_by")->nullable();
	        $table->uuid("update_by")->nullable();
            $table->timestamps();
	        $table->primary("id");
	        // $table->foreign("staff")->references("id")->on("users");
		});
		Schema::create("p_packinglist_stocks", function(Blueprint $table){
			$table->uuid("id");
	        $table->uuid("packing_id");
	        $table->uuid("product_id");
	        $table->Integer("quantity")->default(0);
	        $table->Integer("picked")->default(0);
            $table->timestamps();
	        $table->primary("id");
	        $table->foreign("packing_id")->references("id")->on("m_packing_list");
	        $table->foreign("product_id")->references("id")->on("t_stocks");
		});
    }

    public function down() {
		Schema::disableForeignKeyConstraints();
        Schema::dropIfExists("m_packing_list");
		Schema::dropIfExists("p_packinglist_stocks");
    }
}
