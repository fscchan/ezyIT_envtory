<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("t_invoice_detail", function (Blueprint $table) {
            $table->uuid("id");
            $table->uuid("t_invoice_master_id");
            $table->integer("m_products_id")->unsigned()->default(0);
            $table->float("qty");
            $table->float("price");
            $table->boolean("status")->default(1);
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("t_invoice_master_id")->references("id")->on("t_invoice_master")->onDelete("CASCADE")->onUpdate("CASCADE");
            $table->foreign("m_products_id")->references("id")->on("m_products")->onDelete("RESTRICT")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_invoice_detail');
    }
}
