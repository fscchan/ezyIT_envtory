<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStockHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Schema::table('t_stock_histories', function (Blueprint $table) {
            $table->string('note',25)->nullable()->after('is_in');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn("t_stock_histories", "note")) {
			Schema::table('t_stock_histories', function (Blueprint $table) {
				$table->dropColumn('note');
			});
		}
    }
}
