<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePWarehouseRacksCellsTable extends Migration {
    public function up() {
         Schema::create("p_warehouse_racks_cells", function (Blueprint $table) {
            $table->uuid("rack_id");
            $table->uuid("cell_id");
		 });
    }

	public function down() {
        Schema::dropIfExists('p_warehouse_racks_cells');
    }
}
