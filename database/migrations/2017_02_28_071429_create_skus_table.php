<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_product_sku', function (Blueprint $table) {
            $table->uuid('id');
            $table->unsignedInteger('m_products_id');
            $table->string('sku')->nullable();
            $table->uuid('bundle')->nullable();
            $table->uuid('insert_by');
            $table->uuid('update_by');
            $table->timestamps();
            $table->primary('id');
            $table->foreign('bundle')->references('id')->on('m_product_sku')->onDelete('restrict');
            $table->foreign('m_products_id')->references('id')->on('m_products')->onDelete('restrict');
            $table->foreign('insert_by')->references('id')->on('users');
            $table->foreign('update_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_product_sku');
    }
}
