<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateColumnsToCompanyProfilesTable extends Migration {
    public function up() {
		Schema::table("m_company_profiles", function(Blueprint $table) {
			$table->text("report_template_footer")->nullable()->after("profile_timezone");
			$table->text("report_template_header")->nullable()->after("profile_timezone");
			$table->string("report_template_title", 255)->nullable()->after("profile_timezone");
		});
    }

    public function down() {
		Schema::table("m_company_profiles", function(Blueprint $table) {
			$table->dropColumn("report_template_footer");
			$table->dropColumn("report_template_header");
			$table->dropColumn("report_template_title");
		});
    }
}
