<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTemplateTable extends Migration {
    public function up() {
		Schema::create("m_report_templates", function(Blueprint $table) {
			$table->increments("id");
			$table->string("name", 255)->nullable()->default("General template");
			$table->string("paper_size", 16)->default("A4");
			$table->text("header")->nullable();
			$table->text("footer")->nullable();
			$table->timestamps();
		});
		Schema::table("m_company_profiles", function(Blueprint $table) {
			$table->dropColumn("report_template_footer");
			$table->dropColumn("report_template_header");
			$table->dropColumn("report_template_title");
		});
    }

    public function down() {
		Schema::table("m_company_profiles", function(Blueprint $table) {
			$table->text("report_template_footer")->nullable()->after("profile_timezone");
			$table->text("report_template_header")->nullable()->after("profile_timezone");
			$table->string("report_template_title", 255)->nullable()->after("profile_timezone");
		});
		Schema::drop("m_report_templates");
    }
}
