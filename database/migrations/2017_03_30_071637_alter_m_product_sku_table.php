<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMProductSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_product_sku', function (Blueprint $table) {
            $table->decimal('retail_price', 16, 2)->nullable()->after('sku');
            $table->decimal('min_price', 16, 2)->nullable()->after('retail_price');
            $table->decimal('max_price', 16, 2)->nullable()->after('min_price');
            $table->decimal('cost_price', 16, 2)->nullable()->after('max_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_product_sku', function (Blueprint $table) {
            $table->dropColumn('retail_price');
            $table->dropColumn('min_price');
            $table->dropColumn('max_price');
            $table->dropColumn('cost_price');
        });
    }
}
