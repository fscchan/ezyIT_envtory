<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("files", function (Blueprint $table) {
            $table->uuid("id");
            $table->string("module",20);
            $table->uuid("module_id");
            $table->uuid("author_id");
            $table->string("storage",10);
            $table->string("title")->nullable();
            $table->string("path");
            $table->string("filename");
            $table->string("extension");
            $table->string("mime_type")->nullable();
            $table->string("size")->nullable();
            $table->softDeletes();
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
