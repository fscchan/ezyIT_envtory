<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePaymentMethodTable extends Migration {
    public function up() {
		Schema::create("m_payment_method", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("name", 48);
			$table->boolean("is_bank");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
    }

    public function down() {
        Schema::drop("m_payment_method");
    }
}
