<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_product_attributes', function (Blueprint $table) {
            $table->uuid('id');
            $table->unsignedInteger('m_products_id');
            $table->string('name',255);
            $table->string('value',255);
            $table->timestamps();
            $table->primary("id");
            $table->foreign('m_products_id')->references('id')->on('m_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_product_attributes');
    }
}
