<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_shop', function (Blueprint $table) {
            $table->dropForeign('m_shop_m_shop_categories_id_foreign');
            $table->dropColumn('m_shop_categories_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_shop', function (Blueprint $table) {
            $table->uuid("m_shop_categories_id")->nullable()->after('platform_id');
            $table->foreign("m_shop_categories_id")->references("id")->on("m_shop_categories");
        });
    }
}
