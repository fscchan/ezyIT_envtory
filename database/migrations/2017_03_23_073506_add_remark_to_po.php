<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarkToPo extends Migration {
    public function up() {
		Schema::table("t_po_master", function(Blueprint $table){
			$table->string("remarks", 2048)->nullable()->after("reason_closed");
		});
    }

	public function down() {
		Schema::table("t_po_master", function(Blueprint $table){
			$table->dropColumn("remarks");
		});
    }
}
