<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSKUTable extends Migration {
    public function up() {
		Schema::create("m_sku_products", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("sku_product_prefix", 255)->unique();
			$table->string("sku_product_dimension", 255)->nullable()->comment("json format; long, width, height, weight");
			$table->text("sku_product_misc_info")->nullable()->comment("json format");
			$table->text("sku_product_specifications")->nullable()->comment("json format");
			$table->string("sku_product_code", 255);
			$table->integer("sku_product_category");
			$table->integer("sku_product_subcategory");
			$table->string("sku_product_supplier", 512)->default("[]");
			$table->text("sku_product_description")->nullable();
			$table->text("sku_product_tnc")->nullable();
			$table->date("sku_product_active_date");
			$table->integer("sku_product_cyclecount")->default(0);
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
    }

    public function down() {
        Schema::dropIfExists('m_sku_products');
    }
}
