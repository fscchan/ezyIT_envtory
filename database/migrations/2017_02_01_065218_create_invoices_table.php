<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("t_invoice_master", function (Blueprint $table) {
            $table->uuid("id");
            $table->uuid("t_po_master_id");
            $table->string("prefix",45);
            $table->date("date");
            $table->date("due_date")->nullable();
            $table->double("ship_cost")->default(0);
            $table->double("misc_cost")->default(0);
            $table->double("discount")->nullable()->default(0);
            $table->double("total_amount");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("t_po_master_id")->references("id")->on("t_po_master")->onDelete("CASCADE")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_invoice_master');
    }
}
