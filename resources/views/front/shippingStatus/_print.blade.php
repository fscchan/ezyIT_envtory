	<div class="row">
		<div class="col-xs-3">
			<?php $barcode = Barcode::getBarcodePNG("SHIPMENT_NUMBER", "C128", 3, 100); ?>
			<div align="center">
			<img src="data:image/png;base64,{{ $barcode }}" alt="SHIPMENT_NUMBER" width="100%" />
			SHIPMENT_NUMBER
			</div>
		</div>
	    <div class="col-xs-7">
	        <div align="right">
	          <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
	          <p>{{ $company->profile_billing_address }}</p>
	        </div>
	    </div>
	    <div class="col-xs-2">
	    	<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
	    </div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<h2>SHIPMENT</h2>
		</div>
	</div>
	<div class="row invoice-info">
		<div class="col-xs-6 invoice-col">
		  <address>
		    <strong>COMPANY_NAME</strong><br>
		  </address>
		  <hr/>
		  <strong>Shipping To</strong>
		  <address>
		    <strong>FIRST_NAME LAST_NAME</strong><br>
	        ORDER_ADDRESS<br>
		    Email: ORDER_EMAIL
		  </address>
		</div>
		<div class="col-xs-6 invoice-col">
		  <address>
		    <strong>SHOP_NAME</strong><br>
		  </address>
		  <hr/>
		  <table style="width: 100%">
			  <tr>
				  <th style="width: 33%;">Origin</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Tran. date</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Delivery date</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Area dest.</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Tran. type</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Cut-off</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">No. of items</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Volume wt</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Actual wt</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Declared value</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
		  </table>
		  <hr/>
		  <table style="width: 100%">
			  <tr>
				  <th style="width: 33%;">VATable (Freight)</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">VATable (Others)</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">VATable (Valuation)</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">VATable (Box charge)</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">VAT-exempt</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">VAT Zero-rated</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Discount</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Total sales</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
		  </table>
		  <hr/>
		  <table style="width: 100%">
			  <tr>
				  <th style="width: 33%;">12% VAT</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Amount due</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
			  <tr>
				  <th style="width: 33%;">Mode</th>
				  <th style="width: 8px;">:</th>
				  <td style="text-align: right;"></td>
			  </tr>
		  </table>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 table-responsive">
		  <table class="table table-bordered">
		    <tbody>
		    </tbody>
		  </table>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6">
		  <p class="lead">Term and Condition</p>
		  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;"></p>
		</div>
	</div>
