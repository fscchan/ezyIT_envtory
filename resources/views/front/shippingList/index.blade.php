@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group hidden">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<div class="pull-right ">
					@can('create-user')
					<a id="btn-completeShipping" class="btn btn-info btn-flat" href="{{url('app/shippingList/checkout')}}"><i class="fa fa-plus"></i>Shipping Checkout</a>
					@endcan
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="shippingList-table" cellspacing>
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Order date</th>
							<th>Customer</th>
							<th>Platform</th>
							<th>Total items</th>
							<th style="width: 180px"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.structure.min.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		$('#shippingList-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('shippingList.index') !!}',
			columns: [
				{ data: 'order_no', name: 'order_no' },
				{ data: 'date_actual', name: 'date_actual' },
				{ data: 'customer_name_actual', name: 'customer_name_actual' },
				{ data: 'shop_platform', name: 'shop_platform' },
				{ data: 'items_count', name: 'items_count' },
				{ data: 'action', name: 'action', orderable: false, searchable: false }
			],
			createdRow: function(row, data, dataIndex) {
				$(row).attr("id", "row-" + data.id);
			}
		});

		var completeShipping, nz;

		completeShipping = $("#dialog-completeShipping").dialog({
			autoOpen: false,
			height: 400,
			width: 480,
			modal: true,
			show: { effect: "fade", duration: 333 },
			hide: { effect: "fade", duration: 333 },
			buttons: {
				Cancel: function() {
					completeShipping.dialog("close");
				}, 
				Submit: function() {
					/* if(completeShipping.find("input[name=_method]").val() == "POST"){
						var adr = "{!! route('shippingList.store') !!}";
						var dta = { "shippingList-name": completeShipping.find("input#shippingList-name").val(), "shippingList-min": completeShipping.find("input#shippingList-min").val(), "shippingList-max": completeShipping.find("input#shippingList-max").val() };
					} else if(completeShipping.find("input[name=_method]").val() == "PUT") {
						var adr = "{!! url('shippingList') !!}/" + completeShipping.find("input[name=id]").val();
						var dta =	{ "_method": "PUT", "id": completeShipping.find("input[name=id]").val(), "shippingList-name": completeShipping.find("input#shippingList-name").val(), "shippingList-min": completeShipping.find("input#shippingList-min").val(), "shippingList-max": completeShipping.find("input#shippingList-max").val() };
					}
					$.ajax({
						type: "post",
						url: adr,
						dataType: "json",
						data: dta,
						success: function(data) {
							if(data.st) {
								swal("Nice!", "Data saved.", "success");
								setTimeout(function() {
									location.reload();
								}, 1000);
							} else {
								swal("Oops...", "Failed to save data.", "error");
							}
						}
					}); */

					completeShipping.dialog("close")
					swal("Nice!", "Data saved.", "success");
				}
			},
			close: function() {
				nz[ 0 ].reset();
				//allFields.removeClass("ui-state-error");
			}
		});

		nz = completeShipping.find("form").on("submit", function(event) {
			event.preventDefault();
		});

		$("#shippingList-table").on("click", ".btn-completeShippingList", function(evt) {
			evt.preventDefault();
			completeShipping.find("input[name=_method]").val("POST");
			completeShipping.find("input[name=id]").val($(this).data("value"));
			completeShipping.dialog({title: "Update order type"}).dialog("open");
		});
	});
</script>

<div id="dialog-completeShipping">
	<form id="form-completeShipping">
		<input type="hidden" name="_method" value="">
		<input type="hidden" name="id" value="">
		<fieldset>
			<table style="border-collapse: separate; border-spacing: 8px; width: 100%;">
				<tr>
					<td style="width: 128px;"><label for="shippingList-provider">Shipping provider</label></td>
					<td colspan="2">
						<select id="shippingList-provider" class="ui-widget-content ui-corner-all form-control" name="shippingList-provider" style="margin-bottom: 8px;" required="required">
							<option value="Lazada express">Lazada express</option>
							<option value="DHL">DHL</option>
							<option value="FedEx">FedEx</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="shippingList-weight">Weight</label></td>
					<td style="width: 128px;"><input type="number" name="shippingList-weight" id="shippingList-weight" value="1" class="text ui-widget-content ui-corner-all form-control" style="margin-bottom: 8px;"></td>
					<td>kg</td>
				</tr>
				<tr>
					<td style="width: 128px;"><label for="shippingList-tracking">Tracking number</label></td>
					<td colspan="2"><input type="text" id="shippingList-tracking" class="text ui-widget-content ui-corner-all form-control" name="shippingList-tracking	" placeholder="Tracking number" value="" style="margin-bottom: 8px;" maxlength="64" required="required"></td>
				</tr>
			</table>
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>
@endpush