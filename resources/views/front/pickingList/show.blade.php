@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<form role="form" class="form-horizontal" method="POST" id="formPickingProcess" action="{{ url('/app/pickingList/'.$picking->id) }}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<input type="hidden" name="start" value="{{date('H:i:s')}}">
				<div class="box-header">
					<table style="width: 100%;" border="0">
						<tr>
							<th width="8%">Picking id</th>
							<th width="1%">:</th>
							<th width="15%">{{$picking->picking_code}} - {{date('d F Y',strtotime($picking->date))}}</th>
							<th colspan="4"></th>
							<th>Time Start : {{date('H.i')}}</th>
							<th>Time End : 00.00</th>
						</tr>
						<tr>
							<th>No of tray</th>
							<th>:</th>
							<th>{{$picking->qty_of_tray}}</th>
							<th width="8%">No of order</th>
							<th width="1%">:</th>
							<th width="45%">{{$picking->qty_of_order}}</th>
							<th colspan="3"></th>
						</tr>
						<tr>
							<th>Staff</th>
							<th>:</th>
							<th>{{$picking->staff->firstname}}</th>
							<th colspan="4"></th>
							<th style="text-align: right;"><button type="button" id="startScanItem" class="btn btn-warning" style="width: 100%">Start Scan</button></th>
							<th style="text-align: right;">
								<button type="submit" id="btn-finish" class="btn btn-primary" style="width: 100%" disabled="disabled">Finish</button>
							</th>
						</tr>
					</table>
					<div class="clear" style="clear: both;"></div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table dataTable dt-responsive nowrap" id="stockin-table" cellspacing>
						<thead style="border: 1px solid #f4f4f4;">
						<tr>
							<th colspan="3"><progress id="picking-progress" value="0" max="{{$count}}" style="display: block; width: 100%;"></progress></th>
							<th width="5"><span id="prog_count">0</span><span>/{{$count}}</span></th>
						</tr>
						</thead>
					</table>
					<div id="stockins">
						@foreach($items as $item)
						<table class="data-stockin table-hover dataTable"  style="width: 100%;" role="button">
							<tr onclick="startPickItem(this)">
								<td class="media-middle text-center" style="width: 12%; padding: 8px 0px;">
									<div class="media img-circle" style="display: inline-block; width: 96px; height: 96px; border: 1px solid #CCCCCC">
										<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAYAAACAvzbMAAAgAElEQVR4nO3dTXMbZ3ru8asT4JTBDEmR1ospHsEwpUiyLdmkXUc5Z1Q1phZccGV4mU0sfgFb+gJRyV9AclbZkc4mS8GbcIGcMjx1vIirMoTt8ZyZlG3BdCjalCxR4oyoCVjVWXRDgiCAeOvup7uf/6/KJRMAyXsGVl+4n7eWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQuWYLgCAOa7rFiQVJM1KmvAfvuk4zoqhkpAgBAhgCT8s5iW9JS805ju8tOI4zoVIikKiESBASrmue0BeSLzt/1no9Xsdx+HagK4ypgsAEBw/NIryQqNouBwAQNy5rjvvuu6y67r33ACY/t8DAAiZ67oXXdddCyI0CBD0iyEsIGFcb5jqsqT3JB1off7+zo4e7j7Sgwfenw93dyVJd+7e08HJCZ0/92a0BSO1CBAgIToFx5279/Tz3Xu64/+zn4e7j8ItElYhQIAEcF33kqQrkg7U9/a0+dNt/bi1pTt376le3+v55zS6ESAIBAgQY67rFiVdq+/tFRqhsfnTbdNlAZIIECCWXG/T3/L9nZ3572o/aHNrq69OA4gCAQLEjOu6V2/9+NN7N9f/80C3OY2Q1Ez8UiQPAQLEhOu6s9/c/H65/Olns4bnKmomfzmSgwABYqD86WeX/uX/fnqlXq8/sywXiCsCBDCotFqel3TtTw8fzpqupUnVdAFIBgIEMKC0Wi5IWlbnE3FNemC6ACTDX5guALBNabV8VdJNxTM8JOkT0wUgGehAgIj4w1XL6uNY9aCN5HK9vKwWchlICToQIAKl1fI1eZ/sCybrGMk91/U1juPUwq8EaUAHAoSotFqeldd1xGmSfD8V0wUgOehAgJCUVssX5XUdsQmPg5MT3V7y6yjqQDrQgQAhKK2WlyVdNF1Hq0y261/5tSjqQDoQIECASqvlA4pZ19FsfHS020sqEZSBlGAICwiIP99xUzEND6nrKqyq4zjbUdWC5CNAgAD4S3Q/UZs7BMZFNpvptgqrElEpSAkCBBhS02R5bMND6mn46qMo6kB6ECDAEPzwWDZdRy+6rMDadhyHM7DQFwIEGFCSwkOSnt8/QEpR1YH0IECAASQtPKSuHQjDV+gbAQL0qWl3eWJ0CY+a4ziViEpBihAgQB/88EjcabUvHDm039P/FFUdSBcCBOiRv0nwhmK+2qqdLh1IoropxAcBAvTuhgyfpjuIkVxuvyW8JU7fxaAIEKAH/nHs86brGMTU/sNXH0ZVB9KHAAG6KK2Wi5Iuma5jUMempzo9xeQ5hkKAAPvw5z0SO0cwPja63/DV1ShrQfoQIMD+Ejlp3tCl+1iJsBSkEAECdFBaLV9SQuc9GvLTRzs9RfeBoTmmCwDiqLRaLsi7uVJiu4/89FHNnX2l3VM1x3FeiroepA8dCNDeshIcHtK+w1d0HwgEAQK08FddzZuuYxgHJyc6bR6sMPeBoBAgQBN/1dU103UMa6aQ7/QU3QcCQ4AAT7usBO42bzaSy2nqcNvNgyvs+0CQCBDA50+cv2e6jmGdOjHT7uFteeEIBIYAAZ64ooRPnI+PjSrffvL8quM421HXg3RjGS+gx93HTdN1DOv8uTfbTZ5XHMe5YKIepBsdCOC5YrqAYXVYebUtaclAObAAAQLr+d3HRcNlDG3u7KvtHl7iuHaEhQABUtB9HC/kNZJ7rvXhFcdxSibqgR0IEFjN3/dRNF3HMEZyuXYrr6pi1RVCRoDAdpeV8JVXZ14+qWwm0/zQtryhK1ZdIVQECGz3d6YLGMbUkUPtNg0uOY5TNVEP7EKAwFr+mVcF03UMKpvNtJs4v8y8B6JCgMBmb5suYBjn5l5vHbpacRznuql6YB82EsJK/uT5PdN1DOp4Ia8zp082P7TiOA77PRApOhDYKrErr8bHRlvDo0R4wAQCBLZK5PBVNpvR+XNvNj9UETvNYQhDWLBOkoev5s//jcZHRxtfMmwFo+hAYKN50wUMYu7sq4QHYoUAgY3eMl1Av44X8s3HtF8nPBAHme4vAVInURPo+emjzZPmS9zTHHHBHAiskrT5j/z0Uc2dfUXyjie5wA5zxAlDWLDNvOkCetUUHhVJLxEeiBsCBLaZM11AL5rC4wPHcS5wMCLiiDkQ2OZXpgvoxg+PqjgUETFHgMA2BdMF7OfMyyd1/MX8B47jJP4mV0g/AgS2KZguoJ1sNqPjL+Yrx1/MX6brQFIQILBGabU8b7qGDmrTLxy5fPqvj3MMOxKFAAHMqkh6Z/bMK0ySI3EIENhk1nQBLT4oLi4w14HEIkBgkwnTBfiqkpaKiwvMdSDRCBAgWnQdSA0CBIhGRdJlug6kCQEChKsmLzhYYYXUIUCAcGxLulpcXLhuuhAgLAQIEKyapA8lrRQXF1iai1QjQIBgVCV9WFxcWDFcBxAZAgQY3LakFUkfJWFy3HXdA+pxL4zjOJVwq0EaECAYiOu68z2+dDtlZzvV5K2o+jiOE+Ou687KO+9rTtJrkhqhcaDPn9P412153dW2pC/l3YyrKqnKEfMgQNCW67oFeReiC5Ly/r83/un3Z0lPLkQ1SV/IuwBVhi60P2sDfE9NXt2fSqrEqdPwO4p5efd4n1U4N8tq/A6p5VbArus23tNfy/v/tuo4Ti2EGhBTBAgkPe4oLsi7X0bfn1h70HwhavxOybsAVeRfoEP+VFvzf9d+z6+r6VN23CbCXdctyguMeZk/mqXxns43HnBdt6an389a9GUhKtwT3VL+UMe8pLcVr9u8ViR9LKnExedxl1GU9z4Vu7w8jhofED5K2VAmRIBYxQ+Nd+VdiApmq+lJ4+LzoU1h4ofGRXnvlekuI0g1SSURJqlBgKScP5dRlPS+khEanVQlfaSUdiYp6DT61Xg/V5iMTy4CJKX8OY33lc6LUUnSx47jrJguZFj++9ToCrvOOz3cfaSHu7u6v7Ojvfqe7ty999TjvTg46R1KnM1mND46Kkl6fnLiqa8jtC3v/byaxg8GaUeApIzruhclXVGXbuPO3Xt6uPtIu7u7T118GhekbpovNgcnJ5TL5TSSe+7xxSkijYvPh0kaEvG7wiVJf6d93qeHu490f2dHDx7s6M7de7q/s6N6fS/0+kaa3suxsVGNj45qJPdc6L9XT97LShS/DMMjQFJiv+C4c/eefvYvQPcf/LHnT6qDGvcvOs9PTujg5ERUF5+qvCNESnEdEvFXUDW6jWfU9/a0+dNt/Xz3nh/w4b5P/RjJ5TQ+9ovH72nInUpFXkdSCfOXYHgESMK1C477Ozv68afbuuNfiEwbyeU0deSQnp+c0NThQ2H/ulh1Jd26jcZ7tbl1W/cf7ERc3eBGcjkdnJzQC0cOhfmeliRdZmgrvgiQhPLHzq/JX6WzuXXbvxBtRTLMMahsNqOpw4fDvvA0GJuo9YP9XbVZIn1/Z0c/bGxq86fbseoyhjF15JBeOHxY+empMH78B5KuxbWztBkBkjD+J9prkor3d3b0Xe2H2IdGJxGHSWP5aGjHj/jLpBsLF56aEH+4+0jffb+eqtBop/GezhSOBT3MVZPXjcTu+BibESAJ4rrupXq9fmVz686B775fT9SQRzfZbEb56aM6Nj0V9vh6Y4jr4yAuRn5ovKM2Q1T1vT2tb9zSDxubqXqvejU+NqqZF/OaOnJI2Uxgh16UJC3RjcQDAZIAruvO/vFPD5drP/zn7PrGrUR2G/0I6cLTzrae7Hzv6diNlvOn2m7I3Ny6rR82bmnzp9sBlppc2WxGx1/Ma6aQD+r93JYXInQjhhEgMffj1p1Lt3786dr6xi3TpRjR6EoiWh7cOBywKulBy3OvyQuLtjvDH+4+0g8bt7S+sZnqIaphhBAk1+Wt1qIbMYQAianSarkw+ou/Wt7545/mTdcSByO5nGYKx5SfPhp2V9KX9Y1N/bi1RbfRh4CDpCrpHVZqmUGAxExptXxA3rLcS6ZriauIu5JnNCbEbRhODFM2m9GZ06eCWLm1LS9EKsNXhX4QIDFSWi1fkhceQR+lnkojuZzy01M6Nn009M2KjU1+P2zcisXemjQ5ODmhMy+fDGLxxFIajrdJEgIkBkqr5Xk17elA/8bHRnVsekpThw8HGiZJ2V+TBscLeZ06MTPssNaK4zhLQdWE/REgBjFcFY5GmAxy5EZ9zzugkNAwYySX07k3Xhu2GyFEIkKAGOJ3HctK9hHrsZfNZh4HydjYqLKZjMb9P+t7e7r/YEcPdx/pwY5/YKGF+zXi6PSJGZ06MTPMjyBEIkCAGFBaLV+V9Pem6wDi7ODkhM698fowQ1qESMgIkAj5Q1Y3FK9byAKxlc1mdP7cm8MMaREiISJAIuIPWd0QK6yAvgSw3JcQCclfmC7ABv7y3E9EeAB9q9f3tPbV1/rDN98N+iMuuq57Ncia4KEDCVlptbws6aLpOoA0yE8f1dzZVwb9dvaJBIwACQnzHUA4hgyRuTjcaCwtCJAQ+OHxidgYCIRiiBDZlvQSBzAGgzmQgBEeQPjWN25p7avfDfKtjZEBBIAACRDhAURniBCZZ1I9GAxhBYTwAMw4XsjrzOmTg3zrBU7wHQ4dSAAID8Ccb2vrWt/YHORbl4OuxTYEyJAID8C8ta++7veY/ZokNhcO6S9NF5BkhAcQHz9u3db01AvKZruenXVd0t86jvP7CMpKNTqQ4XAPDyAm6vU9fb72hep7HY/g35Y373GZZbzBoAMZkH+i7vum6wDwxJ///F/685//S1NHDrU+VZH0f+g6gkUHMoDSavmiOI4diKX1jVva3Lrd/NAHjuNcoOsIHh1In0qr5VlJ/ywp3JtwAxjY1p2fdfSFI9v/I5v9W8dx/tF0PWlFB9IHf9J8WZyqC8Ravb63/a+//mzJcZyS6VrSjADpD5PmQMz94q9GqpJeKi4uEB4hG/hekbbx5z0uGi4DQAfZbEZnTp2s5v/nUeY7IsJRJj0orZYLktbE0BUQS+Njo5o98/LKgbExluhGiA6kN8x7ADHln4XFbWsNIEC68G9HO2+6DgBPa7pXOuFhCENY+2DoCoinbDaj8+fe1PjoaNVxnDnT9diKDmR/10R4ALEyPjaq8+feVDaTqUq6YLoem9GBdFBaLc/LOygRQEwcnJzQuTdefxweTJibRQfSGfcKAGKk6T7o25KWCA/z2EjYhj9xXjBdBwBPU3hI0juO41RN1gMPQ1gt/ONKboq5DyAWWsLjsuM4103WgyfoQJ51WYQHEAst4VEiPOKFDqQJ3QcQHy3hUZM0x7xHvNCBPI3uA4iB8bFRnXn5ZPND7xAe8UOA+Pzu4z3TdQC2a9rn0XjoAybN44kAeYLuAzAsm81o7uwrzeFRdRznisma0BkB8gTdB2DYubnXNT462vwQZ1zFGAGix/f6oPsADDrz8kkdnJxofoihq5gjQDzvmy4AsNnUkUM6/mK++aGavLPoEGPWB4h/5hW3qQUMGcnlNHf21daHuTFUAlgfIJLeNV0AYLOWSXNJqjiOw/3ME8DqjYT+0t17pusAbHX6xIxOnZhpffiC4zgVA+WgT7Z3IEXTBQC2Gh8bbRceFcIjOWwPEIavAEPOnD7Z7uGrUdeBwVkbIP7taucNlwFY6Xgh37pkV6L7SBxrA0QMXwFGZLOZdkNXEt1H4tgcIAxfAQacOjHTuupKkmp0H8ljZYD4w1fs/QAiNpLLtW4YbKD7SCArA0TMfQBGdBi62pbEvo8EsjVA3jZdAGCbg5MTyk9PtXuqxK7zZLI1QJhAByJ2bPpop6c+jrIOBMe6APHPvgIQoZFcrlP3sc2xJcllXYBIumC6AMA2HeY+JOY+Es3GAPmV6QIAm2SzmU7dh8TwVaLZGCDzpgsAbJLvPPchhq+SzaoAYf4DiN5M+30fEsNXiWdVgIj5DyBS42OjGsk91+npT6OsBcGzLUBeM10AYJN9ug9JqkRUBkJiW4BwfAkQoakjhzo+5zhONcJSEAJrAsS/+2DBdB2ALQ5OTrQ7NLGhEmEpCIk1ASK6DyBSL+zTfUii+0gBmwKECXQgQlOHD+/39BdR1YHw2BQg+87mAQjOSC633+orSapFVApCZFOAFEwXANiize1qWzGElQI2Bci86QIAWzzfJUA4vj0drAgQfwUWgIh06UAqEZWBkFkRIGIFFhCZbDbTbf4DKWFLgNCBABEZHx3t9hKGr1LClgCZM10AYIvxsa4B8mUUdSB8tgQIgIjkGL6yhi0Bwk2kgIj0MISFlLAlQABEZCSXM10CImJLgDCJDkSEFVj2sCVAWMYLAAGzJUAARKCHI0yQIgQIAGAgqQ8QjjEBgHCkPkDE/AcAhMKGAAEQEZbw2oUAARCYHpfwsrE3JQgQAMBACBAAganv7fXyMha2pAQBAiAw9x/s9PIyFrakBAECIHKu69KFpAABAsAEupAUIEAAmFAwXQCGR4AACMzD3Ue9vvT1MOtANAgQAIF5uLvb60sZwkoBAgSACQRIChAgAAJ1f6enpbwHXNclRBKOAAEQqHq9p82EEl1I4tkQINumCwBs8vPde72+9K0w60D4Uh8gxcWFqukaAJv0eJyJJM2HWAYikPoAARCtHo8zkaQC8yDJRoAACFSPk+gN8yGVgQjYEiDMgwARqdf3+tlQ+G6YtSBctgQI8yBAhO70PpE+67puIcRSECJbAgRAhB70N4xVDKsOhMuWAGEIC4hQHx2IxDBWYtkSIF+aLgCwyf0HO/0s551lNVYy2RIgACK2+dPtfl7+flh1IDy2BMia6QIA2/SxI12SitylMHlsCRDmQICIbW5t9fPyA2IyPXFsCZCa6QIA29Tre9rc6msY60pYtSAcVgRIcXGhZroGwEY/9jcPUnBd92JIpSAEVgSIr2a6AMA2fQ5jSXQhiUKAAAhNvb6n9Y3Nfr6FLiRBCBAAofph41a/30IXkhA2Bci66QIAG925e6+fwxUlrwu5GlY9CI5NAfKJ6QIAW/3hm+/6/Zb3OGQx/mwKEPaCAIZsbm31c7SJ5O0LuRZSOQiINQHCrW0Bc+r1PX1X63sUuei6LpsLY8yaAPERIoAh336/3m8XIknLHHESXwQIgEgM2IUckLQcQjkIgG0BwkoswKBvv1/vd0WW5A1lXQyhHAzJtgBhJRZgUL2+N8iKLEm6xj1D4se2AGEICzBsfeOW7vd3y1vJH8piPiRerAqQ4uLCttiRDhi39tXvBvm2WbG0N1asChAfXQhg2P0HO/r2+4GmJC+ySz0+bAwQ7o8OxMAfvvlukAl1Sfp7JtXjwcYAYSIdiIF6fU9rX3096Lcvu647H2A5GICNAcIQFhATd+7eG3QoS5JusDLLLOsCxJ9IJ0SAmPjt//+PQVZlSd7KrE8IEXOsCxAfAQLEyOe/+XKQY04kQsQoWwPkU9MFAHji4e6uPv/NF4N+OyFiiK0BUjFdAICn3bl7T7/9/X8M+u2NEOH03ghZGSDFxYWa2FAIxM63tfV+76He7IC8ifWLwVWE/VgZIL6K6QIAPGvtq6+HCRHJW+LLjvUI2BwgzIMAMfXb3/9h0JVZDZdc1/2Es7PCZXOAVEwXAKC9en1Pn33+78OGyLykNSbXw2NtgDAPAsRbQCFSkBcil4KpCs2sDRBfyXQBADoLKEQk734iNxjSCpbtAcI8CBBzjRDZ3Lo97I8qSrrJUt/g2B4gFdMFAOiuXt/T57/5YtjVWdKTpb43XNctDF+Z3awOEP9crIrpOgD0Zu2rrwe9JW6rory5Ee4tMgSrA8T3sekCAPTu9998p7Wvfjfo2VnNDsi7twjDWgMiQJhIBxJnfeNWUJPrkrdS64a/b2Q+iB9oC8d0AXFQWi3flPcfEYAEyWYzmjv7qqYOHwryx1YkXXUcpxLkD00jOhAPXQiQQI3J9YCGtBrm5R3MeJOOZH8EiOcj0wUAGNz6xi1VPvs33bl7L8gfW5C0HeQPTBuGsHwMYwHpcLyQ16kTM8pmMsP+qJrjOC8FUVNa0YE8wTAWkALf1tZV+ezfgth4yDWhCwLkCYaxgJRo3OHws8//XQ93Hw36Y7gmdMEQVhOGsYB0yk8f1akTMxrJPdfrtzB81QM6kKfRsgIptL5xS+VP/5/Wvvpdrx0J14Ie0IE0Ka2WZyWtma4DQLjy00c1Uzim8dHRTi95yXGcWoQlJRIB0oJhLMAOI7mcFt463+6pquM4c1HXk0QMYT3rn0wXACB8U0c67l7/MMo6kowAeday6QIAhO/Y9FSnp5j/6BEB0sK/1W3VdB0AwjOSy3Wa/1hxHIfd5z0iQNqjhQVSbJ/hK/Z+9IEAaa8kzsABUqvD8FWNE3j7Q4C04d+pkHFQIIX2Gb7i7oR9IkA6o5UFUmimcKzdw3xoHAAB0kFxcaEiqWa4DAABmzp8uN3DJSbP+0eA7I/JdCBFpo4c6nQeFsNXAyBA9rciJtOB1Hihc/dRi7iUVCBA9sFkOpAe2WxG+farrxhpGBAB0h3/cQEpkJ8+2u7hKkt3B0eAdFFcXKhKqpiuA8BwZl7Mt3uYD4hDIEB6w5JeIMEOTk60mzyvOY6zYqCc1CBAelBcXFgRS3qBxJoptO0+WHk1JAKkd7S6QAKN5HKaOvzM2Vd0HwEgQHq3Ipb0AonTYec53UcACJAe+Ut6/8F0HQB65y3dfWb1Fd1HQAiQ/lwzXQCA3h1/Ma9sJtP6MN1HQAiQPvhdyIrpOgD05hjdR6gIkP7x6QVIgPz00XZLd/n7GyACpE/+LW9XDJcBoItTJ2ZaH6L7CBgBMhg+xQAx1qH7uGyiljQjQAZAFwLEW5vuo+I4DgejBowAGRxdCBBDzH1EhwAZEF0IEE9tuo8VTtwNBwEyHD7VADHSpvvYFn9PQ0OADMHvQq6brgOAp0338Q/cbTA8BMjwroozsgDj2nQfNcdxrpiqxwYEyJA4IwuIhzbdB8t2Q0aABOOauF8IYMzpEzOt3UeJZbvhI0AC4HchTNQBBmSzmdYbRm2L7iMSBEhA/LsWVsxWAdjn1ImZ1hN3rzJxHg0CJFh0IUCERnI5HX/xqe6j6jgOKyMjQoAEqLi4UBGbC4HIzJ19pfWhJRN12IoACd5lsawXCN3ByQkdnJxofugDx3GqpuqxEQESMCbUgWjMnX21+csqez6iR4CEoLi4cF0Sn4SAkLRZtsvQlQEESHhYRgiEYCSXa122y9CVIQRISPwJdVaDAAGbO/tK87LdCkNX5hAg4boqdqgDgZk6cqh54nxbDF0ZRYCEyJ9Q5z9wIADZbKZ14pwNg4YRICFjKAsIxtzZV5uHrkpsGDSPAIkGQ1nAEKaOHNLU4UONL+nsY4IAiQBDWcDg2gxdLTmOw2bdGCBAIsJQFjCYlqGr6xzTHh8ESLSuig2GQM9ahq6q4pSHWCFAIsRQFtA7hq7ijwCJWHFxoSrpA9N1AHF3bu715qGry+w2jx/HdAG2Kq2WP5E0b7oOII6OF/I6c/pk48uS4zjvmKwH7dGBmLMkjn0HnjGSy+nUiZnGlzUx7BtbBIghxcWFmviLATzj3BuvNQ9dvcO8R3wRIAYVFxdK4g6GwGOnT8xofHS08eUS8x7xRoCYd1ks7QU0PjbaPHS14jjOisFy0AMm0WOgtFqelbRmug7AlGw2o/lf/u/GTaKqjuPMma4J3dGBxIC/tJcbUMFac2dfbYTHtqQLhstBjwiQmPBvg8sRDbBOfvpoY7f5tqQLTJonBwESL0vi1F5YZHxsVGdefrzfg82CCUOAxIh/1Mk7Yn8ILOAdVfL49rRLTJonDwESM8yHwBZnTp9qLNllxVVCESAxVFxcWBFHvyPF8tNHlZ+ekrzwYENtQrGMN8ZKq+U1SbOm6wCCND42qvlf/o1EeCQeHUi8XRCT6kiRbDaj8+felAiPVCBAYoxJdaTN+XNvKpvJEB4pQYDEnD+pzl82JN7c2Vc1PjpKeKQIAZIA/qGL/KVDYp0+MaP89NR1wiNdCJCEYGUWkio/fVSnTswsOY7D8vSUYRVWwpRWy8uSLpquA+jFoecnt3/5v964wA7zdKIDSZji4sKSuIcIkqF6++e7LxEe6UUHklCl1fINSUXTdQAdVCVd8FcSIqXoQJJrSdyICvFEeFiCAEko/y/nBXEEPOKF8LAIQ1gpwMT6vqryNmL+2v/6k6bnasXFhVrrN5RWywf09BEyjRscvSapII6X6YTwsAwBkhKEiCTv2JeKpC8kVYuLC5WwfpF/G+J5Sa/7fxbC+l0JseIv8IBFCJAUKa2WL0m6ZrqOiJUkfSqp1K6biKyIJ4HyruzrUK4XFxfY42EhAiRlSqvli/JC5IDhUsJUkvSxvNCI3XBJabVckLdCLu1hsi3psr/JFRYiQFLI/zR8Q+kaVqlK+kjeUEnsQqMTP0zelze8mKZQr0pa8s9qg6UIkJTyJ4KXlfy9IiuSPgpzPiMqfnf4vpLflVyXdDVJQY5wECApl9AhrZqkD5WwbqNXpdXyvLzhrYtmK+lbTd6QFUvHIYkAsYI/jHJN8e9GKpI+tOUC5b8vS5LeU/wD/gNJ19IY6BgcAWIR/5PvsuI1N7Itb1L8qslVVKb5neK78lZyxcmKLH9v0BkBYiH/YnVFZoOkIm9SPJYrqUxpmnQvyuz7syKCA10QIBYzMKnbWElldM9GUpRWy0VJbyu6jYqJXOkGcwgQNJb9vqvgP/Vuy+s0jG/0S7qmjYpv+X8GNWdS0ZM9NbWAfiYsQYDgKS0Xqln1Hig1/5+qnhwlwh6BkPhDXQU9OafrVy0vOeD/U2t6bFvSl5JuivcHASBA0FXTxaqdKsMdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABnX5IIAAAAaSURBVAAAAAAAAAAAAAAAAAAAAAAAAGCt/wZk81FEgJSwkwAAAABJRU5ErkJggg==" width="100%" height="100%">
									</div>
								</td>
								<td class="media-body text-left" style="width: 16%; padding: 16px 0px;">
									<div class="order_no" data-value="{{$item->stockInBatch->orderDetail->order->order_no}}">
										<b>{{$item->stockInBatch->orderDetail->order->order_no}}</b>
									</div>
									<div class="product_code" data-value="{{$item->stockInBatch->orderDetail->sku->product->product_code}}"
										 data-qty="{{$item->stockInBatch->qty}}" data-batch="{{$item->stockInBatch->batch_id}}" data-tray="{{ $item->tray->prefix }}">
										<b>{{$item->stockInBatch->orderDetail->sku->product->product_code}}</b>
									</div>
									<div class="product_name" data-value="{{$item->stockInBatch->orderDetail->sku->product->attribute('name')}}">
										<b>{{$item->stockInBatch->orderDetail->sku->product->attribute('name')}}</b>
									</div>
								</td>
								<td class="media-body text-left" style="padding: 16px 0px;">
									<div class="location"><b>{{@$item->stockInBatch->cell->prefix}}</b></div>
									<div class="batch" data-value="{{$item->stockInBatch->batch_id}}">
										<b>{{$item->stockInBatch->batch_id}}</b>
									</div>
								</td>
								<td class="media-body text-center" style="width: 16%; padding: 16px 0px;">
									<input type="hidden" value="0" class="hasPick">
									<div class="h4 media-heading"><b>Qty to pick</b></div>
									<div class="h2 media-heading product_qty" data-value="{{$item->stockInBatch->qty}}">
										<b class="pick-qty">0/{{$item->stockInBatch->qty}}</b>
									</div>
								</td>
							</tr>
						</table>
						@endforeach
					</div>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-body">
						<a href="{{route('pickingList.index')}}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('front.pickingList.scann')
<div id="modalPickProcess" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Picking Item</h4>
			</div>
			<div class="modal-body">
                <input type="hidden" id="target-row" value="">
                <div class="info-box">
                    <div class="media img-circle info-box-icon">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAYAAACAvzbMAAAgAElEQVR4nO3dTXMbZ3ru8asT4JTBDEmR1ospHsEwpUiyLdmkXUc5Z1Q1phZccGV4mU0sfgFb+gJRyV9AclbZkc4mS8GbcIGcMjx1vIirMoTt8ZyZlG3BdCjalCxR4oyoCVjVWXRDgiCAeOvup7uf/6/KJRMAyXsGVl+4n7eWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQuWYLgCAOa7rFiQVJM1KmvAfvuk4zoqhkpAgBAhgCT8s5iW9JS805ju8tOI4zoVIikKiESBASrmue0BeSLzt/1no9Xsdx+HagK4ypgsAEBw/NIryQqNouBwAQNy5rjvvuu6y67r33ACY/t8DAAiZ67oXXdddCyI0CBD0iyEsIGFcb5jqsqT3JB1off7+zo4e7j7Sgwfenw93dyVJd+7e08HJCZ0/92a0BSO1CBAgIToFx5279/Tz3Xu64/+zn4e7j8ItElYhQIAEcF33kqQrkg7U9/a0+dNt/bi1pTt376le3+v55zS6ESAIBAgQY67rFiVdq+/tFRqhsfnTbdNlAZIIECCWXG/T3/L9nZ3572o/aHNrq69OA4gCAQLEjOu6V2/9+NN7N9f/80C3OY2Q1Ez8UiQPAQLEhOu6s9/c/H65/Olns4bnKmomfzmSgwABYqD86WeX/uX/fnqlXq8/sywXiCsCBDCotFqel3TtTw8fzpqupUnVdAFIBgIEMKC0Wi5IWlbnE3FNemC6ACTDX5guALBNabV8VdJNxTM8JOkT0wUgGehAgIj4w1XL6uNY9aCN5HK9vKwWchlICToQIAKl1fI1eZ/sCybrGMk91/U1juPUwq8EaUAHAoSotFqeldd1xGmSfD8V0wUgOehAgJCUVssX5XUdsQmPg5MT3V7y6yjqQDrQgQAhKK2WlyVdNF1Hq0y261/5tSjqQDoQIECASqvlA4pZ19FsfHS020sqEZSBlGAICwiIP99xUzEND6nrKqyq4zjbUdWC5CNAgAD4S3Q/UZs7BMZFNpvptgqrElEpSAkCBBhS02R5bMND6mn46qMo6kB6ECDAEPzwWDZdRy+6rMDadhyHM7DQFwIEGFCSwkOSnt8/QEpR1YH0IECAASQtPKSuHQjDV+gbAQL0qWl3eWJ0CY+a4ziViEpBihAgQB/88EjcabUvHDm039P/FFUdSBcCBOiRv0nwhmK+2qqdLh1IoropxAcBAvTuhgyfpjuIkVxuvyW8JU7fxaAIEKAH/nHs86brGMTU/sNXH0ZVB9KHAAG6KK2Wi5Iuma5jUMempzo9xeQ5hkKAAPvw5z0SO0cwPja63/DV1ShrQfoQIMD+Ejlp3tCl+1iJsBSkEAECdFBaLV9SQuc9GvLTRzs9RfeBoTmmCwDiqLRaLsi7uVJiu4/89FHNnX2l3VM1x3FeiroepA8dCNDeshIcHtK+w1d0HwgEAQK08FddzZuuYxgHJyc6bR6sMPeBoBAgQBN/1dU103UMa6aQ7/QU3QcCQ4AAT7usBO42bzaSy2nqcNvNgyvs+0CQCBDA50+cv2e6jmGdOjHT7uFteeEIBIYAAZ64ooRPnI+PjSrffvL8quM421HXg3RjGS+gx93HTdN1DOv8uTfbTZ5XHMe5YKIepBsdCOC5YrqAYXVYebUtaclAObAAAQLr+d3HRcNlDG3u7KvtHl7iuHaEhQABUtB9HC/kNZJ7rvXhFcdxSibqgR0IEFjN3/dRNF3HMEZyuXYrr6pi1RVCRoDAdpeV8JVXZ14+qWwm0/zQtryhK1ZdIVQECGz3d6YLGMbUkUPtNg0uOY5TNVEP7EKAwFr+mVcF03UMKpvNtJs4v8y8B6JCgMBmb5suYBjn5l5vHbpacRznuql6YB82EsJK/uT5PdN1DOp4Ia8zp082P7TiOA77PRApOhDYKrErr8bHRlvDo0R4wAQCBLZK5PBVNpvR+XNvNj9UETvNYQhDWLBOkoev5s//jcZHRxtfMmwFo+hAYKN50wUMYu7sq4QHYoUAgY3eMl1Av44X8s3HtF8nPBAHme4vAVInURPo+emjzZPmS9zTHHHBHAiskrT5j/z0Uc2dfUXyjie5wA5zxAlDWLDNvOkCetUUHhVJLxEeiBsCBLaZM11AL5rC4wPHcS5wMCLiiDkQ2OZXpgvoxg+PqjgUETFHgMA2BdMF7OfMyyd1/MX8B47jJP4mV0g/AgS2KZguoJ1sNqPjL+Yrx1/MX6brQFIQILBGabU8b7qGDmrTLxy5fPqvj3MMOxKFAAHMqkh6Z/bMK0ySI3EIENhk1nQBLT4oLi4w14HEIkBgkwnTBfiqkpaKiwvMdSDRCBAgWnQdSA0CBIhGRdJlug6kCQEChKsmLzhYYYXUIUCAcGxLulpcXLhuuhAgLAQIEKyapA8lrRQXF1iai1QjQIBgVCV9WFxcWDFcBxAZAgQY3LakFUkfJWFy3HXdA+pxL4zjOJVwq0EaECAYiOu68z2+dDtlZzvV5K2o+jiOE+Ou687KO+9rTtJrkhqhcaDPn9P412153dW2pC/l3YyrKqnKEfMgQNCW67oFeReiC5Ly/r83/un3Z0lPLkQ1SV/IuwBVhi60P2sDfE9NXt2fSqrEqdPwO4p5efd4n1U4N8tq/A6p5VbArus23tNfy/v/tuo4Ti2EGhBTBAgkPe4oLsi7X0bfn1h70HwhavxOybsAVeRfoEP+VFvzf9d+z6+r6VN23CbCXdctyguMeZk/mqXxns43HnBdt6an389a9GUhKtwT3VL+UMe8pLcVr9u8ViR9LKnExedxl1GU9z4Vu7w8jhofED5K2VAmRIBYxQ+Nd+VdiApmq+lJ4+LzoU1h4ofGRXnvlekuI0g1SSURJqlBgKScP5dRlPS+khEanVQlfaSUdiYp6DT61Xg/V5iMTy4CJKX8OY33lc6LUUnSx47jrJguZFj++9ToCrvOOz3cfaSHu7u6v7Ojvfqe7ty999TjvTg46R1KnM1mND46Kkl6fnLiqa8jtC3v/byaxg8GaUeApIzruhclXVGXbuPO3Xt6uPtIu7u7T118GhekbpovNgcnJ5TL5TSSe+7xxSkijYvPh0kaEvG7wiVJf6d93qeHu490f2dHDx7s6M7de7q/s6N6fS/0+kaa3suxsVGNj45qJPdc6L9XT97LShS/DMMjQFJiv+C4c/eefvYvQPcf/LHnT6qDGvcvOs9PTujg5ERUF5+qvCNESnEdEvFXUDW6jWfU9/a0+dNt/Xz3nh/w4b5P/RjJ5TQ+9ovH72nInUpFXkdSCfOXYHgESMK1C477Ozv68afbuuNfiEwbyeU0deSQnp+c0NThQ2H/ulh1Jd26jcZ7tbl1W/cf7ERc3eBGcjkdnJzQC0cOhfmeliRdZmgrvgiQhPLHzq/JX6WzuXXbvxBtRTLMMahsNqOpw4fDvvA0GJuo9YP9XbVZIn1/Z0c/bGxq86fbseoyhjF15JBeOHxY+empMH78B5KuxbWztBkBkjD+J9prkor3d3b0Xe2H2IdGJxGHSWP5aGjHj/jLpBsLF56aEH+4+0jffb+eqtBop/GezhSOBT3MVZPXjcTu+BibESAJ4rrupXq9fmVz686B775fT9SQRzfZbEb56aM6Nj0V9vh6Y4jr4yAuRn5ovKM2Q1T1vT2tb9zSDxubqXqvejU+NqqZF/OaOnJI2Uxgh16UJC3RjcQDAZIAruvO/vFPD5drP/zn7PrGrUR2G/0I6cLTzrae7Hzv6diNlvOn2m7I3Ny6rR82bmnzp9sBlppc2WxGx1/Ma6aQD+r93JYXInQjhhEgMffj1p1Lt3786dr6xi3TpRjR6EoiWh7cOBywKulBy3OvyQuLtjvDH+4+0g8bt7S+sZnqIaphhBAk1+Wt1qIbMYQAianSarkw+ou/Wt7545/mTdcSByO5nGYKx5SfPhp2V9KX9Y1N/bi1RbfRh4CDpCrpHVZqmUGAxExptXxA3rLcS6ZriauIu5JnNCbEbRhODFM2m9GZ06eCWLm1LS9EKsNXhX4QIDFSWi1fkhceQR+lnkojuZzy01M6Nn009M2KjU1+P2zcisXemjQ5ODmhMy+fDGLxxFIajrdJEgIkBkqr5Xk17elA/8bHRnVsekpThw8HGiZJ2V+TBscLeZ06MTPssNaK4zhLQdWE/REgBjFcFY5GmAxy5EZ9zzugkNAwYySX07k3Xhu2GyFEIkKAGOJ3HctK9hHrsZfNZh4HydjYqLKZjMb9P+t7e7r/YEcPdx/pwY5/YKGF+zXi6PSJGZ06MTPMjyBEIkCAGFBaLV+V9Pem6wDi7ODkhM698fowQ1qESMgIkAj5Q1Y3FK9byAKxlc1mdP7cm8MMaREiISJAIuIPWd0QK6yAvgSw3JcQCclfmC7ABv7y3E9EeAB9q9f3tPbV1/rDN98N+iMuuq57Ncia4KEDCVlptbws6aLpOoA0yE8f1dzZVwb9dvaJBIwACQnzHUA4hgyRuTjcaCwtCJAQ+OHxidgYCIRiiBDZlvQSBzAGgzmQgBEeQPjWN25p7avfDfKtjZEBBIAACRDhAURniBCZZ1I9GAxhBYTwAMw4XsjrzOmTg3zrBU7wHQ4dSAAID8Ccb2vrWt/YHORbl4OuxTYEyJAID8C8ta++7veY/ZokNhcO6S9NF5BkhAcQHz9u3db01AvKZruenXVd0t86jvP7CMpKNTqQ4XAPDyAm6vU9fb72hep7HY/g35Y373GZZbzBoAMZkH+i7vum6wDwxJ///F/685//S1NHDrU+VZH0f+g6gkUHMoDSavmiOI4diKX1jVva3Lrd/NAHjuNcoOsIHh1In0qr5VlJ/ywp3JtwAxjY1p2fdfSFI9v/I5v9W8dx/tF0PWlFB9IHf9J8WZyqC8Ravb63/a+//mzJcZyS6VrSjADpD5PmQMz94q9GqpJeKi4uEB4hG/hekbbx5z0uGi4DQAfZbEZnTp2s5v/nUeY7IsJRJj0orZYLktbE0BUQS+Njo5o98/LKgbExluhGiA6kN8x7ADHln4XFbWsNIEC68G9HO2+6DgBPa7pXOuFhCENY+2DoCoinbDaj8+fe1PjoaNVxnDnT9diKDmR/10R4ALEyPjaq8+feVDaTqUq6YLoem9GBdFBaLc/LOygRQEwcnJzQuTdefxweTJibRQfSGfcKAGKk6T7o25KWCA/z2EjYhj9xXjBdBwBPU3hI0juO41RN1gMPQ1gt/ONKboq5DyAWWsLjsuM4103WgyfoQJ51WYQHEAst4VEiPOKFDqQJ3QcQHy3hUZM0x7xHvNCBPI3uA4iB8bFRnXn5ZPND7xAe8UOA+Pzu4z3TdQC2a9rn0XjoAybN44kAeYLuAzAsm81o7uwrzeFRdRznisma0BkB8gTdB2DYubnXNT462vwQZ1zFGAGix/f6oPsADDrz8kkdnJxofoihq5gjQDzvmy4AsNnUkUM6/mK++aGavLPoEGPWB4h/5hW3qQUMGcnlNHf21daHuTFUAlgfIJLeNV0AYLOWSXNJqjiOw/3ME8DqjYT+0t17pusAbHX6xIxOnZhpffiC4zgVA+WgT7Z3IEXTBQC2Gh8bbRceFcIjOWwPEIavAEPOnD7Z7uGrUdeBwVkbIP7taucNlwFY6Xgh37pkV6L7SBxrA0QMXwFGZLOZdkNXEt1H4tgcIAxfAQacOjHTuupKkmp0H8ljZYD4w1fs/QAiNpLLtW4YbKD7SCArA0TMfQBGdBi62pbEvo8EsjVA3jZdAGCbg5MTyk9PtXuqxK7zZLI1QJhAByJ2bPpop6c+jrIOBMe6APHPvgIQoZFcrlP3sc2xJcllXYBIumC6AMA2HeY+JOY+Es3GAPmV6QIAm2SzmU7dh8TwVaLZGCDzpgsAbJLvPPchhq+SzaoAYf4DiN5M+30fEsNXiWdVgIj5DyBS42OjGsk91+npT6OsBcGzLUBeM10AYJN9ug9JqkRUBkJiW4BwfAkQoakjhzo+5zhONcJSEAJrAsS/+2DBdB2ALQ5OTrQ7NLGhEmEpCIk1ASK6DyBSL+zTfUii+0gBmwKECXQgQlOHD+/39BdR1YHw2BQg+87mAQjOSC633+orSapFVApCZFOAFEwXANiize1qWzGElQI2Bci86QIAWzzfJUA4vj0drAgQfwUWgIh06UAqEZWBkFkRIGIFFhCZbDbTbf4DKWFLgNCBABEZHx3t9hKGr1LClgCZM10AYIvxsa4B8mUUdSB8tgQIgIjkGL6yhi0Bwk2kgIj0MISFlLAlQABEZCSXM10CImJLgDCJDkSEFVj2sCVAWMYLAAGzJUAARKCHI0yQIgQIAGAgqQ8QjjEBgHCkPkDE/AcAhMKGAAEQEZbw2oUAARCYHpfwsrE3JQgQAMBACBAAganv7fXyMha2pAQBAiAw9x/s9PIyFrakBAECIHKu69KFpAABAsAEupAUIEAAmFAwXQCGR4AACMzD3Ue9vvT1MOtANAgQAIF5uLvb60sZwkoBAgSACQRIChAgAAJ1f6enpbwHXNclRBKOAAEQqHq9p82EEl1I4tkQINumCwBs8vPde72+9K0w60D4Uh8gxcWFqukaAJv0eJyJJM2HWAYikPoAARCtHo8zkaQC8yDJRoAACFSPk+gN8yGVgQjYEiDMgwARqdf3+tlQ+G6YtSBctgQI8yBAhO70PpE+67puIcRSECJbAgRAhB70N4xVDKsOhMuWAGEIC4hQHx2IxDBWYtkSIF+aLgCwyf0HO/0s551lNVYy2RIgACK2+dPtfl7+flh1IDy2BMia6QIA2/SxI12SitylMHlsCRDmQICIbW5t9fPyA2IyPXFsCZCa6QIA29Tre9rc6msY60pYtSAcVgRIcXGhZroGwEY/9jcPUnBd92JIpSAEVgSIr2a6AMA2fQ5jSXQhiUKAAAhNvb6n9Y3Nfr6FLiRBCBAAofph41a/30IXkhA2Bci66QIAG925e6+fwxUlrwu5GlY9CI5NAfKJ6QIAW/3hm+/6/Zb3OGQx/mwKEPaCAIZsbm31c7SJ5O0LuRZSOQiINQHCrW0Bc+r1PX1X63sUuei6LpsLY8yaAPERIoAh336/3m8XIknLHHESXwQIgEgM2IUckLQcQjkIgG0BwkoswKBvv1/vd0WW5A1lXQyhHAzJtgBhJRZgUL2+N8iKLEm6xj1D4se2AGEICzBsfeOW7vd3y1vJH8piPiRerAqQ4uLCttiRDhi39tXvBvm2WbG0N1asChAfXQhg2P0HO/r2+4GmJC+ySz0+bAwQ7o8OxMAfvvlukAl1Sfp7JtXjwcYAYSIdiIF6fU9rX3096Lcvu647H2A5GICNAcIQFhATd+7eG3QoS5JusDLLLOsCxJ9IJ0SAmPjt//+PQVZlSd7KrE8IEXOsCxAfAQLEyOe/+XKQY04kQsQoWwPkU9MFAHji4e6uPv/NF4N+OyFiiK0BUjFdAICn3bl7T7/9/X8M+u2NEOH03ghZGSDFxYWa2FAIxM63tfV+76He7IC8ifWLwVWE/VgZIL6K6QIAPGvtq6+HCRHJW+LLjvUI2BwgzIMAMfXb3/9h0JVZDZdc1/2Es7PCZXOAVEwXAKC9en1Pn33+78OGyLykNSbXw2NtgDAPAsRbQCFSkBcil4KpCs2sDRBfyXQBADoLKEQk734iNxjSCpbtAcI8CBBzjRDZ3Lo97I8qSrrJUt/g2B4gFdMFAOiuXt/T57/5YtjVWdKTpb43XNctDF+Z3awOEP9crIrpOgD0Zu2rrwe9JW6rory5Ee4tMgSrA8T3sekCAPTu9998p7Wvfjfo2VnNDsi7twjDWgMiQJhIBxJnfeNWUJPrkrdS64a/b2Q+iB9oC8d0AXFQWi3flPcfEYAEyWYzmjv7qqYOHwryx1YkXXUcpxLkD00jOhAPXQiQQI3J9YCGtBrm5R3MeJOOZH8EiOcj0wUAGNz6xi1VPvs33bl7L8gfW5C0HeQPTBuGsHwMYwHpcLyQ16kTM8pmMsP+qJrjOC8FUVNa0YE8wTAWkALf1tZV+ezfgth4yDWhCwLkCYaxgJRo3OHws8//XQ93Hw36Y7gmdMEQVhOGsYB0yk8f1akTMxrJPdfrtzB81QM6kKfRsgIptL5xS+VP/5/Wvvpdrx0J14Ie0IE0Ka2WZyWtma4DQLjy00c1Uzim8dHRTi95yXGcWoQlJRIB0oJhLMAOI7mcFt463+6pquM4c1HXk0QMYT3rn0wXACB8U0c67l7/MMo6kowAeday6QIAhO/Y9FSnp5j/6BEB0sK/1W3VdB0AwjOSy3Wa/1hxHIfd5z0iQNqjhQVSbJ/hK/Z+9IEAaa8kzsABUqvD8FWNE3j7Q4C04d+pkHFQIIX2Gb7i7oR9IkA6o5UFUmimcKzdw3xoHAAB0kFxcaEiqWa4DAABmzp8uN3DJSbP+0eA7I/JdCBFpo4c6nQeFsNXAyBA9rciJtOB1Hihc/dRi7iUVCBA9sFkOpAe2WxG+farrxhpGBAB0h3/cQEpkJ8+2u7hKkt3B0eAdFFcXKhKqpiuA8BwZl7Mt3uYD4hDIEB6w5JeIMEOTk60mzyvOY6zYqCc1CBAelBcXFgRS3qBxJoptO0+WHk1JAKkd7S6QAKN5HKaOvzM2Vd0HwEgQHq3Ipb0AonTYec53UcACJAe+Ut6/8F0HQB65y3dfWb1Fd1HQAiQ/lwzXQCA3h1/Ma9sJtP6MN1HQAiQPvhdyIrpOgD05hjdR6gIkP7x6QVIgPz00XZLd/n7GyACpE/+LW9XDJcBoItTJ2ZaH6L7CBgBMhg+xQAx1qH7uGyiljQjQAZAFwLEW5vuo+I4DgejBowAGRxdCBBDzH1EhwAZEF0IEE9tuo8VTtwNBwEyHD7VADHSpvvYFn9PQ0OADMHvQq6brgOAp0338Q/cbTA8BMjwroozsgDj2nQfNcdxrpiqxwYEyJA4IwuIhzbdB8t2Q0aABOOauF8IYMzpEzOt3UeJZbvhI0AC4HchTNQBBmSzmdYbRm2L7iMSBEhA/LsWVsxWAdjn1ImZ1hN3rzJxHg0CJFh0IUCERnI5HX/xqe6j6jgOKyMjQoAEqLi4UBGbC4HIzJ19pfWhJRN12IoACd5lsawXCN3ByQkdnJxofugDx3GqpuqxEQESMCbUgWjMnX21+csqez6iR4CEoLi4cF0Sn4SAkLRZtsvQlQEESHhYRgiEYCSXa122y9CVIQRISPwJdVaDAAGbO/tK87LdCkNX5hAg4boqdqgDgZk6cqh54nxbDF0ZRYCEyJ9Q5z9wIADZbKZ14pwNg4YRICFjKAsIxtzZV5uHrkpsGDSPAIkGQ1nAEKaOHNLU4UONL+nsY4IAiQBDWcDg2gxdLTmOw2bdGCBAIsJQFjCYlqGr6xzTHh8ESLSuig2GQM9ahq6q4pSHWCFAIsRQFtA7hq7ijwCJWHFxoSrpA9N1AHF3bu715qGry+w2jx/HdAG2Kq2WP5E0b7oOII6OF/I6c/pk48uS4zjvmKwH7dGBmLMkjn0HnjGSy+nUiZnGlzUx7BtbBIghxcWFmviLATzj3BuvNQ9dvcO8R3wRIAYVFxdK4g6GwGOnT8xofHS08eUS8x7xRoCYd1ks7QU0PjbaPHS14jjOisFy0AMm0WOgtFqelbRmug7AlGw2o/lf/u/GTaKqjuPMma4J3dGBxIC/tJcbUMFac2dfbYTHtqQLhstBjwiQmPBvg8sRDbBOfvpoY7f5tqQLTJonBwESL0vi1F5YZHxsVGdefrzfg82CCUOAxIh/1Mk7Yn8ILOAdVfL49rRLTJonDwESM8yHwBZnTp9qLNllxVVCESAxVFxcWBFHvyPF8tNHlZ+ekrzwYENtQrGMN8ZKq+U1SbOm6wCCND42qvlf/o1EeCQeHUi8XRCT6kiRbDaj8+felAiPVCBAYoxJdaTN+XNvKpvJEB4pQYDEnD+pzl82JN7c2Vc1PjpKeKQIAZIA/qGL/KVDYp0+MaP89NR1wiNdCJCEYGUWkio/fVSnTswsOY7D8vSUYRVWwpRWy8uSLpquA+jFoecnt3/5v964wA7zdKIDSZji4sKSuIcIkqF6++e7LxEe6UUHklCl1fINSUXTdQAdVCVd8FcSIqXoQJJrSdyICvFEeFiCAEko/y/nBXEEPOKF8LAIQ1gpwMT6vqryNmL+2v/6k6bnasXFhVrrN5RWywf09BEyjRscvSapII6X6YTwsAwBkhKEiCTv2JeKpC8kVYuLC5WwfpF/G+J5Sa/7fxbC+l0JseIv8IBFCJAUKa2WL0m6ZrqOiJUkfSqp1K6biKyIJ4HyruzrUK4XFxfY42EhAiRlSqvli/JC5IDhUsJUkvSxvNCI3XBJabVckLdCLu1hsi3psr/JFRYiQFLI/zR8Q+kaVqlK+kjeUEnsQqMTP0zelze8mKZQr0pa8s9qg6UIkJTyJ4KXlfy9IiuSPgpzPiMqfnf4vpLflVyXdDVJQY5wECApl9AhrZqkD5WwbqNXpdXyvLzhrYtmK+lbTd6QFUvHIYkAsYI/jHJN8e9GKpI+tOUC5b8vS5LeU/wD/gNJ19IY6BgcAWIR/5PvsuI1N7Itb1L8qslVVKb5neK78lZyxcmKLH9v0BkBYiH/YnVFZoOkIm9SPJYrqUxpmnQvyuz7syKCA10QIBYzMKnbWElldM9GUpRWy0VJbyu6jYqJXOkGcwgQNJb9vqvgP/Vuy+s0jG/0S7qmjYpv+X8GNWdS0ZM9NbWAfiYsQYDgKS0Xqln1Hig1/5+qnhwlwh6BkPhDXQU9OafrVy0vOeD/U2t6bFvSl5JuivcHASBA0FXTxaqdKsMdAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABnX5IIAAAAaSURBVAAAAAAAAAAAAAAAAAAAAAAAAGCt/wZk81FEgJSwkwAAAABJRU5ErkJggg==" width="100%" height="100%">
                    </div>
                    <div class="info-box-content">
                        <span class="info-box-text"><b>Product code : <span id="p_code"></span></b></span>
                        <span class="info-box-text"><b>Product name : <span id="p_name"></span></b></span>
                        <span class="info-box-text"><b>Quantity to pick : <span id="p_qty"></span></b></span>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
                <button class="btn btn-primary" type="button" onclick="donePicking($('#target-row').val())">Done</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<style>
    .picking-done {
        background: #5cb85c;
    }
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
    });

    $('#startScanItem').on('click', function () {
        $('#modalScannTray span').text('Scan Batch')
        $('#modalScannTray').modal('show')
    })
    function startPickItem(e) {
//        if(!$(e).hasClass('picking-done')){
//            $('#target-row').val($(e).parents('table').index());
//            $('#p_code').text($(e).find('.product_code').data('value'));
//            $('#p_name').text($(e).find('.product_name').data('value'));
//            $('#p_qty').text($(e).find('.product_qty').data('value'));
//            $('#modalPickProcess').modal('show');
//        }
    }

    function donePicking(i) {
        $('.data-stockin:eq('+i+')').removeAttr('role')
        $('.data-stockin:eq('+i+')').removeClass('table-hover')
        $('.data-stockin tr:eq('+i+')').addClass('picking-done');
        $('#modalPickProcess').modal('hide');
        checkAll()
    }
    
    function checkAll() {
        var valid = true, count = parseInt($('#picking-progress').val());
        $('.data-stockin tr').each(function (i,e) {
            if(!$(e).hasClass('picking-done')){
                valid = false
//                setInterval(function(){
//                    $('#modalScannTray').modal('show')
//                }, 2000);
            }else{
                $('#picking-progress').val(count+1)
				$('#prog_count').text(count+1)
			}
        })
        if(valid){
            $('#btn-finish').removeAttr('disabled')
            $('#startScanItem').prop('disabled',true)
        }
    }

    var hasPick = 0, qty = 0;
    function checkScann(code) {
		if(code!=''){
            $('.product_code').each(function (i,e) {
                qty = $(e).data('qty');
				hasPick = parseInt($(e).parents('table').find('.hasPick').val());
				if(code.toUpperCase() == $(e).data('batch').toUpperCase() || code.toUpperCase() == $(e).data('value').toUpperCase()){
                    $('#modalScannTray').modal('hide')
                    if(hasPick==qty){
                        return false
                    }

                    var tray_of_item = $(e).data('tray');
                    swal({
                        title: "Match!",
                        text: "Preparing for scan tray...",
						closeOnConfirm: true
                    },
                    function (){
                        $('#modalScannTray span').text('Scan Tray');
                        $('#modalScannTray #target_tray').val(tray_of_item);
                        $('#modalScannTray #target_index').val(i);
                        $('#modalScannTray').modal('show')
                    });

                    return false;
				}
            })
		}
    }

    function confirmTray(i) {
        hasPick+=1;
        $('.data-stockin:eq('+i+')').find('.hasPick').val(hasPick);
        $('.data-stockin:eq('+i+')').find('.pick-qty').text(hasPick+'/'+qty);
        $('#modalScannTray').modal('hide');
        if(hasPick==qty){
            $('.data-stockin:eq('+i+')').removeAttr('role').removeClass('table-hover');
            $('.data-stockin:eq('+i+') tr').addClass('picking-done');
            checkAll()
        }
        $('#target_tray').val('')
		$('#target_index').val('')
    }

</script>
@endpush