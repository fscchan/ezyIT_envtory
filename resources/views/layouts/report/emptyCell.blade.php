<html>
	<head>
		<title>{{$title or ''}}</title>
		@include('layouts.report.style')
	</head>
	<body>
	{!! @$template->header !!}
	<div class="report">
		<h3>{{ $title }}</h3>
		<h2>Empty cell report</h2>
		<p><b>As for: </b>{{ date("d M Y", strtotime($filter->dateRange)) }}</p>
		<table>
			<thead><tr><th>No.</th><th>Zone category</th><th>Rack ID</th><th>Cell ID</th></tr></thead>
			<tbody>
				@for($i = 0; $i < count($emptyCells); $i++)
					<tr>
						<td>{{ $i + 1 }}</td>
						<td>{{ @$emptyCells[$i]->rack()->first()->zone()->first()->name }}</td>
						<td>{{ @$emptyCells[$i]->rack()->first()->prefix }}</td>
						<td>{{ @$emptyCells[$i]->prefix }}</td>
					</tr>
				@endfor
			</tbody>
		</table>
		<h2>Empty cell zone</h2>
		<p><b>As for: </b>{{ date("d M Y", strtotime($filter->dateRange)) }}</p>
		<table>
			<thead><tr><th>No.</th><th>Zone category</th></tr></thead>
			<tbody>
				@for($i = 0; $i < count($emptyCategories); $i++)
					<tr>
						<td>{{ $i + 1 }}</td>
						<td>{{ $emptyCategories[$i]->cat_name }}</td>
					</tr>
				@endfor
			</tbody>
		</table>
	</div>
	{!! @$template->footer !!}
	<script type="text/javascript">window.print();</script>
	</body>
</html>