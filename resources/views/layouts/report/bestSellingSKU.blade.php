<html>
	<head>
		<title>{{$title or ''}}</title>
		@include('layouts.report.style')
	</head>
	<body>
	{!! @$template->header !!}
	<div class="report">
		<h3>{{ $title }}</h3>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<h2 class="text-center">{{ $title }}</h2>
		<p class="text-center">
			<div id="skubestSellingSKUChart-chart">{!! !empty($chart) ? '<img src="' . $chart . '" width="100%">' : "" !!}</div>
			@barchart("bestSellingSKUChart", "skubestSellingSKUChart-chart")
		</p>
		<h2>By Category</h2>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<table>
			<thead><tr><th>No.</th><th>Category</th><th>Total amount</th></tr></thead>
			<tbody>
				@foreach($byCategory as $i => $o)
				<tr>
					<td>{{ $i + 1 }}</td>
					<td>{{ $o->cat_name }}</td>
					<td>{{ $o->sku_sold }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<h2>By SKU/Amount sold</h2>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<table>
			<thead><tr><th>No.</th><th>SKU</th><th>Description</th><th>Category</th><th>Total amount</th></tr></thead>
			<tbody>
				@foreach($bySKU as $i => $o)
				<tr>
					<td>{{ $i + 1 }}</td>
					<td>{{ $o->sku }}</td>
					<td>{!! $o->product_sku_description !!}</td>
					<td>{{ $o->cat_name }}</td>
					<td>{{ $o->sku_sold }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! @$template->footer !!}
	<script type="text/javascript">window.print();</script>
	</body>
</html>