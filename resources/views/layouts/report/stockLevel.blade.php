<html>
	<head>
		<title>{{$title or ''}}</title>
		@include('layouts.report.style')
	</head>
	<body>
	{!! @$template->header !!}
	<div class="report">
		<h3>{{ $title }}</h3>
		<h4>Total Active batch id Qty</h4>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<p><b>Active batch ID: </b></p>
		<table>
			<thead><tr><th>No.</th><th>SKU</th><th>Description</th><th>Category</th><th>Quantity</th><th>Batch ID</th></tr></thead>
			<tbody>
				@foreach($stock as $i => $o)
				<?php $product = $o->product()->first(); ?>
				<tr>
					<td>{{ $i + 1 }}</td>
					<td>{{ @$product->sku()->first()->sku }}</td>
					<td>{!! @$product->attribute("description") ? $product->attribute("description") : $product->attribute("short_description") !!}</td>
					<td>{{ @$product->category()->first()->cat_name }}</td>
					<td>{{ App\Stock::active($product) }}</td>
					<td>{{ $o->batch_id }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<h4>Total Cold Batch id Qty</h4>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<p><b>Cold batch ID: </b></p>
		<table>
			<thead><tr><th>No.</th><th>SKU</th><th>Description</th><th>Category</th><th>Quantity</th><th>Batch ID</th></tr></thead>
			<tbody>
				@foreach($stock as $i => $o)
				<?php $product = $o->product()->first(); ?>
				<tr>
					<td>{{ $i + 1 }}</td>
					<td>{{ $product->sku()->first()->sku }}</td>
					<td>{!! $product->attribute("description") ? $product->attribute("description") : $product->attribute("short_description") !!}</td>
					<td>{{ $product->category()->first()->cat_name }}</td>
					<td>{{ App\Stock::cold($product) }}</td>
					<td>{{ $o->batch_id }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! @$template->footer !!}
	<script type="text/javascript">window.print();</script>
	</body>
</html>