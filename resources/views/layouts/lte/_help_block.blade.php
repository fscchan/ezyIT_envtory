@if ($errors->has($helpParameter))
    <span class="help-block">
        <strong>{{ $errors->first($helpParameter) }}</strong>
    </span>
@endif