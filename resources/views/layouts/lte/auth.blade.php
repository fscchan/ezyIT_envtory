<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name') }} | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{!! asset('assets/lte/bootstrap/css/bootstrap.min.css') !!}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="icon" href="/img/favicon32.png" sizes="32x32">
  <link rel="icon" href="/img/favicon48.png" sizes="48x48">
  <link rel="stylesheet" href="{!! asset('assets/lte/dist/css/AdminLTE.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('assets/lte/dist/css/skins/_all-skins.min.css') !!}">
  <link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
  <style>
    .login {
      background: #303085
    }
  </style>
  @stack('styles')

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>\
  <![endif]-->
</head>
<body class="hold-transition login">
@yield('content')
<script src="{!! asset('assets/lte/plugins/jQuery/jquery-2.2.3.min.js') !!}"></script>
<script src="{!! asset('assets/lte/bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
@stack('scripts')
</body>
</html>
