<aside class="control-sidebar control-sidebar-light">
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-first-tab" data-toggle="tab"><i class="fa fa-bell"></i></a></li>
      <li><a href="#control-sidebar-second-tab" data-toggle="tab"><i class="fa fa-globe"></i></a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="control-sidebar-first-tab">
        <h3 class="control-sidebar-heading"><i class="fa fa-truck"></i> Next Pickup</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">JNE</h4>
                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">POS Laju</h4>
                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">POS Laju</h4>
                <p>nora@example.com</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">SKU below safe levels</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Acer 4321
                <span class="label label-danger pull-right">6</span>
              </h4>
              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 20%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Acer 4321
                <span class="label label-danger pull-right">6</span>
              </h4>
              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 20%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                HP Q13
                <span class="label label-danger pull-right">8</span>
              </h4>
              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 25%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Asus 52AL
                <span class="label label-danger pull-right">3</span>
              </h4>
              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 10%"></div>
              </div>
            </a>
          </li>
        </ul>
      </div>

      <div class="tab-pane" id="control-sidebar-second-tab">
          <h3 class="control-sidebar-heading">Best Selling SKU</h3>
          <ul class="control-sidebar-menu">
            <li>
            <a href="javascript:void(0)">
              <span>Samsung</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-green">35</small>
              </span>
            </a>
            </li>
            <li>
            <a href="javascript:void(0)">
              <span>LG</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-green">19</small>
              </span>
            </a>
            </li>
            <li>
            <a href="javascript:void(0)">
              <span>Apple</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-green">12</small>
              </span>
            </a>
            </li>
          </ul>
          <hr>
          <div class="progress-group">
            <span class="progress-text">Picking</span>
            <span class="progress-number"><b>160</b>/200</span>

            <div class="progress sm">
              <div class="progress-bar progress-bar-blue" style="width: 70%"></div>
            </div>
          </div>
          <div class="progress-group">
            <span class="progress-text">Packing</span>
            <span class="progress-number"><b>310</b>/400</span>

            <div class="progress sm">
              <div class="progress-bar progress-bar-yellow" style="width: 60%"></div>
            </div>
          </div>
          <div class="progress-group">
            <span class="progress-text">Shipped</span>
            <span class="progress-number"><b>480</b>/800</span>

            <div class="progress sm">
              <div class="progress-bar progress-bar-green" style="width: 55%"></div>
            </div>
          </div>
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
