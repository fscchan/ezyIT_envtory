<section class="sidebar">
	<div class="panel-group" id="rightbar-accordion" role="tablist" aria-multiselectable="true">
    <ul class="sidebar-menu">
        {{-- <li class="header">{{ strtoupper('Module Name') }}</li> --}}
        <li><a href="{{url('/app')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        @if (!empty(config('menu')))
        @foreach (config('menu') as $menu)
            <?php $hasAccess = false;  ?>
			@if(!empty($menu['access']))
				@foreach ($menu['access'] as $access)
					<?php $hasAccess = Gate::check($access);?>
				@endforeach
			@endif
            @if($hasAccess)
                <li class="{{ !empty($menu['child']) ? 'treeview' : ''}}">
                    <a href="{{ !empty($menu['route']) ? route($menu['route']) : '#' }}">
                        <i class="{{ $menu['icon'] }}"></i>
                        <span>{{ $menu['name'] }}</span>
                        @if (!empty($menu['child']))
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        @endif
                    </a>
                    @if (!empty($menu['child']))
                        <ul class="treeview-menu">
                            @foreach ($menu['child'] as $child)
                                <?php $hasChildAccess = false;  ?>
                                @foreach ($child['access'] as $childAccess)
                                    <?php $hasChildAccess = Gate::check($childAccess);?>
                                @endforeach
                                @if($hasChildAccess)
                                <li><a href="{{ !empty($child['route']) ? route($child['route']) : '#'}}"><i class="fa fa-lock"></i> {{$child['name']}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endif
        @endforeach
        @endif
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<a role="button" data-toggle="collapse" data-parent="#rightbar-accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <span class="sidebar-form" style="border: none; margin: 0px;">Order List</span></h3></a>
			</div>
			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body" style="padding: 8px;">
					<div class="list">
						@foreach(\App\Order::getOrderList() as $order)
							<table class="dashboard-list">
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-list"></i></th>
									<th colspan="2">{{$order->shop_order_no}}</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: {{$order->items_count}}</td>
								</tr>
								<tr>
									<td></td>
									<td>Order Type</td>
									<td>: {{$order->orderType->name}}</td>
								</tr>
								<tr>
									<td></td>
									<td>Value</td>
									<td>: {{number_format($order->total,2)}}</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<button class="btn btn-danger btn-xs btn-flat" onclick="viewOrder(this)" data-id="{{$order->id}}">View</button>
										<button class="btn btn-warning btn-xs btn-flat" data-toggle="modal"
												data-target="#orderModal">Add PL
										</button>
									</td>
								</tr>
							</table>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo">
				<a class="collapsed" role="button" data-toggle="collapse" data-parent="#rightbar-accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><h3 class="panel-title"><i class="fa fa-file-text-o"></i> <span class="sidebar-form" style="border: none; margin: 0px;">Picking List</span></h3></a>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
				<div class="panel-body" style="padding: 8px;">
					<div class="list">
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start</a>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start</a>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start</a>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingThree">
				<a class="collapsed" role="button" data-toggle="collapse" data-parent="#rightbar-accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><h3 class="panel-title"><i class="fa fa-gift"></i> <span class="sidebar-form" style="border: none; margin: 0px;">Packing List</span></h3></a>
			</div>
			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
				<div class="panel-body" style="padding: 8px;">
					<div class="list">
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-check"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start Packing</a>
										<a href="#" class="btn btn-warning btn-xs btn-flat">Print Invoice</a>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-check"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start Packing</a>
										<a href="#" class="btn btn-warning btn-xs btn-flat">Print Invoice</a>
									</td>
								</tr>
							</tbody>
						</table>
						<table>
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-check"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start Packing</a>
										<a href="#" class="btn btn-warning btn-xs btn-flat">Print Invoice</a>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-check"></i></th>
									<th colspan="2">PL12346666</th>
								</tr>
								<tr>
									<td></td>
									<td>Total Order</td>
									<td>: 10</td>
								</tr>
								<tr>
									<td></td>
									<td>Total Item</td>
									<td>: 5</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td colspan="2">
										<a href="#" class="btn btn-danger btn-xs btn-flat">Start Packing</a>
										<a href="#" class="btn btn-warning btn-xs btn-flat">Print Invoice</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingFour">
				<a class="collapsed" role="button" data-toggle="collapse" data-parent="#rightbar-accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><h3 class="panel-title"><i class="fa fa-truck"></i> <span class="sidebar-form" style="border: none; margin: 0px;">Shipping List<span></h3></a>
			</div>
			<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
				<div class="panel-body" style="padding: 8px;">
					<div class="list">
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">LAZ3445500001</th>
								</tr>
								<tr>
									<td></td>
									<td>Wight</td>
									<td>: 0.7kg</td>
								</tr>
								<tr>
									<td></td>
									<td>Shipping Fee</td>
									<td>: SGD5</td>
								</tr>
								<tr>
									<td></td>
									<td>Bin</td>
									<td>: A</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td>Pickup Carrier</td>
									<td>: POS Laju</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">LAZ3445500001</th>
								</tr>
								<tr>
									<td></td>
									<td>Wight</td>
									<td>: 0.7kg</td>
								</tr>
								<tr>
									<td></td>
									<td>Shipping Fee</td>
									<td>: SGD5</td>
								</tr>
								<tr>
									<td></td>
									<td>Bin</td>
									<td>: A</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td>Pickup Carrier</td>
									<td>: POS Laju</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">LAZ3445500001</th>
								</tr>
								<tr>
									<td></td>
									<td>Wight</td>
									<td>: 0.7kg</td>
								</tr>
								<tr>
									<td></td>
									<td>Shipping Fee</td>
									<td>: SGD5</td>
								</tr>
								<tr>
									<td></td>
									<td>Bin</td>
									<td>: A</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td>Pickup Carrier</td>
									<td>: POS Laju</td>
								</tr>
							</tbody>
						</table>
						<table class="dashboard-list">
							<tbody>
								<tr class="fisrtend">
									<th align="center"><i class="fa fa-star"></i></th>
									<th colspan="2">LAZ3445500001</th>
								</tr>
								<tr>
									<td></td>
									<td>Wight</td>
									<td>: 0.7kg</td>
								</tr>
								<tr>
									<td></td>
									<td>Shipping Fee</td>
									<td>: SGD5</td>
								</tr>
								<tr>
									<td></td>
									<td>Bin</td>
									<td>: A</td>
								</tr>
								<tr class="fisrtend">
									<td></td>
									<td>Pickup Carrier</td>
									<td>: POS Laju</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </ul>
	</div>
</section>
