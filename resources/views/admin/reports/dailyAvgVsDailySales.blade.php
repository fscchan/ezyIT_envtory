@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current() . '/export/xls' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank" class="submit-btn"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current() . '/export/pdf' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank" class="submit-btn"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current() . '/export/html' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<form class="form-inline pull-right">
					<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range" value="{{ $filter->dateFloor }}">
					<input name="ceil" class="datetimepicker form-control" placeholder="Select end date range" value="{{ $filter->dateCeil }}">
					<input name="sku" class="form-control" placeholder="Filter by SKU" value="{{ @$filter->sku }}">
					<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-print"></i> Submit</button>
				</form>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form method="post" id="exportForm">{{ csrf_field() }}<input type="hidden" id="exportImage" name="chartImage"></form>
				<script>
					function readyCallback(event, chart) {
						$("#exportImage").val(chart.getImageURI());
					}
				</script>
				<div class="report">
					<h2 class="text-center">{{ $title }}</h2>
					<p class="text-center">
						<div id="dailySales-chart">{!! !empty($chart) ? '<img src="' . $chart . '" width="100%">' : "" !!}</div>
						@combochart("dailySalesChart", "dailySales-chart")
					</p>
					<h2>By SKU</h2>
					<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
					@if(!empty($filter->sku))
						<p><b>SKU: </b>{{ $filter->sku }}</p>
					@endif
					<table class="table table-bordered table-hover">
						<thead><tr><th>Date</th><th>SKU</th><th>Description</th><th>Category</th><th>Price sold</th><th>Qty sold</th><th>Balance Qty</th><th>Total amount</th></tr></thead>
						<tbody>
							@foreach($dailySales as $i => $o)
							<tr>
								<td>{{ $o->order_date }}</td>
								<td>{{ $o->sku }}</td>
								<td>{!! $o->product_sku_description !!}</td>
								<td>{{ $o->cat_name }}</td>
								<td>{{ $o->average_price }}</td>
								<td>{{ $o->sku_sold }}</td>
								<td></td>
								<td>{{ $o->average_price * $o->sku_sold }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.js') }}"></script>
<script>
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd"
	});
	$(".datetimepicker").datetimepicker({
		showSecond: true,
		dateFormat: "yy-mm-dd",
		timeFormat: "HH:mm:ss"
	});
	
	$(".submit-btn").on("click", function(ev) {
		ev.preventDefault();
		$("#exportForm").attr("action", $(this).attr("href")).submit();
	});
</script>
@endpush