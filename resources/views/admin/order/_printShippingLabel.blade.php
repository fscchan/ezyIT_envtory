<table id="shipping-labels">
	<tr>
		<td><div class="label-label">Merchant copy</div></td>
		<td>
			<div class="row">
				<div class="col-xs-3">
					<?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
					<div align="center">
					<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="100%" />
					{{ $order->order_no }}
					</div>
				</div>
				<div class="col-xs-7">
					<div align="right">
					  <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
					  <p>{{ $company->profile_billing_address }}</p>
					</div>
				</div>
				<div class="col-xs-2">
					<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
				</div>
			</div><hr/>
			<div class="row">
				<div class="col-xs-8">
					<div class="sender">
						<label>SENDER ADDRESS</label><br>
						<address>
							<strong>{{ $order->shop->shop_name }}</strong><br>
						</address>
					</div>
					<div class="receiver">
						<div class="pull-right text-center">
							<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="75%" /><br>
							{{ $order->order_no }}
						</div>
						<address>
							<label>Receiver:</label>
							<strong>{{ $order->bill_first_name . " " . $order->bill_last_name }}</strong><br>
							{{ $order->bill_address }}<br>
							{{ $order->bill_address2 }}<br>
							Email: {{ $order->email }}
							<div class="pull-right">{{ $order->bill_postal_code }}</div>
						</address>
					</div>
				</div>
				<div class="col-xs-4" style="border-left: 1px solid #EEE">
					<label>Pieces:</label><span> {{ count($details) }}</span><br/>
					<label>Weight:</label><span></span><br/>
					<label>Ref no:</label><span></span><br/>
					<label>Credit card / Debit card</label><br/>
					<div style="display: inline-block; border-bottom: 1px dotted #000;">Receiver's name & signature<br/><br/><br/><br/></div><br/><br/>
					<label>NIRC no:</label><span></span><br/>
					<label>Date & Time:</label><span></span><br/>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td><div class="label-label">Billing copy</div></td>
		<td>
			<div class="row">
				<div class="col-xs-3">
					<?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
					<div align="center">
					<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="100%" />
					{{ $order->order_no }}
					</div>
				</div>
				<div class="col-xs-7">
					<div align="right">
					  <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
					  <p>{{ $company->profile_billing_address }}</p>
					</div>
				</div>
				<div class="col-xs-2">
					<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
				</div>
			</div><hr/>
			<div class="row">
				<div class="col-xs-8">
					<div class="sender">
						<label>SENDER ADDRESS</label><br>
						<address>
							<strong>{{ $order->shop->shop_name }}</strong><br>
						</address>
					</div>
					<div class="receiver">
						<address>
							<label>Receiver:</label>
							<strong>{{ $order->bill_first_name . " " . $order->bill_last_name }}</strong><br>
							{{ $order->bill_address }}<br>
							{{ $order->bill_address2 }}<br>
							Email: {{ $order->email }}
							<div class="pull-right">{{ $order->bill_postal_code }}</div>
						</address>
					</div>
				</div>
				<div class="col-xs-4" style="border-left: 1px solid #EEE">
					<label>Pieces: </label><span> {{ count($details) }}</span><br/>
					<label>Weight: </label><span></span><br/>
					<label>Ref no: </label><span></span><br/>
					<label>Credit card / Debit card</label><br/>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td><div class="label-label">POD copy</div></td>
		<td>
			<div class="row">
				<div class="col-xs-3">
					<?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
					<div align="center">
					<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="100%" />
					{{ $order->order_no }}
					</div>
				</div>
				<div class="col-xs-7">
					<div align="right">
					  <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
					  <p>{{ $company->profile_billing_address }}</p>
					</div>
				</div>
				<div class="col-xs-2">
					<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
				</div>
			</div><hr/>
			<div class="row">
				<div class="col-xs-8">
					<div class="sender">
						<label>SENDER ADDRESS</label><br>
						<address>
							<strong>{{ $order->shop->shop_name }}</strong><br>
						</address>
					</div>
					<div class="receiver">
						<address>
							<label>Receiver:</label>
							<strong>{{ $order->bill_first_name . " " . $order->bill_last_name }}</strong><br>
							{{ $order->bill_address }}<br>
							{{ $order->bill_address2 }}<br>
							Email: {{ $order->email }}
							<div class="pull-right">{{ $order->bill_postal_code }}</div>
						</address>
					</div>
				</div>
				<div class="col-xs-4" style="border-left: 1px solid #EEE">
					<label>Pieces:</label><span></span><br/>
					<label>Weight:</label><span></span><br/>
					<label>Ref no:</label><span></span><br/>
					<label>Credit card / Debit card</label><br/>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td><div class="label-label">Receiver copy</div></td>
		<td>
			<div class="row">
				<div class="col-xs-3">
					<?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
					<div align="center">
					<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="100%" />
					{{ $order->order_no }}
					</div>
				</div>
				<div class="col-xs-7">
					<div align="right">
					  <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
					  <p>{{ $company->profile_billing_address }}</p>
					</div>
				</div>
				<div class="col-xs-2">
					<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
				</div>
			</div><hr/>
			<div class="row">
				<div class="col-xs-8">
					<div class="sender">
						<label>SENDER ADDRESS</label><br>
						<address>
							<strong>{{ $order->shop->shop_name }}</strong><br>
						</address>
					</div>
					<div class="receiver">
						<address>
							<label>Receiver:</label>
							<strong>{{ $order->bill_first_name . " " . $order->bill_last_name }}</strong><br>
							{{ $order->bill_address }}<br>
							{{ $order->bill_address2 }}<br>
							Email: {{ $order->email }}
							<div class="pull-right">{{ $order->bill_postal_code }}</div>
						</address>
					</div>
				</div>
				<div class="col-xs-4" style="border-left: 1px solid #EEE">
					<label>Pieces:</label><span></span><br/>
					<label>Weight:</label><span></span><br/>
					<label>Ref no:</label><span></span><br/>
					<label>Credit card / Debit card</label><br/>
					<div style="display: inline-block;"><br/><br/><br/><br/></div><br/><br/>
					<label>Need to return this item?</label><br/>
					<span>Visit</span><br/>
					<span> {{ url("/helpCenter/returnsRefunds") }}</span><br/>
				</div>
			</div>
		</td>
	</tr>
</table>
