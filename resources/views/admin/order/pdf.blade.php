@extends('layouts.report.main')
@section('content')
<h3>ORDER</h3>
<table class="no-border">
	<tr>
		<td width="40%">
			<strong>{{ $order->shop->shop_name }}</strong><br>
		</td>
		<td align="right" width="20%">Shipping To</td>
		<td width="40%">
			<strong>{{ $order->bill_first_name . " " . $order->bill_last_name }}</strong><br>
		</td>
	</tr>
	<tr>
		<td>
			{{ $order->shop->shop_name }}<br>
		</td>
		<td></td>
		<td>
	        {{ $order->bill_address }}<br>
	        {{ $order->bill_address2 }}<br>
			Email: {{ $order->email }}
		</td>
	</tr>
</table>
<table>
	<thead>
	<tr>
	  <th></th>
	  <th>Product Code</th>
	  <th>Product Name</th>
	  <th>Qty</th>
	  <th>Price</th>
	  <th>Amount</th>
	</tr>
	</thead>
	<tbody>
		@foreach ($details as $key => $detail)
			<tr>
				<td>{{++$key}}</td>
				<td>{{$detail->sku->sku}}</td>
				<td>{{$detail->sku->product->attributes()->where("name", "name")->first()->name}}</td>
				<td>{{$detail->qty}}</td>
				<td align="right">{{$currency->code . " " . number_format($detail->price, 2)}}</td>
				<td align="right">{{$currency->code . " " . number_format($detail->qty * $detail->price, 2)}}</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5" align="right" class="name"><strong>Sub Total</strong></td>
			<td align="right">{{ $currency->code . " " . number_format($order->subtotal, 2) }}</td>
		</tr>
		<tr>
			<td colspan="5" align="right" class="name"><strong>Shipping Cost</strong></td>
			<td align="right">{{ $currency->code . " " . number_format($order->shipping_cost, 2) }}</td>
		</tr>
		<tr>
			<td colspan="5" align="right" class="name"><strong>Misc Cost</strong></td>
			<td align="right">{{ $currency->code . " " . number_format($order->tax, 2) }}</td>
		</tr>
		<tr>
			<td colspan="5" align="right" class="name"><strong>Total Amount</strong></td>
			<td align="right">{{ $currency->code . " " . number_format($order->total, 2) }}</td>
		</tr>
	</tfoot>
</table>
<br><br>
<table class="no-border">
	<tr>
		<td width="50%">
			Term and Condition
		</td>
		<td width="20%"></td>
		<td width="30%" align="right"></td>
	</tr>
	<tr>
		<td style="background-color: #f4f4f4;padding: 10px">
			{{ $order->remark }}
		</td>
		<td></td>
		<td align="right">
			<div align="center">
				<?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
				<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="200px" /><br>
				{{ $order->order_no }}
			</div>
		</td>
	</tr>
</table>
@stop