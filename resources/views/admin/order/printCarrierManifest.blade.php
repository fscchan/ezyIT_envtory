<!DOCTYPE html>
<html>
<head>
	<title>{{ $order->order_no }}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{!! asset('assets/lte/bootstrap/css/bootstrap.css') !!}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <link rel="stylesheet" href="{!! asset('assets/lte/dist/css/AdminLTE.min.css') !!}">

    <style>
    	body {
    		/*padding-top: 30px;*/
    	}
    </style>
</head>
<body onload="{{ $print ? 'window.print();' : ''}}">
<div class="wrapper">
	<section class="invoice">
		@include('admin.order._printCarrierManifest')
	</section>
</div>
</body>
</html>