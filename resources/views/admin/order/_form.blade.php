<form role="form" class="form-horizontal" method="POST"
      action="{{ $model->exists ? url('/app/order/'.$model->id) : url('/app/order') }}"
      onsubmit="return validateFormOrder()">
    {{ csrf_field() }}
    @if($model->exists){{ method_field('PUT') }}@endif
    <div class="box-body" id="form">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-primary btn-circle"><i
                                class="fa fa-shopping-cart"></i></a>
                    <p>Order</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled"><i
                                class="fa fa-vcard"></i></a>
                    <p>Bill</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled"><i
                                class="fa fa-shopping-bag"></i></a>
                    <p>Products</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled"><i
                                class="fa fa-truck"></i></a>
                    <p>Shipping</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled"><i
                                class="fa fa-credit-card"></i></a>
                    <p>Payment</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row setup-content" id="step-1">
            <div class="col-xs-12">
                <div class="col-md-6">
                    @if($model->exists)
                    <div class="form-group">
                        <label for="order_no" class="col-sm-4 control-label">Order Number <span
                                    class="red">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="order_no" class="form-control" id="order_no"
                                   value="{{old('order_no') ? old('order_no') : $model->order_no}}" required=""
                                   readonly="">
                        </div>
                    </div>
                    @endif
                    <div class="form-group hidden">
                        <label for="status" class="col-sm-4 control-label">New Order / Open <span
                                    class="red">*</span></label>
                        <div class="col-sm-8">
                            <select name="status" id="status" class="form-control">
                                <option value="0">New</option>
                                <option value="1">Open</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shop" class="col-sm-4 control-label">Shop <span class="red">*</span></label>
                        <div class="col-sm-8">
							<?php $shop_id = old('shop_id') ? old('shop_id') : $model->shop_id ?>
                            <select name="shop" id="shop" class="form-control" onchange="getSON(this)">
                                <option value="">Select Shop</option>
                                @foreach ($shops as $shop)
                                    <option value="{{$shop->id}}" {{ $shop_id==$shop->id ? 'selected' : '' }} data-prefix="{{$shop->prefix}}">{{$shop->shop_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-4 control-label">Customer First Name <span
                                    class="red">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" id="customer_name" name="customer_name" class="form-control"
                                   value="{{old('customer_name') ? old('customer_name') : $model->customer_name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-4 control-label">Customer Last Name</label>
                        <div class="col-sm-8">
                            <input type="text" id="customer_last_name" name="customer_last_name"
                                   class="form-control"
                                   value="{{old('customer_last_name') ? old('customer_last_name') : $model->customer_last_name}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="date" class="col-sm-4 control-label">Date <span class="red">*</span></label>
                        <div class="col-sm-8">
                            <div class="input-group date">
                                <input type="text" name="date" class="form-control datepicker" id="date"
                                       value="{{ $model->exists ? $model->date : date("Y-m-d") }}" required="">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="due_date" class="col-sm-4 control-label">Process Due Date <span
                                    class="red">*</span></label>
                        <div class="col-sm-8">
                            <div class="input-group date">
                                <input type="text" name="due_date" class="form-control datepicker" id="due_date"
                                       value="{{ $model->exists ? $model->due_date : date("Y-m-d") }}" required="">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shop_order_no" class="col-sm-4 control-label">Shop Order Number <span
                                    class="red">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="shop_order_no" class="form-control" id="shop_order_no"
                                   value="{{old('shop_order_no') ? old('shop_order_no') : $model->shop_order_no}}"
                                   required="required" readonly="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="person_in_charge" class="col-sm-4 control-label">Person in Charge</label>
                        <div class="col-sm-8">
                            <input type="text" name="person_in_charge" class="form-control"
                                   value="{{old('person_in_charge') ? old('person_in_charge') : $model->person_in_charge}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="remark" class="col-sm-4 control-label">Remarks</label>
                        <div class="col-sm-8">
                                <textarea name="remark" id="remark" class="form-control"
                                          rows="2">{{old('remark') ? old('remark') : $model->remark}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary nextBtn btn-flat pull-right" type="button">Next</button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-2">
            <div class="col-xs-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="bill_first_name" class="col-sm-4 control-label">Billing First Name</label>
                        <div class="col-sm-8">
                            <input type="text" id="bill_first_name" name="bill_first_name" class="form-control"
                                   value="{{old('bill_first_name') ? old('bill_first_name') : $model->bill_first_name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bill_last_name" class="col-sm-4 control-label">Billing Last Name</label>
                        <div class="col-sm-8">
                            <input type="text" id="bill_last_name" name="bill_last_name" class="form-control"
                                   value="{{old('bill_last_name') ? old('bill_last_name') : $model->bill_last_name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_no" class="col-sm-4 control-label">Billing Address</label>
                        <div class="col-sm-8">
                                <textarea name="bill_address" id="bill_address" class="form-control"
                                          rows="4">{{old('bill_address') ? old('bill_address') : $model->bill_address}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_no" class="col-sm-4 control-label">Billing Address 2</label>
                        <div class="col-sm-8">
                                <textarea name="bill_address2" id="bill_address2" class="form-control"
                                          rows="4">{{old('bill_address2') ? old('bill_address2') : $model->bill_address2}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_no" class="col-sm-4 control-label">Billing Postal Code</label>
                        <div class="col-sm-8">
                            <input type="text" name="bill_postal_code" id="bill_postal_code" class="form-control"
                                   value="{{old('bill_postal_code') ? old('bill_postal_code') : $model->bill_postal_code}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="main_phone_cust" class="col-sm-4 control-label">Tel 1</label>
                        <div class="col-sm-8">
                            <input type="text" name="main_phone_cust" id="main_phone_cust" class="form-control"
                                   value="{{old('main_phone_cust') ? old('main_phone_cust') : $model->main_phone_cust}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="main_phone_cust" class="col-sm-4 control-label">Tel 2</label>
                        <div class="col-sm-8">
                            <input type="text" name="sec_phone_cust" id="sec_phone_cust" class="form-control"
                                   value="{{old('sec_phone_cust') ? old('sec_phone_cust') : $model->sec_phone_cust}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" name="email" id="email" class="form-control"
                                   value="{{old('email') ? old('email') : $model->email}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="state" class="col-sm-4 control-label">Province / State</label>
                        <div class="col-sm-8">
                            <input type="text" name="state" id="state" class="form-control"
                                   value="{{old('state') ? old('state') : $model->state}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-sm-4 control-label">City</label>
                        <div class="col-sm-8">
                            <input type="text" name="city" id="city" class="form-control"
                                   value="{{old('city') ? old('city') : $model->city}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="country" class="col-sm-4 control-label">Country</label>
                        <div class="col-sm-8">
                            <input type="text" name="country" id="country" class="form-control"
                                   value="{{old('country') ? old('country') : $model->country}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary nextBtn btn-flat pull-right" type="button">Next <i
                                class="fa fa-arrow-right"></i></button>
                    <button class="btn btn-default prevBtn btn-flat pull-right" type="button"><i
                                class="fa fa-arrow-left"></i> Previous
                    </button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-3">
            <div class="col-xs-12">
                <div class="col-md-12 col-xs-12 table-responsive">
                    <table class="table" id="productTable">
                        <tbody>
                        @if($model->exists)
                            @foreach ($details as $k=>$detail)
                                <tr id="row-{{$k+1}}">
                                    <td width="300">
                                        <input type="hidden" name="products[{{$k+1}}][id]" value="{{$detail->id}}">
                                        <select name="products[{{$k+1}}][product_sku_id]"
                                                class="form-control search_sku" placeholder="SKU Product">
                                            <option value="">select SKU Product</option>
                                            @foreach($sku_lists as $sku)
                                                <option value="{{$sku->id}}"
                                                        data-product="{{$sku->product->attribute('name')}}" {{$sku->id==$detail->product_sku_id?'selected':''}}>{{$sku->sku}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td width="300"><input type="text" class="form-control"
                                                           placeholder="Product Name" readonly=""
                                                           value="{{$detail->sku->product->attribute('name')}}">
                                    </td>
                                    <td width="200"><input type="number" name="products[{{$k+1}}][price]"
                                                           id="price{{$k+1}}" class="form-control"
                                                           placeholder="Price" min="0"
                                                           onchange="calculateAmount({{$k+1}})"
                                                           value="{{$detail->price}}"></td>
                                    <td width="100"><input type="number" name="products[{{$k+1}}][qty]"
                                                           id="qty{{$k+1}}" class="form-control" placeholder="Qty"
                                                           min="1" value="1" onchange="calculateAmount({{$k+1}})"
                                                           value="{{$detail->qty}}"></td>
                                    <td width="200"><input type="text" class="form-control tableAmount"
                                                           placeholder="Amount" readonly="" id="amount{{$k+1}}"
                                                           value="{{$detail->price*$detail->qty}}"></td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-danger btn-flat"
                                                onclick="removeProductRow({{$k+1}})"><i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <button type="button" class="btn btn-xs btn-primary btn-flat" id="btn-add"><i
                                            class="fa fa-plus"></i> Add Product
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <div class="col-xs-12 table-responsive">
                        <table class="table tabel-bordered">
                            <thead>
                            <tr>
                                <th width="60%">Subtotal</th>
                                <th>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{\App\CompanyProfile::companyCurrency()}}</span>
                                        <input id="subtotal" name="subtotal" readonly="" type="text"
                                               class="form-control"
                                               value="{{old('subtotal') ? old('subtotal') : $model->subtotal}}">
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th>Shipping</th>
                                <th>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{\App\CompanyProfile::companyCurrency()}}</span>
                                        <input onchange="calcTotal();" id="shipping_cost" name="shipping_cost"
                                               type="text" class="form-control"
                                               value="{{old('shipping_cost') ? old('shipping_cost') : $model->shipping_cost}}">
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th>Discount</th>
                                <th>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{\App\CompanyProfile::companyCurrency()}}</span>
                                        <input onchange="calcTotal();" id="discount" name="discount" type="text"
                                               class="form-control"
                                               value="{{old('discount') ? old('discount') : $model->discount}}">
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th>Tax</th>
                                <th>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{\App\CompanyProfile::companyCurrency()}}</span>
                                        <input onchange="calcTotal();" id="tax" name="tax" type="text"
                                               class="form-control"
                                               value="{{old('tax') ? old('tax') : $model->tax}}">
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <th>
                                    <div class="input-group">
                                        <span class="input-group-addon">{{\App\CompanyProfile::companyCurrency()}}</span>
                                        <input id="total" name="total" readonly="" type="text" class="form-control"
                                               value="{{old('total') ? old('total') : $model->total}}">
                                    </div>
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary nextBtn btn-flat pull-right" type="button">Next <i
                                class="fa fa-arrow-right"></i></button>
                    <button class="btn btn-default prevBtn btn-flat pull-right" type="button"><i
                                class="fa fa-arrow-left"></i> Previous
                    </button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-4">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-2 control-label">Shipping method<span
                                    class="red">*</span></label>
                        <div class="col-sm-4">
                            <select name="shipping_method" id="shiping_method" class="form-control shipping-info" required>
                                <option value="">Select shipping provider</option>
								@foreach($shippingMethods as $shippingMethod)
									<option value="{{ $shippingMethod->id }}">{{ $shippingMethod->Name }}</option>
								@endforeach
							</select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <input name="isusebillingaddres" id="isusebillingaddres" type="checkbox"/>
                            <span class="lbl"> use same with billing address</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-4 control-label">First Name</label>
                        <div class="col-sm-8">
                            <input type="text" name="ship_first_name" id="ship_first_name"
                                   class="form-control shipping-info"
                                   value="{{old('ship_first_name') ? old('ship_first_name') : $model->ship_first_name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customer_name" class="col-sm-4 control-label">Last Name</label>
                        <div class="col-sm-8">
                            <input type="text" name="ship_last_name" id="ship_last_name"
                                   class="form-control shipping-info"
                                   value="{{old('ship_last_name') ? old('ship_last_name') : $model->ship_last_name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_no" class="col-sm-4 control-label">Address 1</label>
                        <div class="col-sm-8">
                                <textarea name="ship_address_1" id="ship_address_1" class="form-control shipping-info"
                                          rows="4">{{old('ship_address_1') ? old('ship_address_1') : $model->ship_address_1}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_no" class="col-sm-4 control-label">Address 2</label>
                        <div class="col-sm-8">
                                <textarea name="ship_address_2" id="ship_address_2" class="form-control shipping-info"
                                          rows="4">{{old('ship_address_2') ? old('ship_address_2') : $model->ship_address_2}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="country" class="col-sm-4 control-label">Country</label>
                        <div class="col-sm-8">
                            <input type="text" name="ship_country" id="ship_country"
                                   class="form-control shipping-info"
                                   value="{{old('ship_country') ? old('ship_country') : $model->ship_country}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="state" class="col-sm-4 control-label">Province / State</label>
                        <div class="col-sm-8">
                            <input type="text" name="ship_state" id="ship_state" class="form-control shipping-info"
                                   value="{{old('ship_state') ? old('ship_state') : $model->ship_state}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-sm-4 control-label">City</label>
                        <div class="col-sm-8">
                            <input type="text" name="ship_city" id="ship_city" class="form-control shipping-info"
                                   value="{{old('ship_city') ? old('ship_city') : $model->ship_city}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-sm-4 control-label">Postal Code</label>
                        <div class="col-sm-8">
                            <input type="text" name="ship_postal_code" id="ship_postal_code"
                                   class="form-control shipping-info"
                                   value="{{old('ship_postal_code') ? old('ship_postal_code') : $model->ship_postal_code}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="ship_email" name="ship_email" id="ship_email"
                                   class="form-control shipping-info"
                                   value="{{old('ship_email') ? old('ship_email') : $model->ship_email}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="main_phone_cust" class="col-sm-4 control-label">Phone</label>
                        <div class="col-sm-8">
                            <input type="text" name="ship_phone" id="ship_phone" class="form-control shipping-info"
                                   value="{{old('ship_phone') ? old('ship_phone') : $model->ship_phone}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary nextBtn btn-flat pull-right" type="button">Next <i
                                class="fa fa-arrow-right"></i></button>
                    <button class="btn btn-default prevBtn btn-flat pull-right" type="button"><i
                                class="fa fa-arrow-left"></i> Previous
                    </button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-5">
            <div class="col-xs-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="payment_method" class="col-sm-4 control-label">Payment Method</label>
                        <div class="col-sm-8">
							<?php $pay_method = old('payment_method') ? old('payment_method') : $model->payment_method ?>
                            <select name="payment_method" id="payment_method" class="form-control">
                                <option value="">Select Payment Method</option>
                                <option value="CashOnDelivery" {{$pay_method=='CashOnDelivery' ? 'selected' : ''}}>
                                    Cash On Delivery
                                </option>
                                <option value="CreditCardOnDelivery" {{$pay_method=='CreditCardOnDelivery' ? 'selected' : ''}}>
                                    Credit Card On Delivery
                                </option>
                                <option value="Cybersource" {{$pay_method=='Cybersource' ? 'selected' : ''}}>Cyber
                                    Source
                                </option>
                                <option value="IPay88" {{$pay_method=='IPay88' ? 'selected' : ''}}>IPay88</option>
                                <option value="NoPayment" {{$pay_method=='NoPayment' ? 'selected' : ''}}>No
                                    Payment
                                </option>
                                <option value="Paypal" {{$pay_method=='Paypal' ? 'selected' : ''}}>Paypal</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="delivery_info" class="col-sm-4 control-label">Delivery Info </label>
                        <div class="col-sm-8">
                            <input type="text" name="delivery_info" class="form-control"
                                   value="{{old('delivery_info') ? old('delivery_info') : $model->delivery_info}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-success btn-flat pull-right" type="submit"><i
                                class="fa fa-save"></i>{{ $model->exists ? ' Update Order' : ' Create Order' }}
                    </button>
                    <button class="btn btn-default prevBtn btn-flat pull-right" type="button"><i
                                class="fa fa-arrow-left"></i> Previous
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
