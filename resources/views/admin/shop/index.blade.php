@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        @include('layouts.lte._export')
        <div class="box-tools">
          @can('create-shop')
          <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> Add Shop</a>
          @endcan
        </div>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="shop-table" cellspacing>
          <thead>
              <tr>
                  <th>Platform</th>
                  <th>Shop Name</th>
                  <th>Prefix</th>
                  <th></th>
              </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
@push('styles')
<style>
  .td-right {
    text-align: right;
  }
</style>
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function() {
    $('#shop-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('shop.index') !!}',
        columns: [
            { data: 'platform', name: 'platform' },
            { data: 'shop_name', name: 'shop_name' },
            { data: 'prefix', name: 'prefix' },
            { data: 'actions', name: 'actions', class: 'td-right', orderable: false, searchable: false }
        ]
    });
});
</script>
@endpush