<div class="row">
    <div class="col-xs-3">
        <?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
        <div align="center">
            {{--<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="100%" />--}}
            {{--{{ $order->order_no }}--}}
        </div>
    </div>
    <div class="col-xs-7">
        <div align="right">
            <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
            <p>{{ $company->profile_billing_address }}</p>
        </div>
    </div>
    <div class="col-xs-2">
        <img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-5">
    </div>
    <div class="col-md-7">
        <h3>TAX INVOICE</h3>
    </div>
    <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td width="10%"><b>Invoice Number</b></td>
                <td width="15%">{{ $order->order_no }}</td>
                <td rowspan="4"><div align="center" style="margin-top: 30px;">
                        <img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="70%" />
                        <br>{{ $order->order_no }}
                    </div></td>

            </tr>
            {{--<tr>--}}
                {{--<td><b>Order Number</b></td>--}}
                {{--<td>{{ $order->order_no }}</td>--}}

            {{--</tr>--}}
            <tr>
                <td><b>Order Date</b></td>
                <td>{{ $order->date }}</td>

            </tr>
            <tr>
                <td><b>Invoice To</b></td>
                <td>{{ $order->customer_name }}</td>

            </tr>
            <tr>
                <td><b>Invoice Date</b></td>
                <td>{{ $nowDate }}</td>

            </tr>
            </tbody>

        </table>
    </div>

</div>
{{--<hr/>--}}
<div class="row invoice-info">
    {{--<div class="col-sm-4 invoice-col">--}}
        {{--<address>--}}
            {{--<strong>{{ $order->shop->shop_name }}</strong><br>--}}
        {{--</address>--}}
    {{--</div>--}}
    {{--<div class="col-sm-4 invoice-col">--}}
        {{--<strong class="pull-right"></strong>--}}
    {{--</div>--}}
    <div class="col-sm-6 invoice-col">
        <h4><b>Billing Address</b></h4>
        <address>
            <strong>{{ $order->bill_first_name . " " . $order->bill_last_name }}</strong><br>
            {{ $order->bill_address }}<br>
            {{ $order->bill_address2 }}<br>
            Contact Number: {{ $order->main_phone_cust }}
        </address>
    </div>
    <div class="col-sm-6 invoice-col">
        <h4><b>Shipping Address</b></h4>
        <address>
            <strong>{{ $order->ship_first_name . " " . $order->ship_last_name }}</strong><br>
            {{ $order->ship_address_1 }}<br>
            {{ $order->ship_address_2 }}<br>
            Contact Number: {{ $order->ship_phone}}
        </address>
    </div>
</div>
{{--<h3><b>Payment method:</b> {{$order->payment_method}}</h3>--}}

<div class="row">
    <div class="col-xs-12 table-responsive">
        <h4>Your ordered items for <b>{{$order->shop_order_id}}</b></h4>
        {{--<h4>Your ordered items for <b>{{$order->shop->prefix}}</b></h4>--}}
    </div>
    <div class="col-xs-12 table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th><b>#</b></th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($details as $key => $detail)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$detail->sku->sku}}</td>
                    <td>{{$detail->sku->product->attributes()->where("name", "name")->first()->name}}</td>
                    <td>{{$detail->qty}}</td>
                    <td align="right">{{$currency->code . " " . number_format($detail->price, 2)}}</td>
                    <td align="right">{{$currency->code . " " . number_format($detail->qty * $detail->price, 2)}}</td>
                </tr>
            @endforeach
            </tbody>
            {{--<tfoot>--}}
            {{--<tr>--}}
                {{--<td colspan="5" align="right"><strong>Sub Total(inlc GST)</strong></td>--}}
                {{--<td align="right">{{ $currency->code . " " . number_format($order->subtotal, 2) }}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td colspan="5" align="right"><strong>Shipping Cost</strong></td>--}}
                {{--<td align="right">{{ $currency->code . " " . number_format($order->shipping_cost, 2) }}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td colspan="5" align="right"><strong>Tax</strong></td>--}}
                {{--<td align="right">{{ $currency->code . " " . number_format($order->tax, 2) }}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td colspan="5" align="right"><strong>Total Amount</strong></td>--}}
                {{--<td align="right">{{ $currency->code . " " . number_format($order->total, 2) }}</td>--}}
            {{--</tr>--}}
            {{--</tfoot>--}}
        </table>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <p class="lead">Term and Condition</p>
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">{{ $order->remark }}</p>
    </div>
    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <b>Subtotal (inc GST):</b>
            </div>
            <div class="col-xs-4"  align="right">
                <b>{{ $currency->code . " " . number_format($order->subtotal, 2) }}</b>
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <i>Less: Voucher Applied</i>
            </div>
            <div class="col-xs-4"  align="right">
                <i>-RM 0.00</i>
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-8">
                <hr style="margin-bottom: -1px; margin-top: 0px; border: 0.5px black solid;">
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <b>Total (inc GST):</b>
            </div>
            <div class="col-xs-4"  align="right">
                <b>{{ $currency->code . " " . number_format($order->subtotal, 2) }}</b>
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <i>Shipping (inc GST)</i>
            </div>
            <div class="col-xs-4"  align="right">
                <i>+{{ $currency->code . " " . number_format($order->shipping_cost, 2) }}</i>
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-8">
                <hr style="margin-bottom: -1px;    margin-top: 0px; border: 0.5px black solid;">
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <b>Net Paid:</b>
            </div>
            <div class="col-xs-4"  align="right">
                <b>{{ $currency->code . " " . number_format($order->subtotal+$order->shipping_cost, 2) }}</b>
            </div>


            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-8">
                <hr style="margin-bottom: -1px;    margin-top: 0px; border: 0.5px black solid;">
            </div>

            <div class="col-xs-12">
            <br>
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <i>Total (inc GST)</i>
            </div>
            <div class="col-xs-4"  align="right">
                <i>{{ $currency->code . " " . number_format($order->total, 2) }}</i>
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <i>*GST</i>
            </div>
            <div class="col-xs-4"  align="right">
                <i>-{{ $currency->code . " " . number_format($order->tax, 2) }}</i>
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-8">
                <hr style="margin-bottom: -1px;    margin-top: 0px; border: 0.5px black solid;">
            </div>

            <div class="col-xs-4" align="right"></div>
            <div class="col-xs-4" align="left">
                <b>Total (excl GST)</b>
            </div>
            <div class="col-xs-4"  align="right">
                <b>{{ $currency->code . " " . number_format($order->total, 2) }}</b>
            </div>


            {{--<div class="col-xs-4" align="right"></div>--}}
            {{--<div class="col-xs-8">--}}
                {{--<hr style="margin-bottom: -1px;    margin-top: 0px; border: 0.5px black solid;">--}}
            {{--</div>--}}

        </div>

    </div>
</div>
