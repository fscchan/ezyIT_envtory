<form role="form" class="form-horizontal" id="supplier-form" method="POST" action="{{ $model->exists ? url('/app/supplier/' . $model->id) : url('/app/supplier') }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group has-feedback">
					<label for="company-name" class="col-md-4 control-label">Supplier name<span class="red"> *</span></label>
					<div class="col-md-8">
						<input type="text" name="company-name" class="form-control" id="company-name" placeholder="Supplier name" value="{{ $model->exists ? $model->supplier_name : "" }}" maxlength="255" required="required">
						@if ($errors->has("company-name"))
						<span class="help-block">
							<strong>{{ $errors->first("company-name") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group has-feedback">
					<label for="company-prefix" class="col-md-4 control-label text-left">Prefix<span class="red"> *</span></label>
					<div class="col-md-8">
						<input type="text" name="company-prefix" class="form-control" id="company-prefix" placeholder="Prefix" value="{{ $model->exists ? $model->supplier_prefix : "" }}" maxlength="3" required="required">
						@if ($errors->has("company-prefix"))
						<span class="help-block">
							<strong>{{ $errors->first("company-prefix") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group has-feedback">
					<label for="company-address" class="col-md-4 control-label text-left">Address<span class="red"> *</span></label>
					<div class="col-md-8">
						<textarea name="company-address" class="form-control" id="company-address" placeholder="Address" value="" style="min-height: 5.5em;" maxlength="1024">{{ $model->exists ? $model->supplier_address : "" }}</textarea>
						@if ($errors->has("company-address"))
						<span class="help-block">
							<strong>{{ $errors->first("company-address") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group has-feedback">
					<label for="company-country" class="col-md-4 control-label text-left">Country<span class="red"> *</span></label>
					<div class="col-md-8">
						<select name="company-country" class="form-control" id="company-country" placeholder="Country"></select>
						<input type="hidden" id="placeholder-country" value="{{ $model->exists ? $model->supplier_country : "" }}">
						@if ($errors->has("company-country"))
						<span class="help-block">
							<strong>{{ $errors->first("company-country") }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="form-group has-feedback">
					<label for="company-state" class="col-md-4 control-label text-left">State<span class="red"> *</span></label>
					<div class="col-md-8">
						<select name="company-state" class="form-control" id="company-state" placeholder="Country">{!! $model->exists ? '<option value="' . $model->supplier_state . '" label="' . $model->supplier_state . '" selected="selected">' . $model->supplier_state . '</option>' : "" !!}</select>
						@if ($errors->has("company-state"))
						<span class="help-block">
							<strong>{{ $errors->first("company-state") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group has-feedback">
					<label for="company-city" class="col-md-4 control-label text-left">City<span class="red"> *</span></label>
					<div class="col-md-8">
						<select name="company-city" class="form-control" id="company-city" placeholder="Country">{!! $model->exists ? '<option value="' . $model->supplier_city . '" label="' . $model->supplier_city . '" selected="selected">' . $model->supplier_city . '</option>' : "" !!}</select>
						@if ($errors->has("company-city"))
						<span class="help-block">
							<strong>{{ $errors->first("company-city") }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="form-group has-feedback">
					<label for="company-zipcode" class="col-md-4 control-label text-left">ZIP code<span class="red"> *</span></label>
					<div class="col-md-8">
						<input type="number" name="company-zipcode" class="form-control" id="company-zipcode" placeholder="Zip code" value="{{ $model->exists ? $model->supplier_zipcode : "" }}" max="99999">
						@if ($errors->has("company-zipcode"))
						<span class="help-block">
							<strong>{{ $errors->first("company-zipcode") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<!--<div class="form-group has-feedback">
					<label for="company-mode" class="col-md-4 control-label text-left">Supplier mode<span class="red"> *</span></label>
					<div class="col-md-8">
						<select class="form-control" name="company-mode" id="company-mode" required="required">
							<option value="1"{{ $model->exists && $model->supplier_mode == "1" ? " selected" : "" }}>Consigment</option>
							<option value="2"{{ $model->exists && $model->supplier_mode == "2" ? " selected" : "" }}>Direct</option>
						</select>
						@if ($errors->has("company-mode"))
						<span class="help-block">
							<strong>{{ $errors->first("company-mode") }}</strong>
						</span>
						@endif
					</div>
				</div>-->
			</div>
			<div class="col-md-3">
				<div class="form-group has-feedback">
					<label for="company-minpo" class="col-md-4 control-label text-left">Min PO</label>
					<div class="col-md-8">
						<input type="number" name="company-minpo" class="form-control" id="company-minpo" placeholder="Min PO" value="{{ $model->exists ? $model->supplier_min_po : "0" }}" data-toogle="tooltip" data-placement="bottom">
						@if ($errors->has("company-minpo"))
						<span class="help-block">
							<strong>{{ $errors->first("company-minpo") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group has-feedback">
					<label for="company-maxpo" class="col-md-4 control-label text-left">Max PO</label>
					<div class="col-md-8">
						<input type="number" name="company-maxpo" class="form-control" id="company-maxpo" placeholder="Max PO" value="{{ $model->exists ? $model->supplier_max_po : "0" }}" data-toogle="tooltip" data-placement="bottom">
						@if ($errors->has("company-max"))
						<span class="help-block">
							<strong>{{ $errors->first("company-max") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3>Contact person</h3>
				<table id="supplier-contacts" class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th style="width: 64px;"></th>
						</tr>
					</thead>
					<tbody>
						<tr class="supplier-contacts" id="sc-row-1">
							<td>
								<input type="text" name="contact-name[]" class="form-control" placeholder="Contact name" value="{{ $model->exists ? $contacts[0]->contact_name : "" }}" maxlength="250" required="required">
								@if ($errors->has("contact-name"))
								<span class="help-block">
									<strong>{{ $errors->first("contact-name") }}</strong>
								</span>
								@endif
							</td>
							<td>
								<input type="email" name="contact-email[]" class="form-control" placeholder="Contact email" value="{{ $model->exists ? $contacts[0]->email : "" }}" maxlength="250">
								@if ($errors->has("contact-email"))
								<span class="help-block">
									<strong>{{ $errors->first("contact-email") }}</strong>
								</span>
								@endif
							</td>
							<td>
								<input type="text" name="contact-phone[]" class="form-control" placeholder="Contact phone number" value="{{ $model->exists ? $contacts[0]->phone : "" }}" pattern="[\d]{9,16}" maxlength="16">
								@if ($errors->has("contact-phone"))
								<span class="help-block">
									<strong>{{ $errors->first("contact-phone") }}</strong>
								</span>
								@endif
							</td>
							<td class="text-center"></td>
						</tr>
						@if($model->exists && count($contacts) > 1)
							@for($i = 1; $i < count($contacts); $i++)
								<tr class="supplier-contacts" id="sc-row-{{ $i + 1 }}">
									<td><input type="text" name="contact-name[]" class="form-control" placeholder="Contact name" value="{{ $model->exists ? $contacts[$i]->contact_name : "" }}" maxlength="250" required="required"></td>
									<td><input type="email" name="contact-email[]" class="form-control" placeholder="Contact email" value="{{ $model->exists ? $contacts[$i]->email : "" }}" maxlength="250"></td>
									<td><input type="text" name="contact-phone[]" class="form-control" placeholder="Contact phone number" value="{{ $model->exists ? $contacts[$i]->phone : "" }}" pattern="[\d]{9,16}" maxlength="16"></td>
									<td class="text-center"><button type="button" class="btn btn-xs btn-danger btn-flat" onclick="removeContactRow({{ $i + 1 }})"><i class="fa fa-times"></i></button></td>
								</tr>
							@endfor
						@endif
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3"><button type="button" class="btn btn-xs btn-primary btn-flat" id="btn-addContact"><i class="fa fa-plus"></i> Add contact</button></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-md-6" style="margin-left: auto; margin-right: auto; float: none; display: table;">
				<div class="" style="display: table-cell; width: 45%;">
					<label for="category-list">Available category</label>
					<input type="text" id="categorylist-search" class="form-control" placeholder="Search categories...">
					<select name="category-list" id="category-list" style="display: block; width: 100%; min-height: 256px;" multiple="multiple">
						@foreach($categories as $category)
							@if($category->cat_parent == "0")
								<optgroup label="{{ $category->cat_name }}" value="{{ $category->id }}">
									@foreach($categories as $subcategory)
										@if($subcategory->cat_parent != "0" && $subcategory->cat_parent == $category->id)
											<option value="{{ $subcategory->id }}"{{ $model->exists && !empty($model->supplier_categories) && in_array($subcategory->id, $model->supplier_categories) ? " hidden disabled" : "" }}>{{ $subcategory->cat_name }}</option>
										@endif
									@endforeach
								</optgroup>
							@endif
						@endforeach
					</select>
					@if(count($categories) < 1)
					<span class="help-block">
						<strong><span class="red">Warning</span> There are no categories yet.<br/>Please make some beforehand <a href="{{ url("app/productCategories/create") }}">here</a>.</strong>
					</span>
					@endif
				</div>
				<div class="" style="display: table-cell; width: 10%; vertical-align: middle; text-align: center;">
					<button type="button" class="btn btn-default btn-sm btn-flat" id="btn_addCategories"/><i class="fa fa-arrow-right"></i></button><br /><br />
					<button type="button" class="btn btn-default btn-sm btn-flat" id="btn_remCategories"/><i class="fa fa-arrow-left"></i></button>
				</div>
				<div class="" style="display: table-cell; width: 45%;">
					<label for="supplier-categories">Supplier categories<span class="red"> *</span></label>
					<input type="text" id="suppliercat-search" class="form-control" placeholder="Search categories...">
					<select name="supplier-categories[]" id="supplier-categories" style="display: block; width: 100%; min-height: 256px;" multiple="multiple">
						@foreach($categories as $category)
							@if($category->cat_parent == "0")
								<optgroup label="{{ $category->cat_name }}" value="{{ $category->id }}" hidden disabled>
									@foreach($categories as $subcategory)
										@if($subcategory->cat_parent != "0" && $subcategory->cat_parent == $category->id)
											<option value="{{ $subcategory->id }}"{{ $model->exists && !empty($model->supplier_categories) && in_array($subcategory->id, $model->supplier_categories) ? "" : " hidden disabled" }}>{{ $subcategory->cat_name }}</option>
										@endif
									@endforeach
								</optgroup>
							@endif
						@endforeach
					</select>
					@if ($errors->has("supplier-categories"))
					<span class="help-block">
						<strong>{{ $errors->first("supplier-categories") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/supplier") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
