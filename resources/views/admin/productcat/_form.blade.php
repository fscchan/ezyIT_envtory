<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url('/app/productCategories/' . $model->id) : url('/app/productCategories') }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="category-parent" class="col-md-4 control-label text-left">Category parent<span class="red"> *</span></label>
				<div class="col-md-8">
					<select name="category-parent" class="form-control" id="category-parent" placeholder="Leave blank to make main" >
						<option value="0" default{{ $model->exists ? "" : " selected" }} hidden>_MAIN</option>
						@foreach($parents as $options)
							@if($options->cat_parent == "0")
								<option value="{{ $options->id }}"@if($model->exists && $options->id == @$model->cat_parent) selected @endif>{{ $options->cat_name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<label for="category-name" class="col-md-4 control-label text-left">Category name<span class="red"> *</span></label>
				<div class="col-md-8">
					<input type="text" name="category-name" class="form-control" id="category-name" placeholder="Category name" value="{{ $model->exists ? $model->cat_name : "" }}" required="required">
					@if ($errors->has("company-prefix"))
					<span class="help-block">
						<strong>{{ $errors->first("company-prefix") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/productCategories") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
