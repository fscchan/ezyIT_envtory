<html>
	<head>
		<title>{{$title or 'Title Here'}}</title>
		<style>
			table {
				width: 100%;
				border: 0;
			}
			table th {
				background-color: #dfdfdf;
				border: 0;
			}
		</style>
	</head>
	<body>
	<h1>{{$title or 'Title Here'}}</h1>
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Category parent</th>
					<th>Category name</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($datas as $data)
				<tr>
					<td>{{ $data[0] }}</td>
					<td>{{ $data[1] }}</td>
					<td>{{ $data[2] }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</body>
</html>