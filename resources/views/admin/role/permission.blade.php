@extends('layouts.lte.main')
@section('content')
@include('layouts.lte.status')
<form action="{{url('/app/role/update')}}" method="post" role="form">
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  <input type="hidden" name="role" value="{{$role->id}}" />
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="box box-default">
        <div class="box-body">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th width="1"></th>
                  <th>Module</th>
                  @foreach($permission as $pr)
                  <th>{{ucwords($pr)}}</th>
                  @endforeach
				  @can("create-role")
				  <th>Level</th>
				  @endcan
                </tr>
              </thead>
              <tbody>
                @foreach($modules as $key => $module)
                <?php $allPermission = true; ?>
                @foreach($permission as $pr)
                @if (!$role->hasPermissionTo($pr.'-'.$module))
                  <?php $allPermission = false; ?>
                @endif
                @endforeach
                <tr>
                  <td><input type="checkbox" class="selectAll" {{ $allPermission ? 'checked' : ''}}/></td>
                  <td>{{ucwords($module)}}</td>
                  @foreach($permission as $pr)
                  <td>
                    <input type="checkbox" name="permission[]" value="{{$pr.'-'.$module}}" {{$role->hasPermissionTo($pr.'-'.$module) ? 'checked' : ''}} class="permission" />
				  @endforeach
				  @can("create-role")
				  <td>
					  <select name="level[{{ $module }}]" class="level-select form-control">
						  <option value="1" {{ !empty($levels[$module]) && $levels[$module] == 1 ? 'selected = "selected"' : "" }}>Basic</option>
						  <option value="2" {{ !empty($levels[$module]) && $levels[$module] == 2 ? 'selected = "selected"' : "" }}>Premium</option>
						  <option value="3" {{ !empty($levels[$module]) && $levels[$module] == 3 ? 'selected = "selected"' : "" }}>Advanced</option>
					  </select>
				  </td>
				  @endcan
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="box-footer">
          <a class="btn btn-default btn-flat" href="{{url('app/role')}}"><i class="fa fa-arrow-left"></i> Cancel</a>
          <button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-lock"></i> Set Permission</button>
        </div>
      </div>
    </div>
  </div>
</form>
@stop
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  $('.selectAll').on('ifChecked ifUnchecked', function (e) { 
    permission = $(this).closest('tr').find(".permission");    
    if (e.type == 'ifChecked') {
        permission.iCheck('check');
    } else {
        permission.iCheck('uncheck');
    }
  });
</script>
@endpush