@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">

			</div>
			<div class="box-body">
				<div id="stockins">
					<table class="table-hover dataTable" style="width: 100%;" role="button">
						@forelse ($stockIn as $stock)
<!--							--><?php
//								die($stock);
//								?>
						<tr class="data-stockin" data-value="{{$stock->id}}">
							<td class="media-middle text-center" style="width: 12%; padding: 8px 0px;">
<!--								--><?php //$images = $stock->product->sku->images->first(); ?>
								<div class="media img-circle" style="display: inline-block; width: 96px; height: 96px; border: 1px solid #CCCCCC">
									{{--@if (!empty($images))--}}
										{{--<img src="{{ asset('app/file/sku/'.$images->filename) }}" alt="" width="100%">--}}
									{{--@endif--}}
								</div>
							</td>
							<td class="media-body text-left" style="width: 16%; padding: 16px 0px;">
								<div><b class="batch">{{@$stock->batch_id}}</b></div>
								<div>{{$stock->product->product_code}}</div>
								<div>{{$stock->product->attribute('name')}}</div>
							</td>
							<td class="media-body text-left" style="padding: 16px 0px;">
								@if (!empty($stock->cell))
									<div><b class="cell"><i class="fa fa-bars"></i> {{$stock->cell->prefix}}</b></div>
								@endif
							</td>
							<td class="media-body text-left" style="padding: 16px 0px;">
								@if (!empty($stock->invoice))
									<div>{{$stock->invoice->po->prefix}}</div>
									<div>{{$stock->invoice->po->supplier->supplier_name}}</div>
								@endif
							</td>
							<td class="media-body text-center" style="width: 16%; padding: 16px 0px;">
								<div class="h4 media-heading">Qty to stock in</div>
								<div class="h2 media-heading"><b class="pick-qty">{{ $stock->in->sum('qty') }}</b></div>
							</td>
							<td>
								<?php
								$supplier = isset($stock->invoice->po->supplier->supplier_name) ? $stock->invoice->po->supplier->supplier_name : '';
								$code =	"Batch ID : ".$stock->batch_id.
												"\nSupplier : ".$supplier.
												"\nProduct Code : ".$stock->product->product_code.
												"\nReceive Date: ".$stock->received_date;
								?>
								{!! QrCode::size(100)->generate($code); !!}
							</td>
						</tr>
						@empty
						<tr>
							<td class="media-body text-center" style="width: 16%; padding: 16px 0px;">
								<div class="h2 media-heading"><b>No Batch to Add</b></div>
							</td>
						</tr>
						@endforelse
					</table>
				</div>
			</div>
		</div>
		<div class="" align="center">
			{{ $stockIn->links() }}
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.structure.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}">
<style>
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(".data-stockin").on("click", function() {
		var idx = $(this).data("value");
		var batch = $(this);
		swal({
		  title: "Add to Stock?",
		  text: "Batch <b>"+batch.find('.batch').text()+"</b> to<br>Cell <b>"+batch.find('.cell').text()+"</b>",
		  type: "warning",
		  html: true,
		  showCancelButton: true,
		  closeOnConfirm: false,
		  showLoaderOnConfirm: true,
		},
		function(){
			axios.post('/app/stockIn', {
				id: batch.data("value")
			})
			.then(function(response) {
				batch.remove();
			    swal({
			    	type: 'success',
			    	title: response.data.message
			    });
			})
			.catch(function (error) {
			    swal({
			    	type: 'error',
			    	title: 'Failed to Add Batch to Stock'
			    });
			  });
		});
	});
</script>
@endpush
