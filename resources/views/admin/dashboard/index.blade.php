@extends('layouts.lte.main')
@section('content')
    <div class="callout callout-info">
        <h4>Welcome back, {{ Auth::user()->firstname }}!</h4>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Order List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool">{{count(\App\Order::getOrderList())}}</button>
                    </div>
                </div>
                <div class="box-body list">
                    @foreach(\App\Order::getOrderList() as $order)
                        <table class="dashboard-list">
                            <tr class="fisrtend">
                                <th align="center"><i class="fa fa-list"></i></th>
                                <th colspan="2">{{$order->shop_order_no}}</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Total Item</td>
                                <td>: {{$order->items_count}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Order Type</td>
                                <td>: {{$order->orderType->name}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Value</td>
                                <td>: {{number_format($order->total,2)}}</td>
                            </tr>
                            <tr class="fisrtend">
                                <td></td>
                                <td colspan="2">
                                    <button class="btn btn-danger btn-xs btn-flat" onclick="viewOrder(this)" data-id="{{$order->id}}">View</button>
                                    <a class="btn btn-warning btn-xs btn-flat" href="{{url('app/pickingList')}}"> Add PL</a>
                                </td>
                            </tr>
                        </table>
                    @endforeach
                </div>
            </div>
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-truck"></i> Shipping List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool">5</button>
                    </div>
                </div>
                <div class="box-body list">
                    <table class="dashboard-list">
                        <tr class="fisrtend">
                            <th align="center"><i class="fa fa-star"></i></th>
                            <th colspan="2">LAZ3445500001</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Wight</td>
                            <td>: 0.7kg</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Shipping Fee</td>
                            <td>: SGD5</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Bin</td>
                            <td>: A</td>
                        </tr>
                        <tr class="fisrtend">
                            <td></td>
                            <td>Pickup Carrier</td>
                            <td>: POS Laju</td>
                        </tr>
                    </table>
                    <table class="dashboard-list">
                        <tr class="fisrtend">
                            <th align="center"><i class="fa fa-star"></i></th>
                            <th colspan="2">LAZ3445500001</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Wight</td>
                            <td>: 0.7kg</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Shipping Fee</td>
                            <td>: SGD5</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Bin</td>
                            <td>: A</td>
                        </tr>
                        <tr class="fisrtend">
                            <td></td>
                            <td>Pickup Carrier</td>
                            <td>: POS Laju</td>
                        </tr>
                    </table>
                    <table class="dashboard-list">
                        <tr class="fisrtend">
                            <th align="center"><i class="fa fa-star"></i></th>
                            <th colspan="2">LAZ3445500001</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Wight</td>
                            <td>: 0.7kg</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Shipping Fee</td>
                            <td>: SGD5</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Bin</td>
                            <td>: A</td>
                        </tr>
                        <tr class="fisrtend">
                            <td></td>
                            <td>Pickup Carrier</td>
                            <td>: POS Laju</td>
                        </tr>
                    </table>
                    <table class="dashboard-list">
                        <tr class="fisrtend">
                            <th align="center"><i class="fa fa-star"></i></th>
                            <th colspan="2">LAZ3445500001</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Wight</td>
                            <td>: 0.7kg</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Shipping Fee</td>
                            <td>: SGD5</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Bin</td>
                            <td>: A</td>
                        </tr>
                        <tr class="fisrtend">
                            <td></td>
                            <td>Pickup Carrier</td>
                            <td>: POS Laju</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-file-text-o"></i> Picking List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool">{{count(\App\PickingList::getPickingList())}}</button>
                    </div>
                </div>
                <div class="box-body list">
                    @foreach(\App\PickingList::getPickingList() as $list)
                    <table class="dashboard-list">
                        <tr class="fisrtend">
                            <th align="center"><i class="fa fa-star"></i></th>
                            <th colspan="2">{{$list->picking_code}}</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Total Order</td>
                            <td>: {{$list->qty_of_order}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Total Item</td>
                            <td>: {{$list->total_item}}</td>
                        </tr>
                        <tr class="fisrtend">
                            <td></td>
                            <td colspan="2">
                                <a href="{{url('app/pickingList/' . $list->id)}}" class="btn btn-primary btn-xs btn-flat">Start</a>
                            </td>
                        </tr>
                    </table>
                    @endforeach
                </div>
            </div>
            <div class="box box-default box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><strong>2</strong> Purchase Order</h3>
              </div>
              <div class="box-body no-padding">
                <ul class="nav nav-stacked">
                  <li><a href="#">PO214523 <span class="pull-right badge">10 Oct 2016</span></a></li>
                  <li><a href="#">PO214523 <span class="pull-right badge">10 Oct 2016</span></a></li>
                </ul>
              </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-gift"></i> Packing List</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool">{{count(\App\PickingList::getReadyToPack())}}</button>
                    </div>
                </div>
                <div class="box-body list">
                    @foreach(\App\PickingList::getReadyToPack() as $list)
                        <table class="dashboard-list">
                            <tr class="fisrtend">
                                <th align="center"><i class="fa fa-check"></i></th>
                                <th colspan="2">{{$list->picking_code}}</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Total Order</td>
                                <td>: {{$list->qty_of_order}}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Total Item</td>
                                <td>: {{$list->total_item}}</td>
                            </tr>
                            <tr class="fisrtend">
                                <td></td>
                                <td colspan="2">
                                    <a href="{{url('app/packingList/create')}}" class="btn btn-danger btn-xs btn-flat">Start Packing</a>
                                    <a href="#" class="btn btn-warning btn-xs btn-flat">Print Invoice</a>
                                </td>
                            </tr>
                        </table>
                    @endforeach
                </div>
            </div>
            <div class="box box-default box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><strong>4</strong> Waiting to Stock In</h3>
              </div>
              <div class="box-body no-padding">
                <ul class="nav nav-stacked">
                  <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
                  <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
                  <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
                  <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
                </ul>
              </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="orderModal">Order Detail</h4>
                </div>
                <div class="modal-body">
                    <table class="dashboard-list table table-hover" width="100%" id="tbl_detail_dashboard"></table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                    <a class="btn btn-warning btn-flat" href="{{url('app/pickingList')}}"> Add PL</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@push('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function viewOrder(el) {
        id = ($(el).data('id'));
        $.ajax({
            url: "/app/order/getDetail",
            type: "post",
            data: { order: id }
        }).done(function (result) {
            html = '';
            $.each(result, function(i, v) {
                html += '<tr>'+
                            '<td><img src="" alt=""></td>'+
                            '<td>'+v.shop_order_no+'<br>'+v.product_name+'<br>'+v.product_sku_id+'</td>'+
                            '<td>Qty<br><h3>'+v.qty+'</h3></td>'+
                        '</tr>'
            });
            $('#tbl_detail_dashboard').html(html)
            $('#orderModal').modal('show')
        }).fail(function (jqXHR, textStatus, errorThrown) {
            // needs to implement if it fails
        });
    }


</script>
@endpush
