@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        <div class="btn-group">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <div class="pull-right hidden">
          @can('create-auditTrails')
          <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New category</a>
          @endcan
        </div>
		<div class="clear" style="clear: both;"></div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="trails-table" cellspacing>
          <thead>
              <tr>
                  <th style="vertical-align: top;">Date</th>
                  <th style="vertical-align: top;">Module<br/>
					  <select id="filter-module" class="filter-module">
						<option value="">All</option>
						@foreach(collect(App\Library::modules())->sort() as $module)
						  <option value="{{ $module }}">{{ $module }}</option>
						@endforeach
					  </select>
				  </th>
                  <th style="vertical-align: top;">Information</th>
                  <th style="vertical-align: top;">IP Address</th>
                  <th style="vertical-align: top;">Comment</th>
              </tr>
          </thead>
		  <tbody></tbody>
		  <tfoot>
			  <tr>
				  <th style="text-align: center;">
					  <div class="input-group date">
						<input type="text" id="filter-date" class="form-control datepicker" placeholder="Filter date">
						<div class="input-group-addon">
						  <i class="fa fa-calendar"></i>
						</div>
					  </div>
				  </th>
				  <th style="text-align: center;">
					  <select id="filter-module" class="filter-module form-control">
						<option value="">All</option>
						@foreach(collect(App\Library::modules())->sort() as $module)
						  <option value="{{ $module }}">{{ $module }}</option>
						@endforeach
					  </select>
				  </th>
				  <th style="text-align: center;"><input type="text" id="filter-info" class="form-control" placeholder="Filter information"></th>
				  <th style="text-align: center;"><input type="text" id="filter-ip" class="form-control" placeholder="Filter IP"></th>
				  <th style="text-align: center;"><input type="text" id="filter-comment" class="form-control" placeholder="Filter comment"></th>
			  </tr>
		  </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
var table = $('#trails-table').DataTable({
	processing: true,
	serverSide: true,
	ajax: '{!! route('auditTrails.index') !!}',
	columns: [
		{ data: 'audit_timestamp', name: 'audit_timestamp' },
		{ data: 'audit_trigger', name: 'audit_trigger' },
		{ data: 'audit_info', name: 'audit_info' },
		{ data: 'audit_ip', name: 'audit_ip' },
		{ data: 'audit_comment', name: 'audit_comment' }
	],
	order: [0, "desc"]
});

$("#filter-date").on("change", function() {
	table.columns(0).search($(this).val()).draw();
});

$(".filter-module").on("change", function() {
	table.columns(1).search($(this).val()).draw();
});

$("#filter-info").on("change", function() {
	table.columns(2).search($(this).val()).draw();
});

$("#filter-ip").on("change", function() {
	table.columns(3).search($(this).val()).draw();
});

$("#filter-comment").on("change", function() {
	table.columns(4).search($(this).val()).draw();
});

$(".datepicker").datepicker({
    autoclose: !0,
    format: "yyyy-mm-dd",
    todayHighlight: !0
});
</script>
@endpush