@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box box-primary">
			<div class="box-header">
				<a target="_blank" href="{{ url('/app/po/readyToStockIn/qrcode') }}" class="btn btn-info btn-flat pull-right"><i class="fa fa-qrcode"></i> Download QR Code</a>
			</div>
			<div class="box-body">
				<table class="table dataTable" id="stockin-table">
					<thead>
						<tr>
							<th>Invoice Number</th>
							<th>Batch ID</th>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>Supplier</th>
							<th>Received</th>
							<th>Rejected</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function() {
    $('#stockin-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/app/po/readyToStockIn',
        columns: [
            { data: 'invoice', name: 'invoice' },
            { data: 'batch_id', name: 'batch_id' },
            { data: 'product.product_code', name: 'product.product_code' },
            { data: 'product.attributes[0].value', name: 'product.attributes[0].value' },
            { data: 'supplier', name: 'supplier' },
            { data: 'received', name: 'received' },
            { data: 'rejected', name: 'rejected' },
        ]
    });
});
</script>
@endpush
