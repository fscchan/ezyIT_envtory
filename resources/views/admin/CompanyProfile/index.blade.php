<style>
	td.details-control {
		background: url('{{ asset('img/common/details_open.png') }}') no-repeat center center;
		cursor: pointer;
	}

	tr.shown td.details-control {
		background: url('{{ asset('img/common/details_close.png') }}') no-repeat center center;
	}
</style>
@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        <div class="btn-group hidden">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <div class="pull-right">
          @can('create-companyProfiles')
          <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New profile</a>
          @endcan
        </div>
		<div class="clear" style="clear: both;"></div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="profiles-table" cellspacing>
          <thead>
              <tr>
                  <th style="width: 40px;"></th>
                  <th>Name</th>
                  <th style="width: 128px;"></th>
              </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
  $(function() {
    var table = $('#profiles-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('companyProfiles.index') !!}',
        columns: [
			{
				"className":      'details-control',
				"orderable":      false,
				"data":           null,
				"defaultContent": ''
			},
            { data: 'profile_name', name: 'profile_name' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ],
		order: [1, "asc"]
    });
	$('#profiles-table tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = table.row(tr);
		if (row.child.isShown()) {
			row.child.hide();
			tr.removeClass('shown');
		} else {
			row.child(showChild(row.data())).show();
			tr.addClass('shown');
		}
	});
  });
  
function showChild(data) {
	return '<table style="border-collapse: separate;">' +
		'<tr>' +
			'<th style="text-align: right;">Profile type : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_type + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Billing address : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_billing_address + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">City : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_city + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">State : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_state + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">ZIP code : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_zip + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Country : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_country + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Phone number : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_phone_number + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Fax number : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_fax_number + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Email address : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_email_address + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Website : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_website + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">GST : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_gst + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Currency : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_currency + '</td>' +
		'</tr>' +
		'<tr>' +
			'<th style="text-align: right;">Time zone : </th>' +
			'<td style="padding-left: 8px;">' + data.profile_timezone + '</td>' +
		'</tr>' +
	'</table>';
};
</script>
@endpush