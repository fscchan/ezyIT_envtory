<div id="root">
	<form @submit.prevent="submitForm" role="form" class="form-horizontal" @keydown="errors.clear($event.target.id)" id="product-form" method="POST" action="{{ $model->exists ? url('/app/products/'.$model->id) : url('/app/products') }}" novalidate="">
		{{ csrf_field() }}
		@if($model->exists)
		{{ method_field('PUT') }}
		@endif
		<input type="hidden" name="general[m_product_categories_id]" :value="selectedCategory.id">
	    <div class="stepwizard">
	        <div class="stepwizard-row setup-panel">
	            <div class="stepwizard-step">
	                <button type="button" class="btn btn-circle" :class="{ 'btn-warning' : isSectionActive('categories') }">1</button>
	                <p>Select Category</p>
	            </div>
	            <div class="stepwizard-step">
	                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('productCodeList')}">2</button>
	                <p>Product Code List</p>
	            </div>
	            <div class="stepwizard-step">
	                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('productInformation')}">3</button>
	                <p>General Attributes</p>
	            </div>
	            <div class="stepwizard-step">
	                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('productVariation')}">4</button>
	                <p>Variation Attributes</p>
	            </div>
	            <div class="stepwizard-step">
	                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('assignSuppliers')}">5</button>
	                <p>Mapping to Suppliers</p>
	            </div>
	        </div>
	    </div>
	    <section id="categories" v-show="isSectionActive('categories')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Select a Category</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="categoryContainer">
								<div class="col-md-3 selectCategories" v-for="(category, index) in categories">
									<ul class="nav nav-pills nav-stacked" :id="'category-'+index">
										<li v-for="category in category" :data-id="category.id" @click="getChild(category,index);" :class="{ active: isActive(category,index), selected: isSelected(category) }"><a>@{{ category.cat_name | truncate }} <span class="pull-right" v-show="category.children"><i class="fa fa-angle-right"></i></span></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="col-md-9">
						<ol class="breadcrumb" v-if="activeItem.length">
						  <li v-for="(active, index) in activeItem">@{{ active.cat_name }}</li>
						</ol>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-warning btn-flat pull-right" id="selectCategory" :disabled="selectedCategory.length" @click="getCodeList">Select @{{ selectedCategory.cat_name }}</button>
					</div>
				</div>
			</div>
	    </section>
			<section id="productCodeList" v-show="isSectionActive('productCodeList')">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Product Code List</h3>
					</div>
					<div class="box-body">
						<div class="form-group">
	            <label class="col-sm-2 control-label">Product Category</label>
	            <div class="col-sm-10">
								<ol class="breadcrumb" v-if="activeItem.length">
								  <li v-for="(active, index) in activeItem">@{{ active.cat_name }}</li>
								</ol>
	            </div>
	          </div>
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Product Code</th>
											<th>Name</th>
											<th>Brand</th>
											<th>Model</th>
											<th>Submodel</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="list in codeList">
											<td>@{{ list.product_code }}</td>
											<td>@{{ list.code.name }}</td>
											<td>@{{ list.code.brand }}</td>
											<td>@{{ list.code.model }}</td>
											<td>@{{ list.code.submodel }}</td>
											<td align="right"><a :href="'/app/products/'+list.code.id+'/clone'" class="btn btn-default btn-flat btn-xs"><i class="fa fa-copy"></i> Clone</a></td>
										</tr>
										<tr v-show="!codeList.length">
											<td align="center" colspan="6">
												There is No Product Code with this Category.
												<button type="button" class="btn btn-default btn-flat btn-xs" @click="selectAttr()"><i class="fa fa-plus"></i> Create New Product Code</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="button" class="btn btn-default btn-flat" @click="activateSection('categories')">Back</button>
						<button type="button" class="btn btn-warning btn-flat pull-right" @click="selectAttr()"><i class="fa fa-plus"></i> New Product Code</button>
					</div>
				</div>
			</section>
		<section id="productInformation" v-show="isSectionActive('productInformation')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Product Information</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
            <label class="col-sm-3 control-label">Product Category</label>
            <div class="col-sm-9">
							<ol class="breadcrumb" v-if="activeItem.length">
							  <li v-for="(active, index) in activeItem">@{{ active.cat_name }}</li>
							</ol>
            </div>
          </div>
          <div v-for="attr in attributes.general">
						<attribute :label="attr.label" :error="errors.get(attr.name)" :name="attr.type+'['+attr.name+']'" :id="attr.name" :type="attr.inputType" :options="attr.options" :mandatory="attr.isMandatory" v-model="attr.value" @update="updateValue"></attribute>
          </div>
				</div>
				<div class="box-footer">
					<button type="button" class="btn btn-default btn-flat" {{ $model->exists || $clone ? 'disabled' : ''}} @click="activateSection('productCodeList')">Back</button>
					<button type="button" class="btn btn-warning btn-flat pull-right" @click="validateGeneral">Next</button>
				</div>
			</div>
		</section>
		<section id="productVariation" v-show="isSectionActive('productVariation')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Product Variation</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div v-for="attr in attributes.variation">
							<attributes-variant :label="attr.label" :error="errors.get(attr.name)" :name="'option['+attr.name+'][]'" :id="attr.name" :type="attr.inputType" :options="attr.options" :mandatory="attr.isMandatory" v-model="attr.value" @update="updateVariant" @remove="removeVariant"></attributes-variation>
						</div>
						<div class="form-group" :class="{'has-error': errors.get('prefix')}">
							<label class="col-sm-3 control-label">iSKU <span class="red">*</span></label>
							<div class="col-sm-5" :class="productPrefix.status == 'used' ? 'has-error' : ''">
								<div class="panel-group" id="variationList" role="tablist" aria-multiselectable="true">
								  <div class="panel panel-default" v-for="(variant, index) in recursiveVariation">
								    <div class="panel-heading" role="tab" :id="'heading'+variant">
								      <h4 class="panel-title">
								        <a role="button" data-toggle="collapse" data-parent="#variationList" :href="'#'+variant" aria-expanded="false" :aria-controls="variant">
								          @{{ variant.variation }}
								        </a>
								      </h4>
								    </div>
									    <div :id="variant" class="panel-collapse collapse in" role="tabpanel" :aria-labelledby="'heading'+variant">
								      <div class="panel-body">
												<div class="row">
													<div class="form-group">
														<label class="col-sm-3 control-label">SKU Prefix  <span class="pull-right red"> *</span></label>
														<div class="col-sm-8">
															<input v-for="(vrt, i) in variant.key" class="form-control" type="hidden" :name="'variation['+index+']['+i+']'" :value="vrt">
															<input class="form-control" type="text" :name="'variation['+index+'][SellerSku]'" :value="formValue.brand+' '+formValue.model+' '+formValue.submodel+' '+variant.variation | sanitize">
														</div>
													</div>
												</div>
								      </div>
								    </div>
								  </div>
								</div>
							</div>
						</div>
						@if ($model->exists)
						<input type="hidden" name="product_prefix" id="prefix" class="form-control" value="{{ $model->product_prefix }}" disabled="">
						@else
						<input type="hidden" name="product_prefix" id="prefix" class="form-control" v-model="productPrefix.prefix" @input=validateProductPrefix($event.target.value)>
						<span class="help-block" v-if="productPrefix.status === 'used'">Prefix Used by Other Product</span>
						@endif
					</div>
				</div>
				<div class="box-footer">
					<button type="button" class="btn btn-default btn-flat" @click="activateSection('productInformation')">Back</button>
					<button type="button" class="btn btn-warning btn-flat pull-right" @click="validateVariation">Next</button>
				</div>
			</div>
		</section>
		<section id="assignSuppliers" v-show="isSectionActive('assignSuppliers')">
			<div class="box box-defult">
				<div class="box-header with-border">
					<h3 class="box-title">Assign Suppliers</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-4 col-md-offset-1">
							<p>Available Supplier</p>
							<select id="available" class="form-control noselect2" multiple="" style="width:100%">
								<option v-for="supplier in suppliers.available" :value="supplier.id">@{{supplier.supplier_name}}</option>
							</select>
						</div>
						<div class="col-md-2" style="padding-top: 60px">
							<button class="btn btn-default btn-flat" type="button" onclick="move('available','assigned')"><i class="fa fa-arrow-right"></i></button>
							<button class="btn btn-default btn-flat pull-right" type="button" onclick="move('assigned','available')"><i class="fa fa-arrow-left"></i></button>
						</div>
						<div class="col-md-4">
							<p>Assigned Supplier</p>
							<select name="suppliers[]" id="assigned" class="form-control noselect2" multiple="" style="width:100%">
								<option v-for="supplier in suppliers.assigned" :value="supplier.id">@{{supplier.supplier_name}}</option>
							</select>
							@if ($errors->has("suppliers"))
							<span class="help-block">
								<strong>{{ $errors->first("suppliers") }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="button" class="btn btn-default btn-flat" @click="activateSection('productVariation')">Back</button>
					<button type="button" class="btn btn-warning btn-flat pull-right" data-toggle="modal" data-target="#previewProduct">Preview</button>
				</div>
			</div>
		</section>
		<div id="previewProduct" class="modal fade" tabindex="-1" role="dialog">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Preview Product Data</h4>
		      </div>
		      <div class="modal-body">
		        <div class="row">
							<div class="col-md-4">
								<strong>Product Category</strong>
							</div>
							<div class="col-md-8">
								<ol class="breadcrumb" v-if="activeItem.length">
								  <li v-for="(active, index) in activeItem">@{{ active.cat_name }}</li>
								</ol>
							</div>
		        </div>
		        <div class="row" v-for="attribute in attributes.general">
							<div class="col-md-4">
								<strong>@{{ attribute.label }}</strong>
							</div>
							<div class="col-md-8">
								@{{ formValue[attribute.name] }}
							</div>
		        </div>
		        <div class="row" v-for="attribute in attributes.variation">
							<div class="col-md-4">
								<strong>@{{ attribute.label }}</strong>
							</div>
							<div class="col-md-8">
								@{{ formValue[attribute.name] }}
							</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
		        <button type="submit" class="btn btn-success btn-flat pull-right">{{ $model->exists ? 'Update' : 'Save '}} Product</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>
</div>
