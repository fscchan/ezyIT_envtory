<div class="modal fade" id="qtyModal" tabindex="-1" role="dialog" aria-labelledby="qtyModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" method="POST" @submit.prevent="submitStock()">
        <div class="modal-header">
          <button type="button" class="close" @click="hideQtyModal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Set Quantity</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-sm-3">SKU Product</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" disabled="" v-model="product.id">
            </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-3">Batch Type</label>
              <div class="col-sm-4">
                <select class="form-control" v-model="stock.zone_type">
                  <option value="" disabled="">Select Batch Type</option>
                  @foreach($data['zones'] as $zone)
                    <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                  @endforeach
                </select>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-3">Warehouse</label>
                <div class="col-sm-4">
                <select class="form-control" v-model="stock.warehouse">
                  <option value="" disabled="">Select Warehouse</option>
                  @foreach($data['warehouses'] as $warehouses)
                    <option value="{{ $warehouses->id }}">{{ $warehouses->name }}</option>
                  @endforeach
                </select>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Location</label>
            <div class="col-sm-9">
              <v-select v-model="selectedCell" label="text" :debounce="250" :on-search="searchCell" :options="cells" placeholder="Search Cell"></v-select>
              <input type="hidden" v-model="stock.cell_id">
            </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-3">Quantity</label>
              <div class="col-sm-4">
                <input type="number" class="form-control" v-model="stock.qty" :disabled="!stock.cell_id" min="1">
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning btn-flat" type="submit" :disabled="!stock.qty">Add Stock</button>
        </div>
      </form>
    </div>
  </div>
</div>