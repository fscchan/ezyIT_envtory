@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
	  <div class="box-header with-border">
		<h3 class="box-title">Warehouse Zone - {{$warehouse->name}}</h3>
	  </div>
      @include('admin.warehouse.zones._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.structure.min.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
  .optionParent {
  	font-weight: bold;
  }
  .optionChild {
  	padding-left: 15px;
  }
.categoryContainer {
	width: 100%;
	background-color: #f4f4f4;
	border: 1px solid #ddd;
	height: 302px;
	overflow-x:auto;
	overflow-y:hidden;
	white-space:nowrap;
}
.categoryContainer [class*="col-lg"], 
.categoryContainer [class*="col-md"], 
.categoryContainer [class*="col-sm"] {
    float:none;
    display:inline-block;
}
.selectCategories {
	height: 300px;
	overflow: auto;
	background-color: #fff;
	padding-top: 15px;
	padding-bottom: 15px;
}
li {
	cursor: pointer;
}
.child {
	display: none
}
.nav-stacked>li.active>a,
.nav-stacked>li.active>a:hover {
	background-color: #f7f7f7;
	border-left-color: transparent;
}
.nav-stacked>li.selected>a {
	background-color: #fff;
	border-left-color: #d2d6de;	
}
.nav-stacked>li.selected>a:hover {
	background-color: #f7f7f7;
	border-left-color: #d2d6de;
}
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert2/es6-promise.auto.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
	$(function () {
		// $('input').iCheck({
		// 	checkboxClass: 'icheckbox_square-blue',
		// 	radioClass: 'iradio_square-blue',
		// 	increaseArea: '20%'
		// });

		var newZone, nz;

		newZone = $("#dialog-newZone").dialog({
			autoOpen: false,
			height: 400,
			width: 350,
			modal: true,
			show: { effect: "fade", duration: 333 },
			hide: { effect: "fade", duration: 333 },
			buttons: {
				Cancel: function() {
					newZone.dialog("close");
				}, 
				Submit: function() {
					if(newZone.find("input[name=_method]").val() == "POST"){
						var adr = "{!! route('zones.store') !!}";
						var dta = { "zone-name": newZone.find("input#newzone-name").val(), "zone-prefix": newZone.find("input#newzone-prefix").val(), "cycle-count": newZone.find("input#newzone-cyclecount").val() };
					} else if(newZone.find("input[name=_method]").val() == "PUT") {
						var adr = "{!! url('app/zones') !!}/" + newZone.find("input[name=id]").val();
						var dta =  { "_method": "PUT", "id": newZone.find("input[name=id]").val(), "zone-name": newZone.find("input#newzone-name").val(), "zone-prefix": newZone.find("input#newzone-prefix").val(), "cycle-count": newZone.find("input#newzone-cyclecount").val() };
					}
					$.ajax({
						type: "post",
						url: adr,
						dataType: "json",
						data: dta,
						success: function(data) {
							if(data.st) {
								swal("Nice!", "Data saved.", "success");
							} else {
								swal("Oops...", "Failed to save data.", "error");
							}
						}
					});
				}
			},
			close: function() {
				nz[ 0 ].reset();
			}
		});

		nz = newZone.find("form").on("submit", function(event) {
			event.preventDefault();
		});
	
		$("#newzone-name").on("change", function() {
			if($(this).val() !== "") {
				$.ajax({
					type: "post",
					url: "{!! route("zones.genPrefix") !!}",
					data: { q: $(this).val() },
					dataType: "json",
					success: function(data) {
						$("#newzone-prefix").val(data.data);
					},
					error: function(data){
						var errors = data.responseJSON;
						swal.showInputError(errors.name);
					}
				});
			}
		});
		
		$("#newzone-prefix").on("input", function() {
			var dis = $(this);
			$.ajax({
				type: "post",
				url: "{!! route('zones.checkPrefix') !!}",
				dataType: "json",
				data: { q: dis.val() },
				success: function(data) {
					if(!data.st) {
						dis.parent().append($('<span></span>').attr("id", "product-prefix-warning").addClass("help-block").append($("<strong></strong>").text("Warning: This product prefix has already existed")));
					} else {
						dis.siblings(":not(input)").remove();
					}
				}
			});
		});

		$("#btn-addZone").on("click", function(evt) {
			evt.preventDefault();
			newZone.find("input[name=_method]").val("POST");
			newZone.find("input[name=id]").val("");
			newZone.dialog({title: "Create new zone"}).dialog("open");
		});
	});

	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});
	
    $(".select2").select2();
	
	// $("#zone-type").select2({
	// 	ajax: {
	// 		type: "post",
	// 		url: "{!! route("zones.getZones") !!}",
	// 		dataType: "json",
	// 		delay: 250,
	// 		data: function(params) {
	// 			return { q: params.term };
	// 		},
	// 		processResults: function (data, params) {
	// 			var ret = [];
	// 			$.each(data, function(key, val) {
	// 				ret[key] = { id: val.id, text: val.name };
	// 			})
	// 			return { results: ret };
	// 		}
	// 	}, 
	// 	cache: true
	// });
	
		$("#zone-name").on("change", function() {
			if($(this).val() !== "") {
				$.ajax({
					type: "post",
					url: "{!! route("warehouse.mapping.genPrefix2", [$warehouse->id]) !!}",
					data: { t: $("#zone-type > option:selected").val(), q: $(this).val() },
					dataType: "json",
					success: function(data) {
						$("#zone-prefix").val(data.data);
					},
					error: function(data){
						var errors = data.responseJSON;
						swal.showInputError(errors.name);
					}
				});
			}
		});

	$("#zone-prefix").on("change", function() {
		$.ajax({
			type: "post",
			url: "{!! route("warehouse.mapping.checkPrefix", $warehouse->id) !!}",
			data: { "q": $(this).val() },
			dataType: "json",
			success: function(data) {
				if(!data.st) {
					$("#zone-prefix").parent().children(":not(input)").remove();
					$("#zone-prefix").parent().append($('<span></span>').attr("id", "product-prefix-warning").addClass("help-block").append($("<strong></strong>").text("Warning: This product prefix has already existed")));
				} else {
					$("#zone-prefix").parent().children(":not(input)").remove();
				}
			},
			error: function(data) {
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});
	
	$("#zone-type").on("change", function() {
		if($(this).val() !== "") {
			$.ajax({
				type: "post",
				url: "{!! route("warehouse.mapping.genPrefix", $warehouse->id) !!}",
				data: { t: $("#zone-type > option:selected").val() },
				dataType: "json",
				success: function(data) {
					$("#zone-prefix").val(data.data);
				},
				error: function(data){
					var errors = data.responseJSON;
					swal.showInputError(errors.name);
				}
			});
		}
	});

	// $("#zone-categories").select2({
	// 	ajax: {
	// 		type: "post",
	// 		url: "{!! route("productCategories.getCategory") !!}",
	// 		dataType: "json",
	// 		delay: 250,
	// 		data: function(params) {
	// 			return { q: params.term };
	// 		},
	// 		processResults: function (data, params) {
	// 			return {
	// 				results: data
	// 			};
	// 		}
	// 	}, 
	// 	cache: true
	// });
	
	/* $("#btn-addZone").on("click", function() {
		var dt = {};
		swal({
			title: "Create new zone",
			text: "Enter new zone name",
			inputPlaceholder: "Zone name...",
			input: "text",
			showCancelButton: true,
			preConfirm: function(val) {
				return new Promise(function(resolve, reject) {
					if(val == "") {
						reject("This field is required");
					}
					resolve();
				});
			}
		}).then(function (val) {
			dt.a = val;
			swal({
				title: "Create new zone",
				text: "Enter new zone prefix",
				inputPlaceholder: "Zone prefix",
				input: "text",
				inputValue: val.substr(0, 3).toUpperCase(),
				showCancelButton: true,
				preConfirm: function(val) {
					return new Promise(function(resolve, reject) {
						if(val == "") {
							reject("This field is required");
						}
						$.ajax({
							type: "post",
							url: "{!! route('zones.checkPrefix') !!}",
							dataType: "json",
							data: { q: val },
							success: function(data) {
								if(!data.st) {
									reject("A zone prefix with this name has already exists");
								} else {
									resolve();
								}
							}
						});
					});
				}
			}).then(function (val) {
				dt.b = val;
				swal({
					title: "Create new zone",
					text: "Enter new zone cycle count",
					inputPlaceholder: "Cycle count...",
					input: "number",
					inputValue: 0,
					showCancelButton: true
				}).then(function (val) {
					$.ajax({
						type: "post",
						url: "{!! route('zones.store') !!}",
						dataType: "json",
						data: { "zone-name": dt.a, "zone-prefix": dt.b, "cycle-count": val },
						success: function(data) {
							if(data.st) {
								swal("Nice!", "Data saved.", "success");
							} else {
								swal("Oops...", "Failed to save data.", "error");
							}
						}
					});
				});
			});
		});
	}); */
	
	$("#btn-addCat").on("click", function() {
		$.ajax({
			type: "post",
			url: "{!! route("productCategories.getCategories") !!}",
			data: { "p": 0, "q": ""},
			dataType: "json",
			success: function(data) {
				var d = {};
				var opts = { "0": "_MAIN" };
				data.forEach(function(val, key) {
					opts[val.id] = val.cat_name;
				});

				swal({
					title: "Select parent category",
					input: "select",
					inputOptions: opts,
					showCancelButton: true
				}).then(function (value) {
					d.parent = value;
					swal({
						title: "Type new category name",
						input: "text",
						inputPlaceholder: "New category name",
						showCancelButton: true,
						inputValidator: function (value) {
							return new Promise(function (resolve, reject) {
								if (value != "") {
									resolve();
								} else {
									reject("Please type the new category name!")
								}
							});
						}
					}).then(function (value) {
						d.category = value;

						$.ajax({
							type: "post",
							url: "{!! route("productCategories.storeCategory") !!}",
							data: d,
							dataType: "json",
							success: function(data) {
								swal("Nice!", data.message, "success");
							},
							error: function(data){
								var errors = data.responseJSON;
								swal.showInputError(errors.name);
							}
						});
					});
				});
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});
</script>

<div id="dialog-newZone">
	<form id="form-newZone">
		<input type="hidden" name="_method" value="">
		<input type="hidden" name="id" value="">
		<fieldset style="display: table;">
			<div class="row" style="display: table-row;">
				<label for="zone-name" style="display: table-cell; width: 96px;">Zone name<span class="red"> *</span></label>
				<div style="display: table-cell;"><input type="text" id="newzone-name" class="text ui-widget-content ui-corner-all form-control" name="zone-name" placeholder="Zone name" value="" style="margin-bottom: 8px;" maxlength="64" required="required"></div>
			</div>
			<div class="row" style="display: table-row;">
				<label for="zone-prefix" style="display: table-cell;">Prefix<span class="red"> *</span></label>
				<div style="display: table-cell;"><input type="text" id="newzone-prefix" class="text ui-widget-content ui-corner-all form-control" name="zone-prefix" placeholder="Prefix" value="" style="margin-bottom: 8px;" maxlength="3" required="required"></div>
			</div>
			<div class="row" style="display: table-row;">
				<label type="text" for="zone-cyclecount" style="display: table-cell;">Cycle count</label>
				<div style="display: table-cell;"><input type="number" name="zone-cyclecount" id="newzone-cyclecount" value="1" class="text ui-widget-content ui-corner-all form-control"></div>
			</div>
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>
@endpush