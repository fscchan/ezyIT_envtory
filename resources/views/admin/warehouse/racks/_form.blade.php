<form role="form" method="POST" action="{{ $model->exists ? url("/app/warehouse/" . $warehouse->id . "/mapping/" . $zone->id . "/racks/" . $model->id) : url("/app/warehouse/" . $warehouse->id . "/mapping/" . $zone->id . "/racks") }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="row form-group">
			<div class="col-md-9">
				<label for="rack-id" class="col-md-2 control-label text-left">Rack ID</label>
				<div class="col-md-10">
					<input name="rack-id" class="form-control" id="rack-id" placeholder="Rack ID" value="{{ $model->exists ? $model->prefix : $rackid }}" maxlength="45"{{ $model->exists ? ' readonly="readonly"' : ""}}>
					@if ($errors->has("rack-id"))
					<span class="help-block">
						<strong>{{ $errors->first("rack-id") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-9">
				<label for="rack-name" class="col-md-2 control-label text-left">Description</label>
				<div class="col-md-10">
					<input name="rack-name" class="form-control" id="rack-name" placeholder="Rack description" value="{{ $model->exists ? $model->name : "" }}" maxlength="45">
					@if ($errors->has("rack-name"))
					<span class="help-block">
						<strong>{{ $errors->first("rack-name") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<label for="rack-name" class="col-md-2 control-label text-left">Rack Categories</label>
				<div class="col-md-10" id="categories">
					<div class="categoryContainer">
						<div class="col-md-6 selectCategories">
							<ul class="nav nav-pills nav-stacked">
							@foreach ($zone->categories()->get() as $category)
								<li @click="getChild({{$category->id}});" :class="{ active: isActive({{$category->id}}) }">
									<a>{{ $category->cat_name }}
									<span class="pull-right"><i class="fa fa-angle-right"></i></span>
									</a>
								</li>
							@endforeach
							</ul>
						</div>
						<input type="hidden" v-for="category in categoriesId" :value="category" name="categories[]">
						<div class="col-md-6 selectCategories" v-show="categories.length">
							<ul class="nav nav-pills nav-stacked">
								<li v-for="category in categories">
									<a>
									<label>
									<input type="checkbox" name="cat[]" :value="category.id" v-model="categoriesId">
									@{{ category.cat_name }}
									</label>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="row form-group">
			<div class="col-md-9">
				<label for="rack-dimensions" class="col-md-2 control-label text-left">Dimension</label>
				<div class="col-md-10">
					<div class="col-md-4" style="padding-left: 0px;">
						<label for="rack-length" class="col-md-1 control-label text-left" style="padding: 0px; margin-right: 30px;">L</label>
						<div class="col-md-9" style="padding: 0px;">
							<input type="number" name="rack-length" class="form-control" id="rack-length" placeholder="Length in meters" value="{{ $model->exists ? $model->lenght : "1" }}" title="in meters" min="1">
							@if ($errors->has("rack-length"))
							<span class="help-block">
								<strong>{{ $errors->first("rack-length") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="col-md-4">
						<label for="rack-width" class="col-md-1 control-label text-left" style="padding: 0px; margin-right: 30px;">W</label>
						<div class="col-md-9" style="padding: 0px;">
							<input type="number" name="rack-width" class="form-control" id="rack-width" placeholder="Width in meters" value="{{ $model->exists ? $model->widht : "1" }}" title="in meters" min="1">
							@if ($errors->has("rack-width"))
							<span class="help-block">
								<strong>{{ $errors->first("rack-width") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="col-md-4" style="padding-right: 0px;">
						<label for="rack-height" class="col-md-1 control-label text-left" style="padding: 0px; margin-right: 30px;">H</label>
						<div class="col-md-9" style="padding: 0px;">
							<input type="number" name="rack-height" class="form-control" id="rack-height" placeholder="Height in meters" value="{{ $model->exists ? $model->height : "1" }}" title="in meters" min="1">
							@if ($errors->has("rack-height"))
							<span class="help-block">
								<strong>{{ $errors->first("rack-height") }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group hidden">
			<div class="col-md-9">
				<label for="rack-cellprefix" class="col-md-2 control-label text-left">Cell prefix<span class="red"> *</span></label>
				<div class="col-md-10">
					<input name="rack-cellprefix" class="form-control" id="rack-cellprefix" placeholder="Cell prefix" value="{{ $model->exists ? $model->cell_prefix : "" }}" maxlength="45"{{ $model->exists ? " disabled" : ""}}>
					@if ($errors->has("rack-cellprefix"))
					<span class="help-block">
						<strong>{{ $errors->first("rack-cellprefix") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-9">
				<div class="col-md-2"></div>
				<div class="col-md-3">
					<label for="rack-cellrows" class="col-md-8 control-label text-left" style="padding: 0px;">Cell rows</label>
					<div class="col-md-4" style="padding: 0px;">
						<input type="number" name="rack-cellrows" class="form-control" id="rack-cellrows" placeholder="Cell rows" value="{{ $model->exists ? $model->cell_rows : "1" }}" min="1"{{ $model->exists ? " disabled" : ""}}>
						@if ($errors->has("rack-cellrows"))
						<span class="help-block">
							<strong>{{ $errors->first("rack-cellrows") }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<label for="rack-cellcolumns" class="col-md-8 control-label text-left" style="padding: 0px;">Cell columns</label>
					<div class="col-md-4" style="padding: 0px;">
						<input type="number" name="rack-cellcolumns" class="form-control" id="rack-cellcolumns" placeholder="Cell columns" value="{{ $model->exists ? $model->cell_columns : "1" }}" min="1"{{ $model->exists ? " disabled" : ""}}>
						@if ($errors->has("rack-cellcolumns"))
						<span class="help-block">
							<strong>{{ $errors->first("rack-cellcolumns") }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="col-md-4"><button type="button" id="btn-generateLocation" class="btn btn-default btn-flat"{{ $model->exists ? " disabled" : ""}}>Generate locations</button></div>
			</div>
			<div style="clear: both; padding-bottom: 9px;"></div>
		</div>
		<div id="locs" class="row{{ $model->exists && count($cells) > 0 ? "" : " hidden"}}">
			<hr style="width: 96%" />
			<table id="locations" class="table" style="width: 75%; margin: 0px auto;">
				<thead>
					<tr>
						<th style="width: 64px;">ID</th>
						<th>Cell ID</th>
						<th style="width: 144px;">Status</th>
						<th style="width: 32px; text-align: right;"><input type="checkbox" id="checkCellHeader"></th>
					</tr>
				</thead>
				<tbody id="generated-locations">
					@if($model->exists)
						@for($i = 0; $i < count($cells); $i++)
						<tr><td>{{ $i + 1 }}</td><td><input name="cells[]" value="{{ $cells[$i]->prefix }}" style="border: none; width: 100%; background-color: transparent;" disabled="disabled"></td><td style="text-align: center;"><select class="form-control" name="cellStatus[]"{{ $model->exists ? " disabled" : "" }}><option value="0"{{ $cells[$i]->status == 0 ? " selected" : "" }}>Empty</option><option value="1"{{ $cells[$i]->status == 1 ? " selected" : "" }}>Reserved</option></select></td><td style="text-align: right;"><input type="checkbox" id="cell-{{ $i + 1 }}" class="checkCells" name="checkCells[]" /></td></tr>
						@endfor
					@endif
				</tbody>
				<tfoot>
					<tr>
						<th colspan="4" style="text-align: right;"><button type="button" id="btn-printBarcodes" class="btn btn-default btn-flat"{{ $model->exists ? "" : " disabled" }}><i class="fa fa-barcode"></i> Print barcodes</button></th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/warehouse/" . $warehouse->id . "/mapping") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
<form id="getPDF" action="{{ url("/app/warehouse/" . $warehouse->id . "/mapping/" . $zone->id . "/racks/getBarcodes") }}" method="post" role="form">{{ csrf_field() }}</form>