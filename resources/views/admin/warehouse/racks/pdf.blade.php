@extends('layouts.report.main')
@section('content')
<h3>Cell list</h3>
<table class="no-border">
	<tr><td colspan="2"><h1>{{ $warehouse->name }}</h1></td></tr>
	<tr><td style="width: 96px; text-align: right;"><strong>Address:</strong></td><td>{{ $warehouse->address }}</td></tr>
	<tr><td style="width: 96px; text-align: right;"><strong>Zone:</strong></td><td>{{ $zone->name }} [{{ $zone->prefix }}]</td></tr>
</table>
<table>
    <thead>
		<tr>
			<th style="width: 128px">#</th>
			<th>Cell name</th>
		</tr>
    </thead>
    <tbody>
    @foreach($cells as $key => $cell)
        <tr>
          <td align="center">{{ ++$key }}</td>
          <td align="center">
            <?php $barcode = Barcode::getBarcodePNG($cell, "C128", 3, 100); ?>
            <img src="data:image/png;base64,{{ $barcode }}" alt="{{ $cell }}" width="200px" /><br/>{{ $cell }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@stop