@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        <div class="btn-group hidden">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <div class="pull-right">
          @can('create-warehouseTrays')
          <a id="btn-newTray" class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New tray</a>
          @endcan
        </div>
		<div class="clear" style="clear: both;"></div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="zones-table" cellspacing>
          <thead>
              <tr>
                  <th style="width: 16px">#</th>
                  <th style="width: 80px">Size</th>
                  <th>Tray code</th>
                  <th style="width: 96px">Max qty</th>
                  <th style="width: 96px">Status</th>
                  <th style="width: 128px;"></th>
                  <th style="width: 24px; text-align: center;"><button id="btn-printBarcodes" class="btn btn-default btn-flat" title="Print barcodes..."><i class="fa fa-barcode"></i></button></th>
              </tr>
          </thead>
        </table>
		<form id="getPDF" action="{{ url("/app/warehouse/" . $wid . "/trays/getBarcodes") }}" method="post" role="form">{{ csrf_field() }}</form>
      </div>
    </div>
  </div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<div id="tray-modal" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="tray-form" class="form-horizontal" method="POST" action="{{ url("/app/warehouse/" . $wid . "/trays") }}">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title" id="trayform-title" style="display: inline; font-weight: bold; font-size: 14pt;">Add tray</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group has-feedback">
						<div class="col-md-8">
							<label for="tray-size" class="col-md-4 control-label text-left">Size</label>
							<div class="col-md-8">
								<select id="tray-size" class="form-control" name="tray-size">
									<option value="0">Small</option>
									<option value="1">Medium</option>
									<option value="2">Large</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group has-feedback">
						<div class="col-md-8">
							<label for="tray-code" class="col-md-4 control-label text-left">Tray code<span class="red"> *</span></label>
							<div class="col-md-8">
								<input name="tray-code" class="form-control" id="tray-code" placeholder="Tray code" value="" maxlength="10" required="required">
							</div>
						</div>
					</div>
					<div class="form-group has-feedback">
						<div class="col-md-8">
							<label for="max-qty" class="col-md-4 control-label text-left">Max qty</label>
							<div class="col-md-8">
								<input type="number" name="max-qty" class="form-control" id="max-qty" placeholder="Maximum quantity" value="0">
							</div>
						</div>
					</div>
					<div class="form-group has-feedback hidden">
						<div class="col-md-8">
							<label for="tray-status" class="col-md-4 control-label text-left">Tray is in use</label>
							<div class="col-md-8">
								<input type="checkbox" name="tray-status" class="form-control" id="tray-status" value="1">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		$('#zones-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('warehouse.trays.index', $wid) !!}',
			columns: [
				{ data: function(row, type, val, meta){ return meta.row + 1; }, name: 'id', orderable: false, searchable: false  },
				{ data: 'size', name: 'size' },
				{ data: 'prefix', name: 'prefix' },
				{ data: 'max_qty', name: 'max_qty' },
				{ data: 'status', name: 'status' },
				{ data: 'action', name: 'action', orderable: false, searchable: false },
				{ data: 'checkboxes', name: 'checkboxes', orderable: false, searchable: false }
			]
		});
	});
	
	function deleteTray(i) {
		swal({
			title: "Delete tray?",
			text: "Deleted tray can't be restored",
			type: "warning",
			showCancelButton: !0,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, Delete!",
			cancelButtonText: "Cancel",
			closeOnConfirm: !0
		}, function() {
			$("form#delete-" + i).submit();
		});
	}

	$("#btn-newTray").on("click", function(e){
		e.preventDefault();
		$("#tray-modal").modal("show");
		$.ajax({
			type: "post",
			url: "{!! url("app/warehouse/" . $wid . "/trays/genPrefix") !!}",
			data: { q: $("#tray-size > option:selected").text() },
			dataType: "json",
			success: function(data) {
				$("#tray-code").val(data.data);
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});

	$("#tray-size").on("change", function(){
		$.ajax({
			type: "post",
			url: "{!! url("app/warehouse/" . $wid . "/trays/genPrefix") !!}",
			data: { q: $("#tray-size > option:selected").text() },
			dataType: "json",
			success: function(data) {
				$("#tray-code").val(data.data);
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});

	$("#btn-printBarcodes").on("click", function() {
		if($(".print-barcodes:checked").length > 0) {
			$("form#getPDF").children(":not(input[name=_token])").remove();
			$(".print-barcodes:checked").each(function(i) {
				$("form#getPDF").append('<input type="hidden" name="trays[]" value="' + $(this).val() + '" />');
			});
			$("form#getPDF").submit();
		}
	});
</script>
@endpush