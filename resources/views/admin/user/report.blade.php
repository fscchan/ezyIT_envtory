@extends('layouts.report.main')
@section('content')
<h3>{{$title or 'Title Here'}}</h3>
<table>
	<thead>
		<tr>
			<th>Username</th>
			<th>Firstname</th>
			<th>Lastname</th>
		</tr>
	</thead>
	<tbody>
	@foreach ($users as $user)
		<tr>
			<td>{{$user->username}}</td>
			<td>{{$user->firstname}}</td>
			<td>{{$user->lastname}}</td>
		</tr>
	@endforeach
	</tbody>
</table>
@stop