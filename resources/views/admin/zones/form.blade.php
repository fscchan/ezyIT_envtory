@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      @include('admin.zones._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});

	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$("#zone-prefix").on("change", function() {
		$.ajax({
			type: "post",
			url: "{!! route("zones.checkPrefix") !!}",
			data: { "q": $(this).val() },
			dataType: "json",
			success: function(data) {
				if(!data.st) {
					$("#zone-prefix").parent().children(":not(input)").remove();
					$("#zone-prefix").parent().append($('<span></span>').attr("id", "product-prefix-warning").addClass("help-block").append($("<strong></strong>").text("Warning: This product prefix has already existed")));
				} else {
					$("#zone-prefix").parent().children(":not(input)").remove();
				}
			},
			error: function(data) {
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});
	
	$("#zone-name").on("change", function() {
		if($(this).val() !== "") {
			$.ajax({
				type: "post",
				url: "{!! route("zones.genPrefix") !!}",
				data: { q: $(this).val() },
				dataType: "json",
				success: function(data) {
					$("#zone-prefix").val(data.data);
				},
				error: function(data){
					var errors = data.responseJSON;
					swal.showInputError(errors.name);
				}
			});
		}
	});
</script>
@endpush