@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    @include('layouts.lte.status')
    <div class="box box-primary">
      @include('admin.po._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datepicker/datepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/selectize/css/selectize.bootstrap3.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
  .productSection {
  	{{ !$model->exists ? 'display: none' : ''}}
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/selectize/js/standalone/selectize.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquerymaskplugin/jquery.mask.min.js') }}"></script>
<script src="{{ elixir('js/po.js') }}"></script>
<script>
	$(function(){
		$("#use-address").on("change", function(){
			if($(this).is(":checked")) {
				$("#ship_address").prop("readonly", true).val($("#supplier_address").val());
			} else {
				$("#ship_address").prop("readonly", false);
			}
		});
		$(".input-money-format").mask("000,000,000,000,000.0000", {reverse: true});
	});
	
	$(".btn-delItem").on("click", function(){
		var dta = $(this);
		swal({
			title: "Delete product",
			text: "Delete item from order?",
			type: "warning",
			showCancelButton: true,
			showConfirmButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete!",
			cancelButtonText: "No, cancel!",
			closeOnConfirm: false,
			closeOnCancel: true
		}, function(response){
			if(response){
				$.ajax({
					type: "post",
					url: "{{ url("app/po/delItem") }}",
					dataType: "json",
					data: { i: dta.data("item") },
					success: function(data){
						swal({
							title: "Success!",
							text: "Item deleted",
							type: "success"
						});
						$("#productTable > tbody > tr:nth-child(" + dta.data("row") + ")").remove();
					},
					error: function(data){}
				});
			}
		});
	});
</script>
@endpush