@extends('layouts.lte.main')
@section('content')
<section class="invoice">
@include('admin.po._print')
	<div class="row no-print">
		<hr>
		<div class="col-xs-12">
		  <a href="{{ Request::url().'/print' }}" target="_blank" class="btn btn-default btn-flat"><i class="fa fa-print"></i> Print</a>
		  <a href="{{ Request::url().'/pdf' }}" class="btn btn-primary pull-right btn-flat" style="margin-right: 5px;" target="_blank">
		    <i class="fa fa-download"></i> Download PDF
		  </a>
		</div>
	</div>
</section>
@stop
@push('styles')
<style>
	tfoot td {
		/*background-color: #f4f4f4*/
	}
</style>
@endpush