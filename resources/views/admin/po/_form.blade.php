<form role="form" class="form-horizontal" method="POST" id="po-form" action="{{ $model->exists ? url('/app/po/'.$model->id) : url('/app/po') }}">
  {{ csrf_field() }}
  @if($model->exists){{ method_field('PUT') }}@endif
  <div class="box-body" id="form">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('m_supplier_id') ? ' has-error' : '' }}">
          <label for="m_supplier_id" class="col-sm-4 control-label">Supplier <span class="red">*</span></label>
          <div class="col-sm-8">
            @if ($model->exists)
              <input type="text" class="form-control" disabled="" value="{{ $model->supplier->supplier_name}}">
            @else
            <select class="form-control" name="m_supplier_id" id="supplier" autofocus="" onchange="generatePrefix(this)" required="">
              <option value="">Search Supplier</option>
              @foreach ($suppliers as $supplier)
                <option value="{{ $supplier->id}}">{{ $supplier->supplier_name }}</option>
              @endforeach
            </select>
            @endif
            @if ($errors->has('m_supplier_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('m_supplier_id') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="form-group has-feedback{{ $errors->has('prefix')  ? ' has-error' : '' }}">
          <label for="prefix" class="col-sm-4 control-label">Purchase Order Prefix <span class="red">*</span></label>
          <div class="col-sm-8">
            <input type="text" name="prefix" class="form-control" id="prefix" value="{{old('prefix') ? old('prefix') : $model->prefix}}" readonly="">
            @if ($errors->has('prefix'))
                <span class="help-block">
                    <strong>{{ $errors->first('prefix') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="form-group">
          <label for="supplier_address" class="col-sm-4 control-label">Supplier Address</label>
          <div class="col-sm-8">
            <textarea id="supplier_address" class="form-control" rows="3" readonly="">{{old('ship_address') ? old('ship_address') : $model->ship_address}}</textarea>
          </div>
        </div>
        <div class="form-group">
          <label for="supplier_email" class="col-sm-4 control-label">Supplier Email</label>
          <div class="col-sm-8">
            <select name="email" id="supplier_email" class="form-control">
            @if ($model->exists)
              @foreach ($model->supplier->contacts as $supplier)
                @if ($supplier->email)
                  <option value="{{$supplier->email}}" {{ $model->email == $supplier->email ? 'selected' : ''}}>{{ $supplier->email }}</option>
                @endif
              @endforeach
            @endif
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group has-feedback{{ $errors->has('po_date')  ? ' has-error' : '' }}">
          <label for="po_date" class="col-sm-4 control-label">Date <span class="red">*</span></label>
          <div class="col-sm-8">
            <div class="input-group date">
              <input type="text" name="po_date" class="form-control datepicker" id="po_date" value="{{old('po_date') ? old('po_date') : $model->po_date}}" required="">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              @if ($errors->has('po_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('po_date') }}</strong>
                  </span>
              @endif
            </div>
          </div>
        </div>
        <div class="form-group has-feedback{{ $errors->has('exp_date')  ? ' has-error' : '' }}">
          <label for="exp_date" class="col-sm-4 control-label">Exp Delivery Date</label>
          <div class="col-sm-8">
            <div class="input-group date">
              <input type="text" name="exp_date" class="form-control datepicker" id="exp_date" value="{{old('exp_date') ? old('exp_date') : $model->exp_date}}">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              @if ($errors->has('exp_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('exp_date') }}</strong>
                  </span>
              @endif
            </div>
          </div>
        </div>
        <div class="form-group has-feedback{{ $errors->has('ship_address') ? ' has-error' : '' }}">
          <label for="ship_address" class="col-sm-4 control-label">Shipping Address <span class="red">*</span></label>
          <div class="col-sm-8">
            <textarea name="ship_address" id="ship_address" class="form-control" rows="3" required="">{{old('ship_address') ? old('ship_address') : $model->ship_address}}</textarea>
			<div class="text-right"><label for="use-address">Use supplier address</label> <input type="checkbox" id="use-address"></div>
            @if ($errors->has('ship_address'))
                <span class="help-block">
                    <strong>{{ $errors->first('ship_address') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="form-group has-feedback{{ $errors->has('remarks') ? ' has-error' : '' }}">
          <label for="remarks" class="col-sm-4 control-label">Terms & Conditions</label>
          <div class="col-sm-8">
            <textarea name="remarks" id="remarks" class="form-control" rows="3">{{old('remarks') ? old('remarks') : $model->remarks}}</textarea>
            @if ($errors->has('remarks'))
                <span class="help-block">
                    <strong>{{ $errors->first('remarks') }}</strong>
                </span>
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="productSection">
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group has-feedback">
            <label for="searchProduct" class="col-sm-2 control-label">Search Product Code / Name <span class="red">*</span></label>
            <div class="col-sm-3">
              <select name="searchProduct" class="form-control" id="searchProduct">
                <option value="">Search Product</option>
              </select>
            </div>
            <div class="com-sm-2">
              <button type="button" class="btn btn-primary btn-flat" id="addProduct"><i class="fa fa-plus"></i> Add to List</button>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <table class="table table-striped table-hover" id="productTable">
            <thead>
              <tr>
                <th>Product PO Id</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Product Type</th>
                <th width="100">Qty</th>
                <th width="150">Price</th>
                <th width="150">Amount</th>
                <th style="width: 48px;"></th>
              </tr>
            </thead>
            <tbody>
            @if ($model->exists)
              <?php $row = 1; ?>
              @foreach ($modelDetail as $md)
                <tr class="rowProducts" id="row-{{$row}}">
                  <td>{{ $md->product_po_id }}</td>
                  <td>{{ $md->product->product_code }}<input type="hidden" name="products[{{$row}}][id]" value="{{ $md->id }}"><input type="hidden" name="products[{{$row}}][m_products_id]" id="products-{{$md->m_products_id}}" value="{{ $md->m_products_id }}"></td>
                  <td>{{ $md->product->attribute('name') }}</td>
                  <td>{{ $md->product->category->cat_name }}</td>
                  <td><input type="number" class="form-control" name="products[{{$row}}][qty]" id="qty{{$row}}" min="1" onchange="calculateAmount({{$row}})" required="" value="{{$md->qty}}"></td>
                  <td><input type="number" class="form-control" name="products[{{$row}}][price]" id="price{{$row}}" onchange="calculateAmount({{$row}})" required="" value="{{$md->price}}"></td>
                  <td align="right"><input type="hidden" name="products[{{$row}}][amount]" id="amount{{$row}}" class="amount" value="{{$md->qty*$md->price}}"><span id="amountText{{$row}}">{{$md->qty*$md->price}}</span></td>
                  <td>
					  @if($model->status == 1)
						 <td><button type="button" class="btn-delItem btn btn-xs btn-danger btn-flat" data-row="{{ $row }}" data-item="{{ $md->id }}" {{ $model->exists ? "" : 'disabled="disabled"' }}><i class="fa fa-times"></i></button>
					  @endif
				  </td>
                </tr>
                <?php $row++ ; ?>
              @endforeach
            @endif
            </tbody>
            <tfoot>
              <tr>
                <td colspan="6" align="right">Sub. Total</td>
                <td align="right"><input type="hidden" name="total_det_mount" id="total_det_mount"><span id="subtotal"></span></td>
                <td></td>
              </tr>
              <tr>
                <td colspan="6" align="right">Shipping Cost</td>
                <td align="right">
                  <input type="text" min="0" name="ship_cost" class="input-money-format form-control" id="ship_cost" value="{{old('ship_cost') ? old('ship_cost') : $model->ship_cost}}" step="any" onchange="calculatePo();">
                </td>
                <td></td>
              </tr>
              <tr>
                <td colspan="6" align="right">Misc Cost</td>
                <td align="right">
                  <input type="text" name="misc_cost" class="input-money-format form-control" id="misc_cost" value="{{old('misc_cost') ? old('misc_cost') : $model->misc_cost}}" min="0" step="any" onchange="calculatePo();">
                </td>
                <td></td>
              </tr>
              <tr>
                <td colspan="6" align="right">Grand Total</td>
                <td align="right"><input type="hidden" name="total_amount" id="total_amount"><span id="grandtotal"></span></td>
                <td></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="box-footer productSection">
    <div class="form-group">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-4">
        <a href="{{url('app/po')}}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? 'Update Purchase Order' : 'Add Purchase Order'}}</button>
      </div>
    </div>
  </div>
  @include('admin.po._modalEmail')
</form>
