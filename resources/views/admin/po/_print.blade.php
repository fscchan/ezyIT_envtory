	<div class="row">
		<div class="col-xs-3">
			<?php $barcode = Barcode::getBarcodePNG($po->prefix, "C128", 3, 100); ?>
			<div align="center">
			<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $po->prefix }}" width="100%" />
			{{ $po->prefix }}
			</div>
		</div>
	    <div class="col-xs-7">
	        <div align="right">
	          <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
	          <p>{{ $company->profile_billing_address }}</p>
	        </div>
	    </div>
	    <div class="col-xs-2">
	    	<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
	    </div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<h2>PURCHASE ORDER</h2>
		</div>
	</div>
	<div class="row invoice-info">
		<div class="col-sm-4 invoice-col">
		<address>
		    <strong>{{ $po->supplier->supplier_name }}</strong><br>
		    {!! $po->supplier->supplier_address.'<br>' !!}
		    Email: {{ $po->email }}
		  </address>
		</div>
		<div class="col-sm-4 invoice-col">
			<strong class="pull-right">Shipping To</strong>
		</div>
		<div class="col-sm-4 invoice-col">
		  <address>
		    <strong>{{ $company->profile_name }}</strong><br>
	        {{ $company->profile_billing_address }}<br>
		    Email: {{ Auth::user()->email }}
		  </address>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 table-responsive">
		  <table class="table table-bordered">
		    <thead>
		    <tr>
		      <th></th>
		      <th>Product Code</th>
		      <th>Product Name</th>
		      <th>Qty</th>
		      <th>Price</th>
		      <th>Amount</th>
		    </tr>
		    </thead>
		    <tbody>
		    @foreach ($po->detail as $key => $detail)
		        <tr>
		          <td>{{++$key}}</td>
		          <td>{{$detail->product->product_code}}</td>
		          <td>{{$detail->product->product_name}}</td>
		          <td>{{$detail->qty}}</td>
		          <td align="right">{{$currency->code . " " . number_format($detail->price, 2)}}</td>
		          <td align="right">{{$detail->qty * $detail->price}}</td>
		        </tr>
		    @endforeach
		    </tbody>
		    <tfoot>
		    	<tr>
		    		<td colspan="5" align="right"><strong>Sub Total</strong></td>
		    		<td align="right">{{ $currency->code . " " . number_format($po->total_det_mount, 2) }}</td>
		    	</tr>
		    	<tr>
		    		<td colspan="5" align="right"><strong>Shipping Cost</strong></td>
		    		<td align="right">{{ $currency->code . " " . number_format($po->ship_cost, 2) }}</td>
		    	</tr>
		    	<tr>
		    		<td colspan="5" align="right"><strong>Misc Cost</strong></td>
		    		<td align="right">{{ $currency->code . " " . number_format($po->misc_cost, 2) }}</td>
		    	</tr>
		    	<tr>
		    		<td colspan="5" align="right"><strong>Total Amount</strong></td>
		    		<td align="right">{{ $currency->code . " " . number_format($po->total_amount, 2) }}</td>
		    	</tr>
		    </tfoot>
		  </table>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6">
		  <p class="lead">Term and Condition</p>
		  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">{{ $po->remarks }}</p>
		</div>
	</div>
