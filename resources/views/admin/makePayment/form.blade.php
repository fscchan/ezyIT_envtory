@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      @include('admin.makePayment._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
</style>
@endpush

@push('scripts')
<div id="modal-newPaymentMethod" class="modal fade">
	<div class="modal-dialog" role="document">
		<form class="modal-content" id="form-newPaymentMethod">
			<div class="modal-header">
				<h5 class="modal-title" id="trayform-title" style="display: inline; font-weight: bold; font-size: 14pt;">Add tray</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<label for="method-name" class="col-md-5 control-label text-left">Payment method name<span class="red"> *</span></label>
							<div class="col-md-7">
								<input name="method-name" class="form-control" id="method-name" placeholder="Payment method name" value="" required="required"><br/>
								<label><input type="checkbox" id="method-isbank" name="method-isbank" value="1">Payment method is bank</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" id="btn-submitNewPaymentMethod" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</form>
	</div>
</div>
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function () {
		$("input").iCheck({
			checkboxClass: "icheckbox_square-blue",
			radioClass: "iradio_square-blue",
			increaseArea: "20%"
		});
		
		$(".datepicker").datepicker({autoclose:!0,format:"yyyy-mm-dd",todayHighlight:!0});
	
		$("#supplier-contact").val($("#supplier-id > option:selected").data("email"));

		$(".select2").select2();
	});
	
	$("#supplier-id.select2").on("select2:select", function() {
		$("#supplier-contact").val($("#supplier-id > option:selected").data("email"));
	});

	$("#btn-newPaymentMethod").on("click", function(){
		$("#modal-newPaymentMethod").modal("show");
	});
	
	$("#form-newPaymentMethod").on("submit", function(e){
		e.preventDefault();
		$.ajax({
			type: "post",
			url: "{!! route("paymentMethod.store") !!}",
			data: {
				"_token": $('meta[name="csrf-token"]').attr('content'),
				"method-name": $("#method-name").val(),
				"method-isbank": $("#method-isbank").val()
			},
			dataType: "json",
			success: function(data) {
				if(data.status) {
					$("#modal-newPaymentMethod").modal("hide");
					swal("Success", "New payment method has been saved!", "success");
					$("#supplier-id").select2("destroy");
					$("#payment-method").empty();
					$.each(data.pMethods, function(key, val) {
						$("#payment-method").append('<option value="' + val.id + '" ' + (val.name == $("#method-name").val() ? "selected" : "") + '>' + val.name + '</option>');
					});
					$("#supplier-id").select2();
				}
				$("#form-newPaymentMethod").reset();
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});
</script>
@endpush