@extends('layouts.lte.main')

@section('content')
  @include('admin.sku._formBundle')
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datepicker/datepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/croppie/croppie.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/waitme/waitMe.min.css') }}">
<link rel="stylesheet" href="{{ elixir('css/createProduct.css') }}">
<style media="screen">
  .browseImage {
    border: 1px dashed #dfdfdf;
    text-align: center;
    width: 100%;
  }
  .image-container {
    border: 1px solid #dfdfdf;
    width: 100%;
    height: 100%;
    margin-right: 10px;
    text-align: left;
    padding-top: 0;
    padding-left: 0;
  }
  .btn-trans {
    background-color: transparent;
    color: #3c8dbc;
  }
  .txt-browse {
    white-space: nowrap;
    font-size: 8pt;
  }
  .img-container {
    position: relative;
    margin-right: 10px;
    padding-top: 0;
    padding-left: 0;
    text-align: left;
    background-color: black;
  }

  .image {
  opacity: 1;
  display: block;
  /*width: 100%;*/
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
  }

  .middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 42%;
  /*left: 30%;*/
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  }

  .img-container:hover .image {
  opacity: 0.6;
  }

  .img-container:hover .middle {
  opacity: 1;
  }

  .img-control {
    color: white;
    font-size: 16px;
    width: 100px;
    position: absolute;
    text-align: center;
  }

  .img-control a {
    padding-left: 8px;
    padding-right: 8px;
    color: #fff;
  }

</style>

@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/croppie/croppie.min.js') }}"></script>
<script src="{{ asset('assets/plugins/waitme/waitMe.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/select2/select2.min.js') }}"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="{{ asset('assets/plugins/vue-validate/vee-validate.min.js') }}"></script>

<script type="text/x-template" id="attributes-list">
	<div class="form-group" :class="{'has-error': error}">
		<label class="col-sm-2 control-label">@{{ label }}  <span class="pull-right red" v-show="mandatory"> *</span></label>
		<div class="col-sm-6">
			<input :readonly="readonly > -1" v-if="type === 'text'" type="text" :id="id" :name="name" class="form-control" :value="value" @input="update(id,$event.target.value)">
			<input :readonly="readonly > -1" v-else-if="type === 'numeric'" :id="id" type="number" :name="name" class="form-control" :value="value" @input="update(id,$event.target.value)">
			<div v-else-if="type === 'date'" :id="id" class="input-group date">
				<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<input :readonly="readonly > -1" type="text" :name="name" class="form-control pull-right datepicker" :value="value" @input="update(id,$event.target.value)">
			</div>
			<textarea :readonly="readonly > -1" v-else-if="type === 'richText'" :id="id" :name="name" rows="3" class="form-control" :value="value" @input="update(id,$event.target.value)"></textarea>
			<select :readonly="readonly > -1" v-else-if="type === 'singleSelect' || type === 'multiSelect'" :name="name" class="form-control" :id="id" style="width:100%">
			  <option value="">Please Select</option>
			  <option v-if="id === 'brand'" :value="value" selected="">@{{ value }}</option>
			  <option v-for="option in options" :selected="option.name === value">@{{ option.name }}</option>
			</select>
      <span class="help-block">@{{ error }}</span>
		</div>
	</div>
</script>

@if($model->exists)
<script>
	var sku_id = "{{ $model->id }}";
	var selected = {
		id: "{{ $model->product->id }}",
		product: "{{ $model->product->code_name }}",
	};
</script>
@endif
<script src="{{ elixir('js/bundleSku.js') }}"></script>
@endpush
