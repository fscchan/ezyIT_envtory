<?xml version="1.0" encoding="UTF-8" ?>
<Request>
    <Product>
        <PrimaryCategory>{{ $sku->product->m_product_categories_id }}</PrimaryCategory>
        <SPUId></SPUId>
        <Attributes>
            @foreach($sku->product->attributes()->get() as $attribute)
            <{{$attribute->name}}>{{htmlspecialchars($attribute->value)}}</{{$attribute->name}}>
            @endforeach
        </Attributes>
        <Skus>
            <Sku>
                @foreach($sku->attributes()->get() as $attribute)
                    @if($attribute->name !== 'Images')
                    <{{$attribute->name}}>{{htmlspecialchars($attribute->value)}}</{{$attribute->name}}>
                    @endif
                @endforeach
            </Sku>
        </Skus>
    </Product>
</Request>