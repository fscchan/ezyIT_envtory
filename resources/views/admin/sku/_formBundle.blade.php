<div class="row" id="app">

	<div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <button type="button" class="btn btn-circle" :class="{ 'btn-warning' : isSectionActive('categories') }"><i class="ion-grid"></i></button>
                <p>Select Category</p>
            </div>
            <div class="stepwizard-step">
                <button type="button" class="btn btn-circle" :class="{ 'btn-warning' : isSectionActive('selectSku') }"><i class="ion-navicon"></i></button>
                <p>Select SKU</p>
            </div>
						<div class="stepwizard-step">
								<button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('shop')}"><i class="ion-home"></i></button>
								<p>Assign to Shop</p>
						</div>
            <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('general')}"><i class="ion-document-text"></i></button>
                <p>General Attributes</p>
            </div>
            <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('sku')}"><i class="ion-document-text"></i></button>
                <p>SKU Attributes</p>
            </div>
            <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('qty')}"><i class="ion-filing"></i></button>
                <p>Quantity Setting</p>
            </div>
            <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('price')}"><i class="ion-pricetag"></i></button>
                <p>Manage Price</p>
            </div>
            <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle" :class="{'btn-warning' : isSectionActive('images')}"><i class="ion-images"></i></button>
                <p>Manage Images</p>
            </div>
        </div>
    </div>

<form role="form" class="form-horizontal" id="product-form"  @keydown="errors.clear($event.target.id)"  method="POST" action="{{ $model->exists ? url('/app/sku/bundle/' . $model->id) : url('/app/sku/bundle') }}" novalidate="">

	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif

	<div class="col-md-12">

		<section id="categories" v-show="isSectionActive('categories')">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Select a Category</h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="categoryContainer">
							<div class="col-md-3 selectCategories" v-for="(category, index) in categories">
								<ul class="nav nav-pills nav-stacked" :id="'category-'+index">
									<li v-for="category in category" :data-id="category.id" @click="getChild(category,index);" :class="{ active: isActive(category,index), selected: isSelected(category) }"><a>@{{ category.cat_name | truncate }} <span class="pull-right" v-show="category.children"><i class="fa fa-angle-right"></i></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-md-9">
					<ol class="breadcrumb" v-if="activeItem.length">
						<li v-for="(active, index) in activeItem">@{{ active.cat_name }}</li>
					</ol>
				</div>
				<div class="col-md-3">
					<button type="button" class="btn btn-warning btn-flat pull-right" id="selectCategory" :disabled="selectedCategory.length" @click="activateSection('selectSku')">Select @{{ selectedCategory.cat_name }}</button>
				</div>
			</div>
		</div>
		</section>
		<section id="selectSku" class="noEnterSubmit" v-show="isSectionActive('selectSku')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Select SKU</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label class="col-sm-2 control-label">Search SKU</label>
						<div class="col-sm-9">
							<v-select multiple v-model="mSku.selected" label="sku" :debounce="250" :on-search="searchSku" :options="mSku.options" placeholder="Search Code or Name"></v-select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label text-left">Selected SKU</label>
						<div class="col-md-10">
							<div class="panel-group" id="skuList" role="tablist" aria-multiselectable="false">
								<div class="panel panel-default" v-for="(sku, index) in mSku.selectedData">
									<div class="panel-heading" role="tab" :id="'heading'+index">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#skuList" :href="'#'+index" aria-expanded="false" :aria-controls="sku.sku">
												@{{ sku.sku }}
											</a>
										</h4>
									</div>
									<div :id="index" class="panel-collapse collapse" role="tabpanel" :aria-labelledby="'heading'+index">
										<div class="panel-body">
											<div class="row">
												<div class="form-group">
													<label class="col-sm-2 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" :value="sku.product.code.name" disabled class="form-control" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Brand</label>
													<div class="col-sm-9">
														<input type="text" :value="sku.product.code.brand" disabled class="form-control" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Model</label>
													<div class="col-sm-9">
														<input type="text" :value="sku.product.code.model" disabled class="form-control" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label">Sub Model</label>
													<div class="col-sm-9">
														<input type="text" :value="sku.product.code.submodel" disabled class="form-control" >
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<span class="help-block" v-show="mSku.selected.length < 2">Select at least two SKU</span>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="button" class="btn btn-default btn-flat" @click="activateSection('categories')">Back</button>
					<button type="button" class="btn btn-warning btn-flat pull-right" @click="selectAttr()" :disabled="mSku.selected.length < 2">Bundle SKU</button>
				</div>
			</div>
		</section>

		<section id="shop" v-show="isSectionActive('shop')">
			<div class="box box-default">
				<div class="box-body">
					<div class="col-md-8 col-md-offset-2">
					<h3 class="box-title" align="center">Please Select One Shop or More</h3>
					<hr>
					<div style="min-height: 300px">
					<?php
						$assignedShops = $model->shops()->get()->pluck('id')->toArray();
						$checked = '';
					?>
					@forelse ($shops as $shop)
						<?php
							if($model->exists){
								$checked = in_array($shop->id, $assignedShops) ? 'checked' : '';
							}
						?>
						<div class="col-sm-12">
							<label>
								<input type="checkbox" name="shop[]" class="icheck checkShop" data-id="{{$shop->id}}" data-name="{{$shop->shop_name}}" value="{{$shop->id}}" {{ $checked }} />
								{{$shop->shop_name}}
							</label>
						</div>
					@empty
						No Shop Registered
					@endforelse
					</div>
					</div>
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-default" type="button" @click="activateSection('selectSku')">Back</button>
					<button class="btn btn-flat btn-warning pull-right" type="button" @click="validateShop()">Next</button>
				</div>
			</div>
		</section>

		<section id="general" v-show="isSectionActive('general')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">General Attributes</h3>
				</div>
				<div class="box-body">
					<input type="hidden" name="sku[SellerSku]" v-model="formValue.SellerSku">
					<div v-for="attr in attributes.normal">
						<attribute  :readonly="attributes.readonly.indexOf(attr.name)" :error="errors.get(attr.name)" :label="attr.label" :name="'attribute['+attr.name+']'" :id="attr.name" :type="attr.inputType" :options="attr.options" :mandatory="attr.isMandatory" v-model="attr.value" @update="updateValue"></attribute>
          </div>
					<hr>
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-default" type="button" @click="activateSection('shop')">Back</button>
					<button class="btn btn-flat btn-warning pull-right" type="button" @click="validateProduct()">Next</button>
				</div>
			</div>
		</section>

		<section id="sku" v-show="isSectionActive('sku')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">SKU Attributes</h3>
				</div>
				<div class="box-body">
					<input type="hidden" name="sku[SellerSku]" v-model="formValue.SellerSku">
					<div v-for="attr in attributes.more">
						<attribute  :readonly="attributes.readonly.indexOf(attr.name)" :error="errors.get(attr.name)" :label="attr.label" :name="'attribute['+attr.name+']'" :id="attr.name" :type="attr.inputType" :options="attr.options" :mandatory="attr.isMandatory" v-model="attr.value" @update="updateValue"></attribute>
          </div>
					<hr>
					<div v-for="attr in attributes.sku">
						<attribute :readonly="attributes.readonly.indexOf(attr.name)" :error="errors.get(attr.name)" :label="attr.label" :name="'sku['+attr.name+']'" :id="attr.name" :type="attr.inputType" :options="attr.options" :mandatory="attr.isMandatory" v-model="attr.value" @update="updateValue"></attribute>
          </div>
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-default" type="button" @click="activateSection('general')">Back</button>
					<button class="btn btn-flat btn-warning pull-right" type="button" @click="validateSku()">Next</button>
				</div>
			</div>
		</section>

		<section id="qty" v-show="isSectionActive('qty')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Quantity Setting</h3>
				</div>
				<div class="box-body">
					<div class="form-group" v-for="(bundle, index) in mSku.selectedData">
						<label class="control-label col-sm-3">@{{ bundle.sku }}</label>
						<div class="col-sm-2">
							<input type="number" :name="'bundle['+index+'][qty]'" class="form-control" min="1" v-model="bundle.product.stock_active" readonly="">
							<input type="hidden" :name="'bundle['+index+'][sku_id]'" :value="bundle.id" class="form-control">
						</div>
					</div>
					<hr>
					<div class="form-group" :class="{'has-error': errors.get('bundle_qty')}">
						<label class="control-label col-sm-3">Active Qty</label>
						<div class="col-sm-2">
							<input type="number" name="qty[bundle_qty]" id="bundle_qty" class="form-control" v-model="qty.active" min="1" :max="maxBundleQty">
							<span class="help-block">@{{ errors.get('bundle_qty') }}</span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3">Save Qty</label>
						<div class="col-sm-2">
							<input type="number" name="qty[save_qty]" class="form-control" v-model="qty.save_qty">
						</div>
						<label class="control-label col-sm-2 col-sm-offset-1">Max Shelved Life</label>
						<div class="col-sm-2">
							<input type="number" name="qty[max_shelved_life]" class="form-control" v-model="qty.max_shelved_life">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-3">Min Qty</label>
						<div class="col-sm-2">
							<input type="number" name="qty[min_qty]" class="form-control" v-model="qty.min_qty">
						</div>
						<label class="control-label col-sm-2 col-sm-offset-1">Max Qty</label>
						<div class="col-sm-2">
							<input type="number" name="qty[max_qty]" class="form-control" v-model="qty.max_qty">
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-default" type="button" @click="activateSection('sku')">Back</button>
					<button class="btn btn-flat btn-warning pull-right" type="button" @click="validateQty">Next</button>
				</div>
			</div>
		</section>

		<section id="price" v-show="isSectionActive('price')">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Manage Price</h3>
				</div>
				<div class="box-body">
					<div class="form-group" v-for="(bundle, index) in mSku.selectedData">
						<label class="control-label col-sm-3">@{{ bundle.sku }}</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" min="1" :value="bundle.prices.length ? bundle.prices[0].price : 0" readonly="">
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label class="control-label col-sm-2">Retail Price</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" name="retail_price" v-model="price.retail">
						</div>
						<label class="control-label col-sm-2">Min Price</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" name="min_price" v-model="price.min" min="0" :max="price.max">
						</div>
						<label class="control-label col-sm-2">Max Price</label>
						<div class="col-sm-2">
							<input type="number" class="form-control" name="max_price" v-model="price.max" :min="price.min">
						</div>
					</div>
					<table class="table">
						<thead>
							<tr>
								<th>Shop</th>
								<th>Sale Price</th>
								<th>Special Price</th>
								<th>Special From</th>
								<th>Special To</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(shop, index) in shops">
								<td><input type="hidden" :name="'price['+index+'][shop_id]'" :value="shop.id">@{{ shop.name }}</td>
								<td><input type="number" class="form-control" :name="'price['+index+'][price]'" :min="price.min" :max="price.max"></td>
								<td><input type="number" class="form-control" :name="'price['+index+'][special_price]'"></td>
								<td><input type="text" class="form-control special_period" :name="'price['+index+'][special_from_date]'"></td>
								<td><input type="text" class="form-control special_period" :name="'price['+index+'][special_to_date]'"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-default" type="button" @click="activateSection('qty')">Back</button>
					<button class="btn btn-flat btn-warning pull-right" type="button" @click="activateSection('images')">Next</button>
				</div>
			</div>
		</section>

		<section id="images" v-show="isSectionActive('images')" @click="resizeImageContainer">
			<div class="box box-default">
				<div class="box-body">
					@for ($i=0; $i < 8 ; $i++)
						<div class="col-md-2" style="margin-bottom:15px;margin-top:10px;">
							<div class="browseImage">
								<input type="file" class="fileImage" accept="image/*" style="display: none">
								<button type="button" name="button" class="btn btn-lg btn-trans btn-browse">
									<i class="ion-plus"></i>
								</button>
								<p class="txt-browse">Select Image</p>
							</div>
						</div>
					@endfor
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-default" type="button" @click="activateSection('price')">Back</button>
					<button class="btn btn-flat btn-success pull-right" type="submit">Create SKU</button>
				</div>
			</div>
		</section>

		<div class="modal fade" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="cropModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<form class="form-horizontal" method="POST">
					<div class="modal-header">
						<button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel">Crop Image</h4>
					</div>
					<div class="modal-body">
						<div id="main-cropper"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-flat" id="btn-cancel">Cancel</button>
						<button type="button" class="btn btn-primary btn-flat" id="btn-result">Crop</button>
					</div>
				</form>
				</div>
			</div>
		</div>

		<div class="{{ empty($previous) && empty($next) ? "hidden" : "" }}">
			@if(!empty($previous)) <a href="{{ url("app/sku/" . $previous->id . "/edit") }}" class="btn btn-flat btn-default pull-left">Previous SKU</a> @endif
			@if(!empty($next)) <a href="{{ url("app/sku/" . $next->id . "/edit") }}" class="btn btn-flat btn-default pull-right">Next SKU</a> @endif
		</div>

	</div>

</form>

</div>
