@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-md-12">
    @include('layouts.lte.status')
	@include('admin.processPo._index')
  </div>
</div>
@stop
@push('styles')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/selectize/css/selectize.bootstrap3.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datepicker/datepicker3.css') }}">
	<style>
		.red {
		    color: #dd4b39;
		  }
		.processContent {
		 	display: none;
		 }
		.form-horizontal .label1 {
		    text-align: left;
		}
		.form-horizontal .label2 {
		    text-align: left;
		    font-weight: inherit;
		}
		.form-inline .qty {
			width: 80px
		}
		.form-inline .price {
			width: 100px
		}
		.right {
			text-align: right;
		}
		table.dataTable tbody th,
		table.dataTable tbody td {
		    white-space: nowrap;
		}
		.swalform {
		    display: block !important;
		    margin: 10px 0 !important;
		    padding: 0 50px !important;
		    border: 0;
		    box-shadow: none;
		}
	</style>
@endpush
@push('scripts')
	<script src="{{ asset('assets/plugins/selectize/js/standalone/selectize.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
	<script src="{{ elixir('js/processPo.js') }}"></script>
	<script>
		$("#btn-printInvoice").on("click", function() {
			/* var WinPrint = window.open('', '', 'left=0,top=0,toolbar=0,scrollbars=0,status=0');
			var dataPrint = $("#invoiceModal .modal-body").clone();
			dataPrint.find("#invoice_no").attr("value", $("#invoiceModal #invoice_no").val());
			dataPrint.find("#invoice_date").attr("value", $("#invoiceModal #invoice_date").val());
			WinPrint.document.write($("<div>").append($("link[rel=stylesheet]").clone()).append(dataPrint).html() + "<script>window.print();window.close()<\/script>");
			WinPrint.document.close();
			WinPrint.focus(); */

			$("#form-printInvoice").attr("action", "{{ url("app/po/printInvoice") }}");
			$("#form-printInvoice > input#printIV-po").val($("#invoiceModal [name=po_prefix]").val());
			$("#form-printInvoice > input#printIV-invoice").val($("#invoiceModal #invoice_no").val());
			$("#form-printInvoice").submit();
		});
	</script>
@endpush