<html>
	<head>
		<title>{{$title or 'Title Here'}}</title>
		<link rel="stylesheet" href="http://localhost:8000/assets/lte/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/lte/plugins/select2/select2.min.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/lte/plugins/jQueryUI/jquery-ui.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/lte/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/lte/dist/css/skins/skin-black-light.min.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/plugins/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="/css/dashboard.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/plugins/selectize/css/selectize.bootstrap3.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/lte/plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="http://localhost:8000/assets/lte/plugins/datepicker/datepicker3.css">
	</head>
	<body>
		<div class="row">
			<div class="col-xs-3">
				<?php $barcode = Barcode::getBarcodePNG($po->prefix, "C128", 3, 100); ?>
				<div align="center">
				<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $po->prefix }}" width="100%" />
				{{ $po->prefix }}
				</div>
			</div>
			<div class="col-xs-7">
				<div align="right">
				  <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
				  <p>{{ $company->profile_billing_address }}</p>
				</div>
			</div>
			<div class="col-xs-2">
				<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-1">
				  <div class="form-group">
						<label class="col-sm-5 control-label">Purchase Order Id</label>
						<label class="col-sm-7 control-label label2"><span class="prefix">{{ $po->prefix }}</span></label>
				  </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-md-offset-1">
				<div class="form-group">
					<label class="col-sm-5 control-label">Invoice Number</label>
					<label class="col-sm-7 control-label label2"><span class="prefix">{{ $invoice->prefix }}</span></label>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label class="col-sm-5 control-label">Invoice Date</label>
					<label class="col-sm-7 control-label label2"><span class="prefix">{{ date_format(date_create_from_format("Y-m-d", $invoice->date), "d F Y") }}</span></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-hover table-bordered" id="invoice-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th></th>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>Ordered</th>
							<th>Received</th>
							<th>Invoice Qty</th>
							<th>Price</th>
							<th>Invoice Price</th>
							<th>Amount</th>
						</tr>
					</thead>
					<?php $totalAmount = 0; ?>
					<tbody>
						@foreach($products as $i => $product)
							<tr>
								<td>{{ ++$i }}</td>
								<td>{{ $product->product_code }}</td>
								<td>{{ $product->product_name }}</td>
								<td>{{ $product->qty }}</td>
								<td>{{ $product->received }}</td>
								<td>{{ $product->invoice_qty }}</td>
								<td>{{ $product->price }}</td>
								<td>{{ $product->amount }}</td>
								<td>{{ $product->invoice_amount }}</td>
							</tr>
							<?php $totalAmount = $totalAmount + $product->invoice_amount; ?>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="8" align="right">Shipping Cost</td>
							<td>{{ $po->ship_cost }}</td>
						</tr>
						<tr>
							<td colspan="8" align="right">Misc Cost</td>
							<td>{{ $po->misc_cost }}</td>
						</tr>
						<tr>
							<td colspan="8" align="right">Total Amount</td>
							<td align="right"><span id="text_invoice_total_amount">{{ $totalAmount }}</span></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<script>window.print();</script>
	</body>
</html>