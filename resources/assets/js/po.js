$(document).ready(function(){
    calculatePo();
  })

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  $('.datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
  });

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $('#supplier').selectize({
    valueField: 'id',
    labelField: 'supplier_name',
    searchField: 'supplier_name',
    options: [],
    create: false,
    load: function(query, callback) {
        if (!query.length) return callback();
        dataProvider('/app/po/searchSupplier',{name : query},callback);
    }
  });

  $('#searchProduct').selectize({
    valueField: 'id',
    labelField: 'product_name',
    searchField: 'product_name',
    options: [],
    create: false,
    load: function(query, callback) {
        if (!query.length) return callback();
        dataProvider('/app/po/searchProduct',{name : query,supplier: $('#supplier').val()},callback);
    }
  });

  function generatePrefix(obj){
    var supplier = obj.value;
    var $select = $('#searchProduct').selectize();
    var control = $select[0].selectize;
      control.clearCache();
      control.clearOptions();
    $('#prefix').val('');
    $('#supplier_address').val('');
    $('#supplier_email').empty();
    $('#productTable tbody').empty();
    $('.productSection').hide();
    $.ajax({
        type: 'post',
        url: '/app/po/generatePrefix',
        data: {supplier : supplier},
        dataType: 'json',
        success: function(data){
          if ($.trim(data)) {
            $('#prefix').val(data.prefix);
            $('#supplier_address').val(data.supplier.supplier_address);
            $.each(data.email, function(){
              if (this.email)
              $('#supplier_email').append('<option value="'+this.email+'">'+this.contact_name+' ('+this.email+')</option>');
            });
            $('.productSection').show();
          }
        },
        error: function(data){
          var errors = data.responseJSON;
          console.log(errors);
        }
      });
  }

  $('#addProduct').click(function(){
    var product = $('#searchProduct').val();
    var supplier = $('#supplier').val();
    var add = true;
    if (!supplier) {
      add = false;
      var message = ["Supplier not Selected","Please Select a Supplier First"];
    }
    if (!product) {
      add = false;
      var message = ["Select Product First",""];
    }
    if(!add){
      swal(message[0],message[1],"warning");
    }else{
      $.ajax({
        type: 'post',
        url: '/app/po/getProduct',
        data: {product : product,supplier : supplier},
        dataType: 'json',
        success: function(data){
          // console.log(data);
          addProductToTable(data);
        },
        error: function(data){
          var errors = data.responseJSON;
          console.log(errors);
        }
      });
    }
  })

  function addProductToTable(data){
    var $select = $('#searchProduct').selectize();
    var control = $select[0].selectize;
    control.clear();
    if ($('#products-'+data.id).length == 0) {
      var rowCount = $('#productTable tbody tr').length;
      var row = rowCount + 1;
      $('#productTable tbody').append('<tr class="rowProducts" id="row-'+row+'">'+
          '<td>'+data.product_po_id+'<input type="hidden" name="products['+row+'][product_po_id]" value="'+data.product_po_id+'"></td>'+
          '<td>'+data.product_code+'<input type="hidden" name="products['+row+'][m_products_id]" id="products-'+data.id+'" value="'+data.id+'"></td>'+
          '<td>'+data.attributes[0].value+'</td>'+
          '<td>'+data.category.cat_name+'</td>'+
          '<td><input type="number" class="form-control" name="products['+row+'][qty]" id="qty'+row+'" min="1" onkeyup="calculateAmount('+data.id+')" required=""></td>'+
          '<td><input type="number" class="form-control" name="products['+row+'][price]" id="price'+row+'" onkeyup="calculateAmount('+row+')" required=""></td>'+
          '<td align="right"><input type="hidden" name="products['+row+'][amount]" id="amount'+row+'" class="amount"><span id="amountText'+row+'"></span></td>'+
          '<td><button type="button" class="btn btn-danger btn-xs btn-flat" data-toggle="tooltip" data-placement="bottom" title="Remove this Product" onclick="removeProduct('+row+')"><i class="fa fa-times"></i></button></td>'+
        '</tr>')
        $('#qty'+row).focus();
    }else{
      swal("Product Already Added","Please Add Another Products","warning")
    }
  }

  function calculateAmount(row){
    var qty = $('#qty'+row).val();
    var price = $('#price'+row).val();
    var amount = qty*price;

    $('#amount'+row).val(amount);
    $('#amountText'+row).text(amount);

    calculatePo();
  }

  function calculatePo(){
    var subtotal = 0;
    var misccost = parseFloat($('#misc_cost').val()) || 0;
    var shipcost = parseFloat($('#ship_cost').val()) || 0;
    var amount = 0;
    var grandtotal = 0;
    
    $('.amount').each(function() {
      amount = parseFloat(this.value);
      subtotal += amount;
    });
    if(!subtotal){
      subtotal = 0;
    }
    $('#total_det_mount').val(subtotal);
    $('#subtotal').text(subtotal);

    grandtotal = subtotal + misccost + shipcost;
    if (!grandtotal) {
      grandtotal = 0;
    }
    $('#total_amount').val(grandtotal);
    $('#grandtotal').text(grandtotal);
  }

  function dataProvider(dataUrl,query,callback){
    $.ajax({
      url: dataUrl,
      type: 'POST',
      dataType: 'json',
      data: query,
      error: function() {
          callback();
      },
      success: function(res) {
          callback(res);
      }
    });
  }

  function removeProduct(row){
    swal({
      title: "Remove Product?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Remove!",
      closeOnConfirm: true
    },
    function(){
      $('#row-'+row).remove();
      calculatePo();
    });
  }

  $("#po-form").submit(function(e){
    valid = true;
    var rowCount = $('#productTable tbody tr').length;
    if (rowCount == 0) {
      swal("The products has not been added","Please add some products first","warning");
      valid = false;
    }
    if (valid) {
      $('#emailModal').modal({
        backdrop: 'static', 
        keyboard: false
      });
    }
    e.preventDefault();
  });

  $('#emailModal').on('shown.bs.modal', function (event) {
    $('#email_email').val($('#supplier_email option:selected').val())
    $('#email_subject').val('PO - '+$('#prefix').val())
    $('#email_content').focus()
    $('#email_attach').text('PO-'+$('#prefix').val()+'.pdf')
  });

  $('#emailSubmit').click(function() {
    $('#po-form').unbind('submit');
    $('#po-form').submit();
  })