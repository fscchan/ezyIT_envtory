Vue.component('v-select', VueSelect.VueSelect);
var app = new Vue({
  el: "#app",

  data: {
    stock: {
      m_products_id: '',
      zone_type: '',
      warehouse: '',
      cell_id: '',
      qty: ''
    },
    product: [],
    cells: [],
    selectedCell: null
  },

  watch: {
    'stock.zone_type': function(zone) {
      if(zone){
        this.getCell();
      }
    },
    'stock.warehouse': function(cell) {
      if(cell){
        this.getCell();
      }
    },
    'selectedCell': function(selected) {
      if(selected){
        this.stock.cell_id = this.selectedCell.id
      }else{
        this.stock.cell_id = ''
      }
    },
    'stock.cell_id': function(selected) {
      if(!selected) {
        this.stock.qty = '';
      }
    }
  },

  methods: {
    showQtyModal(product_id) {
      const vm = this;
      axios.get('/app/products/data/'+product_id)
        .then(function(response) {
          vm.product = response.data.data;
          vm.stock.m_products_id = vm.product.id;
        } );
      $('#qtyModal').modal({
        show: true,
        backdrop: false,
        keyboard: false
      })
    },
    hideQtyModal() {
      this.cells = [];
      this.selectedCell = null;
      this.product = [];
      $('#qtyModal').modal('hide')
    },
    getCell() {
      const vm = this;
      if(this.stock.zone_type && this.stock.warehouse){
        axios.get('/app/po/receive/cells/'+this.stock.zone_type+'/'+this.stock.warehouse+'/'+this.product.m_product_categories_id)
          .then(function(response) {
            vm.cells = response.data;
            vm.selectedCell = response.data[0];
          })
          .catch();
      }
    },
    searchCell(search, loading) {
      if(this.stock.zone_type && this.stock.warehouse){
        const vm = this;
        loading(true)
        axios.get('/app/po/receive/cells/search/'+this.stock.zone_type+'/'+this.stock.warehouse+'/'+this.product.m_product_categories_id, {
          params: {
            prefix: search
          }
        }).then(function(response) {
           vm.cells = response.data
           loading(false)
        });
      }
    },
    submitStock() {
      axios.post('/app/products/stock', this.$data.stock)
        .then(function(response) {
          swal({
            title: "Quantity Added",
            text: "Waiting confirmation to Stock In",
            type: "success"
          },function(){
            location.reload();
          });
        })
        .catch(function(error) {
          swal("Failed to Added Quantity", "Please try again", "error");
        });
    }
  }

})
