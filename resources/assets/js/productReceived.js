$(document).ready(function(){
	var prefix = $('#prefix').val();
	if($.trim(prefix))
		loadProductReceived(prefix)
});

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

$('#purchase_prefix').selectize({
    valueField: 'prefix',
    labelField: 'prefix',
    searchField: 'prefix',
    options: [],
    create: false,
    load: function(query, callback) {
        if (!query.length) return callback();
        dataProvider('/app/po/searchPo',{po : query},callback);
    }
});

function setICheck(){
	$('input[type=checkbox]').iCheck({
	  checkboxClass: 'icheckbox_square-blue',
	  radioClass: 'iradio_square-blue',
	  increaseArea: '20%'
	});

	$('.receive').on('ifChecked', function () {
		var row = $(this).closest('tr');
		row.find('.reject').prop('disabled', false);
		row.find('.zone').prop('disabled', false);
		row.find('.warehouse').prop('disabled', false);
		row.find('.cell').prop('disabled', false);
	});

	$('.receive').on('ifUnchecked', function () {
		var row = $(this).closest('tr');
		row.find('.reject').prop('disabled', true);
		row.find('.zone').prop('disabled', true);
		row.find('.warehouse').prop('disabled', true);
		row.find('.cell').prop('disabled', true);
	});
}

function dataProvider(dataUrl,query,callback){
    $.ajax({
      url: dataUrl,
      type: 'POST',
      dataType: 'json',
      data: query,
      error: function() {
          callback();
      },
      success: function(res) {
          callback(res);
      }
    });
}

function loadProductReceived(id){
    $.ajax({
        url: "/app/po/loadProductReceived",
        type: "post",
        data: { po: id }
    }).done(function (result) {
    	$('.prefix').text(result.po.prefix);
    	$('.po_prefix').val(result.po.prefix);
    	$('#supplier_name').text(result.po.supplier_name);
    	$('#po_date').text(result.po.po_date);
    	$('#email').text(result.po.email);
    	$('.t_po_master_id').val(result.po.id);
    	$('.processContent').show();
    	tableReceive.clear().draw();
        tableReceive.rows.add(result.readyToReceive).draw();
        setICheck();
		$('.select2').select2().empty();
        }).fail(function (jqXHR, textStatus, errorThrown) { 
              // needs to implement if it fails
        });
}

var tableReceive = $("#receive-table").DataTable({
	data:[],
	columns: [
	            { "data": "action"},
	            { "data": "cold_batch_id" },
	            { "data": "invoice"  },
	            { "data": "product" },
	            { "data": "qty" },
	            { "data": "reject_qty" },
	            { "data": "zone_type" },
	            { "data": "warehouse" },
	            { "data": "cell_id" },
	],
	rowCallback: function (row, data) {},
	filter: false,
	info: false,
	paging: false,
	ordering: false,
	processing: true,
	retrieve: true,
	language: {
	  emptyTable: "No Product"
	}
});

function setQty(no){
	var cold = parseFloat($('#cold-'+no).text());
	var reject = parseFloat($('#reject-'+no).val());
	var receive = 0;
	receive = cold - reject;
	$('#receive-'+no).text(receive);
}

function getCells(no,category) {
	var zone = $('#zone_type-'+no).val();
	var warehouse = $('#warehouse-'+no).val();
	var cells = [];
	$('.select2').select2().empty();
	if (zone && warehouse) {
		axios.get('/app/po/receive/cells/'+zone+'/'+warehouse+'/'+category)
			.then(function(response) {
				cells = response.data;
				$('.select2').select2({
			    	data: cells,
			    	placeholder: 'Please Select',
			    	ajax: {
		                dataType: 'json',
		                url: '/app/po/receive/cells/search/'+zone+'/'+warehouse+'/'+category,
		                delay: 500,
		                data: function(params) {
		                    return {
		                        prefix: params.term
		                    }
		                },
		                processResults: function (data, page) {
		                  return {
		                    results: data.length ? data : cells
		                  };
		                },
		                cache: true
		            },
		            width: 'resolve',
		            // minimumInputLength: 2,
			    });
			})
	}
}