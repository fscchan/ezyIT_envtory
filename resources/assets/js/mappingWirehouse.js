	Vue.filter('truncate', function(value) {
	  if(value.length < 35) {
	    return value;
	  }
	  return value.substring(0, 23) + ' ...';
	});

	var app = new Vue({
	
		el: "#categories",
		data: {
			categories: [],
			activeItem: [],
			selectedCategory: [],
			categoriesId: [],
			togled: []
		},
		mounted() {
			const vm = this;
			axios.get('/app/products/categories/0').then(function(response){
				vm.categories.push(response.data)
			});
			if (wid) {
				axios.get('/app/warehouse/zoneCategories/'+wid).then(function(response){
					vm.categoriesId = response.data
				});
			}
		},
		methods: {
			isActive(categories,type) {
		      return this.activeItem[type] === categories
		    },
			isSelected(categories) {
				if (this.categoriesId.indexOf(categories) > -1) {
					return true;
				}else{
					return false;
				}
		    },
		    togleParent() {
		    	var id = this.activeItem[0].id;
		    	if (this.togled.indexOf(id) < 0) {
			    	this.togled.push(id);
		    	}
		    },
		    getChild(parent,index) {
				const vm = this;
				length = vm.categories.length;
				vm.categories.splice(index+1,length);
				axios.get('/app/products/categories/'+parent.id).then(function(response){
					vm.categories.push(response.data)
					for (category in response.data) {
						var i = vm.categoriesId.indexOf(response.data[category].id);
						if(i != -1) {
							vm.categoriesId.splice(i, 1);
						}
	                }
					if (vm.categoriesId.indexOf(parent.id) > -1) {
						vm.selectedCategory.push(parent.id);
						for (category in response.data) {
		                    vm.categoriesId.push(response.data[category].id);
		                }
					}
				});
				this.activeItem.splice(index,length); this.activeItem.push(parent);
			},
		}
	})
