$(document).ready(function(){
	$('.noEnterSubmit').keypress(function(e){
    if ( e.which == 13 ) return false;
	});
});

$(function () {
		$('.icheck').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});

		$('.btn-browse').click(function() {
			var input = $(this).closest('.browseImage').find('.fileImage');
			input.click();
		});

		$('.fileImage').change(function() {
			readFile(this);
			var target = $(this).closest('.browseImage');
			var fileImage = $(this).val();
			if (fileImage) {
				target.addClass('target image-container');
				$('#cropModal').modal({
					show: true,
					backdrop: false,
					keyboard: false
				});
			}
		});

		var basic = $('#main-cropper').croppie({
			viewport: { width: 500, height: 500 },
			boundary: { width: 530, height: 530 },
			showZoomer: true,
		});

		function readFile(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					basic.croppie('bind', {
						url: e.target.result
					});
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$('#btn-result').on('click', function (ev) {
				basic.croppie('result', {
					type: 'canvas',
					size: 'viewport'
				}).then(function (resp) {
					processResult({
						src: resp
					});
				});

				$('#cropModal').modal('hide');
			});

			$('#btn-cancel').on('click', function() {
				$('.target').removeClass('target image-container');
				$('#cropModal').modal('hide');
			});

			function processResult(result) {
				var html;
				if (result.html) {
					html = result.html;
				}
				if (result.src) {
					html = '<input type="hidden" name="images[]" value="'+result.src+'" /><img class="img" src="' + result.src + '" width="100%" />';
				}

				$('.target').html(html);
				$('.target').removeClass('target');
			}

});

	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	class Errors {
  	constructor() {
  		this.errors = {};
  	}

  	get(field) {
  		if (this.errors[field]) {
  			return this.errors[field][0];
  		}
  	}

  	record(errors) {
  		this.errors = errors;
  	}

  	clear(field) {
  		this.errors[field] = '';
  	}
  }

	Vue.use(VeeValidate);

	Vue.filter('truncate', function(value) {
	  if(value.length < 30) {
	    return value;
	  }
	  return value.substring(0, 27) + ' ...';
	});

	Vue.component('v-select', VueSelect.VueSelect);

	Vue.component('attribute',{
		props: ['label','type','name','options','mandatory','id','value','readonly','error'],
		template: '#attributes-list',
		methods: {
				update(name,value) {
					this.$emit('update', {'name' : name, 'value': value})
				}
	    },
	    mounted() {
				var vm = this;
	    	$("select[name='attribute[brand]']").select2({
		    	placeholder: 'Enter a Brand Name',
	            ajax: {
	                dataType: 'json',
	                url: '/app/products/brands',
	                delay: 500,
	                data: function (params) {
				      return {
				        query: params.term, // search term
				      };
				    },
              processResults: function (data, page) {
              	var select2Data = $.map(data, function (obj) {
                    obj.id = obj.name;
                    obj.text = obj.name;
                    return obj;
                });
						return {
							results: select2Data,
						};
	                },
	                cache: true
	            },
	            width: 'resolve',
	            language: {
				    noResults: function (params) {
				      return "Type a Brand Name";
				    }
				  }
		    })
	      .trigger('change')
	      .on('change', function () {
	        vm.$emit('update', {'name' : this.id, 'value': this.value})
	      });
		    $("select[name!='attribute[brand]']").not('.noselect2').select2({
		    	placeholder: 'Please Select',
		    	width: 'resolve',
		    })
	      .trigger('change')
	      .on('change', function () {
	        vm.$emit('update', {'name' : this.id, 'value': this.value})
	      });
	    }
	})

	var app = new Vue({

		el: "#app",

		data: {
			mSku : {
				options : [],
				selected: [],
				selectedData: [],
			},
			categories: [],
			activeItem: [],
			selectedCategory: [],
			brands: null,
			activeSection: 'categories',
			products: [],
			product: [],
			selected: '',
			attribute: {
				brand: '',
				model: '',
				color: '',
			},
			attributes: {
				normal : [],
				more : [],
				sku1 : [],
				sku : [],
				code : [],
				blocked: ['SellerSku','__images__','quantity','price','special_price','special_from_date','special_to_date'],
				forced: ['name','brand','model'],
				readonly: [],
				variant: []
			},
			id: '',
			sku: '',
			formValue: {},
			errors: new Errors(),
			codeList: [],
			shops: [],
			qty: {
				mSku: [],
	      sku_id: '',
	      active: '',
	      cold: '',
	      save_qty: '',
	      max_qty: '',
	      min_qty: '',
	      max_shelved_life: '',
	    },
			price: {
				retail: '',
				min: '',
				max: '',
			}
		},

		watch: {
			'selected': function(select){
				if (select) {
					this.getProductData();
				}else{
					this.sku = '';
					this.product = [];
				}
			},
			'mSku.selected': function(selected){
				var sku_id = [];
				const vm = this;
				$('body').waitMe({effect:'rotation'});
				if (selected.length) {
					for (var i = 0; i < selected.length; i++) {
						sku_id.push(selected[i].id);
					}
					axios.get('/app/sku/bundle/data', {
						params: {
							sku: sku_id
						}
					})
					.then(function(response){
						vm.mSku.selectedData = response.data;
					});
				}else{
					vm.mSku.selectedData = [];
				}
				$('body').waitMe("hide");
			}
		},

		computed: {
			skuBundle: function(){
				const vm = this;
				var selected = vm.mSku.selected;
				var skuBundle = '';
				for (var i = 0; i < selected.length; i++) {
					skuBundle = skuBundle + selected[i].sku.substring(0,4);
				}
				return skuBundle;
			},
			maxBundleQty: function(){
				const vm = this;
				var qtyData = vm.mSku.selectedData;
				var qty = [];
				var bundleQty = 0;
				for (var i = 0; i < qtyData.length; i++) {
					qty.push(qtyData[i].product.stock_active);
				}
				if (qty.length) {
					var bundleQty = Math.min(...qty);
				}
				if (bundleQty > 10) {
					return 10;
				}
				vm.qty.active = bundleQty;
				return bundleQty;
			},
			bundlePrice: function(){
				const vm = this;
				var priceData = vm.mSku.selectedData;
				var price = parseFloat(0);
				for (var i = 0; i < priceData.length; i++) {
					if (priceData[i].prices.length) {
						var data = priceData[i].prices;
						if (data[0]) {
							price = price + parseFloat(data[0].price);
						}
					}
				}
				vm.price.retail = price;
				return price;
			},
			qtyIsValid: function(){
				const vm = this;
				var qty = vm.qty.mSku;
				var valid = false;
				for (var i = 0; i < qty.length; i++) {
					if (qty[i] == '') {
						valid = false;
					}else{
						valid = true;
					}
				}
				return valid;
			}
		},

		mounted() {
			vm = this;

			if (typeof sku_id === 'undefined') {
				const vm = this;
				axios.get('/app/products/categories/0').then(function(response){
					vm.categories.push(response.data)
				});
				this.getProducts();
			}else{
				this.selected = selected;
			}

			$('.checkShop').on('ifChecked', function(event){
				var id = $(this).attr('data-id');
				var name = $(this).attr('data-name');
				vm.shops.push({'id':id,'name':name});
			});
			$('.checkShop').on('ifUnchecked', function(event){
				var id = $(this).attr('data-id');
				i = vm.shops.findIndex(x => x.id==id);
				vm.shops.splice(i,1);
			});
		},

		methods: {
				getChild(parent,index) {
					const vm = this;
					length = vm.categories.length;
					vm.categories.splice(index+1,length);
					if (parent.children) {
						this.selectedCategory = [];
						axios.get('/app/products/categories/'+parent.id).then(function(response){
							vm.categories.push(response.data)
						});
					}else{
						this.selectedCategory = parent;
					}
					this.activeItem.splice(index,length); this.activeItem.push(parent);
				},
				isActive(categories,type) {
		      return this.activeItem[type] === categories
		    },
				isSelected(categories) {
		      return this.selectedCategory === categories
		    },
				getCodeList() {
		      $('body').waitMe({effect:'rotation'});
		      const vm = this;
		      axios.get('/app/products/codeList/'+this.selectedCategory.id)
		        .then(function(response){
		          vm.codeList = response.data;
		          vm.activateSection('productCodeList');
		          $('body').waitMe("hide");
		        })
		        .catch(function (error) {
							$('body').waitMe("hide");
							swal('Error','Please Try Again','error');
						});
		    },
				isSectionActive(section) {
		      return this.activeSection === section
		    },
		    activateSection(section) {
		    	this.activeSection = section;
		    	$('html, body').animate({
				    scrollTop: $(".content-header").offset().top
				}, 1000);
		    },
		    updateValue(data) {
					this.formValue[data.name] = data.value;
				},
		    getProductData() {
					const vm = this;
					axios.get('/app/products/data/'+this.selected.id)
					.then(function(response) {
						vm.id = response.data.data.id;
						vm.product = response.data;
						vm.attribute.product_code = vm.product.data.product_code;
						vm.attribute.name = vm.product.data.code.name;
						vm.attribute.brand = vm.product.data.code.brand;
						vm.attribute.model = vm.product.data.code.model;
						vm.attribute.submodel = vm.product.data.code.submodel;
						var data = response.data;
						$.each( data.attributes, function(key,attribute){
							vm.formValue[attribute.name] = attribute.value;
						});
						vm.attributes.variant = [];
						$.each(vm.product.data.variations, function(key,attribute){
							vm.attributes.variant.push(attribute.name)
						})
					});
		    },
		    selectAttr() {
					$('body').waitMe({effect:'rotation'});
					const vm = this;
					vm.attributes.normal = [];
					vm.attributes.more = [];
					vm.attributes.sku = [];
					vm.attributes.code = [];
					axios.get('/app/products/attributes/lazada/'+this.selectedCategory.id)
						.then(function(response){
							$.each( response.data.filtered, function(key,attribute){
									var value = vm.formValue[attribute.name] ? vm.formValue[attribute.name] : '';
									if (attribute.name === 'product_code') {
										value = vm.skuBundle;
									}
									attribute['value'] = value;
									if(jQuery.inArray(attribute.name, vm.attributes.blocked) === -1){
										if (attribute.attributeType === 'normal') {
											if (attribute.name === 'product_code') {
												attribute['label'] = 'SKU';
											}
											vm.attributes.normal.push(attribute);
										}else if (jQuery.inArray(attribute.name, vm.attributes.variant) > -1) {
											vm.attributes.sku1.push(attribute);
										}
										vm.formValue[attribute.name] = value;
									}
							});
							$.each(response.data.all, function(key,attribute){
								if (jQuery.inArray(attribute.name, vm.attributes.variant) === -1 && jQuery.inArray(attribute.name, vm.attributes.forced) === -1  && jQuery.inArray(attribute.name, vm.attributes.blocked) === -1) {
									vm.formValue[attribute.name] = '';
									if (attribute.attributeType === 'sku') {
										vm.attributes.sku.push(attribute);
									}else{
										vm.attributes.more.push(attribute);
									}
								}
							});
							vm.activateSection('shop');
							$('body').waitMe("hide");
						})
						.catch(function (error) {
							console.log(error);
							$('body').waitMe("hide");
							swal('Failed to Retrive Attributes','Please Try Again','error');
						});
				},
				getQty() {
					var vm = this;
					axios.get('/app/sku/get/'+vm.id)
						.then(function(response){
							data = response.data;
							// vm.qty.active = data.active;
							// vm.qty.cold = data.cold;
					});
				},
			generateSku() {
				var sku = $.trim(this.attribute.brand+' '+this.attribute.model+' '+this.attribute.color);
				var slug = sku.replace(/[^a-z0-9-]/gi, '-').
					    replace(/-+/g, '-').
					    replace(/^-|-$/g, '');
				this.sku = slug;
			},
			getAttribute(attributes,name) {
				var attr = $.grep(attributes, function(e){ return e.name == name; });
				if (attr.length == 1) {
					return attr[0].value;
				}
				return null;
			},
			getProducts() {
				const vm = this;
			    axios.get('/app/sku/product/init')
			    .then(response => {
			    	vm.products = response.data
			   });
			},
			searchProduct(search, loading) {
			    loading(true)
			    const vm = this;
			    axios.get('/app/sku/product/'+search)
			    .then(response => {
			    	vm.products = response.data
			       loading(false)
			   });
			},
			searchSku(search, loading) {
			    loading(true)
			    const vm = this;
			    axios.get('/app/sku/bundle/search/'+search)
			    .then(response => {
			    	vm.mSku.options = response.data
			       loading(false)
			   });
			},
			validateProduct() {
				const vm = this;
				var error = {};
				var isValid = true;
				$.each(vm.attributes.normal, function(key,attribute) {
					attribute.value = vm.formValue[attribute.name];
					if (attribute.isMandatory == 1) {
						if (vm.formValue[attribute.name] == "") {
							error[attribute.name] = ['The '+attribute.label+' is Required'];
							isValid = false;
						}
					}
				})
				vm.errors.record(error);
				if (isValid) {
					vm.activateSection('sku');
				}
			},
			validateShop() {
				if (!this.shops.length) {
					swal('No Shop Selected','Please Select at Least one Shop','warning');
				}else{
					$('.special_period').datepicker({
						format: 'yyyy-mm-dd'
					});
					this.activateSection('general');
				}
			},
			validateSku() {
				const vm = this;
				var error = {};
				var isValid = true;
				$.each(vm.attributes.sku, function(key,attribute) {
					attribute.value = vm.formValue[attribute.name];
					if (attribute.isMandatory == 1) {
						if (vm.formValue[attribute.name] == '') {
							error[attribute.name] = ['The '+attribute.label+' is Required'];
							isValid = false;
						}
					}
				})
				$.each(vm.attributes.more, function(key,attribute) {
					attribute.value = vm.formValue[attribute.name];
					if (attribute.isMandatory == 1) {
						if (vm.formValue[attribute.name] == '') {
							error[attribute.name] = ['The '+attribute.label+' is Required'];
							isValid = false;
						}
					}
				})
				vm.errors.record(error);
				if (isValid) {
					vm.activateSection('qty');
				}
			},
			resizeImageContainer() {
				var cw = $('.browseImage').width();
				$('.browseImage').css({'height':cw+'px'});
				$('.btn-browse').css({'padding-top':cw/2-20+'px'});
			},
			validateQty() {
				const vm = this;
				var error = {};
				var valid = true;
				if (this.qty.active > this.maxBundleQty) {
					error['bundle_qty'] = ['Max Bundle Qty is '+ this.maxBundleQty];
					valid = false;
				}
				vm.errors.record(error);
				if (valid) {
					this.activateSection('price')
				}
			},
		},

	})
