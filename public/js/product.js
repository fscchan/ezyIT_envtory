/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

eval("Vue.component('v-select', VueSelect.VueSelect);\nvar app = new Vue({\n  el: \"#app\",\n\n  data: {\n    stock: {\n      m_products_id: '',\n      zone_type: '',\n      warehouse: '',\n      cell_id: '',\n      qty: ''\n    },\n    product: [],\n    cells: [],\n    selectedCell: null\n  },\n\n  watch: {\n    'stock.zone_type': function(zone) {\n      if(zone){\n        this.getCell();\n      }\n    },\n    'stock.warehouse': function(cell) {\n      if(cell){\n        this.getCell();\n      }\n    },\n    'selectedCell': function(selected) {\n      if(selected){\n        this.stock.cell_id = this.selectedCell.id\n      }else{\n        this.stock.cell_id = ''\n      }\n    },\n    'stock.cell_id': function(selected) {\n      if(!selected) {\n        this.stock.qty = '';\n      }\n    }\n  },\n\n  methods: {\n    showQtyModal: function showQtyModal(product_id) {\n      var vm = this;\n      axios.get('/app/products/data/'+product_id)\n        .then(function(response) {\n          vm.product = response.data.data;\n          vm.stock.m_products_id = vm.product.id;\n        } );\n      $('#qtyModal').modal({\n        show: true,\n        backdrop: false,\n        keyboard: false\n      })\n    },\n    hideQtyModal: function hideQtyModal() {\n      this.cells = [];\n      this.selectedCell = null;\n      this.product = [];\n      $('#qtyModal').modal('hide')\n    },\n    getCell: function getCell() {\n      var vm = this;\n      if(this.stock.zone_type && this.stock.warehouse){\n        axios.get('/app/po/receive/cells/'+this.stock.zone_type+'/'+this.stock.warehouse+'/'+this.product.m_product_categories_id)\n          .then(function(response) {\n            vm.cells = response.data;\n            vm.selectedCell = response.data[0];\n          })\n          .catch();\n      }\n    },\n    searchCell: function searchCell(search, loading) {\n      if(this.stock.zone_type && this.stock.warehouse){\n        var vm = this;\n        loading(true)\n        axios.get('/app/po/receive/cells/search/'+this.stock.zone_type+'/'+this.stock.warehouse+'/'+this.product.m_product_categories_id, {\n          params: {\n            prefix: search\n          }\n        }).then(function(response) {\n           vm.cells = response.data\n           loading(false)\n        });\n      }\n    },\n    submitStock: function submitStock() {\n      axios.post('/app/products/stock', this.$data.stock)\n        .then(function(response) {\n          swal({\n            title: \"Quantity Added\",\n            text: \"Waiting confirmation to Stock In\",\n            type: \"success\"\n          },function(){\n            location.reload();\n          });\n        })\n        .catch(function(error) {\n          swal(\"Failed to Added Quantity\", \"Please try again\", \"error\");\n        });\n    }\n  }\n\n})\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL3Byb2R1Y3QuanM/NzhmOSJdLCJzb3VyY2VzQ29udGVudCI6WyJWdWUuY29tcG9uZW50KCd2LXNlbGVjdCcsIFZ1ZVNlbGVjdC5WdWVTZWxlY3QpO1xudmFyIGFwcCA9IG5ldyBWdWUoe1xuICBlbDogXCIjYXBwXCIsXG5cbiAgZGF0YToge1xuICAgIHN0b2NrOiB7XG4gICAgICBtX3Byb2R1Y3RzX2lkOiAnJyxcbiAgICAgIHpvbmVfdHlwZTogJycsXG4gICAgICB3YXJlaG91c2U6ICcnLFxuICAgICAgY2VsbF9pZDogJycsXG4gICAgICBxdHk6ICcnXG4gICAgfSxcbiAgICBwcm9kdWN0OiBbXSxcbiAgICBjZWxsczogW10sXG4gICAgc2VsZWN0ZWRDZWxsOiBudWxsXG4gIH0sXG5cbiAgd2F0Y2g6IHtcbiAgICAnc3RvY2suem9uZV90eXBlJzogZnVuY3Rpb24oem9uZSkge1xuICAgICAgaWYoem9uZSl7XG4gICAgICAgIHRoaXMuZ2V0Q2VsbCgpO1xuICAgICAgfVxuICAgIH0sXG4gICAgJ3N0b2NrLndhcmVob3VzZSc6IGZ1bmN0aW9uKGNlbGwpIHtcbiAgICAgIGlmKGNlbGwpe1xuICAgICAgICB0aGlzLmdldENlbGwoKTtcbiAgICAgIH1cbiAgICB9LFxuICAgICdzZWxlY3RlZENlbGwnOiBmdW5jdGlvbihzZWxlY3RlZCkge1xuICAgICAgaWYoc2VsZWN0ZWQpe1xuICAgICAgICB0aGlzLnN0b2NrLmNlbGxfaWQgPSB0aGlzLnNlbGVjdGVkQ2VsbC5pZFxuICAgICAgfWVsc2V7XG4gICAgICAgIHRoaXMuc3RvY2suY2VsbF9pZCA9ICcnXG4gICAgICB9XG4gICAgfSxcbiAgICAnc3RvY2suY2VsbF9pZCc6IGZ1bmN0aW9uKHNlbGVjdGVkKSB7XG4gICAgICBpZighc2VsZWN0ZWQpIHtcbiAgICAgICAgdGhpcy5zdG9jay5xdHkgPSAnJztcbiAgICAgIH1cbiAgICB9XG4gIH0sXG5cbiAgbWV0aG9kczoge1xuICAgIHNob3dRdHlNb2RhbChwcm9kdWN0X2lkKSB7XG4gICAgICBjb25zdCB2bSA9IHRoaXM7XG4gICAgICBheGlvcy5nZXQoJy9hcHAvcHJvZHVjdHMvZGF0YS8nK3Byb2R1Y3RfaWQpXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgdm0ucHJvZHVjdCA9IHJlc3BvbnNlLmRhdGEuZGF0YTtcbiAgICAgICAgICB2bS5zdG9jay5tX3Byb2R1Y3RzX2lkID0gdm0ucHJvZHVjdC5pZDtcbiAgICAgICAgfSApO1xuICAgICAgJCgnI3F0eU1vZGFsJykubW9kYWwoe1xuICAgICAgICBzaG93OiB0cnVlLFxuICAgICAgICBiYWNrZHJvcDogZmFsc2UsXG4gICAgICAgIGtleWJvYXJkOiBmYWxzZVxuICAgICAgfSlcbiAgICB9LFxuICAgIGhpZGVRdHlNb2RhbCgpIHtcbiAgICAgIHRoaXMuY2VsbHMgPSBbXTtcbiAgICAgIHRoaXMuc2VsZWN0ZWRDZWxsID0gbnVsbDtcbiAgICAgIHRoaXMucHJvZHVjdCA9IFtdO1xuICAgICAgJCgnI3F0eU1vZGFsJykubW9kYWwoJ2hpZGUnKVxuICAgIH0sXG4gICAgZ2V0Q2VsbCgpIHtcbiAgICAgIGNvbnN0IHZtID0gdGhpcztcbiAgICAgIGlmKHRoaXMuc3RvY2suem9uZV90eXBlICYmIHRoaXMuc3RvY2sud2FyZWhvdXNlKXtcbiAgICAgICAgYXhpb3MuZ2V0KCcvYXBwL3BvL3JlY2VpdmUvY2VsbHMvJyt0aGlzLnN0b2NrLnpvbmVfdHlwZSsnLycrdGhpcy5zdG9jay53YXJlaG91c2UrJy8nK3RoaXMucHJvZHVjdC5tX3Byb2R1Y3RfY2F0ZWdvcmllc19pZClcbiAgICAgICAgICAudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgdm0uY2VsbHMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgdm0uc2VsZWN0ZWRDZWxsID0gcmVzcG9uc2UuZGF0YVswXTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5jYXRjaCgpO1xuICAgICAgfVxuICAgIH0sXG4gICAgc2VhcmNoQ2VsbChzZWFyY2gsIGxvYWRpbmcpIHtcbiAgICAgIGlmKHRoaXMuc3RvY2suem9uZV90eXBlICYmIHRoaXMuc3RvY2sud2FyZWhvdXNlKXtcbiAgICAgICAgY29uc3Qgdm0gPSB0aGlzO1xuICAgICAgICBsb2FkaW5nKHRydWUpXG4gICAgICAgIGF4aW9zLmdldCgnL2FwcC9wby9yZWNlaXZlL2NlbGxzL3NlYXJjaC8nK3RoaXMuc3RvY2suem9uZV90eXBlKycvJyt0aGlzLnN0b2NrLndhcmVob3VzZSsnLycrdGhpcy5wcm9kdWN0Lm1fcHJvZHVjdF9jYXRlZ29yaWVzX2lkLCB7XG4gICAgICAgICAgcGFyYW1zOiB7XG4gICAgICAgICAgICBwcmVmaXg6IHNlYXJjaFxuICAgICAgICAgIH1cbiAgICAgICAgfSkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICB2bS5jZWxscyA9IHJlc3BvbnNlLmRhdGFcbiAgICAgICAgICAgbG9hZGluZyhmYWxzZSlcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSxcbiAgICBzdWJtaXRTdG9jaygpIHtcbiAgICAgIGF4aW9zLnBvc3QoJy9hcHAvcHJvZHVjdHMvc3RvY2snLCB0aGlzLiRkYXRhLnN0b2NrKVxuICAgICAgICAudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgIHN3YWwoe1xuICAgICAgICAgICAgdGl0bGU6IFwiUXVhbnRpdHkgQWRkZWRcIixcbiAgICAgICAgICAgIHRleHQ6IFwiV2FpdGluZyBjb25maXJtYXRpb24gdG8gU3RvY2sgSW5cIixcbiAgICAgICAgICAgIHR5cGU6IFwic3VjY2Vzc1wiXG4gICAgICAgICAgfSxmdW5jdGlvbigpe1xuICAgICAgICAgICAgbG9jYXRpb24ucmVsb2FkKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbihlcnJvcikge1xuICAgICAgICAgIHN3YWwoXCJGYWlsZWQgdG8gQWRkZWQgUXVhbnRpdHlcIiwgXCJQbGVhc2UgdHJ5IGFnYWluXCIsIFwiZXJyb3JcIik7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG59KVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHJlc291cmNlcy9hc3NldHMvanMvcHJvZHVjdC5qcyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==");

/***/ }
/******/ ]);