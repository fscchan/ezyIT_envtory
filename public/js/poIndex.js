  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(function() {
    var poTable = $('#po-table').DataTable({
        processing: true,
        serverSide: true,
        filter: true,
        paging: true,
        ajax: '/app/po',
        columns: [
            { data: 'po_date', name: 'po_date' },
            { data: 'prefix', name: 'prefix' },
            { data: 'supplier', name: 'supplier' },
            { data: 'exp_date', name: 'exp_date' },
            { data: 'total_amount', name: 'total_amount' },
            { data: 'postatus', name: 'postatus' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false, class: "right action"}
        ],
    });
    $('#statusPo').change(function(){
      $('#searchForm').submit();
    })
  });

  var tableProduct = $("#product-table").DataTable({
    data:[],
    columns: [
                { "data": "no" },
                { "data": "product_po_id" },
                { "data": "product_code"  },
                { "data": "product_name"  },
                { "data": "qty", "class": "center", orderable: false },
                { "data": "received", "class": "center", orderable: false},
                { "data": "price", "class": "right" },
                { "data": "amount", "class": "right" },
    ],
    rowCallback: function (row, data) {},
    filter: false,
    info: false,
    paging: false,
    ordering: true,
    processing: true,
    retrieve: true,
    scrollX: true,
    // scrollCollapse: true,
    language: {
      emptyTable: "No Products"
    }
  });

  var invoiceList = $("#invoice-list").DataTable({
    data:[],
    columns: [
                { "data": "no" },
                { "data": "prefix"  },
                { "data": "date"  },
                { "data": "qty_received", "class": "center"  },
                { "data": "total_amount", "class": "right" },
    ],
    rowCallback: function (row, data) {},
    filter: false,
    info: false,
    paging: false,
    ordering: true,
    processing: true,
    retrieve: true,
    language: {
      emptyTable: "No Invoice Yet"
    }
  });

  $('#viewPoModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var po = button.data('po')
    $('#closePoBtn').hide();
    $.ajax({
      url: "/app/po/loadPoData",
      type: "post",
      data: { po: po }
    }).done(function(result){
      $('.prefix').text(result.po.prefix);
      $('#prefix').val(result.po.prefix);
      $('#supplier_name').text(result.po.supplier.supplier_name);
      $('#po_date').text(result.po.po_date);
      $('#email').text(result.po.email);
      tableProduct.clear().draw();
      tableProduct.rows.add(result.products).draw();
      invoiceList.clear().draw();
      invoiceList.rows.add(result.invoice).draw();
      loadFileList(result.file);
      if (result.po.close) {
        $('.modal-footer').append(result.po.close);
      }else{
        $('#closePoBtn').remove();
      }
    })
  })

  function loadFileList(data){
    var list = $('#fileList tbody');
    list.empty();
    if (data.length > 0) {
      $.each(data, function(){
        list.append('<tr><td><a href="'+this.url+'" target="_blank">'+this.title+'</a></td></tr>');
      });
    }else{
      list.append('<tr><td>There is no files</td></tr>');
    }
  }

  $('#poForm').submit(function(event){
    event.preventDefault();
  });

  function closePo(){
    swal({
      title: "Close Purchase Order?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#f39c12",
      confirmButtonText: "Yes, Close it!",
      closeOnConfirm: false
    },
    function(){
      $('#closeForm').submit();
    });
  }

  $('#uploadModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    $('.t_po_master_id').val(id)
  });
//# sourceMappingURL=poIndex.js.map
