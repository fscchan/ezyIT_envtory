function confirmDelete(id,question) {
  q = question ? question : "Data";
  swal({
    title: "Delete "+q+"?",
    text: "Deleted "+q+" Can't be restored",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, Delete!",
    cancelButtonText: "Cancel",
    closeOnConfirm: true
  }, function(){
    $('form#delete-'+id).submit();
  });
}

//# sourceMappingURL=dashboard.js.map
