<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
	use Traits\Uuids;

    protected $table = 't_invoice_detail';

    protected $fillable = ['t_invoice_master_id','m_products_id','qty','price','status',"insert_by","update_by"];
	
	public $incrementing = false;

    public function invoice()
    {
        return $this->belongsTo('App\Invoice','t_invoice_master_id','id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product','m_products_id','id');
    }
}
