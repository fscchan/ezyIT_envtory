<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierContacts extends Model {
	use Traits\Uuids;

	protected $table	= "m_suppliers_contact";
	protected $fillable	= [
		"id",
		"supplier_id",
		"contact_name",
		"email",
		"phone",
		"insert_by",
		"update_by",
		"insert_by",
		"update_by"
	];
	protected $hidden	= [];
	public $incrementing = false;
}
