<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCode extends Model
{
    protected $table = 'm_product_code';

    protected $fillable = ['m_product_categories_id','product_code','name','brand','model','submodel'];

    public function products() {
  		return $this->hasMany('App\Product','m_product_code_id','id');
  	}

    public function category() {
        return $this->belongsTo("App\ProductCategories", "m_product_categories_id", "id");
    }
}
