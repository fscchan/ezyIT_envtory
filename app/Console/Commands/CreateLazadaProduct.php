<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Spatie\ArrayToXml\ArrayToXml;

use App\Shop;
use App\Sku;
use App\Library;

class CreateLazadaProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:create-product {sku}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Created SKU to Lazada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sku = Sku::find($this->argument('sku'));
        $shops = $sku->shops()
                ->whereNotNull('account')
                ->whereNotNull('api_key')
                ->get();
        foreach ($shops as $key => $shop) {
            $this->createProduct($sku,$shop);
        }
    }

    protected function createProduct($sku,$shop)
    {
            $account = $shop->account;
            $api_key = $shop->api_key;

            $api = Library::lazadaShopApi($account, $api_key,'CreateProduct');

            $attributes = [];
            foreach ($sku->product->attributes()->get() as $key => $value) {
                $attributes[$value->name] = $value->value;
            }

            $skuAttributes = [];
            foreach ($sku->attributes()->get() as $key => $value) {
                if ($value->name !== 'Image') {
                    $skuAttributes[$value->name] = $value->value;
                }
            }

            $array = [
                'Product' => [
                    'PrimaryCategory' => $sku->product->m_product_categories_id,
                    'Attributes' => $attributes,
                    'Skus' => [
                        'Sku' => $skuAttributes
                    ]
                ]
            ];

            $xml = ArrayToXml::convert($array, 'Request', false);

            if ($api && $xml) {

                $client = new \GuzzleHttp\Client();

                try {
                    $request = $client->post($api, ['body' => $xml]);
                    $response = json_decode($request->getBody(), true);
                    if (isset($response['ErrorResponse'])) {
                      $this->comment($response['ErrorResponse']['Body']['Errors'][0]['Message']);
                    }
                } catch (HttpException $ex) {
                  $this->comment($ex);
                }

            }

    }
}
