<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Library;
use App\Product;
use App\ProductAttributes;
use App\Sku;
use App\Shop;
use App\SkuAttributes;
use App\Stock;
use App\StockHistories;
use App\SyncLog;

use Uuid;
use DateTime;

class SyncLazadaProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:sync-products {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Product with Lazada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastSync = SyncLog::where('name', 'product')
                    ->where('platform','lazada')
                    ->where('status','success')
                    ->orderBy('created_at', 'desc')
                    ->first();

        $shops = Shop::where('platform_id',1)
                ->where('account', '<>', '')
                ->where('api_key', '<>', '')
                ->get();

        $created = [];
        $updated = [];


        if ($lastSync) {

            $dateTime = new DateTime($lastSync->created_at);

            $iso8601 = $dateTime->format(DateTime::ATOM);

            $this->comment('Last Sync : '.$iso8601);

            $created = ['CreatedAfter' => $iso8601];
            $updated = ['UpdatedAfter' => $iso8601];

        }

        $sync = SyncLog::create([
            'name' => 'product',
            'platform' => 'lazada',
            'status' => 'running',
            'user' => $this->argument('user_id')
        ]);

        foreach ($shops as $key => $shop) {
            $this->getNewProduct($shop,50,0,$created);
            if ($lastSync) {
                $this->getUpdatedProduct($shop,50,0,$updated);
            }
        }


        $sync->update(['status' => 'success']);

        $this->comment('Sync Finished');

    }

    protected function getNewProduct($shop,$limit=50,$offset=0,$params=[])
    {
        $account = $shop->account;
        $api_key = $shop->api_key;

        $api = Library::lazadaShopApi($account,$api_key,'GetProducts',$limit,$offset,$params);
        if ($api) {
            $client = new \GuzzleHttp\Client();
            $res = json_decode($client->get($api)->getBody(), true);
            // $resProducts = $res["SuccessResponse"]["Body"]["Products"];
            if (!empty($res["SuccessResponse"]["Body"]["Products"])) {
                $this->saveProducts($res["SuccessResponse"]["Body"]["Products"],$shop);
                $this->getNewProduct($shop, $limit+50, $offset+50, $params);
            }
        }
    }

    protected function getUpdatedProduct($shop, $limit=50,$offset=0,$params=[])
    {
        $account = $shop->account;
        $api_key = $shop->api_key;

        $api = Library::lazadaShopApi($account,$api_key,'GetProducts',$limit,$offset,$params);
        if ($api) {
            $client = new \GuzzleHttp\Client();
            $res = json_decode($client->get($api)->getBody(), true);
            // $resProducts = $res["SuccessResponse"]["Body"]["Products"];
            // dd($resProducts);
            if (!empty($res["SuccessResponse"]["Body"]["Products"])) {
                $this->updateProducts($res["SuccessResponse"]["Body"]["Products"],$shop);
                $this->getUpdatedProduct($shop, $limit+50, $offset+50, $params);
            }
        }
    }

    protected function saveProducts($resProducts,$shop)
    {
        foreach ($resProducts as $key => $value) {

            $count = count($value['Skus']);

            $category_id = $value['PrimaryCategory'];
            $attributesData = $value['Attributes'];
            $skuData = $value['Skus'];

            for ($i=0; $i < $count; $i++) {

                $cekSku = Sku::withTrashed()->where('sku',$skuData[$i]['SellerSku'])->count();

                if (!$cekSku) {

                    $product = [
                        'm_product_categories_id' => $category_id,
                        'product_prefix' => $this->generateCode(8, substr($category_id, 0, 3)),
                        'product_code' => $this->generateCode(14, substr($category_id, 0, 3)),
                        'insert_by' => $this->argument('user_id'),
                        'update_by' => $this->argument('user_id'),
                    ];
                    $p = Product::create($product);

                    $attributes = [];
                    foreach ($attributesData as $name => $value) {
                        $attribute['id'] = Uuid::generate()->string;
                        $attribute['m_products_id'] = $p->id;
                        $attribute['name'] = $name;
                        $attribute['value'] = $value;
                        $attribute['type'] = 'attribute';
                        $attribute['created_at'] =  \Carbon\Carbon::now();
                        $attribute['updated_at'] =  \Carbon\Carbon::now();
                        array_push($attributes, $attribute);
                    }
                    ProductAttributes::insert($attributes);

                    $sku = [
                        'm_products_id' => $p->id,
                        'sku' => $skuData[$i]['SellerSku'],
                        'bundle' => null,
                        'insert_by' => $this->argument('user_id'),
                        'update_by' => $this->argument('user_id'),
                    ];
                    $s = Sku::create($sku);

                    $s->shops()->attach($shop->id);

                    $skus = [];
                    foreach ($skuData[$i] as $name => $value) {
                        $skuValue = $value;
                        if ($name == 'Images') {
                            $skuValue = json_encode($value);
                        }
                        $skuAttr['id'] = Uuid::generate()->string;
                        $skuAttr['m_product_sku_id'] = $s->id;
                        $skuAttr['name'] = $name;
                        $skuAttr['value'] = $skuValue;
                        $skuAttr['created_at'] =  \Carbon\Carbon::now();
                        $skuAttr['updated_at'] =  \Carbon\Carbon::now();
                        array_push($skus, $skuAttr);
                    }

                    $quantity = isset($skuData[$i]['quantity']) ? $skuData[$i]['quantity'] : 0;

                    if ($quantity > 0) {
                      $this->saveStock($p->id,$quantity);
                    }


                    SkuAttributes::insert($skus);

                    $this->info($skuData[$i]['SellerSku'].' Imported');

                }

            }

        }
    }

    protected function updateProducts($resProducts,$shop)
    {
        foreach ($resProducts as $key => $value) {

            $count = count($value['Skus']);

            $category_id = $value['PrimaryCategory'];
            // print_r($category_id);
            $attributesData = $value['Attributes'];
            // print_r($attributesData);
            $skuData = $value['Skus'];
            // print_r($skuData);

            if (!empty($skuData)) {

                for ($i=0; $i < $count; $i++) {

                    $cekSku = Sku::withTrashed()->where('sku',$skuData[$i]['SellerSku']);

                    if ($cekSku->count()) {

                        $p = Product::whereHas('sku', function($sku) use ($skuData, $i) {
                                $sku->where('sku',$skuData[$i]['SellerSku']);
                            })->first();

                        $this->comment($p->id);

                        $sku = $cekSku->first();
                        $this->comment($sku->id);

                        foreach ($attributesData as $name => $value) {
                            ProductAttributes::where('m_products_id', $p->id)
                                ->where('name', $name)
                                ->update(['value'=> $value]);
                        }

                        foreach ($skuData[$i] as $name => $value) {
                            $skuValue = $value;
                            if ($name == 'Images') {
                                $skuValue = json_encode($value);
                            }
                            SkuAttributes::where('m_product_sku_id', $sku->id)
                                ->where('name', $name)
                                ->update(['value'=> $skuValue]);
                        }

                        $this->info($skuData[$i]['SellerSku'].' Updated');

                    }else{

                        $product = [
                            'm_product_categories_id' => $category_id,
                            'product_prefix' => $this->generateCode(8, substr($category_id, 0, 3)),
                            'product_code' => $this->generateCode(14, substr($category_id, 0, 3)),
                            'insert_by' => $this->argument('user_id'),
                            'update_by' => $this->argument('user_id'),
                        ];
                        $newp = Product::create($product);
                        $this->comment($newp->id);

                        $attributes = [];
                        foreach ($attributesData as $name => $value) {
                            $attribute['id'] = Uuid::generate()->string;
                            $attribute['m_products_id'] = $newp->id;
                            $attribute['name'] = $name;
                            $attribute['value'] = $value;
                            $attribute['type'] = 'attribute';
                            $attribute['created_at'] =  \Carbon\Carbon::now();
                            $attribute['updated_at'] =  \Carbon\Carbon::now();
                            array_push($attributes, $attribute);
                        }
                        ProductAttributes::insert($attributes);

                        $nsku = [
                            'm_products_id' => $p->id,
                            'sku' => $skuData[$i]['SellerSku'],
                            'bundle' => null,
                            'insert_by' => $this->argument('user_id'),
                            'update_by' => $this->argument('user_id'),
                        ];
                        $news = Sku::create($nsku);
                        $this->comment($news->id);

                        $news->shops()->attach($shop->id);

                        $skus = [];
                        foreach ($skuData[$i] as $name => $value) {
                            $skuValue = $value;
                            if ($name == 'Images') {
                                $skuValue = json_encode($value);
                            }
                            $skuAttr['id'] = Uuid::generate()->string;
                            $skuAttr['m_product_sku_id'] = $news->id;
                            $skuAttr['name'] = $name;
                            $skuAttr['value'] = $skuValue;
                            $skuAttr['created_at'] =  \Carbon\Carbon::now();
                            $skuAttr['updated_at'] =  \Carbon\Carbon::now();
                            array_push($skus, $skuAttr);
                        }
                        SkuAttributes::insert($skus);

                        $this->info($skuData[$i]['SellerSku'].' Imported');

                    }
                }


            }

        }
    }

    protected function saveStock($product_id, $quantity)
    {
        $stock = [
            't_invoice_master_id' => null,
            'm_products_id' => $product_id,
            'received_date' => date('Y-m-d'),
            'batch_id' => $this->getBatchId($product_id),
            'stock_type' => 'AC',
            'status' => 3,
            'zone_type' => null
        ];
        $qty = new StockHistories([
            'qty' => $quantity,
            'note' => 'QTY FROM LAZADA'
        ]);
        $saveStock = Stock::create($stock);
        $saveStock->histories()->save($qty);
    }

    protected function getBatchId($product_id)
    {
      $product = Product::find($product_id);
      $productPrefix = strtoupper(substr($product->product_code, 0, 3));
      $firstPrefix = 'LAZ'.$productPrefix.date('dmy');
      $lastNumber = Stock::selectRaw('RIGHT(max(batch_id),4) as max_number')
                      ->where('batch_id','like',$firstPrefix.'%')
                      ->first();
      $newNumber = sprintf("%04d", $lastNumber->max_number+1);
      $prefix = $firstPrefix.$newNumber;
      return $prefix;
    }

    protected function generateCode($length,$init='')
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*_-+=.?/';
        $string = $init;
        $max = strlen($characters) - 1;
        for ($i = 2; $i < $length; $i++) {
          $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
}
