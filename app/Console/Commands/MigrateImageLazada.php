<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\ArrayToXml\ArrayToXml;

use App\Sku;
use App\File;
use App\Library;

class MigrateImageLazada extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:migrate-image {image} {sku} {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate image from system to Lazada site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $sku = Sku::find($this->argument('sku'));
      $shop = $sku->shops()
              ->whereNotNull('account')
              ->whereNotNull('api_key')
              ->first();

      if ($shop) {
        $this->migrateImage($sku,$shop);
      }

    }

    protected function migrateImage($sku,$shop)
    {
      $account = $shop->account;
      $api_key = $shop->api_key;

      $api = Library::lazadaShopApi($account, $api_key,'MigrateImage');

      $array = [
        'Image' => [
          'Url' => $this->argument('image')
        ]
      ];

      $xml = ArrayToXml::convert($array, 'Request', false);

      if ($api && $xml) {
        $client = new \GuzzleHttp\Client();

        try {

            $request = $client->post($api, ['body' => $xml]);
            $response = json_decode($request->getBody(), true);

            if ($response['SuccessResponse']) {
              $lazadaUrl = $response['SuccessResponse']['Body']['Image'];
              $this->info($lazadaUrl['Url']);
              $data = [
                'module' => 'sku',
                'module_id' => $this->argument('sku'),
                'author_id' => $this->argument('user_id'),
                'storage' => 'lazada',
                'title' => $lazadaUrl['Code'],
                'path' => $this->argument('image'),
                'filename' => $lazadaUrl['Url'],
                'extension' => pathinfo($lazadaUrl['Url'], PATHINFO_EXTENSION),
                'mime_type' => null,
                'size' => null
              ];

              $check = File::where('module_id',$this->argument('sku'))->where('filename',$lazadaUrl['Url'])->count();
              if (empty($check)) {
                File::create($data);
              }

              $this->call('lazada:update-product', [
                  'sku' => $this->argument('sku'),
              ]);

            }
        } catch (HttpException $ex) {
          $this->comment($ex);
        }
      }

    }
}
