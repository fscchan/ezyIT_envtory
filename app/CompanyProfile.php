<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model {
	use Traits\Uuids;

	protected $table = "m_company_profiles";
	protected $fillable = [
		"uid",
		"profile_type", 
		"profile_name", 
		"profile_billing_address", 
		"profile_city", 
		"profile_state", 
		"profile_zip", 
		"profile_country", 
		"profile_phone_number", 
		"profile_fax_number", 
		"profile_email_address", 
		"profile_website", 
		"profile_business_number", 
		"profile_gst", 
		"profile_currency", 
		"profile_timezone",
		"data_status",
		"insert_by",
		"update_by"
	];
	protected $hidden = [];
	
	public $incrementing = false;

	public static function companyCurrency()
	{
		$comp = CompanyProfile::first();
		if($comp){
			$cur = Library::currency($comp->profile_currency);
			return $cur->symbol_native;
		}else{
			return 'Rp';
		}
	}
}
