<?php
namespace App\Traits;

use Auth;
use App\User;
use Illuminate\Support\Facades\Schema;

trait ActorNUuid {
	protected static function boot() {
		parent::boot();

		static::creating(function ($model) {
			if(Schema::hasColumn($model->getTable(), "insert_by")) {
				$model->insert_by = Auth::user() == null ? User::first()->id : Auth::user()->id;
			}
			if(Schema::hasColumn($model->getTable(), "update_by")) {
				$model->update_by = Auth::user() == null ? User::first()->id : Auth::user()->id;
			}
		});

		static::updating(function ($model) {
			if(Schema::hasColumn($model->getTable(), "update_by")) {
				$model->update_by = Auth::user() == null ? User::first()->id : Auth::user()->id;
			}
		});
	}
}