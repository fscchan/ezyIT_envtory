<?php
namespace App\Traits;
use Carbon\Carbon;
Trait ValetTrait
{
    protected function generateCode($objectModel, $atrribute, $length=8, $prefix=''){
        $result = $objectModel->where($atrribute, 'LIKE', $prefix.'%')->max($atrribute);
        $prefixLen = strlen($prefix);
        $subPrefix = substr(trim($result),$prefixLen);
        return $prefix.(str_pad((int)$subPrefix+1, $length-$prefixLen, "0", STR_PAD_LEFT));
    }

    protected function getDateTime(){
        return Carbon::now();
    }

    protected function getHumanDateTime($dateTime=null){
        $dateTime= ($dateTime==null) ? $this->getDateTime(): $dateTime;
        $dtime = Carbon::createFromFormat('Y-m-d H:i:s',$dateTime);
        return $dtime->format('M d, Y h:i A');
    }

    protected function getHumanDate($dateTime=null){
        $dateTime= ($dateTime==null) ? $this->getDateTime(): $dateTime;

        $dateTime= ($dateTime==null) ? $this->getDateTime(): $dateTime;
        $dtime = Carbon::createFromFormat('Y-m-d H:i:s',$dateTime);
        return $dtime->format('M d, Y');
    }

    protected function getHumanFormatDate($dateTime=null){
        $dateTime= ($dateTime==null) ? $this->getDateTime(): $dateTime;
        $dtime = Carbon::createFromFormat('Y-m-d H:i:s',$dateTime);
        return $dtime->format('m/d/Y');
    }
}