<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseRacks extends Model {
	use Traits\Uuids;

    protected $table = "m_warehouse_racks";
	protected $fillable = [
		"uid",
		"name",
		"prefix",
		"lenght",
		"widht",
		"height",
		"cell_prefix",
		"cell_columns",
		"cell_rows",
		"m_warehouse_zones_id",
		"insert_by",
		"update_by"
	];
	protected $hidden = [];
	
	public $incrementing = false;
	
	public function zone() {
		return $this->belongsTo("App\WarehouseZones", "m_warehouse_zones_id", "id");
	}
	
	public function cells() {
		return $this->belongsToMany("App\WarehouseCells", "p_warehouse_racks_cells", "rack_id", "cell_id");
	}

	public function categories() {
		return $this->belongsToMany("App\ProductCategories", "m_warehouse_racks_categories", "rack_id", "category_id");
	}
}
