<?php
namespace App;

class Warehouse extends CoreModel {
	use Traits\Uuids;

    protected $table='m_warehouses';
    protected $fillable = ["uid",'name', 'is_default', 'prefix', 'address',"insert_by","update_by"];

	public $incrementing = false;

    public function contacts(){
        return $this->hasMany(\App\WarehouseContact::class, 'm_warehouse_id');
    }
	
	public function zones() {
		return $this->hasMany("App\WarehouseZones", "m_warehouse_id", "id");
	}

	public function trays() {
		return $this->hasMany("App\WarehouseTray", "m_warehouse_id", "id");
	}
}
