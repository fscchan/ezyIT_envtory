<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model {
	use Traits\Uuids;

    protected $table = "m_carriers";
	protected $fillable = [
		"id",
		"Name", 
		"Default", 
		"ApiIntegration", 
		"TrackingCodeValidationRegex", 
		"TrackingCodeExample", 
		"TrackingUrl", 
		"DeliveryOption", 
		"insert_by", 
		"update_by"
	];
	protected $hidden = [];
	public $incrementing = false;
	
	public function deliveryOptions() {
		return $this->hasMany("App\CarrierDeliveryOption", "carrier_id", "id");
	}
}
