<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingList extends Model {
//    use Traits\Uuids;
    protected $table	= "t_shippings";

    protected $fillable	= [
        "prepare_date",
        "weight",
        "packing_logistic_id",
        "courier_id",
        "order_id",
        "shipping_bin_id",
        "tracking_number",
        "courier_name"
    ];

    protected $hidden	= ['created_at','updated_at'];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function courier()
    {
        return $this->belongsTo(Carrier::class, 'courier_id', 'id');
    }


}
