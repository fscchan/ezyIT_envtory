<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseContact extends Model {
	use Traits\Uuids;

    protected $table='m_warehouse_contacts';
    protected  $fillable = [
        'pic',
        'email',
        'phone',
        'mobile',
        'm_warehouse_id',
		"insert_by",
		"update_by"
    ];
	public $incrementing = false;

}
