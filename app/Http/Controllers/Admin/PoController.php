<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PoRequest;

use App\Po;
use App\PoDetail;
use App\Supplier;
use App\Product;
use App\Library;
use App\Invoice;
use App\Stock;
use App\AuditTrails;
use App\CompanyProfile;
use App\ReportTemplate;
use Datatables;
use Barcode;
use PDF;
use Mail;
use Uuid;

class PoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = isset($_GET['status']) ? $_GET['status'] : null;
        if (request()->ajax()) {
            $status = isset($_GET['status']) ? $_GET['status'] : null;
              $po = Po::all();
            return Datatables::of($po)
              ->addColumn('supplier', function ($po) {
                  return !empty($po->supplier->supplier_name) ? $po->supplier->supplier_name : 'DELETED';
              })
              ->addColumn('postatus', function ($po) {
                  return Library::poStatus($po->status,true);
              })
              ->addColumn('actions', function ($po) {
                $batch = Stock::whereHas('invoice', function($inv) use ($po) {
                    $inv->where('t_po_master_id',$po->id);
                })->where('status',2)->count();
                $otherAction = '<div class="btn-group">
                                  <button type="button" class="btn btn-flat btn-xs btn-default" data-po="'.$po->prefix.'" data-toggle="modal" data-target="#viewPoModal"><i class="fa fa-file-text-o"></i> View</button>
                                  <button type="button" class="btn btn-default btn-flat btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                  </button>
                                  <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="po/'.$po->prefix.'"> <i class="fa fa-print"></i> Print</a></li>';
                if (Auth::user()->can('update-po')) {
                  if ($po->status != 0 && $po->status != 9) {
                    $otherAction .= '<li class="divider"></li>';
                    $otherAction .= '<li><a href="po/process?po='.$po->prefix.'"><i class="fa fa-check-square-o"></i> Process Order</a></li><li><a href="" data-toggle="modal" data-target="#uploadModal" data-id="'.$po->id.'"><i class="fa fa-upload"></i> Upload File</a></li>';
                    if ($batch) {
                      $otherAction .= '<li><a href="po/receive?po='.$po->prefix.'"><i class="fa fa-arrow-down"></i> Product Received</a></li>';
                    }
                    $otherAction .= '</ul></div> ';
                  }
                }
                if ($po->status != 1) {
                  return $otherAction;
                }
                return Library::gridAction($po,'po',$otherAction );
              })
              ->make(true);
        }
        return view('admin.po.index',[
            'pageTitle' => 'Purchase Order',
            'status' => Library::poStatus(),
            'activeStatus' => $status,
            'suppliers' => Supplier::limit(10),
            'products' => Product::limit(10)
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Po;
        $model->po_date = date('Y-m-d');
        $model->misc_cost = 0;
        $model->ship_cost = 0;
        $modelDetail = new PoDetail;
		$mailTemplate = ReportTemplate::where("name", "Email template")->first();
        return view('admin.po.form',[
            'model'=>$model,
            'modelDetail'=>$modelDetail,
            'suppliers'=>Supplier::orderBy('supplier_name')->get(),
            'pageTitle'=>'Add Purchase Order',
			'mailTemplate' => (object) ['header' => !empty($mailTemplate) ? $mailTemplate->header : null, 'footer' => !empty($mailTemplate) ? $mailTemplate->footer : null, 'missingTemplate' => !empty($mailTemplate) ? false : true]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PoRequest $request)
    {
        $data = $request->except(['searchProduct','products']);
        if (empty($request->exp_date)) {
            unset($data['exp_date']);
        }
        $data['status'] = 1;
        $dataDetail = $request->only(['products']);
        $poDetail = [];
        if ($po = Po::create($data)) {
            foreach ($dataDetail['products'] as $key => $value) {
                $value['id'] = Uuid::generate()->string;
                $value['t_po_master_id'] = $po->id;
                $value['created_at'] =  \Carbon\Carbon::now();
                $value['updated_at'] =  \Carbon\Carbon::now();
                $value['insert_by'] =  Auth::user()->id;
                $value['update_by'] =  Auth::user()->id;
                unset($value['amount']);
                array_push($poDetail,$value);
            }
            PoDetail::insert($poDetail);
            Library::saveTrail('po', 'create', $po->id, 'PREFIX : '. $po->prefix);
            $mail = [
              'subject' => $request->email_subject,
              'content' => $request->email_content,
            ];
            $this->sendEmail($po,$mail);
            return redirect('app/po')
              ->with('status', 'success')
              ->with('message', 'Purchase Order Successfully Added');
        }
    }

    protected function sendEmail($po,$mail)
    {
      $pdf = PDF::loadView('admin.po.pdf', [
                'po' => $po,
                'pageTitle' => 'PO - '.$po->prefix,
                'print' => false
              ]);
      Mail::send('email.po', ['content' => $mail['content']], function($message) use($pdf,$po,$mail)
      {
          $message->from('purchase@envitory.com', Auth::user()->firstname);

          $message->to($po->email)->subject($mail['subject']);

          $message->attachData($pdf->output(), "PO-".$po->prefix.".pdf");
      });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $po = Po::where('prefix',$id)->first();
        if (!$po) {
          abort(404);
        }
		$c = CompanyProfile::first();
        return view('admin.po.view',[
          'po' => $po,
          'pageTitle' => 'PO - '.$id,
		  'company' => $c,
		  'currency' => Library::currency($c->profile_currency)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Po::find($id);
    		if($model === null) { abort(404); }
        if ($model->status == 2) {
          abort(403);
        }
        $modelDetail = PoDetail::where('t_po_master_id',$id)->get();
		$mailTemplate = ReportTemplate::where("name", "Email template")->first();
        return view('admin.po.form',[
            'model'=>$model,
            'modelDetail'=>$modelDetail,
            'suppliers'=>Supplier::orderBy('supplier_name')->get(),
            'pageTitle'=>'Update Purchase Order',
			'mailTemplate' => (object) ['header' => @$mailTemplate->header, 'footer' => @$mailTemplate->footer, 'missingTemplate' => empty($mailTemplate->footer) || empty($mailTemplate->header) ? true : false]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except(['searchProduct','products','m_supplier_id','prefix']);

        if (empty($request->exp_date)) {
            $data['exp_date'] = null;
        }

        $dataDetail = $request->only(['products']);

        $po = Po::find($id);

        $poDetail = [];

        if ($po->update($data)) {
            foreach ($dataDetail['products'] as $key => $value) {
                unset($value['amount']);
                $value['updated_at'] =  \Carbon\Carbon::now();
                if (array_key_exists('id', $value)) {
                    unset($value['t_po_master_id']);
                    unset($value['m_product_id']);
                    PoDetail::find($value['id'])->update($value);
                }else{
                    $value['t_po_master_id'] = $po->id;
                    array_push($poDetail,$value);
                }
            }

            if (!empty($poDetail)) {
                PoDetail::insert($poDetail);
            }

            Library::saveTrail('po', 'update', $po->id, 'PREFIX : '. $po->prefix);

            return redirect('app/po')
              ->with('status', 'success')
              ->with('message', 'Purchase Order Successfully Updated');
        }

        return redirect()->back()
            ->with('status', 'danger')
            ->with('message', 'Failed to Update Purchase Order ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Po $po)
    {
        PoDetail::where('t_po_master_id',$po->id)->delete();
        $po->delete();
        Library::saveTrail('po', 'delete', $po->id, 'PREFIX : '. $po->prefix);
        return redirect()->back()
                ->with('status', 'success')
                ->with('message', 'Purchase Order Deleted');
    }

    public function searchSupplier(Request $request)
    {
        $q = $request->name;
        $search = Supplier::select('id','supplier_name')
                    ->where('supplier_name','like','%'.$q.'%')
                    ->orderBy('supplier_name')->get();
        return response()->json($search);
    }

    public function generatePrefix(Request $request)
    {
        $id = $request->supplier;
        $supplier = Supplier::find($id);
        $data = [];
        if ($supplier) {
          $supplierPrefix = strtoupper(substr($supplier->supplier_prefix, 0, 3));
          $firstPrefix = $supplierPrefix.'PO'.date('my');
          $lastNumber = Po::selectRaw('RIGHT(max(prefix),4) as max_number')
                          ->where('prefix','like',$firstPrefix.'%')
                          ->first();
          $newNumber = sprintf("%04d", $lastNumber->max_number+1);
          $prefix = $firstPrefix.$newNumber;
          $data = [
              'supplier' => Supplier::select('supplier_name','supplier_address')->where('id',$id)->first(),
              'email' => $supplier->contacts()->get(),
              'prefix' => $prefix
          ];
        }
        return response()->json($data);
    }

    public function searchProduct(Request $request)
    {
        $data = Product::with('attributes')
                    ->whereHas('supplier', function($supplier) use ($request) {
                      $supplier->where('id',$request->supplier);
                    })
                    ->where('product_code','like','%'.$request->name.'%')
                    ->get()
                    ->pluck('code_name','id');
        $products = [];
        foreach ($data as $id => $name) {
          $products[] = [
            'id' => $id,
            'product_name' => $name,
          ];
        }
        return response()->json($products);
    }

    public function getProduct(Request $request)
    {
        $product = Product::with(array('attributes'=>function($att){
          $att->where('name', 'name');
        }))->with('category')->find($request->product);
        $supplier = Supplier::find($request->supplier);
        $product->product_po_id = $this->generateProductPoId($product->product_code,$supplier->supplier_prefix);
        return response()->json($product);
    }

    protected function generateProductPoId($product_code,$supplier_prefix)
    {
      $productPrefix = strtoupper(substr($product_code, 0, 3));
      $supplierPrefix = strtoupper(substr($supplier_prefix, 0, 3));
      $firstPrefix = $supplierPrefix.$productPrefix.date('dmy');
      $lastNumber = PoDetail::selectRaw('RIGHT(max(product_po_id),4) as max_number')
                      ->where('product_po_id','like',$firstPrefix.'%')
                      ->first();
      $newNumber = sprintf("%04d", $lastNumber->max_number+1);
      $prefix = $firstPrefix.$newNumber;
      return $prefix;
    }

    public function printPo($prefix)
    {
        $po = Po::where('prefix',$prefix)->first();
        if (!$po) {
          abort(404);
        }
        return view('admin.po.print',[
          'po' => $po,
          'pageTitle' => 'PO - '.$prefix,
		  "company" => CompanyProfile::first(),
		  "currency" => Library::currency(Library::defaultCurrency()),
          'print' => true
        ]);
    }

    public function getPdf($prefix)
    {
      $po = Po::where('prefix',$prefix)->first();
      if (!$po) {
        abort(404);
      }
      $pdf = PDF::loadView('admin.po.pdf', [
                'po' => $po,
                'pageTitle' => 'PO - '.$prefix,
                'print' => false
              ]);
      return $pdf->download('PO - '.$prefix.'.pdf');
    }

    public function close(Request $request)
    {
      $po = Po::where('prefix',$request->purchase_prefix)->first();
      if ($request->purchase_prefix) {
        if ($po->update(['status'=>0])) {
          return redirect()->back()
                ->with('status', 'info')
                ->with('message', 'Purchase Order Successfully Closed');
        }
      }
      return redirect()->back()
            ->with('status', 'danger')
            ->with('message', 'Failed to Close Purchase Order');
    }
	
	public function delItem(Request $request) {
		PoDetail::where("id", $request->input("i"))->delete();
		return ["status" => true];
	}

}
