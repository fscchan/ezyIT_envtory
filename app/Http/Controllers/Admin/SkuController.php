<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SkuPriceRequest;

use App\Sku;
use App\SkuPrice;
use App\SkuPriceHistories;
use App\Product;
use App\ProductAttributes;
use App\SkuAttributes;
use App\Stock;
use App\Shop;
use App\Zones;
use App\Library;
use App\File;

use Uuid;
use Datatables;
use Artisan;
use Image;
use Storage;
use Auth;

class SkuController extends Controller
{
    public function index()
    {
        $shop_id = isset($_GET['shop']) ? $_GET['shop'] : null;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $status = isset($_GET['status']) ? $_GET['status'] : '';
        $qty = isset($_GET['qty']) ? $_GET['qty'] : '';

        $filter = [
            'shop' => $shop_id,
            'limit' => $limit,
            'status' => $status,
            'qty' => $qty,
            'search' => $search,
        ];

        $sku = Sku::with('shops')->with('product')
                ->whereHas('product', function($product) {
                  $product->has('code');
                });

        if ($status) {
          if ($status == 'bundle') {
            $sku = Sku::where('bundle',1);
          }else{
            $sku->whereHas('attributes', function($attribute) use ($status) {
              $attribute->where('name','Status')->where('value', $status);
            });
          }
        }
        if ($shop_id) {
            $sku->whereHas('shops', function($shop) use ($shop_id) {
                $shop->where('id',$shop_id);
            });
        }
        if ($search) {
            $sku->where('sku', 'like', '%'.$search.'%')
            ->orWhereHas('product', function($product) use ($search) {
                $product->where('product_code', 'like', '%'.$search.'%')
                ->orwhereHas('attributes', function($attribute) use ($search) {
                    $attribute->where('name', 'name')->where('value', 'like', '%'.$search.'%');
                });
            });
        }
        if ($qty) {
            switch ($qty) {
                case 'save':
                    break;
                case 'below-save':
                    break;
                case 'empty':
                    break;

                default:
                    # code...
                    break;
            }
        }

        return view('admin.sku.index', [
            'sku' => $sku->paginate($limit),
            'pageTitle' => 'Manage SKU',
            'filter' => $filter
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Sku;
		$model->clone = false;
        $shops = Shop::all();
        return view('admin.sku.form', [
            'pageTitle' => 'Register SKU',
            'model' => $model,
  			'next' => null,
  			'previous' => null,
            'shops' => $shops
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataAttributes = [];
        $dataSkuAttributes = [];
        $attributes = $request->only('attribute');
        $skuAttributes = $request->only('sku');
        $skuAttributes['sku']['price'] = 1;
        $product = Product::find($request->id);
        if($product) {
            $sku = Sku::create([
                'm_products_id' => $product->id,
                'sku' => $request->sku['SellerSku'],
                'retail_price' => $request->retail_price,
                'min_price' => $request->min_price,
                'max_price' => $request->max_price,
                'save_qty' => $request->qty['save_qty'],
                'max_shelved_life' => $request->qty['max_shelved_life'],
                'min_qty' => $request->qty['min_qty'],
                'max_qty' => $request->qty['max_qty'],
                'bundle' => null,
            ]);
            foreach ($skuAttributes['sku'] as $name => $value) {
                if (!empty($value)) {
                    $skuAttr['id'] = Uuid::generate()->string;
                    $skuAttr['m_product_sku_id'] = $sku->id;
                    $skuAttr['name'] = $name;
                    $skuAttr['value'] = $value;
                    $skuAttr['created_at'] =  \Carbon\Carbon::now();
                    $skuAttr['updated_at'] =  \Carbon\Carbon::now();
                    array_push($dataSkuAttributes,$skuAttr);
                }
            }
            foreach ($attributes['attribute'] as $name => $value) {
                if (!empty($value)) {
                    $check = ProductAttributes::where('m_products_id',$product->id)
                                ->where('name',$name);
                    if ($check->count()) {
                        $check->update(['value'=>$value]);
                    }else{
                        $attribute['id'] = Uuid::generate()->string;
                        $attribute['m_products_id'] = $product->id;
                        $attribute['name'] = $name;
                        $attribute['value'] = $value;
                        $attribute['created_at'] =  \Carbon\Carbon::now();
                        $attribute['updated_at'] =  \Carbon\Carbon::now();
                        array_push($dataAttributes,$attribute);
                    }
                }else{
                    ProductAttributes::where('m_products_id',$product->id)
                                ->where('name',$name)
                                ->delete();
                }
            }
            ProductAttributes::insert($dataAttributes);
            SkuAttributes::insert($dataSkuAttributes);

            foreach ($request->price as $key => $price) {
              $priceData = [];
              if (!empty($price['price'])) {

                $priceData['sku_id'] = $sku->id;
                $priceData['shop_id'] = $price['shop_id'];
                $priceData['price'] = $price['price'];
                $priceData['special_price'] = $price['special_price'];
                $priceData['special_from_date'] = $price['special_from_date'];
                $priceData['special_to_date'] = $price['special_to_date'];

                if ($price['price'] + 0 == 0) {
                    $priceData['special_price'] = null;
                    $priceData['special_from_date'] = null;
                    $priceData['special_to_date'] = null;
                }

                SkuPrice::create($priceData);
                SkuPriceHistories::create($priceData);
              }
            }

            if (!empty($request->shop)) {
                $sku->shops()->attach($request->shop);
                Artisan::queue('lazada:create-product', [
                    'sku' => $sku->id
                ]);
            }

            if (!empty($request->images)) {
              foreach ($request->images as $key => $image) {
                $title = Library::randomString(12);
                $save = File::add64('sku',$sku->id,$image,$title,'public/sku');
              }
            }


            return redirect("app/sku")
                    ->with("status", "success")
                    ->with("message", "SKU Registered");
        }
        return redirect("app/sku")
                    ->with("status", "danger")
                    ->with("message", "Failed to Register SKU");
    }

    public function storePrice(SkuPriceRequest $request)
    {
        $data = $request->only('sku_id','shop_id');

        $priceData = $request->except('sku_id','shop_id');

        if ($request->special_price + 0 == 0) {
            $priceData['special_price'] = null;
            $priceData['special_from_date'] = null;
            $priceData['special_to_date'] = null;
        }

        $skuPrice = $request->only(['retail_price','max_price','min_price']);

        $sku = Sku::find($request->sku_id);

        $sku->update($skuPrice);

        $price = SkuPrice::where('sku_id', $request->sku_id)->where('shop_id', $request->shop_id)->first();

        $message = [];
        $message['status'] = 'success';

        if ($price) {
            $price->update($priceData);
            $message['action'] = 'update';
        }else{
            $price = SkuPrice::create(array_merge($data,$priceData));
            $message['action'] = 'create';
        }

        SkuPriceHistories::create(array_merge($data,$priceData));

        $update = ['price','special_price','special_from_date','special_to_date'];

        foreach ($update as $key => $name) {
          $skuAttr = SkuAttributes::where('m_product_sku_id',$sku->id)
                    ->where('name',$name);

          if (isset($priceData[$name])) {
            $dataPrice = [
              'name' => $name,
              'value' => $priceData[$name],
            ];

            if ($skuAttr->count()) {
              $skuAttr->update($dataPrice);
            }else{
              $qty = new SkuAttributes(array_merge($dataPrice,['id'=>Uuid::generate()->string]));
              $sku->attributes()->save($qty);
            }
          }else{
            $skuAttr->delete();
          }
        }


        Artisan::queue('lazada:update-price-qty', [
            'product_id' => $sku->product->id
        ]);

        return ['price'=>SkuPrice::where('sku_id', $request->sku_id)->get(),'message'=>$message];
    }

    public function setQtySetting(Request $request)
    {
        $sku = Sku::find($request->sku_id);
        $data = $request->except(['sku_id','active','cold']);
        $sku->update($data);
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getSku($id)
    {
        $sku = Sku::with(['product.attributes' => function($attribute) {
            $attribute->where('name', 'name')->first();
        }])->with('shops')->find($id);
        $qty = [
            'active' => Stock::active($sku->product),
            'cold' => Stock::cold($sku->product)
        ];
        $data = array_merge($sku->toArray(),$qty);
        return $data;
    }

    public function getQty($id)
    {
      $qty = [
          'active' => Stock::active($id),
          'cold' => Stock::cold($id)
      ];
      return $qty;
    }

    public function getSkuPrice($sku_id,$shop_id)
    {
        $price = [];
        $skuPrice = SkuPrice::where('sku_id',$sku_id)->where('shop_id',$shop_id)->first();
        if ($skuPrice) {
            $price = $skuPrice;
        }
        return $price;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $models = Sku::where("id", ">=", $id)->take(2)->get();
		$models[0]->clone = false;
		$previous = @Sku::where("id", "<=", $id)->orderBy("id", "desc")->take(2)->get()[1];
        $shops = Shop::all();
        return view('admin.sku.form', [
            'pageTitle' => 'Register SKU',
            'model' => $models[0],
			'next' => !empty(@$models[1]) ? $models[1] : null,
			'previous' => !empty($previous) ? $previous : null,
            'shops' => $shops
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->shop;
        $sku = Sku::find($id);
        $product = $sku->product;
        $dataAttributes = [];
        $dataSkuAttributes = [];
        $attributes = $request->only('attribute');
        $skuAttributes = $request->only('sku');

        if($product) {
            foreach ($skuAttributes['sku'] as $name => $value) {
                if (!empty($value)) {
                    $check = SkuAttributes::where('m_product_sku_id',$sku->id)
                            ->where('name',$name);
                    if ($check->count()) {
                        $check->update(['value'=>$value]);
                    }else{
                        $skuAttr['id'] = Uuid::generate()->string;
                        $skuAttr['m_product_sku_id'] = $sku->id;
                        $skuAttr['name'] = $name;
                        $skuAttr['value'] = $value;
                        $skuAttr['created_at'] =  \Carbon\Carbon::now();
                        $skuAttr['updated_at'] =  \Carbon\Carbon::now();
                        array_push($dataSkuAttributes,$skuAttr);
                    }
                }
            }
            foreach ($attributes['attribute'] as $name => $value) {
                if (!empty($value)) {
                    $check = ProductAttributes::where('m_products_id',$product->id)
                                ->where('name',$name);
                    if ($check->count()) {
                        $check->update(['value'=>$value]);
                    }else{
                        $attribute['id'] = Uuid::generate()->string;
                        $attribute['m_products_id'] = $product->id;
                        $attribute['name'] = $name;
                        $attribute['value'] = $value;
                        $attribute['created_at'] =  \Carbon\Carbon::now();
                        $attribute['updated_at'] =  \Carbon\Carbon::now();
                        array_push($dataAttributes,$attribute);
                    }
                }else{
                    ProductAttributes::where('m_products_id',$product->id)
                                ->where('name',$name)
                                ->delete();
                }
            }
            ProductAttributes::insert($dataAttributes);
            SkuAttributes::insert($dataSkuAttributes);

            if (!empty($sku->shops()->get())) {
                $sku->shops()->sync($request->shop);
            }else{
                $sku->shops()->attach($request->shop);
            }

            return redirect("app/sku")
                    ->with("status", "success")
                    ->with("message", "SKU Updated");
        }
        return redirect("app/sku")
                    ->with("status", "danger")
                    ->with("message", "Failed to Register SKU");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Library::saveTrail("sku", "destroy", $id, "Sku: " . Sku::find($id)->sku);
        Sku::find($id)->delete();
        return redirect("app/sku")->with("status", "success")->with("message", "SKU deleted successfully");
    }

    public function getProducts()
    {
        $products = [];
        $data = Product::has('sku','<',1)->has('code')->limit(5)->get()->pluck('code_name','id');
        foreach ($data as $key => $value) {
            $product = [
                'id' => $key,
                'product' => $value
            ];
            array_push($products, $product);
        }
        return $products;
    }

    public function searchProduct($q)
    {
        $products = [];
        if ($q) {
            $data = Product::whereHas('attributes', function($attr) use ($q) {
                $attr->where('name', 'name')
                    ->where('value', 'like', '%'.$q.'%');
            })
            ->orWhere('product_code', 'like', '%'.$q.'%')
            ->has('sku', '<', 1)
            ->get()
            ->pluck('code_name','id');
            foreach ($data as $key => $value) {
                $cekSku = Sku::withTrashed()->where('m_products_id',$key)->count();
                if (empty($cekSku)) {
                    $product = [
                        'id' => $key,
                        'product' => $value
                    ];
                    array_push($products, $product);
                }
            }
        }
        return $products;
    }

    public function xml($sku)
    {
        $xml = Sku::xml($sku);
        if ($xml) {
            return $xml;
        }
        abort(404);
    }

    public function indexImage()
    {
      $sku = Sku::with('images');

      $status = isset($_GET['status']) ? $_GET['status'] : '';
      $search = isset($_GET['search']) ? $_GET['search'] : '';
      $filter = [
        'status' => $status,
        'search' => $search
      ];

      $total = [
        'all' => Sku::count(),
        'missing' => Sku::has('images', '<', 1)->count(),
      ];

      if ($status) {
        $sku->has('images', '<', 1);
      }

      if ($search) {
          $sku->where('sku', 'like', '%'.$search.'%')
          ->orWhereHas('product', function($product) use ($search) {
              $product->where('product_code', 'like', '%'.$search.'%')
              ->orWhereHas('attributes', function($attribute) use ($search) {
                  $attribute->where('name', 'name')->where('value', 'like', '%'.$search.'%');
              });
          });
      }


      return view('admin.sku.image', [
        'sku' => $sku->paginate(10),
        'pageTitle' => 'Manage Product Image',
        'filter' => $filter,
        'total' => $total
      ]);
    }

    public function setImage(Request $request)
    {
      $title = Library::randomString(12);
      $save = File::add64('sku',$request->sku,$request->image,$title,'public/sku');
      if ($save) {
        Artisan::queue('lazada:migrate-image', [
            'image' => asset(Storage::url('sku/'.$save->filename)),
            'sku' => $request->sku,
            'user_id' => Auth::user()->id
        ]);
      }
      return ['save'=>$save];
    }

    public function removeImage($id)
    {
      $file = File::find($id);
      if ($file->delete()) {
        $lazadaFile = File::where('storage','lazada')
          ->where('path',$file->filename)->delete();
        Storage::delete('public/sku/'.$file->filename);

        Artisan::queue('lazada:update-product', [
            'sku' => $file->module_id,
        ]);
      }
      return ['message'=>'deleted'];
    }

    public function setStatus(Request $request)
    {
      $status = SkuAttributes::where('m_product_sku_id',$request->sku)
                ->where('name','Status');
      if ($status->update(['value'=>$request->status])) {
        Artisan::queue('lazada:update-product', [
            'sku' => $request->sku
        ]);
        return ['message'=>'success'];
      }else{
        return ['message'=>'error'];
      }
    }

	public function cloneSKU($id) {
        $model = Sku::findOrFail($id);
		$model->exists = true;
		$model->clone = true;
        $shops = Shop::all();
        return view('admin.sku.form', [
            'pageTitle' => 'Register SKU',
            'model' => $model,
			'next' => null,
			'previous' => null,
            'shops' => $shops
        ]);
	}

}
