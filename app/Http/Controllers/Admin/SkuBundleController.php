<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Sku;
use App\SkuBundle;
use App\SkuAttributes;
use App\Product;
use App\ProductAttributes;
use App\Shop;
use App\SkuPrice;
use App\SkuPriceHistories;
use App\Library;
use App\File;
use Artisan;
use Uuid;

class SkuBundleController extends Controller
{
    public function searchSku($q)
    {
      $skuList = [];
      if ($q) {
        $sku = Sku::with('product')->where('sku', 'like', '%'.$q.'%')
        ->whereHas('product', function($product){
          $product->has('code');
        })
        ->get();
        if ($sku) {
          foreach ($sku as $key => $s) {
            array_push($skuList, ['id'=>$s->id,'sku'=>$s->sku.' - '.$s->product->attribute('name')]);
          }
        }
      }
      return $skuList;
    }

    public function getSkuData()
    {
      $sku_id = $_GET['sku'];
      $sku = Sku::with('prices')->with('product.code')->with('product.attributes')->whereIn('id', $sku_id)->get();
      return $sku;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'You Should Not See This';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.sku.formBundle', [
        'model' => new Sku,
        'shops' => Shop::all(),
        'pageTitle' => 'Create a SKU Bundle',
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
        //  return $request->all();
         $dataAttributes = [];
         $dataSkuAttributes = [];
         $attributes = $request->only('attribute');
         $skuAttributes = $request->only('sku');
         $skuAttributes['sku']['price'] = 1;

         $sku = Sku::create([
           'm_products_id' => null,
           'sku' => $request->attribute['product_code'],
           'retail_price' => $request->retail_price,
           'min_price' => $request->min_price,
           'max_price' => $request->max_price,
           'save_qty' => $request->qty['save_qty'],
           'max_shelved_life' => $request->qty['max_shelved_life'],
           'min_qty' => $request->qty['min_qty'],
           'max_qty' => $request->qty['max_qty'],
           'bundle' => 1,
           'bundle_qty' => $request->qty['bundle_qty'],
         ]);

         if ($sku) {
           foreach ($request->bundle as $key => $bundle) {
             $bundleData = [
               'bundle_id' => $sku->id,
               'sku_id' => $bundle['sku_id'],
               'qty' => $bundle['qty'],
             ];
             SkuBundle::create($bundleData);
           }

           foreach (array_merge($skuAttributes['sku'],$attributes['attribute']) as $name => $value) {
             if (!empty($value)) {
               if ($name == 'SellerSku') {
                 $value = $request->attribute['product_code'];
               }
               $skuAttr['id'] = Uuid::generate()->string;
               $skuAttr['m_product_sku_id'] = $sku->id;
               $skuAttr['name'] = $name;
               $skuAttr['value'] = $value;
               $skuAttr['created_at'] =  \Carbon\Carbon::now();
               $skuAttr['updated_at'] =  \Carbon\Carbon::now();
               array_push($dataSkuAttributes,$skuAttr);
             }
           }
           SkuAttributes::insert($dataSkuAttributes);

           foreach ($request->price as $key => $price) {
             $priceData = [];
             if (!empty($price['price'])) {

               $priceData['sku_id'] = $sku->id;
               $priceData['shop_id'] = $price['shop_id'];
               $priceData['price'] = $price['price'];
               $priceData['special_price'] = $price['special_price'];
               $priceData['special_from_date'] = $price['special_from_date'];
               $priceData['special_to_date'] = $price['special_to_date'];

               if ($price['price'] + 0 == 0) {
                 $priceData['special_price'] = null;
                 $priceData['special_from_date'] = null;
                 $priceData['special_to_date'] = null;
               }

               SkuPrice::create($priceData);
               SkuPriceHistories::create($priceData);
             }
           }

            if (!empty($request->shop)) {
              $sku->shops()->attach($request->shop);
              Artisan::queue('lazada:create-product', [
              'sku' => $sku->id
              ]);
            }

           if (!empty($request->images)) {
             foreach ($request->images as $key => $image) {
               $title = Library::randomString(12);
               $save = File::add64('sku',$sku->id,$image,$title,'public/sku');
             }
           }

           return redirect("app/sku")
           ->with("status", "success")
           ->with("message", "Bundle SKU Created");
         }


         return redirect("app/sku")
           ->with("status", "danger")
           ->with("message", "Failed to Bundle SKU");
     }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
