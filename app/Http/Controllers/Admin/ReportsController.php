<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\ReportTemplate;
use App\ProductCategories;
use App\Warehouse;
use App\WarehouseCells;
use App\Stock;
use App\User;

use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class ReportsController extends Controller {
	public function _construct() {
		DB::enableQueryLog(); // debugging. delete when done. outout query using DB::getQueryLog()
	}
	
	private static function getDateDiff($floor, $ceil) {
		if($ceil < $floor) {
			return [];
		} else {
			$return[0] = substr($floor, 0, 10);
			$floor = date_format(date_create_from_format("Y-m-d", $return[0]), "U");
			$ceil = date_format(date_create_from_format("Y-m-d", substr($ceil, 0, 10)), "U");
			$diff = $ceil - $floor;
			for($i = 0; $floor < $ceil; $i++){
				$floor += 86400;
				$return[] = date_format(date_create_from_format("U", $floor), "Y-m-d");
			}
			return $return;
		}
	}
	
	public function index() {
		return view("admin.reports.index", [
			"pageTitle"	=> "Reports",
			"active"	=> ["setting", "reports"]
		]);
	}

	public function show() {}
	public function create() {}
	public function edit() {}
	public function store() {}
	public function update() {}
	public function destroy() {}

	private function setSize($size) {
		switch(strtoupper(@$size)) {
			default:
			case "A4":
			case "LEGAL":
			case "LETTER": {
				return 12;
			}
			case "A3": {
				return 24;
			}
			case "A5": {
				return 6;
			}
		}
	}

	public function bestSellingSKU(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$byCat = DB::select("select `m_product_categories`.`cat_name`, sum(`t_order_detail`.`qty`) as `sku_sold` from `t_order_detail` inner join `m_product_sku` on `m_product_sku`.`id` = `t_order_detail`.`product_sku_id` inner join `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` inner join `m_product_categories` on `m_products`.`m_product_categories_id` = `m_product_categories`.`id` where `t_order_detail`.`created_at` between :dateFloor and :dateCeil group by `m_product_categories`.`cat_name` order by `sku_sold` desc", $filter);
		$bySku = DB::select("select `m_product_sku`.`sku`, `m_products`.`id` as `product_id`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, `m_product_categories`.`cat_name`, sum(`t_order_detail`.`qty`) as `sku_sold` from `t_order_detail` inner join `m_product_sku` on `m_product_sku`.`id` = `t_order_detail`.`product_sku_id` inner join `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` inner join `m_product_categories` on `m_products`.`m_product_categories_id` = `m_product_categories`.`id` where `t_order_detail`.`created_at` between :dateFloor and :dateCeil group by `m_product_categories`.`cat_name` order by `sku_sold` desc", $filter);
		/* $bestSellingSKUChart = Lava::DataTable();
		foreach($bySku as $o){
			$bestSellingSKUChart->addNumberColumn($o->sku);
			$t[] = $o->sku_sold;
		}
		$bestSellingSKUChart->addRow($t); */
		$bestSellingSKUChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Sold");
		foreach($bySku as $o){
			$bestSellingSKUChart->addRow([
				$o->sku,
				$o->sku_sold
			]);
		}
		Lava::BarChart('bestSellingSKUChart', $bestSellingSKUChart, [
			"orientation" => "horizontal",
			"events" => [
				"ready" => "readyCallback"
			]
		]);
		return view("admin.reports.bestSellingSKU",[
			"title" => "Best Selling SKU",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"byCategory" => $byCat,
			"bySKU" => $bySku
		]);
	}

	public function dailyPriceVsDailySales(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		if(!empty($request->input("sku"))) { $filter["sku"] = $request->input("sku"); }
		$dailySales = !empty($request->input("sku")) ?
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `m_product_sku`.`sku` = :sku AND `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter) :
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter);
		$dailySalesChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Price")->addNumberColumn("Sales");
		foreach($dailySales as $o){
			$dailySalesChart->addRow([
				$o->sku,
				$o->average_price,
				$o->average_price * $o->sku_sold
			]);
		}
		Lava::ComboChart('dailySalesChart', $dailySalesChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		]);
		return view("admin.reports.dailyAvgVsDailySales",[
			"title" => "Daily average vs daily sales",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"dailySales" => $dailySales
		]);
	}

	public function emptyCell(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = ["dateRange" => $request->input("date") ? $request->input("date") : date("Y-m-d")];
		$emptyCells = WarehouseCells::whereDoesntHave("stock", function($cells) use($filter) {
			$cells->where("created_at", "like", $filter["dateRange"] . "%");
		})->get();
		$emptyCategories = ProductCategories::whereHas("racks", function($eca) use($filter) {
			$eca->whereHas("cells", function($ecb) use($filter) {
				$ecb->whereDoesntHave("stock", function($cells) use($filter) {
					$cells->where("created_at", "like", $filter["dateRange"] . "%");
				});
			});
		})->get();
		// $emptyCategories = DB::select("SELECT `m_warehouse_types`.`name` as `type_name`, `m_warehouse_zones_categories`.`category_id` as `category_id`, (SELECT count(`m_products`.`id`) FROM `m_products` WHERE `m_products`.`m_product_categories_id` = `category_id` AND `m_products`.`created_at` = :dateRange) as `product_count` FROM `m_warehouse_zones_categories` INNER JOIN `m_warehouse_types` ON `m_warehouse_types`.`id` = `m_warehouse_zones_categories`.`warehouse_zones_id` WHERE `product_count` = 0;", $filter);
		return view("admin.reports.emptyCell",[
			"title" => "Empty cells",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"emptyCells" => $emptyCells,
			"emptyCategories" => $emptyCategories
		]);
	}

	public function fastMovingSKU(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$fastSKU = DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`  FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `m_product_sku`.`sku` ORDER BY `sku_sold` DESC", $filter);
		$fastSKUChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Sold");
		foreach($fastSKU as $o){
			$fastSKUChart->addRow([
				$o->sku,
				$o->sku_sold
			]);
		}
		Lava::BarChart('fastMovingSKUChart', $fastSKUChart, [
			"orientation" => "horizontal",
			"events" => [
				"ready" => "readyCallback"
			]
		]);
		return view("admin.reports.fastMovingSKU",[
			"title" => "Fast moving SKU",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"fastSKU" => $fastSKU
		]);
	}

	public function slowMovingSKU(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$slowSKU = DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`  FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `m_product_sku`.`sku` ORDER BY `sku_sold` ASC", $filter);
		$slowSKUChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Sold");
		foreach($slowSKU as $o){
			$slowSKUChart->addRow([
				$o->sku,
				$o->sku_sold
			]);
		}
		Lava::BarChart('slowMovingSKUChart', $slowSKUChart, [
			"orientation" => "horizontal",
			"events" => [
				"ready" => "readyCallback"
			]
		]);
		return view("admin.reports.slowMovingSKU",[
			"title" => "Slow moving SKU",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"slowSKU" => $slowSKU
		]);
	}

	public function newBatch_StockInReport(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$stockIn = DB::select("SELECT `t_stocks`.`received_date` as `received_date`, `t_stocks`.`batch_id`, `m_warehouse_zones`.`name` as `zone_name`, `m_warehouse_racks`.`name` as `rack_name`, `m_warehouse_cells`.`prefix` as `cell_name`, `m_products`.`id` as `product_id`, `m_products`.`product_prefix`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, (SELECT count(`id`) FROM `m_product_sku` WHERE `m_product_sku`.`m_products_id` = `product_id` AND `m_product_sku`.`created_at` LIKE concat(`received_date`, '%')) as `product_qty` FROM `t_stocks` INNER JOIN `m_warehouse_cells` ON `t_stocks`.`cell_id` = `m_warehouse_cells`.`id` INNER JOIN `m_warehouse_racks` ON `m_warehouse_cells`.`m_warehouse_racks_id` = `m_warehouse_racks`.`id` INNER JOIN `m_warehouse_zones` ON `m_warehouse_racks`.`m_warehouse_zones_id` = `m_warehouse_zones`.`id` INNER JOIN `m_products` ON `t_stocks`.`m_products_id` = `m_products`.`id` WHERE `t_stocks`.`received_date` BETWEEN :dateFloor AND :dateCeil", $filter);
		$staff = User::find(Auth::user()->id);
		return view("admin.reports.newBatch_StockInReport",[
			"title" => "New batch & stock in report",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"stockIn" => $stockIn,
			"staff" => $staff
		]);
	}

	public function salesBySKU(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		if(!empty($request->input("sku"))) { $filter["sku"] = $request->input("sku"); }
		// $dailySales = !empty($request->input("sku")) ? DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `m_product_sku`.`sku` = :sku AND `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter) : DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter);
		$dailySales = !empty($request->input("sku")) ?
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `m_product_sku`.`sku` = :sku AND `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter) :
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter);
		$dailySalesChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Price")->addNumberColumn("Sales");
		foreach($dailySales as $o){
			$dailySalesChart->addRow([
				$o->sku,
				$o->average_price,
				$o->average_price * $o->sku_sold
			]);
		}
		Lava::ComboChart('dailySalesChart', $dailySalesChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		]);
		return view("admin.reports.salesBySKU",[
			"title" => "Sales by SKU",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"salesBySKU" => $dailySales
		]);
	}

	public function stockLevel(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		$stock = Stock::whereBetween("updated_at", [$filter["dateFloor"], $filter["dateCeil"]])->get(); 
		return view("admin.reports.stockLevel",[
			"title" => "Stock level",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"stock" => $stock
		]);
	}
	
	public function warehouseOccupancy(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.warehouseOccupancy"); }
		/* $warehouse = Warehouse::whereHas("zones", function($a) use($filter){
			$a->whereHas("racks", function($b) use($filter){
				$b->whereHas("cells", function($c) use($filter){
					$c->whereHas("stocks", function($d) use($filter){
						$d->whereBetween("updated_at", [$filter["dateFloor"], $filter["dateCeil"]]);
					});
				});
			});
		}); */
		$warehouse = [];
		foreach(Self::getDateDiff($filter["dateFloor"], $filter["dateCeil"]) as $date) {
			$results = DB::select("SELECT 
					`m_warehouses`.`prefix`, 
					`m_warehouses`.`name`, 
					COUNT(`m_warehouse_cells`.`id`) AS `total_cells`,
					`empty_cells_table`.`empty_cells` AS `empty_cells`,
					((COUNT(`m_warehouse_cells`.`id`) - `empty_cells`) / COUNT(`m_warehouse_cells`.`id`)) * 100 AS `percentage`,
					? AS `date`
				FROM `m_warehouse_cells` 
				INNER JOIN `m_warehouse_racks` 
				ON `m_warehouse_racks`.`id` = `m_warehouse_cells`.`m_warehouse_racks_id` 
				INNER JOIN `m_warehouse_zones` 
				ON `m_warehouse_zones`.`id` = `m_warehouse_racks`.`m_warehouse_zones_id`
				INNER JOIN `m_warehouses` 
				ON `m_warehouses`.`id` = `m_warehouse_zones`.`m_warehouse_id` 
				INNER JOIN (
					SELECT 
						`m_warehouses`.`prefix`, 
						`m_warehouses`.`name`, 
						COUNT(`m_warehouse_cells`.`id`) AS `empty_cells`
					FROM `m_warehouse_cells` 
					INNER JOIN `m_warehouse_racks` 
					ON `m_warehouse_racks`.`id` = `m_warehouse_cells`.`m_warehouse_racks_id` 
					INNER JOIN `m_warehouse_zones` 
					ON `m_warehouse_zones`.`id` = `m_warehouse_racks`.`m_warehouse_zones_id`
					INNER JOIN `m_warehouses` 
					ON `m_warehouses`.`id` = `m_warehouse_zones`.`m_warehouse_id` 
					WHERE NOT EXISTS (
						SELECT * 
						FROM `t_stocks` 
						WHERE 
							`t_stocks`.`cell_id` = `m_warehouse_cells`.`id` AND 
							`updated_at` LIKE ?
					)
					GROUP BY `m_warehouses`.`prefix`
				) AS `empty_cells_table`
				ON `empty_cells_table`.`prefix` = `m_warehouses`.`prefix`
				GROUP BY `m_warehouses`.`prefix`;", [$date, $date . "%"]
			);
			$warehouse = array_merge($warehouse, $results);
		}
		$warehouseOccupancyChart->addRows($chartDataB);
		Lava::LineChart('warehouseOccupancyChart', $warehouseOccupancyChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		]);
		return view("admin.reports.warehouseOccupancy",[
			"title" => "Warehouse occupancy",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"warehouse" => $warehouse
		]);
	}
	
	public function staffPickingReport(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d"),
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d"),
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.staffPickingReport"); }
		$results = collect(DB::select("SELECT 
				`t_picking_lists`.`date`,
				CONCAT(IFNULL(`users`.`lastname`, 'NO_LAST_NAME'), ' ', IFNULL(`users`.`firstname`, 'NO_FIRST_NAME')) AS `staff_name`,
				SEC_TO_TIME(AVG(TIME_TO_SEC(`t_picking_lists`.`end`) - TIME_TO_SEC(`t_picking_lists`.`start`))) AS `average_time`,
				AVG(TIME_TO_SEC(`t_picking_lists`.`end`) - TIME_TO_SEC(`t_picking_lists`.`start`)) AS `average_time_in_sec`
			FROM `t_picking_lists`
			INNER JOIN `users`
			ON `users`.`id` = `t_picking_lists`.`user_id`
			WHERE `t_picking_lists`.`status` = '2' AND `date` BETWEEN :dateFloor AND :dateCeil
			GROUP BY `t_picking_lists`.`date`;", $filter
		));
		$staffPickingChart = Lava::DataTable()->addDateColumn("Date");
		$chartStaffs = $results->pluck("staff_name")->unique();
		foreach($chartStaffs as $i => $o){
			$staffPickingChart->addNumberColumn($o);
			$chartDataOrder[$o] = $i + 1;
		}
		$chartData = [];
		for($i = 0, $dt = @$results[0]->date; $i < count($results); $i++){
			$chartData[$results[$i]->date][$chartDataOrder[$results[$i]->staff_name]] = $results[$i]->average_time_in_sec;
		}
		$chartDataB = []; $ii = 0; foreach($chartData as $i => $o){
			$chartDataB[$ii++] = array_merge([$i], $o);
		}
		$staffPickingChart->addRows($chartDataB);
		Lava::LineChart('staffPickingChart', $staffPickingChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		])->NumberFormat([
			"suffix" => "seconds"
		]);
		return view("admin.reports.staffPickingReport",[
			"title" => "Staff Picking Report",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"pickingReport" => $results
		]);
	}

	public function staffPackingReport(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d"),
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d"),
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.staffPackingReport"); }
		$results = collect(DB::select("SELECT
				*
			FROM (
				SELECT 
					SUBSTRING(`t_packings`.`updated_at`, 1, 10) AS `date`,
					CONCAT(IFNULL(`users`.`lastname`, 'NO_LAST_NAME'), ' ', IFNULL(`users`.`firstname`, 'NO_FIRST_NAME')) AS `staff_name`,
					SEC_TO_TIME(AVG(TIME_TO_SEC(SUBSTRING(`t_packings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_packings`.`created_at`, 12, 10)))) AS `average_time`,
					AVG(TIME_TO_SEC(SUBSTRING(`t_packings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_packings`.`created_at`, 12, 10))) AS `average_time_in_sec`
				FROM `t_packings`
				INNER JOIN `users`
				ON `users`.`id` = `t_packings`.`update_by`
				WHERE `t_packings`.`status` = '2'
				GROUP BY `date`
			) AS `t`
			WHERE `date` BETWEEN :dateFloor AND :dateCeil;", $filter
		));
		$staffPackingChart = Lava::DataTable()->addDateColumn("Date");
		$chartStaffs = $results->pluck("staff_name")->unique();
		foreach($chartStaffs as $i => $o){
			$staffPackingChart->addNumberColumn($o);
			$chartDataOrder[$o] = $i + 1;
		}
		$chartData = [];
		for($i = 0, $dt = @$results[0]->date; $i < count($results); $i++){
			$chartData[$results[$i]->date][$chartDataOrder[$results[$i]->staff_name]] = $results[$i]->average_time_in_sec;
		}
		$chartDataB = []; $ii = 0; foreach($chartData as $i => $o){
			$chartDataB[$ii++] = array_merge([$i], $o);
		}
		$staffPackingChart->addRows($chartDataB);
		Lava::LineChart('staffPackingChart', $staffPackingChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		])->NumberFormat([
			"suffix" => "seconds"
		]);
		return view("admin.reports.staffPackingReport",[
			"title" => "Staff Packing Report",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"packingReport" => $results
		]);
	}

	public function staffShippingReport(Request $request) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d"),
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d"),
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.staffShippingReport"); }
		$results = collect(DB::select("SELECT
				*
			FROM (
				SELECT 
					SUBSTRING(`t_shippings`.`updated_at`, 1, 10) AS `date`,
					CONCAT(IFNULL(`users`.`lastname`, 'NO_LAST_NAME'), ' ', IFNULL(`users`.`firstname`, 'NO_FIRST_NAME')) AS `staff_name`,
					SEC_TO_TIME(AVG(TIME_TO_SEC(SUBSTRING(`t_shippings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_shippings`.`created_at`, 12, 10)))) AS `average_time`,
					AVG(TIME_TO_SEC(SUBSTRING(`t_shippings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_shippings`.`created_at`, 12, 10))) AS `average_time_in_sec`
				FROM `t_shippings`
				INNER JOIN `users`
				ON `users`.`id` = `t_shippings`.`update_by`
				WHERE `t_shippings`.`status` = '2'
				GROUP BY `date`
			) AS `t`
			WHERE `date` BETWEEN :dateFloor AND :dateCeil;", $filter
		));
		$staffShippingChart = Lava::DataTable()->addDateColumn("Date");
		$chartStaffs = $results->pluck("staff_name")->unique();
		foreach($chartStaffs as $i => $o){
			$staffShippingChart->addNumberColumn($o);
			$chartDataOrder[$o] = $i + 1;
		}
		$chartData = [];
		for($i = 0, $dt = @$results[0]->date; $i < count($results); $i++){
			$chartData[$results[$i]->date][$chartDataOrder[$results[$i]->staff_name]] = $results[$i]->average_time_in_sec;
		}
		$chartDataB = []; $ii = 0; foreach($chartData as $i => $o){
			$chartDataB[$ii++] = array_merge([$i], $o);
		}
		$staffShippingChart->addRows($chartDataB);
		Lava::LineChart('staffShippingChart', $staffShippingChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		])->NumberFormat([
			"suffix" => "seconds"
		]);
		return view("admin.reports.staffShippingReport",[
			"title" => "Staff Shipping Report",
			"fontSize" => $fontSize,
			"template" => $template,
			"filter" => (object) $filter,
			"shippingReport" => $results
		]);
	}

	public function bestSellingSKUExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$byCat = DB::select("select `m_product_categories`.`cat_name`, sum(`t_order_detail`.`qty`) as `sku_sold` from `t_order_detail` inner join `m_product_sku` on `m_product_sku`.`id` = `t_order_detail`.`product_sku_id` inner join `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` inner join `m_product_categories` on `m_products`.`m_product_categories_id` = `m_product_categories`.`id` where `t_order_detail`.`created_at` between :dateFloor and :dateCeil group by `m_product_categories`.`cat_name` order by `sku_sold` desc", $filter);
		$bySku = DB::select("select `m_product_sku`.`sku`, `m_products`.`id` as `product_id`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, `m_product_categories`.`cat_name`, sum(`t_order_detail`.`qty`) as `sku_sold` from `t_order_detail` inner join `m_product_sku` on `m_product_sku`.`id` = `t_order_detail`.`product_sku_id` inner join `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` inner join `m_product_categories` on `m_products`.`m_product_categories_id` = `m_product_categories`.`id` where `t_order_detail`.`created_at` between :dateFloor and :dateCeil group by `m_product_categories`.`cat_name` order by `sku_sold` desc", $filter);
		$bestSellingSKUChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Sold");
		foreach($bySku as $o){
			$bestSellingSKUChart->addRow([
				$o->sku,
				$o->sku_sold
			]);
		}
		Lava::BarChart('bestSellingSKUChart', $bestSellingSKUChart, [
			"orientation" => "horizontal"
		]);

		switch($type) {
			case "xls": {
				$export = ["byCat" => $byCat, "bySKU" => $bySku];
				Excel::create("best_selling_sku_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("By category", function($sheet) use($export) {
						array_unshift($export["byCat"], ["No.", "Category", "Total amount"]);
						$sheet->fromArray($export["byCat"], null, "A1", false, false);
					});
					$excel->sheet("By amount", function($sheet) use($export) {
						array_unshift($export["bySKU"], ["No.", "SKU", "Description", "Category", "Total amount"]);
						$sheet->fromArray($export["bySKU"], null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadHTML(view("layouts.report.bestSellingSKU",[
					"title" => "Best Selling SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"byCategory" => $byCat,
					"bySKU" => $bySku,
					"chart" => $request->input("chartImage")
				])->render())->stream("best_selling_sku_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.bestSellingSKU",[
					"title" => "Best Selling SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"byCategory" => $byCat,
					"bySKU" => $bySku,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function dailyPriceVsDailySalesExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		if(!empty($request->input("sku"))) { $filter["sku"] = $request->input("sku"); }
		$dailySales = !empty($request->input("sku")) ?
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `m_product_sku`.`sku` = :sku AND `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter) :
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter);
		$dailySalesChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Price")->addNumberColumn("Sales");
		foreach($dailySales as $o){
			$dailySalesChart->addRow([
				$o->sku,
				$o->average_price,
				$o->average_price * $o->sku_sold
			]);
		}
		Lava::ComboChart('dailySalesChart', $dailySalesChart);

		switch($type) {
			case "xls": {
					$export = $dailySales;
					Excel::create("daily_price_vs_daily_sales_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						array_unshift($export, ["Date", "SKU", "Description", "Category", "Price sold", "Qty sold", "Balance Qty", "Total amount"]);
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.dailyAvgVsDailySales",[
					"title" => "Daily average vs daily sales",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"dailySales" => $dailySales,
					"chart" => $request->input("chartImage")
				])->stream("daily_price_vs_daily_sales_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.dailyAvgVsDailySales",[
					"title" => "Daily average vs daily sales",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"dailySales" => $dailySales,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function emptyCellExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = ["dateRange" => $request->input("date") ? $request->input("date") : date("Y-m-d")];
		$model["emptyCells"] = WarehouseCells::whereDoesntHave("stock", function($cells) use($filter) {
			$cells->where("created_at", "like", $filter["dateRange"] . "%");
		})->get();
		$model["emptyCategories"] = ProductCategories::whereHas("racks", function($eca) use($filter) {
			$eca->whereHas("cells", function($ecb) use($filter) {
				$ecb->whereDoesntHave("stock", function($cells) use($filter) {
					$cells->where("created_at", "like", $filter["dateRange"] . "%");
				});
			});
		})->get();
		
		switch($type) {
			case "xls": {
				$export = ["emptyCells" => [], "emptyCategories" => []];
				for($i = 0; $i < count($model["emptyCells"]); $i++) {
					$export["emptyCells"][$i]["No"] = $i + 1;
					$export["emptyCells"][$i]["zone_category"] = @$model["emptyCells"][$i]->rack()->first()->zone()->first()->name;
					$export["emptyCells"][$i]["rack_id"] = @$model["emptyCells"][$i]->rack()->first()->prefix;
					$export["emptyCells"][$i]["cell_id"] = @$model["emptyCells"][$i]->prefix;
				}
				for($i = 0; $i < count($model["emptyCategories"]); $i++) {
					$export["emptyCategories"][$i]["No"] = $i + 1;
					$export["emptyCategories"][$i]["zone_category"] = @$model["emptyCategories"][$i]->cat_name;
				}
				Excel::create("empty_cells_" . preg_replace("/\D/i", "", $filter["dateRange"]), function($excel) use($export) {
					$excel->sheet("Empty cells", function($sheet) use($export) {
						$sheet->fromArray($export["emptyCells"], null, "A1", false, false);
					});
					$excel->sheet("Empty categories", function($sheet) use($export) {
						$sheet->fromArray($export["emptyCategories"], null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.emptyCell",[
					"title" => "Empty cells",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"emptyCells" => $model["emptyCells"],
					"emptyCategories" => $model["emptyCategories"],
					"chart" => $request->input("chartImage")
				])->stream("emptycell_" . preg_replace("/\D/i", "", $filter["dateRange"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.emptyCell",[
					"title" => "Empty cells",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"emptyCells" => $model["emptyCells"],
					"emptyCategories" => $model["emptyCategories"],
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function fastMovingSKUExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$fastSKU = DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`  FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `m_product_sku`.`sku` ORDER BY `sku_sold` DESC", $filter);
		$fastSKUChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Sold");
		foreach($fastSKU as $o){
			$fastSKUChart->addRow([
				$o->sku,
				$o->sku_sold
			]);
		}
		Lava::BarChart('fastMovingSKUChart', $fastSKUChart, ["orientation" => "horizontal"]);

		switch($type) {
			case "xls": {
				$export = $fastSKU;
				Excel::create("categories_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						array_unshift($export, ["No.", "SKU", "Description", "Category", "Qty sold", "Balance Qty", "Total amount"]);
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.fastMovingSKU",[
					"title" => "Fast moving SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"fastSKU" => $fastSKU,
					"chart" => $request->input("chartImage")
				])->stream("fast_moving_sku_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.fastMovingSKU",[
					"title" => "Fast moving SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"fastSKU" => $fastSKU,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function slowMovingSKUExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$slowSKU = DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`  FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `m_product_sku`.`sku` ORDER BY `sku_sold` ASC", $filter);
		$slowSKUChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Sold");
		foreach($slowSKU as $o){
			$slowSKUChart->addRow([
				$o->sku,
				$o->sku_sold
			]);
		}
		Lava::BarChart('slowMovingSKUChart', $slowSKUChart, ["orientation" => "horizontal"]);

		switch($type) {
			case "xls": {
				$export = $slowSKU;
				Excel::create("categories_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						array_unshift($export, ["No.", "SKU", "Description", "Category", "Qty sold", "Balance Qty", "Total amount"]);
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.slowMovingSKU",[
					"title" => "Slow moving SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"slowSKU" => $slowSKU,
					"chart" => $request->input("chartImage")
				])->stream("slow_moving_sku_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.slowMovingSKU",[
					"title" => "Slow moving SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"slowSKU" => $slowSKU,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function newBatch_StockInReportExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59"
		];
		$stockIn = DB::select("SELECT `t_stocks`.`received_date` as `received_date`, `t_stocks`.`batch_id`, `m_warehouse_zones`.`name` as `zone_name`, `m_warehouse_racks`.`name` as `rack_name`, `m_warehouse_cells`.`prefix` as `cell_name`, `m_products`.`id` as `product_id`, `m_products`.`product_prefix`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, (SELECT count(`id`) FROM `m_product_sku` WHERE `m_product_sku`.`m_products_id` = `product_id` AND `m_product_sku`.`created_at` LIKE concat(`received_date`, '%')) as `product_qty` FROM `t_stocks` INNER JOIN `m_warehouse_cells` ON `t_stocks`.`cell_id` = `m_warehouse_cells`.`id` INNER JOIN `m_warehouse_racks` ON `m_warehouse_cells`.`m_warehouse_racks_id` = `m_warehouse_racks`.`id` INNER JOIN `m_warehouse_zones` ON `m_warehouse_racks`.`m_warehouse_zones_id` = `m_warehouse_zones`.`id` INNER JOIN `m_products` ON `t_stocks`.`m_products_id` = `m_products`.`id` WHERE `t_stocks`.`received_date` BETWEEN :dateFloor AND :dateCeil", $filter);
		$staff = User::find(Auth::user()->id);

		switch($type) {
			case "xls": {
				$export = $stockIn;
				Excel::create("categories_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						array_unshift($export, ["No.", "Date", "Active batch ID", "Zone ID", "Rack ID", "Cell ID", "SKU", "Description", "Qty"]);
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.newBatch_StockInReport",[
					"title" => "New batch & stock in report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"stockIn" => $stockIn,
					"staff" => $staff
				])->stream("new_batch-stock_in_report_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.newBatch_StockInReport",[
					"title" => "New batch & stock in report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"stockIn" => $stockIn,
					"staff" => $staff
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function salesBySKUExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		if(!empty($request->input("sku"))) { $filter["sku"] = $request->input("sku"); }
		// $dailySales = !empty($request->input("sku")) ? DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `m_product_sku`.`sku` = :sku AND `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter) : DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter);
		$dailySales = !empty($request->input("sku")) ?
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `m_product_sku`.`sku` = :sku AND `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter) :
			DB::select("SELECT `m_products`.`id` as `product_id`, `m_product_sku`.`sku`, `m_product_categories`.`cat_name`, (select `value` from `m_product_attributes` where `m_products_id` = `product_id` and `name` = 'description') as `product_sku_description`, avg(`t_order_detail`.`price`) as `average_price`, sum(`t_order_detail`.`qty`) as `sku_sold`, DATE(`t_order_detail`.`created_at`) as `order_date` FROM `t_order_detail` INNER JOIN `m_product_sku` on `t_order_detail`.`product_sku_id` = `m_product_sku`.`id` INNER JOIN `m_products` on `m_products`.`id` = `m_product_sku`.`m_products_id` INNER JOIN `m_product_categories` ON `m_products`.`m_product_categories_id` = `m_product_categories`.`id` WHERE `t_order_detail`.`created_at` BETWEEN :dateFloor AND :dateCeil GROUP BY `order_date`, `m_product_sku`.`sku` ORDER BY `order_date` ASC", $filter);
		$dailySalesChart = Lava::DataTable()->addStringColumn("SKU")->addNumberColumn("Price")->addNumberColumn("Sales");
		foreach($dailySales as $o){
			$dailySalesChart->addRow([
				$o->sku,
				$o->average_price,
				$o->average_price * $o->sku_sold
			]);
		}
		Lava::ComboChart('dailySalesChart', $dailySalesChart);

		switch($type) {
			case "xls": {
				$export = $dailySales;
				Excel::create("categories_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						array_unshift($export, ["Date", "SKU", "Description", "Category", "Qty sold", "Balance Qty", "Total amount"]);
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.salesBySKU",[
					"title" => "Sales by SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"salesBySKU" => $dailySales,
					"chart" => $request->input("chartImage")
				])->stream("sales_by_sku_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.salesBySKU",[
					"title" => "Sales by SKU",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"salesBySKU" => $dailySales,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function stockLevelExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		$stock = Stock::whereBetween("updated_at", [$filter["dateFloor"], $filter["dateCeil"]])->get();
		
		switch($type) {
			case "xls": {
				$export = ["activeBatch" => [], "coldBatch" => []];
				for ($i = 0; $i < count($stock); $i++) {
					$product = $stock[$i]->product()->first();
					$export["activeBatch"][$i]["No"] = $i + 1;
					$export["activeBatch"][$i]["sku"] = @$product->sku()->first()->sku;
					$export["activeBatch"][$i]["description"] = @$product->attribute("description") ? $product->attribute("description") : $product->attribute("short_description");
					$export["activeBatch"][$i]["category"] = @$product->category()->first()->cat_name;
					$export["activeBatch"][$i]["quantity"] = Stock::active($product);
					$export["activeBatch"][$i]["batch_id"] = $o->batch_id;

					$export["coldBatch"][$i]["No"] = $export["activeBatch"][$i]["No"];
					$export["coldBatch"][$i]["sku"] = $export["activeBatch"][$i]["sku"];
					$export["coldBatch"][$i]["description"] = $export["activeBatch"][$i]["description"];
					$export["coldBatch"][$i]["category"] = $export["activeBatch"][$i]["category"];
					$export["coldBatch"][$i]["quantity"] = Stock::cold($product);
					$export["coldBatch"][$i]["batch_id"] = $export["activeBatch"][$i]["batch_id"];
				}
				Excel::create("stock_level_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Active batch", function($sheet) use($export) {
						$sheet->fromArray($export["activeBatch"], null, "A1", false, false);
					});
					$excel->sheet("Cold batch", function($sheet) use($export) {
						$sheet->fromArray($export["coldBatch"], null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.stockLevel",[
					"title" => "Stock level",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"stock" => $stock
				])->stream("stock_level_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.stockLevel",[
					"title" => "Stock level",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"stock" => $stock
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}
	
	public function warehouseOccupancyExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d") . " 00:00:00",
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d") . " 23:59:59",
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.warehouseOccupancy"); }
		/* $warehouse = Warehouse::whereHas("zones", function($a) use($filter){
			$a->whereHas("racks", function($b) use($filter){
				$b->whereHas("cells", function($c) use($filter){
					$c->whereHas("stocks", function($d) use($filter){
						$d->whereBetween("updated_at", [$filter["dateFloor"], $filter["dateCeil"]]);
					});
				});
			});
		}); */
		$warehouse = [];
		foreach(Self::getDateDiff($filter["dateFloor"], $filter["dateCeil"]) as $date) {
			$results = DB::select("SELECT 
					`m_warehouses`.`prefix`, 
					`m_warehouses`.`name`, 
					COUNT(`m_warehouse_cells`.`id`) AS `total_cells`,
					`empty_cells_table`.`empty_cells` AS `empty_cells`,
					((COUNT(`m_warehouse_cells`.`id`) - `empty_cells`) / COUNT(`m_warehouse_cells`.`id`)) * 100 AS `percentage`,
					? AS `date`
				FROM `m_warehouse_cells` 
				INNER JOIN `m_warehouse_racks` 
				ON `m_warehouse_racks`.`id` = `m_warehouse_cells`.`m_warehouse_racks_id` 
				INNER JOIN `m_warehouse_zones` 
				ON `m_warehouse_zones`.`id` = `m_warehouse_racks`.`m_warehouse_zones_id`
				INNER JOIN `m_warehouses` 
				ON `m_warehouses`.`id` = `m_warehouse_zones`.`m_warehouse_id` 
				INNER JOIN (
					SELECT 
						`m_warehouses`.`prefix`, 
						`m_warehouses`.`name`, 
						COUNT(`m_warehouse_cells`.`id`) AS `empty_cells`
					FROM `m_warehouse_cells` 
					INNER JOIN `m_warehouse_racks` 
					ON `m_warehouse_racks`.`id` = `m_warehouse_cells`.`m_warehouse_racks_id` 
					INNER JOIN `m_warehouse_zones` 
					ON `m_warehouse_zones`.`id` = `m_warehouse_racks`.`m_warehouse_zones_id`
					INNER JOIN `m_warehouses` 
					ON `m_warehouses`.`id` = `m_warehouse_zones`.`m_warehouse_id` 
					WHERE NOT EXISTS (
						SELECT * 
						FROM `t_stocks` 
						WHERE 
							`t_stocks`.`cell_id` = `m_warehouse_cells`.`id` AND 
							`updated_at` LIKE ?
					)
					GROUP BY `m_warehouses`.`prefix`
				) AS `empty_cells_table`
				ON `empty_cells_table`.`prefix` = `m_warehouses`.`prefix`
				GROUP BY `m_warehouses`.`prefix`;", [$date, $date . "%"]
			);
			$warehouse = array_merge($warehouse, $results);
		}
		$warehouse = collect($warehouse);
		$warehouseOccupancyChart = Lava::DataTable()->addDateColumn("Date");
		$chartWarehouses = $warehouse->pluck("prefix")->unique();
		foreach($chartWarehouses as $i => $o){
			$warehouseOccupancyChart->addNumberColumn($o);
			$chartDataOrder[$o] = $i + 1;
		}
		$chartDataA = [];
		for($i = 0, $dt = $warehouse[0]->date; $i < count($warehouse); $i++){
			$chartDataA[$warehouse[$i]->date][$chartDataOrder[$warehouse[$i]->prefix]] = $warehouse[$i]->percentage;
		}
		$chartDataB = [];
		foreach($chartDataA as $i => $o){
			$chartDataB[] = array_merge([$i], $o);
		}
		$warehouseOccupancyChart->addRows($chartDataB);
		Lava::LineChart('warehouseOccupancyChart', $warehouseOccupancyChart);
		
		switch($type) {
			case "xls": {
				$export = $warehouse->toArray();
				array_unshift($export, (object) ["prefix" => "Warehouse prefix", "name" => "Warehouse name", "total_cells" => "Total cells", "empty_cells" => "Empty cells", "percentage" => "Occupancy percentage", "date" => "Date"]);
				for($i = 0; $i < count($export); $i++) {
					$export[$i] = (array) $export[$i];
				}
				Excel::create("warehouse_occupancy_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.warehouseOccupancy",[
					"title" => "Warehouse occupancy",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"warehouse" => $warehouse,
					"chart" => $request->input("chartImage")
				])->stream("warehouse_occupancy_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.warehouseOccupancy",[
					"title" => "Warehouse occupancy",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"warehouse" => $warehouse,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}
	
	public function staffPickingReportExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d"),
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d"),
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.staffPickingReport"); }
		$results = collect(DB::select("SELECT 
				`t_picking_lists`.`date`,
				CONCAT(IFNULL(`users`.`lastname`, 'NO_LAST_NAME'), ' ', IFNULL(`users`.`firstname`, 'NO_FIRST_NAME')) AS `staff_name`,
				SEC_TO_TIME(AVG(TIME_TO_SEC(`t_picking_lists`.`end`) - TIME_TO_SEC(`t_picking_lists`.`start`))) AS `average_time`,
				AVG(TIME_TO_SEC(`t_picking_lists`.`end`) - TIME_TO_SEC(`t_picking_lists`.`start`)) AS `average_time_in_sec`
			FROM `t_picking_lists`
			INNER JOIN `users`
			ON `users`.`id` = `t_picking_lists`.`user_id`
			WHERE `t_picking_lists`.`status` = '2' AND `date` BETWEEN :dateFloor AND :dateCeil
			GROUP BY `t_picking_lists`.`date`;", $filter
		));
		$staffPickingChart = Lava::DataTable()->addDateColumn("Date");
		$chartStaffs = $results->pluck("staff_name")->unique();
		foreach($chartStaffs as $i => $o){
			$staffPickingChart->addNumberColumn($o);
			$chartDataOrder[$o] = $i + 1;
		}
		$chartData = [];
		for($i = 0, $dt = @$results[0]->date; $i < count($results); $i++){
			$chartData[$results[$i]->date][$chartDataOrder[$results[$i]->staff_name]] = $results[$i]->average_time_in_sec;
		}
		$chartDataB = []; $ii = 0; foreach($chartData as $i => $o){
			$chartDataB[$ii++] = array_merge([$i], $o);
		}
		$staffPickingChart->addRows($chartDataB);
		Lava::LineChart('staffPickingChart', $staffPickingChart)->NumberFormat([
			"suffix" => "seconds"
		]);
		
		switch($type) {
			case "xls": {
				$export = $results->toArray();
				array_unshift($export, (object) ["date" => "Date", "staff_name" => "Staff name", "average_time" => "Average time", "average_time_in_sec" => "Average time (in seconds)"]);
				for($i = 0; $i < count($export); $i++) {
					$export[$i] = (array) $export[$i];
				}
				Excel::create("staff_picking_report_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.staffPickingReport",[
					"title" => "Staff Picking Report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"pickingReport" => $results,
					"chart" => $request->input("chartImage")
				])->stream("staff_picking_report_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("layouts.report.staffPickingReport",[
					"title" => "Staff Picking Report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"pickingReport" => $results,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function staffPackingReportExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d"),
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d"),
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.staffPackingReport"); }
		$results = collect(DB::select("SELECT
			*
			FROM (
				SELECT 
					SUBSTRING(`t_packings`.`updated_at`, 1, 10) AS `date`,
					CONCAT(IFNULL(`users`.`lastname`, 'NO_LAST_NAME'), ' ', IFNULL(`users`.`firstname`, 'NO_FIRST_NAME')) AS `staff_name`,
					SEC_TO_TIME(AVG(TIME_TO_SEC(SUBSTRING(`t_packings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_packings`.`created_at`, 12, 10)))) AS `average_time`,
					AVG(TIME_TO_SEC(SUBSTRING(`t_packings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_packings`.`created_at`, 12, 10))) AS `average_time_in_sec`
				FROM `t_packings`
				INNER JOIN `users`
				ON `users`.`id` = `t_packings`.`update_by`
				WHERE `t_packings`.`status` = '2'
				GROUP BY `date`
			) AS `t`
			WHERE `date` BETWEEN :dateFloor AND :dateCeil;", $filter
		));
		$staffPackingChart = Lava::DataTable()->addDateColumn("Date");
		$chartStaffs = $results->pluck("staff_name")->unique();
		foreach($chartStaffs as $i => $o){
			$staffPackingChart->addNumberColumn($o);
			$chartDataOrder[$o] = $i + 1;
		}
		$chartData = [];
		for($i = 0, $dt = @$results[0]->date; $i < count($results); $i++){
			$chartData[$results[$i]->date][$chartDataOrder[$results[$i]->staff_name]] = $results[$i]->average_time_in_sec;
		}
		$chartDataB = []; $ii = 0; foreach($chartData as $i => $o){
			$chartDataB[$ii++] = array_merge([$i], $o);
		}
		$staffPackingChart->addRows($chartDataB);
		Lava::LineChart('staffPackingChart', $staffPackingChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		])->NumberFormat([
			"suffix" => "seconds"
		]);
		
		switch($type) {
			case "xls": {
				$export = $results->toArray();
				array_unshift($export, (object) ["date" => "Date", "staff_name" => "Staff name", "average_time" => "Average time", "average_time_in_sec" => "Average time (in seconds)"]);
				for($i = 0; $i < count($export); $i++) {
					$export[$i] = (array) $export[$i];
				}
				Excel::create("staff_packing_report_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("layouts.report.staffPackingReport",[
					"title" => "Staff Packing Report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"pickingReport" => $results,
					"chart" => $request->input("chartImage")
				])->stream("staff_picking_report_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("admin.reports.staffPackingReport",[
					"title" => "Staff Packing Report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"packingReport" => $results,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}

	public function staffShippingReportExport(Request $request, $type) {
		$template = $request->input("template") == null ? ReportTemplate::first() : ReportTemplate::find($request->input("template"));
		$fontSize = $this->setSize($template->paper_size);
		$filter = [
			"dateFloor" => !empty($request->input("floor")) ? $request->input("floor") : date("Y-m-d"),
			"dateCeil" => !empty($request->input("ceil")) ? $request->input("ceil") : date("Y-m-d"),
		];
		if($filter["dateCeil"] < $filter["dateFloor"]){ return redirect()->route("reports.staffShippingReport"); }
		$results = collect(DB::select("SELECT
				*
			FROM (
				SELECT 
					SUBSTRING(`t_shippings`.`updated_at`, 1, 10) AS `date`,
					CONCAT(IFNULL(`users`.`lastname`, 'NO_LAST_NAME'), ' ', IFNULL(`users`.`firstname`, 'NO_FIRST_NAME')) AS `staff_name`,
					SEC_TO_TIME(AVG(TIME_TO_SEC(SUBSTRING(`t_shippings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_shippings`.`created_at`, 12, 10)))) AS `average_time`,
					AVG(TIME_TO_SEC(SUBSTRING(`t_shippings`.`updated_at`, 12, 10)) - TIME_TO_SEC(SUBSTRING(`t_shippings`.`created_at`, 12, 10))) AS `average_time_in_sec`
				FROM `t_shippings`
				INNER JOIN `users`
				ON `users`.`id` = `t_shippings`.`update_by`
				WHERE `t_shippings`.`status` = '2'
				GROUP BY `date`
			) AS `t`
			WHERE `date` BETWEEN :dateFloor AND :dateCeil;", $filter
		));
		$staffShippingChart = Lava::DataTable()->addDateColumn("Date");
		$chartStaffs = $results->pluck("staff_name")->unique();
		foreach($chartStaffs as $i => $o){
			$staffShippingChart->addNumberColumn($o);
			$chartDataOrder[$o] = $i + 1;
		}
		$chartData = [];
		for($i = 0, $dt = @$results[0]->date; $i < count($results); $i++){
			$chartData[$results[$i]->date][$chartDataOrder[$results[$i]->staff_name]] = $results[$i]->average_time_in_sec;
		}
		$chartDataB = []; $ii = 0; foreach($chartData as $i => $o){
			$chartDataB[$ii++] = array_merge([$i], $o);
		}
		$staffShippingChart->addRows($chartDataB);
		Lava::LineChart('staffShippingChart', $staffShippingChart, [
			"events" => [
				"ready" => "readyCallback"
			]
		])->NumberFormat([
			"suffix" => "seconds"
		]);
		
		switch($type) {
			case "xls": {
				$export = $results->toArray();
				array_unshift($export, (object) ["date" => "Date", "staff_name" => "Staff name", "average_time" => "Average time", "average_time_in_sec" => "Average time (in seconds)"]);
				for($i = 0; $i < count($export); $i++) {
					$export[$i] = (array) $export[$i];
				}
				Excel::create("staff_shipping_report_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				return PDF::loadView("admin.reports.staffShippingReport",[
					"title" => "Staff Shipping Report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"shippingReport" => $results,
					"chart" => $request->input("chartImage")
				])->stream("staff_picking_report_" . preg_replace("/\D/i", "", $filter["dateFloor"]) . "-" . preg_replace("/\D/i", "", $filter["dateCeil"]) . ".pdf");
				break;
			}
			case "html": {
				return view("admin.reports.staffShippingReport",[
					"title" => "Staff Shipping Report",
					"fontSize" => $fontSize,
					"template" => $template,
					"filter" => (object) $filter,
					"shippingReport" => $results,
					"chart" => null
				]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}
	
	public function testChart() {
		$stocksTable = Lava::DataTable()->addDateColumn("Day of Month")->addNumberColumn("Projected")->addNumberColumn("Official");
		for ($a = 1; $a < 30; $a++) {
			$stocksTable->addRow([
				"2017-03-" . $a, 
				rand(800, 1000), 
				rand(800, 1000)
			]);
		}
		$chart = Lava::ComboChart('MyStocks', $stocksTable);
		echo '<div id="stocks-chart"></div>';
		echo Lava::render('ComboChart', 'MyStocks', 'stocks-chart');
	}
}
