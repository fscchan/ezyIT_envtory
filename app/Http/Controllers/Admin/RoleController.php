<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RoleRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Role;
use App\Permission;
use App\ModuleLevel;
use App\Library;
use App\AuditTrails;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $roles = Role::all();
      return view('admin.role.index',[
        'roles'=>$roles,
        'pageTitle'=>'Role'
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        Library::saveTrail("role", "create", Role::create(['name'=>strtolower($request->name)])->id);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $role = Role::find($id);
	  if(!$role) { abort(404); }
	  $modules = ModuleLevel::pluck("level", "module");
      return view('admin.role.permission',[
        'role'=>$role,
        'modules'=>Library::modules(),
        'permission'=>Library::permission(),
        'pageTitle' => ucwords($role->name).' Permissions',
		'levels' => $modules ? $modules : []
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $role = Role::find($request->role);
      $allPermission = Permission::pluck('name');
      $newPermission = $request->permission;
      foreach ($allPermission as $p) {
        $role->revokePermissionTo($p);
      }
      if ($newPermission) {
        $role->givePermissionTo($newPermission);
      }
	  ModuleLevel::truncate();
	  foreach($request->input("level") as $module => $level) {
		ModuleLevel::create(["module" => $module, "level" => $level]);
	  }
	  Library::saveTrail("role", "update", $role->id);
      return redirect('/app/role')
        ->with('status', 'success')
        ->with('message', 'Permission for '.$role->name.' Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		Library::saveTrail("role", "destroy", $id, "Name: " . Role::find($id)->name);
    }
}
