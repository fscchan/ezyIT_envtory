<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProfileRequest;

use App\User;
use App\Library;

use Auth;
use Yajra\Datatables\Facades\Datatables;

class ProfileController extends Controller {
	public function show() {
		//
	}

	public function edit($id) {
		if(Auth::user()->id != $id) {
			abort(403);
		}
		$model		= User::find(Auth::user()->id);
		return view("admin.profile.form",[
		  "pageTitle"	=> "Edit Profile",
		  "model"		=> $model,
		  "active"		=> ['setting', 'profiles'],
		]);
	}

	public function update(ProfileRequest $request, $id) {
		if(Auth::user()->id != $id) {
			abort(403);
		}
		$insert["email"]		=	$request->input("profile-email");
		$insert["firstname"]	=	$request->input("profile-firstname");
		$insert["lastname"]		=	$request->input("profile-lastname");
		$insert["password"]		=	bcrypt($request->input("profile-password"));
		$profile = User::find($id);
		if($profile->update($insert)) {
			Library::saveTrail("profiles", "update", $profile->id);
			return redirect("app")->with("status", "success")->with("message", "Profile updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update profile");
		}
	}

	public function destroy(Profile $profile) {
		Library::saveTrail("profiles", "destroy", $profile->id, "Name: " . $profile->profile_username);
        $profile->delete();
        return redirect()->back()->with("status", "success")->with("message", "Data Deleted");
	}
}
