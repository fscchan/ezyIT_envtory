<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\storeLazadaCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProductCategoriesRequest;
use App\Http\Requests\NewCategoryRequest;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\ProductCategories;
use App\Library;
use App\AuditTrails;
use Datatables;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Artisan;

class ProductCatController extends Controller {
	public function index(Request $request) {
		$lastSync = ProductCategories::select('created_at')->orderBy('created_at','desc')->first();
		return view("admin.productcat.index", [
			"pageTitle"	=> "Product categories",
			"lastSync" => $lastSync
		]);
	}

	public function create() {
		$parents	= ProductCategories::all();
		$model		= new ProductCategories;
		return view("admin.productcat.form",[
		  "pageTitle"	=> "Create new product category",
		  "parents"		=> $parents,
		  "model"		=> $model,
		  "active"		=> ['setting', 'productCategories'],
		]);
	}

	public function edit($id) {
		$parents	= ProductCategories::all();
		$model		= ProductCategories::find($id);
		if($model === null) { abort(404); }
		return view("admin.productcat.form",[
		  "pageTitle"	=> "Create new product category",
		  "parents"		=> $parents,
		  "model"		=> $model,
		  "active"		=> ['setting', 'productCategories'],
		]);
	}

	public function store(ProductCategoriesRequest $request) {
		$insert["cat_parent"]	= $request->input("category-parent");
		$insert["cat_name"]		= $request->input("category-name");
		$insert["data_status"]	= 2;
		if(Library::saveTrail("productCategories", "create", ProductCategories::create($insert)->id)) {
			return redirect("app/productCategories")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(ProductCategoriesRequest $request, $id) {
		$insert["cat_parent"]	= $request->input("category-parent");
		$insert["cat_name"]		= $request->input("category-name");
		$insert["data_status"]	= 2;
		$category = ProductCategories::find($id);
		if($category->update($insert)) {
			Library::saveTrail("productCategories", "update", $category->id);
			return redirect("app/productCategories")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy(ProductCategories $category) {
		Library::saveTrail("productCategories", "destroy", $category->id, "Name: " . $category->cat_name);
		$category->delete();
        return redirect("app/productCategories")->with("status", "success")->with("message", "Data deleted successfully");
	}
	
	public function import(Request $request) {
		Excel::load($request->file("import_file"), function ($reader) {
			$datas = $reader->toArray();
			if ($request->input("mode") == "append") {
				for ($i = 0; $i < count($datas); $i++) {
					if ($datas[$i]["parent"] == "") {
						$check = json_decode(json_encode(DB::table($this->table)->where("cat_name", $request->input("input_name"))->get()), true);
						if (count($check) > 0) {
							$insertid[$datas[$i]["name"]] = $check[0]["cat_name"];
						} else {
							$insertid[$datas[$i]["name"]] = DB::table($this->table)->insertGetId(array("cat_parent"=>"0", "cat_name"=>$datas[$i]["name"], "data_status"=>"2"));
						}
					} else {
						DB::table($this->table)->insert(array("cat_parent"=>$insertid[$datas[$i]["parent"]], "cat_name"=>$datas[$i]["name"], "data_status"=>"2"));
					}
				}
			} elseif ($request->input("mode") == "replace") {
				DB::table($this->table)->truncate();
				for ($i = 0; $i < count($datas); $i++) {
					if ($datas[$i]["parent"] == "") {
						$insertid[$datas[$i]["name"]] = DB::table($this->table)->insertGetId(array("cat_parent"=>"0", "cat_name"=>$datas[$i]["name"], "data_status"=>"2"));
					} else {
						DB::table($this->table)->insert(array("cat_parent"=>$insertid[$datas[$i]["parent"]], "cat_name"=>$datas[$i]["name"], "data_status"=>"2"));
					}
				}
			} else {
				$object["st"]	= false;
				$object["msg"]	= "Invalid operation method.";
				$object["data"]	= NULL;
				die(json_encode($object, JSON_PRETTY_PRINT));
			}
		});
	}
	
	public function export($type) {
		$cats = ProductCategories::all();
		$export[0] = array("ID", "PARENT", "NAME");
		for ($i = 0, $ii = 0; $i < count($cats); $i++) {
			if($cats[$i]->cat_parent == "0") {
				$export[++$ii] = array($ii, "_MAIN", $cats[$i]->cat_name);
				foreach($cats as $childs) {
					if($childs->cat_parent != "0" && $childs->cat_parent == $cats[$i]->id) {
						$export[++$ii] = array($ii, ProductCategories::find($childs->cat_parent)["cat_name"], $childs->cat_name);
					}
				}
			}
		}
		switch($type) {
			case "xls": {
				Excel::create("categories_" . date("Y-m-d"), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				$pdf = PDF::loadView("admin.productcat.report", [
				  "datas"	=> array_splice($export, 1),
				  "title"	=> "Product categories " . date("d F Y")
				  ]);
				return $pdf->stream();
				break;
			}
			case "html": {
				return View("admin.productcat.report", [
				  "datas"	=> array_splice($export, 1),
				  "title"	=> "Product categories " . date("d F Y")
				  ]);
				break;
			}
			default: {
				abort(404);
				break;
			}
		}
	}
	
	public function getCategories(Request $request) {
		return ProductCategories::where("cat_parent", $request->input("p"))->where("cat_name", "like", "%" . $request->input("q") . "%")->get();
	}
	
	public function getCategory(Request $request) {
		$rets = ProductCategories::where("cat_name", "like", "%" . $request->input("q") . "%")->get();
		$shown = [];
		for($i = 0; $i < count($rets); $i++) {
			if(!in_array($rets[$i]->id, $shown)) {
				if($rets[$i]->cat_parent == "0") {
					$cats[$i] = ["id" => $rets[$i]->id, "text"=> $rets[$i]->cat_name, "children" => []];
					$tmp = $rets[$i]->childCategories()->get();
					foreach($tmp as $t) {
						array_push($cats[$i]["children"], ["id" => $t->id, "text" => $t->cat_name]);
						array_push($shown, $t->id);
					}
				} else {
					$tmp = $rets[$i]->parentCategory()->first();
					$cats[$i] = ["id" => $tmp->id, "text" => $tmp->cat_name, "children" => [["id" => $rets[$i]->id, "text" => $rets[$i]->cat_name]]];
				}
			}
			array_push($shown, $rets[$i]->id);
		}
		return $cats;
	}

	public function storeCategory(NewCategoryRequest $request) {
		if(AuditTrails::create(["audit_actor" => Auth::user()->id, "audit_trigger" => "productCategories", "audit_event" => "create", "audit_item" => ProductCategories::create(["cat_parent" => $request->input("parent"),"cat_name" => $request->input("category"), "data_status"=> "2"])->id, "audit_ip" => $request->ip(), "audit_timestamp" => date("Y-m-d H:i:s")])) {
			return [
				"success"	=> true, 
				"message"	=> ($request->input("parent") == "0" ? "Main c": "C") . "ategory '" . $request->category . "' saved",
				"data"		=> json_decode(json_encode(ProductCategories::all()))
			];
		} else {
			return [
				"success"	=> false, 
				"message"	=> "Failed saving data",
				"data"		=> null
			];
		}
    }


    public function storeLazadaCategories(Request $request)
    {
    	if (request()->ajax()) {
			Artisan::queue('lazada:import-categories', [
		        'user_id' => Auth::user()->id
		    ]);
			return response()->json([
				'status' => 'success'
				]);
    	}
    }

    function extractCategories($resCategories,$parent=0)
    {
    	global $categories;
    	foreach ($resCategories as $key => $category) {
	    	$data = [
				'id' => $category['categoryId'],
				'cat_name' => $category['name'],
				'cat_parent' => $parent,
				'data_status' => 2,
                'insert_by' => Auth::user()->id,
                'update_by' => Auth::user()->id,
			];
			if ($data) {
				array_push($categories, $data);
			}
			if (!empty($category['children'])) {
				$data = $this->extractCategories($category['children'],$category['categoryId']);
				if ($data) {
					array_push($categories, $data);
				}
			}
    	}
    }
	
	public function getChildren(Request $request) {
		$children = ProductCategories::where("cat_parent", $request->input("id"))->get();
		return ["data" => $children, "count" => count($children)];
	}
}