<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\StockInLogistic;

use App\Library;
use Yajra\Datatables\Facades\Datatables;

class StockInLogisticController extends Controller {
	public function index() {}

	public function show($id) {}

	public function create(Request $request) {
		$model = !empty($request->input("poid")) ? StockInLogistic::findOrFail($request->input("poid")) : new StockInLogistic;
		return view("admin.stockInLogistic.form",[
			"pageTitle"			=> "Manage Stock In Logistic",
			"model"				=> $model,
			"active"			=> ["setting", "stockInLogistic"]
		]);
	}

	public function edit($id) {}

	public function store(Request $request) {
		$insert["bin_id"] = $request->input("bin-id");
		if(empty($request->input("id"))) {
			$inserted = ShippingBin::create($insert)->id;
			$editmode = false;
		} else {
			$old = ShippingBin::findOrFail($request->input("id"));
			$inserted = $old->id;
			$old->update($insert);
			$editmode = true;
		}
		if(Library::saveTrail("stockInLogistic", $editmode ? "update" : "create", $inserted)) {
			return redirect("app/logisticInventory")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(Request $request, $id) {}

	public function destroy($id) {}
}
