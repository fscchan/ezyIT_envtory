<?php
/*
 * Products controller
 * Table: Products
 *
 * — Eeno
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Jobs\SyncLazadaProducts;

use App\Product;
use App\ProductAttributes;
use App\Sku;
use App\SkuAttributes;
use App\ProductCode;
use App\ProductCategories;
use App\Brands;
use App\Supplier;
use App\SKUProduct;
use App\SyncLog;
use App\Shop;
use App\Stock;
use App\StockHistories;
use App\Library;
use App\Zones;
use App\Warehouse;
use App\AuditTrails;

use Datatables;
use Uuid;
use Artisan;

class ProductController extends Controller {
	public function index() {
		if (request()->ajax()) {
		}

		$sync = [
			'enable' => true,
			'reason' => '<i class="fa fa-refresh"></i>',
		];

		$checkSync = SyncLog::where('platform', 'lazada')
				->where('name', 'product')
				->where('status', 'running')
				->count();
		$shop = Shop::where('platform_id',1)
				->whereNotNull('account')
		        ->whereNotNull('api_key')
		        ->count();

        if ($checkSync) {
        	$sync['enable'] = false;
        	$sync['reason'] = '<i class="fa fa-spin fa-refresh"></i> Sync on Progress';
        }

        if (!$shop) {
        	$sync['enable'] = false;
        	$sync['reason'] = 'No Shop Configured with Lazada';
        }

        $products = Product::has('code');

        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $search = isset($_GET['search']) ? $_GET['search'] : '';

        $filter = [
            'limit' => $limit,
            'search' => $search,
        ];


        if ($search) {
            $products = Product::has('code')->where('product_code', 'like', '%'.$search.'%')
	            ->orwhereHas('attributes', function($attribute) use ($search) {
	                $attribute->where('name', 'name')->where('value', 'like', '%'.$search.'%');
	            });
        }

        $data = [
	        'zones' => Zones::all(),
	        'warehouses' => Warehouse::all(),
        ];

		return view("admin.product.index", [
			"pageTitle"	=> "Products",
			'sync' => $sync,
			'products' => $products->orderBy('created_at', 'desc')->paginate($limit),
			'data' => $data,
			'filter' => $filter
		]);
	}

	public function create() {
		$model		= new Product;
		$suppliers = Supplier::all();
		return view('admin.product.form',[
		  'pageTitle'	=> 'Add New Product',
		  'model'		=> $model,
		  'suppliers'	=> $suppliers,
			'clone' => false
		]);
	}

	public function cloneProduct($code) {
		$code = Product::find($code);
		$model		= new Product;
		$suppliers = Supplier::all();
		return view('admin.product.form',[
		  'pageTitle'	=> 'Clone Product',
		  'model'		=> $model,
		  'suppliers'	=> $suppliers,
			'clone' => true,
			'code' => $code
		]);
	}

	public function getCategories($parent)
	{
		$categories = DB::table('m_product_categories AS a')
					->leftJoin(DB::raw('(SELECT cat_parent, COUNT(cat_parent) as children
						FROM m_product_categories
						WHERE cat_parent <> id
						GROUP BY cat_parent) b'), function($join){
							$join->on('a.id', '=', 'b.cat_parent');
						})
					->select('a.cat_name','a.id','b.children')
					->where('a.cat_parent',$parent)->orderBy('a.cat_name')->get();

		return $categories;
	}

	public function getBrands($id=null)
	{
		$brands = [];
		$query = isset($_GET['query']) ? $_GET['query'] : null;
		if ($query) {
			$brands = Brands::select('id','name')->where('name','like',$query.'%')->limit(10)->get();
		}
		if ($id) {
			return Brands::find($id);
		}
		return $brands;
	}

	public function getCodeList($category)
	{
		return Product::with('code')->where('m_product_categories_id',$category)->whereHas('code')->get();
	}

	public function getAttributes($category)
	{
		$productAttributes = Product::productAttributes();
		$attributes = [];
		$attributes = $productAttributes['general'];

		$api = Library::lazadaApi('GetCategoryAttributes',null,null,['PrimaryCategory'=>$category]);
		if ($api) {
			$client = new \GuzzleHttp\Client();
      $res = json_decode($client->get($api)->getBody(), true);
      if (isset($res["ErrorResponse"])) {
          return $res["ErrorResponse"];
      }
      $resAttributes = $res["SuccessResponse"]["Body"];
			if ($resAttributes) {
				foreach ($resAttributes as $attribute) {
					if ($attribute['attributeType'] == 'sku' && ($attribute['inputType'] == 'singleSelect' || $attribute['inputType'] == 'multiSelect') && !in_array($attribute['name'],$productAttributes['blocked'])) {
						$attribute['value'] = [];
						$attribute['type'] = 'variation';
						array_push($attributes, $attribute);
					}
				}
				return $attributes;
				// return $resAttributes;
			}
		}
	}

	public function getLazadaAttributes($category)
	{
		$productAttributes = Product::productAttributes();
		$attributes = [];
		$attributes = $productAttributes['general'];

		$api = Library::lazadaApi('GetCategoryAttributes',null,null,['PrimaryCategory'=>$category]);
		if ($api) {
			$client = new \GuzzleHttp\Client();
      $res = json_decode($client->get($api)->getBody(), true);
      if (isset($res["ErrorResponse"])) {
          return $res["ErrorResponse"];
      }
      $resAttributes = $res["SuccessResponse"]["Body"];
			if ($resAttributes) {
				foreach ($resAttributes as $attribute) {
					if ($attribute['attributeType'] == 'sku' && ($attribute['inputType'] == 'singleSelect' || $attribute['inputType'] == 'multiSelect') && !in_array($attribute['name'],$productAttributes['blocked'])) {
						$attribute['value'] = [];
						$attribute['type'] = 'variation';
						array_push($attributes, $attribute);
					}
				}
				return ['all'=>$resAttributes,'filtered'=>$attributes];
				// return $resAttributes;
			}
		}
	}

	public function validatePrefix($prefix) {
		$search = Product::where('product_prefix',$prefix)->count();
		if ($search) {
			return 'used';
		}
		return 'ok';
	}

	public function store(ProductRequest $request) {
		// return $request->all();

		DB::beginTransaction();
		try {

			if (!empty($request->variation)) {

				$general = ['name', 'brand', 'model'];

				$productCode = ProductCode::create($request->general);

				if ($productCode) {


					foreach ($request->variation as $key => $variation) {

						$variations = [];

						$product = Product::create([
							'm_product_code_id' => $productCode->id,
							'm_product_categories_id' => $productCode->m_product_categories_id,
							'product_code' => $variation['SellerSku'],
						]);

						if ($product) {

							foreach ($request->general as $name => $value) {
								$attribute = [];
								$attribute['id'] = Uuid::generate()->string;
								$attribute['m_products_id'] = $product->id;
								$attribute['name'] = $name;
								$attribute['value'] = $value;
								$attribute['type'] = 'attribute';
								$attribute['created_at'] =  \Carbon\Carbon::now();
								$attribute['updated_at'] =  \Carbon\Carbon::now();
								array_push($variations,$attribute);
							}

							foreach ($request->code as $name => $value) {
								$attribute = [];
								$attribute['id'] = Uuid::generate()->string;
								$attribute['m_products_id'] = $product->id;
								$attribute['name'] = $name;
								$attribute['value'] = $value;
								$attribute['type'] = 'code';
								$attribute['created_at'] =  \Carbon\Carbon::now();
								$attribute['updated_at'] =  \Carbon\Carbon::now();
								array_push($variations,$attribute);
							}

							foreach ($variation as $name => $value) {
								$attribute = [];
								if (!empty($value)) {
									$attribute['id'] = Uuid::generate()->string;
									$attribute['m_products_id'] = $product->id;
									$attribute['name'] = $name;
									$attribute['value'] = $value;
									$attribute['type'] = 'code';
									$attribute['created_at'] =  \Carbon\Carbon::now();
									$attribute['updated_at'] =  \Carbon\Carbon::now();
									array_push($variations,$attribute);
								}
							}
							ProductAttributes::insert($variations);

							if (!empty($request->suppliers)) {
								foreach ($request->suppliers as $supplier) {
									$product->supplier()->attach($supplier);
								}
							}

						}
					}
				}


				DB::commit();

				return redirect("app/products")
						->with("status", "success")
						->with("message", "Product Saved Successfully");
			}

		} catch (Exception $e) {
			DB::rollback();
			return $e;
			redirect()->back()
				->with("status", "danger")
				->with("message", "Failed to Save Product, Please Try Again");
		}

	}

	public function store1(ProductRequest $request) {
		$dataProduct = $request->except('attribute');
		$dataAttributes = [];
		$code = $request->only('code');
		$attributes = $request->only('attribute');
		$suppliers = $request->only('suppliers');
		if (empty($attributes['attribute'])) {
			redirect()->back()
				->with('status', 'danger')
				->with('message', 'Please Fill the Attributes');
		}
		$product = Product::create($dataProduct);
		if($product) {
			if (!empty($code['code'])) {
				foreach ($code['code'] as $name => $value) {
					if (!empty($value)) {
						$attribute['id'] = Uuid::generate()->string;
						$attribute['m_products_id'] = $product->id;
						$attribute['name'] = $name;
						$attribute['value'] = $value;
						$attribute['type'] = 'code';
						$attribute['created_at'] =  \Carbon\Carbon::now();
            $attribute['updated_at'] =  \Carbon\Carbon::now();
						array_push($dataAttributes,$attribute);
					}
				}
			}
			foreach ($attributes['attribute'] as $name => $value) {
				if (!empty($value)) {
					$attribute['id'] = Uuid::generate()->string;
					$attribute['m_products_id'] = $product->id;
					$attribute['name'] = $name;
					$attribute['value'] = $value;
					$attribute['type'] = 'attribute';
					$attribute['created_at'] =  \Carbon\Carbon::now();
	                $attribute['updated_at'] =  \Carbon\Carbon::now();
					array_push($dataAttributes,$attribute);
				}
			}

			if (!empty($suppliers['suppliers'])) {
				foreach ($suppliers['suppliers'] as $supplier) {
					$product->supplier()->attach($supplier);
				}
			}
			// return $dataAttributes;
			ProductAttributes::insert($dataAttributes);
			return redirect("app/products")
					->with("status", "success")
					->with("message", "Product Saved Successfully");
		} else {
			redirect()->back()
				->with("status", "danger")
				->with("message", "Failed to Save Product");
		}
	}

	public function show($id = 0) {
		//
	}

	public function getProductData($product_id)
	{
		$data = [];

		$product = Product::with('code')->with('variations')->find($product_id);

		$data['data'] = $product;

		$data['attributes'] = $this->getProductAttributes($product->id);

		$data['categories'] = $this->getProductCategories($product->m_product_categories_id);

		$data['category'] = $product->category;

		$data['assigned'] = $product->supplier()->get();

		$assigned = [];
		foreach ($data['assigned'] as $key => $value) {
			array_push($assigned, $value['id']);
		}

		$cat = $data['categories'][1]['id'];
		$data['suppliers'] = $this->getSuppliers($cat,$assigned);

		return $data;
	}

	public function getProductCodeData($code_id)
	{
		$data = [];

		$code = ProductCode::find($code_id);

		$data['data'] = $code;

		// $data['attributes'] = $this->getProductAttributes($code->id);

		$data['categories'] = $this->getProductCategories($code->m_product_categories_id);

		$data['category'] = $code->category;

		$cat = $data['categories'][1]['id'];
		$data['suppliers'] = $this->getSuppliers($cat);

		return $data;
	}

	public function getProductAttributes($product_id,$attribute=null)
	{
		$attributes = [];
		$product = Product::find($product_id);
		if ($product) {
			$attributes = $product->attributes()->get();
			if (!empty($product->sku)) {
				$sku = $product->sku->attributes()->get();
				$attributes = $attributes->merge($sku);
			}
		}
		return $attributes;
	}

	public function getProductCategories($category)
	{
		global $categories;
		$categories = [];
		$category = ProductCategories::where('id',$category)->first();
		array_unshift($categories, $category);
		if ($category->cat_parent) {
			$this->getProductCategoriesChild($category->cat_parent);
		}
		return $categories;
	}

	protected function getProductCategoriesChild($category)
	{
		global $categories;
		$category = ProductCategories::where('id',$category)->first();
		array_unshift($categories, $category);
		if ($category->cat_parent) {
			$this->getProductCategoriesChild($category->cat_parent);
		}
	}

	public function getSuppliers($category=null,$except=null)
	{
		$supplier = Supplier::select('id','supplier_name')->whereHas('categories', function($cat) use ($category) {
			$cat->where('id',$category);
		});
		if ($except) {
			$supplier->whereNotIn('id', $except);
		}
		return $supplier->get();
	}

	public function edit($id) {
		$model = Product::find($id);
		if($model === null) { abort(404); }
		$suppliers = Supplier::all();
		return view('admin.product.form',[
		  'pageTitle'	=> 'Update Product',
		  'model'		=> $model,
		  'suppliers'	=> $suppliers,
			'clone' => false,
		]);
	}

	public function duplicate($id) {
		$model						= Product::find($id);
		if($model === null) { abort(404); }
		$model->product_code		= "";
		$model->product_prefix		= "";
		$model->product_suppliers	= json_decode($model->product_suppliers);
		$suppliers					= Supplier::all();
		return view("admin.product.form",[
			"pageTitle"		=> "Duplicate Product",
			"categories"	=> ProductCategories::all(),
			"model"			=> $model,
			"suppliers"		=> $suppliers,
			"active"		=> ['setting', 'products'],
			'clone'	=> true,
		]);
	}

	public function update(Request $request, $id) {
		$dataAttributes = [];
		$attributes = $request->only('attribute');
		$code = $request->only('code');
		// return $code;
		$suppliers = $request->only('suppliers');
		$product = Product::find($id);
		// return $product->sku->attributes()->get();
		if($product) {
			foreach ($attributes['attribute'] as $name => $value) {
				if (!empty($value)) {
					$check = ProductAttributes::where('m_products_id',$product->id)
								->where('name',$name);
					if ($check->count()) {
						$check->update(['value'=>$value]);
					}else{
						$attribute['id'] = Uuid::generate()->string;
						$attribute['m_products_id'] = $product->id;
						$attribute['name'] = $name;
						$attribute['value'] = $value;
						$attribute['type'] = 'attribute';
						$attribute['created_at'] =  \Carbon\Carbon::now();
		                $attribute['updated_at'] =  \Carbon\Carbon::now();
						array_push($dataAttributes,$attribute);
					}
				}else{
					ProductAttributes::where('m_products_id',$product->id)
								->where('name',$name)
								->delete();
				}
			}
			if ($code['code']) {
				foreach ($code['code'] as $name => $value) {
					$check = ProductAttributes::where('m_products_id',$product->id)
					->where('name',$name);
					if ($check->count()) {
						$check->update(['value'=>$value]);
					}else{
						$attribute['id'] = Uuid::generate()->string;
						$attribute['m_products_id'] = $product->id;
						$attribute['name'] = $name;
						$attribute['value'] = $value;
						$attribute['type'] = 'code';
						$attribute['created_at'] =  \Carbon\Carbon::now();
						$attribute['updated_at'] =  \Carbon\Carbon::now();
						array_push($dataAttributes,$attribute);
					}
					if (!empty($product->sku)) {
						$sku = $product->sku;
						$checkSku = SkuAttributes::where('m_product_sku_id',$sku->id)
						->where('name',$name);
						$checkSku->update(['value'=>$value]);
					}
				}
			}
			if ($suppliers['suppliers']) {
				if (!empty($product->supplier()->get())) {
					$product->supplier()->sync($suppliers['suppliers']);
				}else{
					$product->supplier()->attach($suppliers['suppliers']);
				}
			}else{
				$product->supplier()->detach();
			}
			// return $dataAttributes;
			ProductAttributes::insert($dataAttributes);
			return redirect("app/products")
					->with("status", "success")
					->with("message", "Product Updated Successfully");
		} else {
			redirect()->back()
				->with("status", "danger")
				->with("message", "Failed to Update Product");
		}
		if($product->update($insert)) {
			Library::saveTrail("products", "update", $product->id);
			return redirect("app/products")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($id) {
		Library::saveTrail("products", "destroy", $id, "Name: " . Product::find($id)->product_name);
		Product::find($id)->delete();
        return redirect("app/products")->with("status", "success")->with("message", "Data deleted successfully");
	}

	public function checkPrefix(Request $request) {
		if(count(Product::where("product_prefix", $request->input("q"))->get()) !== 0) {
			return ["st" => false];
		} else {
			return ["st" => true];
		}
	}

	public function genPrefix(Request $request) {
		if(count(explode(" ", $request->input("q"))) === 1) {
			$prefix = strtoupper(substr($request->input("q"), 0, 2) . str_pad(count(Product::where("product_prefix", "like", substr($request->input("q"), 0, 2) . "%")->get()) + 1, 4, "0", STR_PAD_LEFT));
		} else {
			$words = explode(" ", $request->input("q"));
			$prefix = strtoupper(substr($words[0], 0, 2) . substr($words[1], 0, 2) . str_pad(count(Product::where("product_prefix", "like", substr($request->input("q"), 0, 2) . "%")->get()) + 1, 2, "0", STR_PAD_LEFT));
		}
		return ["st" => true, "data" => $prefix];
	}

	public function getProductIdList(Request $request) {
		$model = Product::where("product_code", "LIKE", $request->input("q") . "%")->get();
		if(count($model) !== 0) {
			if(count($model) === 1) {
				$prefix_modal = strtoupper((!empty($model[0]->product_name) ? substr($model[0]->product_name, 0 , 2) : "00") . (!empty($model[0]->product_brand) ? substr($model[0]->product_brand, 0 , 2) : "00") . (!empty($model[0]->product_model) ? substr($model[0]->product_model, 0 , 2) : "00") . (!empty($model[0]->product_submodel) ? substr($model[0]->product_submodel, 0 , 2) : "00") . (!empty($model[0]->product_size) ? substr($model[0]->product_size, 0 , 2) : "00") . (!empty($model[0]->product_color) ? substr($model[0]->product_color, 0 , 2) : "00"));
				$prefix = $prefix_modal . str_pad(count(SKUProduct::where("sku_product_prefix", $prefix_modal)->get()) + 1, 2, "0", STR_PAD_LEFT);
			} else {
				$prefix = null;
			}
			return ["st" => true, "data" => $model, "prefix" => $prefix];
		} else {
			return ["st" => false, "data" => null, "prefix" => null];
		}
	}

	public function syncLazada(Request $request)
	{
		Artisan::queue('lazada:sync-products', [
	        'user_id' => Auth::user()->id
	    ]);
		return response()->json([
			'status' => 'success'
			]);
	}

	public function stroeStock(Request $request)
	{
		$stock = $request->except('qty','warehouse');
		$data = [
			'received_date' => date('Y-m-d'),
			'stock_type' => 'AC',
			'status' => 3,
			'batch_id' => $this->getBatchId($request->m_products_id),
		];

		$qty = new StockHistories([
            'qty' => $request->qty,
            'note' => 'ADDED DIRECTLY'
        ]);

        $saveStock = Stock::create(array_merge($stock,$data));
        $saveStock->histories()->save($qty);

        return ['message'=>'success'];
	}

	protected function getBatchId($product_id)
    {
      $product = Product::find($product_id);
      $productPrefix = strtoupper(substr($product->product_code, 0, 3));
      $firstPrefix = 'BYP'.$productPrefix.date('dmy');
      $lastNumber = Stock::selectRaw('RIGHT(max(batch_id),4) as max_number')
                      ->where('batch_id','like',$firstPrefix.'%')
                      ->first();
      $newNumber = sprintf("%04d", $lastNumber->max_number+1);
      $prefix = $firstPrefix.$newNumber;
      return $prefix;
    }
}
