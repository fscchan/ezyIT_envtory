<?php

namespace App\Http\Controllers\Admin;

use App\Traits\ValetTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductReceivedRequest;

use App\Po;
use App\Invoice;
use App\InvoiceHistories;
use App\Stock;
use App\StockHistories;
use App\Zones;
use App\Library;
use App\ProductCategories;
use App\Warehouse;
use App\WarehouseZones;
use App\WarehouseRacks;
use App\WarehouseCells;
use Datatables;

class ProductReceived2Controller extends Controller
{
    use ValetTrait;

    protected function getPoList($request){
        $dataPo= \DB::table('t_invoice_master as i')
            ->join('t_po_master as po', 'po.id', '=', 'i.t_po_master_id')
            ->select('i.id as invoice_id', 'po.prefix as po_number', 'po.po_date as po_date', 'i.prefix as invoice_number');
        $dataPo = $dataPo->where('po.status','!=', 3);
        $input = $request->all();
        if(isset($input['number']) && !empty($input['number'])){
            $po = Po::where('prefix', $input['number'])->first();
            $invoice = Invoice::where('prefix', $input['number'])->first();
            if($po){
                $dataPo = $dataPo->where('po.prefix', $input['number']);
            }elseif($invoice){
                $dataPo = $dataPo->where('i.prefix', $input['number']);
            }else{
                return array();
            }
        }

        $dataPo = $dataPo->orderBy('po.created_at', 'asc');
        $pos = array();

        foreach ($dataPo->get() as $key=>$data){
            $data->cold_batch_id ="-";
            $data->stock_type ="-";
            $data->action ='<input type="checkbox" name="batch['.$data->invoice_number.'][receive]" class="receive" /><input type="hidden" name="batch['.$data->invoice_number.'][id]" value="'.$data->invoice_number.'" />';
            $pos[]=$data;
        }
//        $pos = Po::where('status','!=',3)->limit(5)->orderBy('prefix')->orderBy('created_at')->get();
        return $pos;
    }


    public function index(Request $request)
    {
        $pos = $this->getPoList($request);
        if (request()->ajax()) {
            return $pos;
        }
//        $po = isset($_GET['po']) ? $_GET['po'] : null;
//        $po = Po::where('prefix',$po)->first();

        $todayDate = $this->getHumanFormatDate();
        $zones = Zones::all();

//        if (empty($po)) {
//            $po = new Po;
//        }
        return view('admin.productReceived2.index',[
            'pageTitle' => 'Product Received',
            'pos' => $pos,
            'todayDate' => $todayDate,
            'zones' => $zones
        ]);
    }
}