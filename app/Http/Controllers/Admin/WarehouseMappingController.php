<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Warehouse;
use App\WarehouseZones;
use App\Zones;
use App\Http\Requests\WarehouseMappingRequest;
use App\Library;
use App\ProductCategories;

class WarehouseMappingController extends Controller {
	public function index($wid){
		$warehouse = Warehouse::find($wid);
		return view("admin.warehouse.zones.index",[
			"pageTitle"	=> $warehouse->name . " - Warehouse Mapping",
			"warehouse"	=> $warehouse,
			"zones"		=> $warehouse->zones()->get()
		]);
	}
	
	public function show($wid, $zid) {
		if (request()->ajax()) {
			return WarehouseZones::where("id", "");
		}
	}

	public function create($wid){
		$warehouse = Warehouse::find($wid);
		if($warehouse === null) { abort(404); }
		$model = new WarehouseZones;
		$model->categories = [];
		$categories = ProductCategories::with('children')->where('cat_parent',0)->orderBy('cat_name')->get();
		return view("admin.warehouse.zones.form",[
			"pageTitle"	=> "Create New Warehouse Zone",
			"warehouse"	=> $warehouse,
			"model"		=> $model,
			"categories" => $categories,
			"zones" => Zones::all()
		]);
	}
	
	public function edit($wid, $zid) {
		$warehouse = Warehouse::find($wid);
		if($warehouse === null) { abort(404); }
		$model = $warehouse->zones()->where("id", $zid)->first();
		if($model === null) { abort(404); }
		$model->categories = $this->getCategories($zid);
		$categories = ProductCategories::with('children')->where('cat_parent',0)->orderBy('cat_name')->get();
		return view("admin.warehouse.zones.form",[
			"pageTitle"	=> "Create New Warehouse Zone",
			"warehouse"	=> $warehouse,
			"model"		=> $model,
			"zones" => Zones::all(),
			"categories" => $categories
		]);
	}

	public function store(WarehouseMappingRequest $request, $wid) {
		$wirehouseZone = WarehouseZones::create(
							array_merge([
								"m_warehouse_id" => $wid,
							], $request->except(["zone-categories"]))
						);
		if(Library::saveTrail("warehouseMapping", "create", $wirehouseZone->id)) {
			$wirehouseZone->categories()->attach($request->input("zone-categories"));
			return redirect()->route("warehouse.mapping.index", $wid)->with("status", "success")->with("message", "Successfully saved data");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}
	
	public function update(WarehouseMappingRequest $request, $wid, $zid) {
		$zone = WarehouseZones::find($zid);
		$zone->update(array_merge(["m_warehouse_id" => $wid], $request->except(["zone-categories"])));
		if(Library::saveTrail("warehouseMapping", "update", $zid)) {
			$zone->categories()->sync($request->input("zone-categories"));
			return redirect()->route("warehouse.mapping.index", $wid)->with("status", "success")->with("message", "Successfully saved data");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}
	
	public function destroy(WarehouseZones $zone, $wid, $zid) {
		$zone = $zone->find($zid);
		if(Library::saveTrail("warehouseMapping", "destroy", $zone->id, "Name:" . $zone->name . " (" . $zone->prefix . ")")) {
			$zone->delete();
			return ["st" => true, "msg" => "Data successfully deleted."];
		} else {
			return ["st" => false, "msg" => "Failed to delete data."];
		}
	}

	public function genPrefix(Request $request, $wid) {
		$zoneprefix = Zones::find($request->input("t"));
		return ["st" => true, "data" => strtoupper(substr($zoneprefix->prefix, 0, 3) . str_pad(count(WarehouseZones::where("prefix", "like", "%" . $zoneprefix->prefix)->get()) + 1, 2, "0", STR_PAD_LEFT))];
	}

	public function genPrefix2(Request $request, $wid) {
		$zoneprefix = Zones::find($request->input("t"));
		return ["st" => true, "data" => strtoupper(substr(preg_replace("/[^a-zA-Z0-9]/", "", $zoneprefix->name), 0, 3) . str_pad(count(WarehouseZones::where("prefix", "like", strtoupper(substr(preg_replace("/[^a-zA-Z0-9]/", "", $zoneprefix->name), 0, 3)) . "%")->get()) + 1, 2, "0", STR_PAD_LEFT))];
	}
	
	public function checkPrefix(Request $request, $wid) {
		if(count(WarehouseZones::where("prefix", $request->input("q"))->get()) !== 0) {
			return ["st" => false];
		} else {
			return ["st" => true];
		}
	}

	public function getCategories($wid) {
		$wz = WarehouseZones::find($wid);
		$categories = $wz->categories()->get();
		$zones = [];
		if ($wz) {
			foreach ($categories as $key => $value) {
				array_push($zones,$value->id);
			}
		}
		return $zones;
	}
}
