<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\OrderDetail;
use App\OrderDetailStock;
use App\PickingType;
use App\Shop;
use App\Sku;
use App\User;
use App\WarehouseTrays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\PickingList;
use App\Picking;
use App\Library;
use Datatables;
use Illuminate\Support\Facades\DB;
use Uuid;

class PickingListController extends Controller
{

	public function index(Request $request)
	{
		$this->recheckStockOrders();
		if (request()->ajax()) {
			$lists = PickingList::limit($request->length);
			return Datatables::of($lists)
				->addColumn('picking_staff', function ($lists) {
					return $lists->staff->firstname;
				})
				->addColumn('picking_status', function ($lists) {
					$status = ['PENDING', 'INPROGRESS', 'READY TO PACK'];
					return $status[$lists->status];
				})
				->addColumn("action", function ($list) {
					$action = '';
					$delete = '<button type="button" onclick="delPickList(this)" data-id="' . $list->id . '" class="btn btn-danger btn-xs btn-flat"><i class="fa fa-trash"></i> Delete</button> ';
					$pick = '<a href="' . url('app/pickingList/' . $list->id) . '" class="btn-startPickingList btn btn-primary btn-xs btn-flat" data-value="' . $list->id . '"><i class="fa fa-shopping-basket"></i> Start picking</a> ';
					$info = '<a href="' . url('app/pickingList/assign_tray/'. $list->id) . '" class="btn btn-success btn-xs btn-flat"><i class="fa fa-pencil"></i> Assign Tray</a> ';
//					$info = '<button type="button" onclick="showInfo()" class="btn btn-warning btn-xs btn-flat"><i class="fa fa-info"></i> Start picking</button> ';
					if (Auth::user()->can('update-pickingList') && $list->status == 0) {
						$action .= $info;
					}
					if (Auth::user()->can('update-pickingList') && $list->status == 1) {
						$action .= $pick;
					}
					if (Auth::user()->can('delete-pickingList') && $list->status == 0) {
						$action .= $delete;
					}
					return $action;
				})->make(true);
		}
		$users = User::where('active', 1)->get();
		return view("front.pickingList.index", [
			"pageTitle" => "Picking list",
			"users" => $users,
			"type" => PickingType::all(),
			"trays" => WarehouseTrays::all()
		]);
	}

	public function getDetail(Request $request)
	{
		$details = Picking::where('picking_list_id', $request->picking_id)->get();
		$data = [];
		foreach ($details as $detail) {
			if (@$detail->tray->prefix == '') {
				$action = '<button type="button" onclick="assignTray(this)" data-id="' . $detail->id . '" class="btn btn-warning btn-xs btn-flat"><i class="fa fa-pencil"></i> Assign Tray</button>';
			} else {
				$action = '';
			}
			$data[] = [
				'tray_id' => @$detail->tray->prefix,
				'order_no' => $detail->stockInBatch->orderDetail->order->order_no,
				'product_no' => $detail->stockInBatch->orderDetail->sku->product->attribute('name'),
				'batch_id' => $detail->stockInBatch->batch_id,
				'cell_id' => @$detail->stockInBatch->cell->prefix,
				'product_sku' => $detail->stockInBatch->orderDetail->sku->sku,
				'qty' => $detail->stockInBatch->qty,
				'action' => $action
			];
		}
		return response()->json($data);
	}

	public function show($id)
	{
		$picking = PickingList::find($id);
		$items = Picking::where('picking_list_id', $id)->get();
		return view('front.pickingList.show', [
			'picking' => $picking,
			'items' => $items,
			'count' => count($items),
			'pageTitle' => 'Process Picking'
		]);
	}

	public function store(Request $request)
	{
		$data = $request->except(['details', 'orders']);
		$data['picking_code'] = PickingList::generatePickingCode();
		$data['date'] = date('Y-m-d');
		$orders = $request->input('orders');
		$details = $request->input('details');
		$pickingDetails = [];
		if ($pickList = PickingList::create($data)) {
			Library::saveTrail("picking_list", "create", $pickList->id);
			$total_item = 0;
			foreach ($details['det_stock_id'] as $k => $detail) {
				$params = [
					'id' => Uuid::generate()->string,
					'picking_list_id' => $pickList->id,
					'det_stock_id' => $details['det_stock_id'][$k],
					'item_id' => $details['item_id'][$k],
					'created_at' => \Carbon\Carbon::now(),
					'updated_at' => \Carbon\Carbon::now()
				];
				array_push($pickingDetails, $params);
				$total_item += $details['qty'][$k];
			}
			if (Picking::insert($pickingDetails)) {
				$qty_of_order = count($orders['order_id']);
				foreach ($orders['order_id'] as $order) {
					Order::where('id', $order)->update(['status' => 3]);
				}
				$pickList->update(['qty_of_order' => $qty_of_order, 'total_item' => $total_item]);
			}

			//set status pack by marketPlace to lazada
			$ords = Order::whereIn('id', $orders['order_id'])->get();
			$this->setStatusInLazada($ords);

		}

		return response()->json(['status' => 'success', 'msg' => 'Picking List Successfully created.', 'text' => 'Saved!']);
	}

	public function setStatusInLazada($orders)
	{
		$arrItemIds = [];
		$shops = [];
		foreach ($orders as $order) {
			if ($order->shop->platform_id == 1) { //is lazada
				$shops[$order->shop->id] = $order->shop;
				$details = $order->detail;
				foreach ($details as $d) {
					$arrItemIds[] = $d->order_item_id;
				}
			}
		}
		$shops = array_unique($shops);
		$itemsIds = implode(',', $arrItemIds);
		foreach ($shops as $shop) {
			Order::setStatusToPackByMarketPlace($shop, $itemsIds);
		}
	}

	public function orderList(Request $request)
	{
		$orders = Order::getOrderList($request->get('type'));
		foreach ($orders as $k => $order) {
			$cekAvailable = OrderDetailStock::where('t_order_detail_id', $order->detail[0]->id)->get();
			if (count($cekAvailable) > 0) {
				$order->date = date('d F Y', strtotime($order->date));
				$order->shopPlatform = Library::shopPlatform($order->shop->platform_id);
				$order->shop_name = $order->shop->shop_name;
				$order->type = $order->orderType->name;
			} else {
				unset($orders[$k]);
			}
		}
		return response()->json($orders);
	}

	public function update(Request $request, PickingList $pickingList)
	{
		$data = $request->all();
		$pickingList->update([
			'start' => $data['start'],
			'end' => date('H:i:s'),
			'status' => 2 //done
		]);

		$det_pick = $pickingList->detail;
		foreach ($det_pick as $det) {
			$order_id = $det->stockInBatch->orderDetail->t_order_master_id;
			Order::where('id', $order_id)->update(['status' => 5]); //PICKCOM
		}
		return redirect('app/pickingList')
			->with('status', 'success')
			->with('message', 'Picking process done!');
	}

	public function destroy($id)
	{
		$picking_lists = PickingList::find($id);
		$det_pick = $picking_lists->detail;
		$picking_lists->delete();
		foreach ($det_pick as $det) {
			$order_id = $det->stockInBatch->orderDetail->t_order_master_id;
			Order::where('id', $order_id)->update(['status' => 2]);
		}

		Library::saveTrail('picking_list', 'delete', $id, 'PREFIX : ' . $picking_lists->picking_code);
		return response()->json(['status' => 'success', 'msg' => 'Picking list Deleted.', 'text' => 'Deleted!']);
	}

	public function getDetailInStock(Request $request)
	{
		$details = OrderDetail::where('t_order_master_id', $request->order)->get();
		$data = [];
		foreach ($details as $detail) {
			$inStocks = OrderDetailStock::where('t_order_detail_id', $detail->id)->get();
			foreach ($inStocks as $i) {
				$data[] = [
					'order_no' => $detail->order->order_no,
					'product_sku_id' => $detail->sku->sku,
					'product_name' => $detail->sku->product->attribute('name'),
					'qty' => $i->qty,
					'det_stock_id' => $i->id,
					'batch_id' => $i->batch_id,
					'cell_id' => @$i->cell->prefix
				];
			}
		}
		return response()->json($data);
	}

	function recheckStockOrders()
	{
		$orders = Order::where('status', 2)->get(); //collect new orders
		$affected = 0;
		foreach ($orders as $order){
			$details = $order->detail;
			foreach ($details as $d) {
				$cekAvailable = OrderDetailStock::where('t_order_detail_id', $order->detail[0]->id)->get();
				if (count($cekAvailable) == 0) {
					$product_sku = Sku::find($d->product_sku_id);
					if ($product_sku) {
						$data_stocks = Library::findStockBatch($product_sku->product->id, $d->qty);
						foreach ($data_stocks as $stock) {
							if (OrderDetailStock::create([
								't_order_detail_id' => $d->id,
								't_stocks_id' => $stock['stocks_id'],
								'batch_id' => $stock['batch_id'],
								'cell_id' => $stock['cell_id'],
								'qty' => $stock['qty_to_pick']
							])
							) {
								Library::updateStock($stock['stocks_id'], $stock['qty_to_pick'], 'out', $order->order_no);
								$affected++;
							};
						}
					}
				}
			}
		}

		return $affected;
	}

	public function assign_tray($id)
	{
		$trays = WarehouseTrays::all();
		$pickingList = PickingList::find($id);
		$details = Picking::where('picking_list_id', $id)->get();
		$data = [];
		foreach ($details as $detail) {
			$data[$detail->stockInBatch->orderDetail->order->order_no][] = [
				'detail_id' => $detail->id,
				'tray_id' => @$detail->tray->prefix,
				'order_no' => $detail->stockInBatch->orderDetail->order->order_no,
				'product_name' => $detail->stockInBatch->orderDetail->sku->product->attribute('name'),
				'batch_id' => $detail->stockInBatch->batch_id,
				'cell_id' => @$detail->stockInBatch->cell->prefix,
				'product_sku' => $detail->stockInBatch->orderDetail->sku->sku,
				'qty' => $detail->stockInBatch->qty,
			];
		}
		return view('front.pickingList.assign_tray', [
			'data' => $data,
			'pickingList' => $pickingList,
			'trays' => $trays,
			'pageTitle' => 'Assign Tray'
		]);

	}

	public function setTray(Request $request)
	{
		$input = $request->input('items');
		DB::beginTransaction();
		try {
			foreach ($input['tray_id'] as $k=>$v){
				$picking = Picking::find($input['picking_id'][$k]);
				$picking->update([
					'tray_id' => $input['tray_id'][$k],
					'status' => 1
				]);
				$pickList = PickingList::find($picking->picking_list_id);
				$pickList->update(['status' => 1]);
			}

			DB::commit();
			return redirect("app/pickingList")->with("status", "success")->with("message", "Data saved successfully");
		} catch (\Exception $e){
			DB::rollback();
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}

	}

}
