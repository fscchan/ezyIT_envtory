<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Warehouse;
use App\WarehouseTrays;
use App\Http\Requests\WarehouseTraysRequest;
use App\Library;

use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use Barcode;

class WarehouseTraysController extends Controller {
	public function index($wid) {
		if (request()->ajax()) {
			$trays = WarehouseTrays::all();
			return Datatables::of(collect($trays))->editColumn("size", function ($tray) {
				return $tray->size == 0 ? '<span class="label label-info">Small</span>' : ($tray->size == 1 ? '<span class="label bg-light-blue">Medium</span>' : ($tray->size == 2 ? '<span class="label bg-blue">Large</span>' : '<span class="label bg-gray">undefined</span>'));
			})->editColumn("status", function ($tray) {
				return $tray->status == 0 ? '<span class="label label-success">Available</span>' : '<span class="label label-info">In use</span>';
			})->addColumn("action", function ($tray) {
				return '<form action="' . url('app/warehouse/' . $tray->m_warehouse_id . '/trays/' . $tray->id) . '" method="POST" id="delete-' . $tray->id . '">' . csrf_field() . method_field("DELETE") . (Auth::user()->can('update-warehouseTrays') ? '<a href="' . route('warehouse.trays.edit', [$tray->m_warehouse_id, $tray->id]) . '" class="btn btn-warning btn-xs btn-flat hidden"><i class="fa fa-pencil"></i> Update</a> ' : '') . (Auth::user()->can('update-warehouseTrays') ? '<button type="button" class="btn btn-danger btn-xs btn-flat" onclick="deleteTray(' . $tray->id . ')"><i class="fa fa-trash"></i> Delete</button>' : '') . '</form>';
			})->addColumn("checkboxes", function ($tray) {
				return '<input type="checkbox" class="print-barcodes" name="barcode[]" value="' . $tray->prefix . '">';
			})->make(true);
		}
		return view("admin.warehouse.trays.index", [
			"pageTitle"	=> "Warehouse trays",
			"wid"		=> $wid,
			"active"	=> ["setting", "warehouse"]
		]);
	}

	public function show($wid, $tid) {}

	public function create($wid) {
		$model = new WarehouseTrays;
		return view("admin.warehouse.trays.form", [
			"pageTitle"	=> "Create New Tray",
			"model"		=> $model,
			"wid"		=> $wid,
			"active"	=> ['setting', 'zones']
		]);
	}

	public function edit($wid, $tid) {
		$model = WarehouseTrays::find($tid);
		return view("admin.warehouse.trays.form", [
			"pageTitle"	=> "Update Zone",
			"model"		=> $model,
			"wid"		=> $wid,
			"active"		=> ['setting', 'zones']
		]);
	}

	public function store(WarehouseTraysRequest $request, $wid) {
		$insert["m_warehouse_id"]	=	$wid;
		$insert["prefix"]			=	$request->input("tray-code");
		$insert["size"]				=	$request->input("tray-size");
		$insert["status"]			=	!empty($request->input("tray-status")) ? 1 : 0;
		$insert["max_qty"]			=	!empty($request->input("max-qty")) ? $request->input("max-qty") : 0;
		if(Library::saveTrail("warehouseTrays", "create", WarehouseTrays::create($insert)->id)) {
			return redirect("app/warehouse/" . $wid . "/trays")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(WarehouseTraysRequest $request, $wid, $tid) {
		$insert["m_warehouse_id"]	=	$wid;
		$insert["prefix"]			=	$request->input("tray-code");
		$insert["size"]				=	$request->input("tray-size");
		$insert["status"]			=	!empty($request->input("tray-status")) ? 1 : 0;
		$insert["max_qty"]			=	!empty($request->input("max-qty")) ? $request->input("max-qty") : 0;
		WarehouseTrays::find($tid)->update($insert);
		if(Library::saveTrail("warehouseTrays", "update", WarehouseTrays::find($tid)->id)) {
			return redirect("app/warehouse/" . $wid . "/trays")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($wid, $tid) {
		$tray = WarehouseTrays::find($tid);
		Library::saveTrail("warehouseTrays", "destroy", $tray->id, "Name: " . $tray->name);
		$tray->delete();
        return redirect("app/warehouse/" . $wid . "/trays")->with("status", "success")->with("message", "Data deleted successfully");
	}
	
	public function genPrefix (Request $request, $wid) {
		$prefix = Warehouse::find($wid)->prefix . "TR" . strtoupper(substr($request->input("q"), 0, 1));
		return ["st" => true, "data" => $prefix . (str_pad(count(WarehouseTrays::where("prefix", $prefix)->get()) + 1, 3, "0", STR_PAD_LEFT))];
	}

    public function getBarcodes(Request $request, $wid) {
		if (empty($request->input("trays"))) {
			abort(404);
		}
		
		$a = "";
		foreach($request->input("trays") as $cell) {
			$a .= '<div style="text-align: center; margin-bottom: 16px;"><img src="data:image/png;base64,' . Barcode::getBarcodePNG($cell, "C128", 3, 100) . '" alt="' . $cell . '" width="200px" /><br/>' . $cell . '</div>';
		}
		$a .= '<script type="text/javascript">window.print();</script>';
		return $a;
    }
}
