<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Picking;
use App\PickingList;
use Datatables;

class PickingController extends Controller {
	public function index() {
		return view("front.picking.index", [
			"pageTitle"	=> "Picking",
			"active"	=> ["setting", "picking"]
		]);
	}

	public function update(Request $request, Picking $picking)
	{
		$data = $request->all();
		$picking->update([
			'tray_id' => $data['tray_id'],
			'status' => 1
		]);
		$pickList = PickingList::find($picking->picking_list_id);
		$pickList->update(['status' => 1]);
		return response()->json($picking);
	}
}
