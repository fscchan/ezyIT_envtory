<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardSystem extends Controller
{
    public function index()
    {
    	return view('admin.dashboard.system',[
            "pageTitle"	=> "Systems",
    		'controlSidebar' => true
		]);
    }
}
