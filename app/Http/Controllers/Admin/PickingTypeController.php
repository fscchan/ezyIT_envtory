<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;;
use Illuminate\Support\Facades\Auth;

use App\PickingType;
use App\OrderType;
use App\AuditTrails;
use App\Library;
use Datatables;

class PickingTypeController extends Controller {
	public function index() {
		if (request()->ajax()) {
			return Datatables::of(PickingType::all())->addColumn("order_type_name", function ($pType) {
				return $pType->orderType()->first()->name;
			})->addColumn("action", function ($pType) {
				return Library::gridAction($pType, "pickingTypes");
			})->make(true);
		}
		return view("admin.pickingTypes.index", [
			"pageTitle"	=> "Picking types",
			"active"	=> ["setting", "pickingTypes"]
		]);
	}

	public function show($id) {}

	public function create() {
		$model = new PickingType;
		return view("admin.pickingTypes.form",[
		  "pageTitle"		=> "Create New Picking Type",
		  "model"			=> $model,
		  "orderTypes"		=> OrderType::all(),
          "troyNeededPer"	=> [['value'=>'order','label'=>'order'],['value'=>'picking','label'=>'picking']],
		  "active"			=> ['setting', 'pickingTypes']
		]);
	}

	public function edit($id) {
		$model = PickingType::find($id);
		if($model === null) { abort(404); }
		return view("admin.pickingTypes.form",[
		  "pageTitle"		=> "Edit Picking Type",
		  "model"			=> $model,
		  "orderTypes"		=> OrderType::all(),
          "troyNeededPer"	=> [['value'=>'order','label'=>'order'],['value'=>'picking','label'=>'picking']],
		  "active"			=> ['setting', 'pickingTypes']
		]);
	}

	public function store(Request $request) {
        $insert["picking_type_name"]	= $request->input("pickingtype-name");
        $insert["qty"]					= $request->input("pickingtype-qty");
        $insert["order_type"]			= $request->input("pickingtype-ordertype");
        $insert["troy_needed"]			= $request->input("pickingtype-troy-needed");
        $insert["troy_needed_per"]			= $request->input("pickingtype-troy-needed-per");
		if(Library::saveTrail("pickingTypes", "create", PickingType::create($insert)->id)) {
			return redirect("app/pickingTypes")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(Request $request, $id) {
        $insert["picking_type_name"]	= $request->input("pickingtype-name");
        $insert["qty"]					= $request->input("pickingtype-qty");
        $insert["order_type"]			= $request->input("pickingtype-ordertype");
        $insert["troy_needed"]			= $request->input("pickingtype-troy-needed");
        $insert["troy_needed_per"]		= $request->input("pickingtype-troy-needed-per");
		$pickingType = PickingType::find($id);
		if($pickingType->update($insert)) {
			Library::saveTrail("pickingTypes", "update", $pickingType->id);
			return redirect("app/pickingTypes")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function destroy($id) {
		Library::saveTrail("pickingTypes", "destroy", $id, "Name: " . PickingType::find($id)->picking_type_name);
		PickingType::find($id)->delete();
        return redirect("app/pickingTypes")->with("status", "success")->with("message", "Data deleted successfully");
	}
}
