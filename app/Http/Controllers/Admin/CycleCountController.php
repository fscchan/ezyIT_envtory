<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Warehouse;
use App\WarehouseZones;
use App\Stock;
use App\StockHistories;

class CycleCountController extends Controller {
	public function index() {
		$warehouses = Warehouse::orderBy('is_default', 'desc')->get();
		return view("admin.cycleCount.form", [
			"pageTitle"		=> "Cycle Count",
			"warehouses"	=> $warehouses,
			"step" => 1
		]);
	}

	public function steps(Request $request) {
		$warehouse = Warehouse::find($request->input("warehouse-id"));
		$zone = null;
		$stocks = null;
		if(!empty($request->input("zone-id"))) {
			$zone = $warehouse->zones()->where("id", $request->input("zone-id"))->first();
			$filter = ["warehouse" => $warehouse->id, "zone" => $zone->id];
			/* $SKUs = Sku::whereHas("product", function($a) use($filter) {
				$a->whereHas("stock", function($b) use($filter) {
					$b->whereHas("cell", function($c) use($filter) {
						$c->whereHas("rack", function($d) use($filter) {
							$d->whereHas("zone", function($e) use($filter) {
								$e->where("id", $filter["zone"]);
							});
						});
					});
				});
			})->get(); */
			$stocks = Stock::whereHas("cell", function($a) use($filter) {
				$a->whereHas("rack", function($b) use($filter) {
					$b->whereHas("zone", function($c) use($filter) {
						$c->where("id", $filter["zone"]);
					});
				});
			});
		}
		return view("admin.cycleCount.form", [
			"pageTitle"		=> "Cycle Count",
			"warehouse"		=> $warehouse,
			"zones"			=> $warehouse->zones()->get(),
			"zone"			=> $zone,
			"stocks"		=> $stocks,
			"step"			=> empty($stocks) ? 2 : 3
		]);
	}

	public function show() {}
	public function create() {}
	public function edit() {}
	public function store() {}
	public function update() {}
	public function destroy() {}
	
	public function getStocks(Request $request) {
		$stock = Stock::select("t_stocks.*, sum(t_stocks_histories.qty) as qty")->join("t_stocks_histories", "t_stocks_histories.t_stocks_id", "=", "t_stocks.id")->where("m_products_id", $request->input("id"))->get();
		for($i = 0; $i < count($stock); $i++) {
			$stock[$i]->received_date = date_format(date_create_from_format("Y-m-d", $stock[$i]->received_date), "d F Y");
		}
		return $stock;
	}
	
	public function saveCycleCount(Request $request) {
		foreach($request as $i => $o) {
			
		}
	}
}
