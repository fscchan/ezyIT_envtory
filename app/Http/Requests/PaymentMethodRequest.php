<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentMethodRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
		$id = $this->route("paymentMethod") ? $this->route("paymentMethod") : null;
        return [
            "method-name" => "required|unique:m_payment_method,name," . $id
        ];
    }
	
	public function messages() {
		return [
			"method-name.required" => "This field is required"
		];
	}
}
