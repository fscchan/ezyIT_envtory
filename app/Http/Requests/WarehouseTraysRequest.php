<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseTraysRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            "tray-code" => "required|unique:m_warehouse_trays,prefix," . (!empty($this->input("id")) ? $this->input("id") : "null")
        ];
    }
	
	public function messages() {
		return [
			"tray-code.required"	=> "This field is required",
			"tray-code.unique"		=> "Another tray by this code has already exists"
		];
	}
}
