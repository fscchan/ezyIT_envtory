<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseMappingRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
		return [
            "name"		=> "required",
			"prefix"	=> "required|unique:m_warehouse_zones,prefix," . (!empty($this->input("id")) ? $this->input("id") : null),
			"type"		=> "required"
        ];
    }

    public function messages() {
        return [
            "name.required"		=> "This field is required",
			"prefix.required"	=> "This field is required",
			"prefix.unique"		=> "Another prefix by that name has already exists",
			"type.required"		=> "This field is required"
        ];
    }
}
