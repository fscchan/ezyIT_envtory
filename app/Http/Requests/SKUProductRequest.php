<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SKUProductRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
		$id = !empty($this->input("id")) ? $this->input("id") : "null";
        return [
            "sku-productcode"			=> "required|exists:m_products,product_code",
            "sku-prefix"				=> "required|unique:m_sku_products,sku_product_prefix," . $id,
			"sku-productcategory"		=> "required",
			"sku-productsubcategory"	=> "required",
			"sku-productshop"			=> "min:1"
        ];
    }
	
	public function messages() {
		return [
			"sku-productcode.required" => "This field is required",
			"sku-productcode.exists" => "Provided product code does not exists",
			"sku-prefix.required" => "This field is required",
			"sku-prefix.unique" => "Another product SKU by this serial number has already exists",
			"sku-productcategory.required" => "This field is required",
			"sku-productsubcategory.required" => "This field is required",
			"sku-productshop.min" => "Please select at least one merchant"
		];
	}
}
