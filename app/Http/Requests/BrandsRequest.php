<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandsRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            "brand-name"		=> "required",
			"global-identifier"	=> "required"
        ];
    }
	
	public function messages() {
		return [
			"brand-name.required"			=> "This field is required",
			"global-identifier.required"	=> "This field is required"
		];
	}
}
