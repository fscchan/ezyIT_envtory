<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
		$id = $this->input("id") ? $this->input("id") : null;
        $validation = [
            "profile-username"	=>	"required|unique:m_profiles,profile_username," . $id, 
            "profile-email"		=>	"required|email|unique:m_profiles,profile_email," . $id, 
            "profile-password"	=>	"required|confirmed", 
        ];
		if ($this->isMethod("PUT")) {
			$validation["profile-password"] = "confirmed";
		}
		return $validation;
    }
}
