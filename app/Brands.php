<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model {
    protected $table = "m_brands";
	protected $fillable = [
		'id',
		'name',
		'global_identifier',
		'insert_by',
		'update_by'
	];
	protected $hidden = ['insert_by','update_by','created_at','updated_at'];
}