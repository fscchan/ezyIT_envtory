<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuPriceHistories extends Model
{
    use Traits\Uuids;

    public $incrementing = false;

    protected $table = 'm_product_sku_price_histories';

    protected $fillable = ['id','sku_id','shop_id','price','special_price','special_from_date','special_to_date','remark','insert_by','update_by'];
}
