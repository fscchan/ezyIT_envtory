<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttributes extends Model
{
    protected $table = 'm_product_attributes';

    protected $fillable = [
	    'id', 'm_products_id', 'name', 'value', 'type'
    ];

    protected $hidden = ['id','created_at','updated_at','m_products_id'];

    public $incrementing = false;
}
