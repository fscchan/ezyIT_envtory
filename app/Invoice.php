<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	use Traits\Uuids;

    protected $table = 't_invoice_master';

    protected $fillable = ['t_po_master_id','prefix','date','due_date','ship_cost','misc_cost','discount','total_amount','invoice',"insert_by","update_by"];
	
	public $incrementing = false;

    public function detail()
	{
	  return $this->hasMany('App\InvoiceDetail','t_invoice_master_id','id');
	}

	public function po()
    {
        return $this->belongsTo('App\Po','t_po_master_id','id');
    }

	public function stock()
	{
		return $this->hasOne('App\Stock','t_invoice_master_id','id');
	}
	
	public function payment()
	{
		return $this->hasOne('App\MakeInvoicePayment','invoice_id','id');
	}

}
