<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleLevel extends Model {
	use Traits\Uuids;

	protected $table = "module_levels";
	protected $fillable = [
		"id",
		"module",
		"level",
		"insert_by", 
		"update_by"
	];
	protected $hidden = [];
	public $incrementing = false;
}
