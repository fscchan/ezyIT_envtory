<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MakeOrderPayment extends Model {
	use Traits\Uuids;

	protected $table = "m_order_payments";
	protected $fillable = [
		"payment_id",
		"order_id",
		"payment_date",
		"payment_method"
	];
	protected $hidden = [];
	public $incrementing = false;

	public function invoicePayments() {
		return $this->hasMany("App\MakeInvoicePayment", "payment_id");
	}
}
