<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zones extends Model {
	use Traits\Uuids;

    protected $table = "m_zone_type";
	protected $fillable = [
		"uid",
		"name",
		"prefix",
		"cycle_count",
		"insert_by",
		"update_by"
	];
	protected $hidden = ['insert_by','update_by','created_at','updated_at'];
	public $incrementing = false;
}
