<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {
	use Traits\Uuids;

	protected $table = "m_suppliers";
	
	protected $fillable = [
		"uid",
		"supplier_name", 
		"supplier_prefix", 
		"supplier_address", 
		"supplier_mode", 
		"supplier_country", 
		"supplier_state", 
		"supplier_city", 
		"supplier_zipcode", 
		"supplier_min_po", 
		"supplier_max_po",
		"supplier_categories",
		"data_status",
		"insert_by",
		"update_by"
	];

	protected $hidden = ["insert_by","update_by"];

	public $incrementing = false;
	
	public function contacts() {
		return $this->hasMany("App\SupplierContacts", "supplier_id", "id");
	}

	public function products() {
		return $this->belongsToMany("App\Product", "p_supplier_product",  "supplier_id", "product_id");
	}

	public function categories() {
		return $this->belongsToMany('App\ProductCategories', 'p_category_supplier', 'supplier_id', 'category_id');
	}
}
