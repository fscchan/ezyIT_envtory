<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model {
    protected $table = "m_order_statuses";

    public function orders()
    {
    	return $this->hasMany(Order::class,'status','id');
    }
}
