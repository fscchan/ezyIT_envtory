<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Self_;
use Spatie\ArrayToXml\ArrayToXml;

class Product extends Model {

	use Traits\ActorNUuid;
	use SoftDeletes;

	protected $table = "m_products";

	protected $fillable = [
		'm_product_code_id',
		'product_prefix',
		'product_code',
		'm_product_categories_id',
		'insert_by',
		'update_by'
	];

	protected $hidden = ['created_at','updated_at','insert_by','update_by','deleted_at'];

	protected $dates = ['deleted_at'];

	public function code() {
      return $this->belongsTo("App\ProductCode", "m_product_code_id", "id");
  }

	public function category() {
      return $this->belongsTo("App\ProductCategories", "m_product_categories_id", "id");
  }

	public function supplier() {
		return $this->belongsToMany("App\Supplier", "p_supplier_product", "product_id", "supplier_id");
	}

	public function variations() {
		return $this->hasMany('App\ProductAttributes','m_products_id','id')->where('type','code')->where('name','<>','SellerSku');
	}

	public function attributes() {
		return $this->hasMany('App\ProductAttributes','m_products_id','id');
	}

	public function attribute($name) {
		return Self::attributes()->where('name',$name)->pluck('value')->first();
	}

	public function sku() {
		return $this->hasOne('App\Sku','m_products_id','id');
	}

	public function stock()
	{
	  return $this->hasMany('App\Stock','m_products_id','id');
	}

	public function getCodeNameAttribute()
	{
	    return $this->product_code . " - " . $this->attribute('name');
	}

	protected $appends = ['stock_active','stock_cold','stock_active_pending'];

	public function getStockActiveAttribute()
	{
		return Stock::active($this);
	}

	public function getStockActivePendingAttribute()
	{
		return Stock::activePending($this);
	}

	public function getStockColdAttribute()
	{
		return Stock::cold($this);
	}

	/**
	 * Update stock and price product to lazada.
	 *
	 * @param  object  $shop
	 * @param  array   $item[[SellerSku],[Quantity],[Price],[SalePrice],[SaleStartDate],[SaleEndDate]]
	 * @return array
	 */
	public static function updateStockPriceLazada($shop, $item)
	{
		$result = [];
		$account = $shop->account;
		$api_key = $shop->api_key;

		$api = Library::lazadaShopApi($account, $api_key, 'UpdatePriceQuantity');

		$sku = [];
		foreach ($item as $k=>$i){
			$sku[$k] = $i;
		}

		$array = [
			'Product' => [
				'Skus' => [
					'Sku' => $sku
				]
			]
		];

		$xml = ArrayToXml::convert($array, 'Request', 'false');
		if ($api && $xml) {
			$client = new \GuzzleHttp\Client();
			$request = $client->post($api, ['body' => $xml]);
			$response = json_decode($request->getBody(), true);
			if (isset($response['ErrorResponse'])) {
				$result = ['status'=>'error','msg'=>$response["ErrorResponse"]['Body']['Errors'][0]];
			}else{
				$result = ['status'=>'success','msg'=>'Successfully update product'];
			}

		}

		return $result;
	}

	public static function productAttributes()
	{
		$attributes = [
			'general' => ['code','brand','model','submodel','name'],
			'general' => [
				[
					'attributeType' => 'normal',
					'inputType' => 'text',
					'isMandatory' => 1,
					'label' => 'Product Code',
					'name' => 'product_code',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'text',
					'isMandatory' => 1,
					'label' => 'Product Name (English)',
					'name' => 'name',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'text',
					'isMandatory' => 1,
					'label' => 'Product Name (Malay)',
					'name' => 'name_ms',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'singleSelect',
					'isMandatory' => 1,
					'label' => 'Brand',
					'name' => 'brand',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'text',
					'isMandatory' => 1,
					'label' => 'Model',
					'name' => 'model',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'text',
					'isMandatory' => 0,
					'label' => 'Sub Model',
					'name' => 'submodel',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'richText',
					'isMandatory' => 1,
					'label' => 'Highlights',
					'name' => 'short_description',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'richText',
					'isMandatory' => 0,
					'label' => 'Product Description',
					'name' => 'description',
					'options' => [],
					'type' => 'general',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'singleSelect',
					'isMandatory' => 1,
					'label' => 'Taxes',
					'name' => 'tax_class',
					'options' => [
						['name'=>'tax 6'],
						['name'=>'default'],
					],
					'type' => 'code',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'numeric',
					'isMandatory' => 0,
					'label' => 'Product Weight (Kg)',
					'name' => 'product_weight',
					'options' => [],
					'type' => 'code',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'numeric',
					'isMandatory' => 1,
					'label' => 'Package Weight (Kg)',
					'name' => 'package_weight',
					'options' => [],
					'type' => 'code',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'numeric',
					'isMandatory' => 1,
					'label' => 'Package Length (cm)',
					'name' => 'package_length',
					'options' => [],
					'type' => 'code',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'numeric',
					'isMandatory' => 1,
					'label' => 'Package Width (cm)',
					'name' => 'package_width',
					'options' => [],
					'type' => 'code',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'numeric',
					'isMandatory' => 1,
					'label' => 'Package Height (cm)',
					'name' => 'package_height',
					'options' => [],
					'type' => 'code',
					'value' => '',
				],
				[
					'attributeType' => 'normal',
					'inputType' => 'numeric',
					'isMandatory' => 0,
					'label' => 'Dimensions (Length x Width x Height in cm)',
					'name' => 'product_measures',
					'options' => [],
					'type' => 'code',
					'value' => '',
				],
			],
			'blocked' => ['tax_class']
		];
		return $attributes;
	}

}
