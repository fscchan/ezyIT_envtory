<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseCells extends Model {
	use Traits\Uuids;

    protected $table = "m_warehouse_cells";
	protected $fillable = [
		"uid",
		"prefix",
		"colomn",
		"row",
		"barcode",
		"status",
		"m_warehouse_racks_id",
		"insert_by",
		"update_by"
	];
	protected $hidden = [];
	public $incrementing = false;
	
	public function rack() {
		return $this->belongsTo("App\WarehouseRacks", "m_warehouse_racks_id");
	}
	
	public function stock() {
		return $this->hasMany("App\Stock", "cell_id", "id");
	}
}
