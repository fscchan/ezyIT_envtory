<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoDetail extends Model
{
	use Traits\Uuids;

    protected $table = 't_po_detail';

    protected $fillable = [
	    'id',
		't_po_master_id',
		'm_products_id',
        'product_po_id',
		'qty',
		'price',
		"insert_by",
		"update_by"
    ];

    protected $hidden = [];
	
	public $incrementing = false;

    public function po()
    {
        return $this->belongsTo('App\Po','t_po_master_id','id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product','m_products_id','id');
    }
}
